<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Regresi extends CI_Controller
{
    function __Construct() {
        parent::__Construct();
        $this->load->model("Server");
    }

    function model () {

        $id_cutomer = $this->session->userdata('id_customer');
        $id_member = $this->session->userdata('ids');
        $plugin['plugin'] = "datatable_assets";

        $data = "";
        $plugin = "";

       
        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/regresi/model", $data);
        $this->load->view("template/footer", $plugin);
    }

    function makroekonomi () {

        $id_cutomer = $this->session->userdata('id_customer');
        $id_member = $this->session->userdata('ids');
        $plugin['plugin'] = "datatable_assets";

        $data = "";
        $plugin = "";

       
        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/regresi/makroekonomi", $data);
        $this->load->view("template/footer", $plugin);
    }

    function total () {

        $id_cutomer = $this->session->userdata('id_customer');
        $id_member = $this->session->userdata('ids');
        $plugin['plugin'] = "datatable_assets";

        $data = "";
        $plugin = "";

       
        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/regresi/total", $data);
        $this->load->view("template/footer", $plugin);
    }
}