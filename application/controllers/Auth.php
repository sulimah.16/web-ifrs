<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{
    function __Construct() {
        parent::__Construct();
        $this->load->model("Server");
    }

    function index() { $this->load->view("login/login"); }

    function register(){
        $this->load->view("login/register");
    }

    function validate() {
        $username = $this->input->post("username");
        $password = $this->input->post("password");
        $device = $_SERVER['HTTP_USER_AGENT'];
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip_address = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip_address = $_SERVER['REMOTE_ADDR'];
        }

        $data = array(
            'device' => $device."|".$ip_address,
            'password' => $password,
            'username' => $username
        );

        $result = $this->Server->POST("auth", $data );
        // echo json_encode($result);exit;

        if($result->status != '1') { 
            $this->session->set_flashdata("err-message", $result->message);
            redirect("auth");
        } else {
            $authentication = 
                array(
                    "ids" => $result->data->user->id_user,
                    "username" => $result->data->user->username,
                    "role" => $result->data->user->id_role,
                    "name_role" => $result->data->user->role->nama,
                    "image" => $result->data->user->path_profile,
                    "authtoken" => $result->data->token,
                    "nama" => $result->data->user->nama,
                    "email" => $result->data->user->email,
                    "telepon" => $result->data->user->no_telp,
                    "jk" => $result->data->user->j_k,
                    "alamat" => $result->data->user->alamat,
                    "status" => $result->data->user->status,
                    "divisi" => $result->data->user->divisi,
                    "jabatan" => $result->data->user->jabatan,
                );
            
            $this->session->set_userdata($authentication);

            redirect("home");
        }
       

        // if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        //     $ip_address = $_SERVER['HTTP_CLIENT_IP'];
        // } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        //     $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
        // } else {
        //     $ip_address = $_SERVER['REMOTE_ADDR'];
        // }

        // $status = '0';

        // $data = array(
        //     'device' => $device."|".$ip_address,
        //     'password' => $password,
        //     'username' => $username
        // );

      



       
    }

    function recoverpassword() {
        $param['email'] = $this->input->post("email");
        var_dump($param);

        $result = $this->Server->POST_JSON("password/forgot", $param);

        if($result->status == '0') {
            $this->session->set_flashdata("message", $result->message);
            redirect("auth");
        } else {
            $this->session->set_flashdata("message", $result->message);
            redirect("auth");
        }
    }

    function signout() {
        $this->session->sess_destroy();
        redirect("auth");
    }
    
    function updatePass() {
        $old_password = $this->input->post('old_password');
        $new_password = $this->input->post('new_password');
        $confirm_password = $this->input->post('confirm_password');

        $arr = array (
            "password_old" => $old_password,
            "password_new" => $new_password,
            "password_confirm" => $confirm_password
        );

        $url = "change_password";

        // var_dump($arr);

        $add = $this->Server->POST($url, $arr);

        // var_dump($add);

        if($add->status == '1' || $add->status == 1){
            // $this->session->sess_destroy();
            $authentication = 
            array(
                "authtoken" => null
            );
            $this->session->set_userdata($authentication);
            $this->session->set_flashdata("message", $add->message);
            redirect("auth");
            // redirect("auth/signout");


        } else {
            $this->session->set_flashdata("err-message", $add->message);
            redirect("anggota");
        }
       
    }

    function profile () {
        $tmpFile = $_FILES['add_image']['tmp_name'];
        $typeFile = $_FILES['add_image']['type'];
        $nameFile = $_FILES['add_image']['name']; 
        $path = $this->input->post("path");

        if ( $nameFile == "" ) {
            $image_path = $path;
        } else {
            $url = 'upload_file';
            $data = array('file'=> new CURLFile($tmpFile,$typeFile,$nameFile),'type' => 'profile');


            $upload = $this->Server->UPLOAD($url, $data );

            $image_path = $upload->data->path_name;
        }

        $ids = $this->input->post("ids");
        $name = $this->input->post("add_fullname");
        $gender = $this->input->post("gender");
        $role = $this->input->post("role");
        $alamat = $this->input->post("add_address");
        $phone = $this->input->post("add_phone");
        $email = $this->input->post("add_email");
        $status = $this->session->userdata("status");
        $username = $this->input->post("add_username");
        $password = $this->input->post("add_password");
        $divisi = $this->input->post("add_divisi");
        $jabatan = $this->input->post("add_jabatan");
                
        $url = "/user/$ids";
        $datas = array(
            "username" => $username,
            "password" => $password,
            "id_role" => $role,
            "status" => $status,
            "email" => $email,
            "nama" => $name,
            "no_telp" => $phone,
            "alamat" => $alamat,
            "divisi" => $divisi,
            "jabatan" => $jabatan,
            "jk" => $gender,
            "path_profile" => $image_path
        );

        $result = $this->Server->PUT($url, $datas );


        if($result->status == '1' || $result->status == 1){

            $authentication = 
            array(
                "role" => $result->data->id_role,
                "image" => $result->data->path_profile,
                "nama" => $result->data->nama,
                "email" => $result->data->email,
                "telepon" => $result->data->no_telp,
                "jk" => $result->data->j_k,
                "alamat" => $result->data->alamat,
                "status" => $result->data->status,
            );    
            $this->session->set_userdata($authentication);
            
            $this->session->set_flashdata("message", $result->message);
            redirect("home");


        } else {
            $this->session->set_flashdata("err-message", $result->message);
            redirect("home");
        }

       
       
    }
}