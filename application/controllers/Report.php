<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller
{
    function __Construct() {
        parent::__Construct();
        $this->load->model("Server");
    }

    function pd () {

        $id_cutomer = $this->session->userdata('id_customer');
        $id_member = $this->session->userdata('ids');
        $plugin['plugin'] = "datatable_assets";

        $data = "";
        $plugin = "";

       
        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/report/trendpd", $data);
        $this->load->view("template/footer", $plugin);
    }

    function ecl () {

        $id_cutomer = $this->session->userdata('id_customer');
        $id_member = $this->session->userdata('ids');
        $plugin['plugin'] = "datatable_assets";

        $data = "";
        $plugin = "";

       
        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/report/ecl", $data);
        $this->load->view("template/footer", $plugin);
    }

    function jurnal () {

        $id_cutomer = $this->session->userdata('id_customer');
        $id_member = $this->session->userdata('ids');
        $plugin['plugin'] = "datatable_assets";

        $data = "";
        $plugin = "";

       
        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/report/jurnal", $data);
        $this->load->view("template/footer", $plugin);
    }

}