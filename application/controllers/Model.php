<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model extends CI_Controller
{
    function __Construct() {
        parent::__Construct();
        $this->load->model("Server");
    }

    function mev () {

        $data['data'] = $this->Server->GET("input-mev");
        $plugin['plugin'] = "datatable_anggota";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/model/mev", $data);
        $this->load->view("template/footer", $plugin);
    }

    function input_mev () {
        $tmpFile = $_FILES['file_c']['tmp_name'];
        $typeFile = $_FILES['file_c']['type'];
        $nameFile = $_FILES['file_c']['name']; 

        $url = 'input-mev/upload';
        $data = array('file'=> new CURLFile($tmpFile,$typeFile,$nameFile));

        $result = $this->Server->UPLOAD($url, $data );

        if ($result->status == '1' || $result->status == 1) {
            $this->session->set_flashdata("message", $result->message);
            $data['data'] = $result;

            $plugin['plugin'] = "datatable_anggota";
            $this->load->view("template/header");
            $this->load->view("template/nav", $data);
            $this->load->view("pages/upload/report-mev", $data);
            $this->load->view("template/footer", $plugin);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
            redirect('input');
        }
    }

    function fl () {

        $id = $this->input->post("segment");
        $segment = $this->Server->GET("segment");

        if ($id != null || $id != "") {
            $rsp = $this->Server->GET("input-fl/segment/$id");
        } else {
            if (count($segment->data) > 0) {
                $id = $segment->data[0]->id_segment;
                $rsp = $this->Server->GET("input-fl/segment/$id");
            } else {
                $rsp = [];
            }
        }
        
        $data['segment'] = $segment;
        $data['data'] = $rsp;
        $plugin['plugin'] = "datatable_anggota";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/model/fl", $data);
        $this->load->view("template/footer", $plugin);
    }

    function get_fl_live(){
        $idb = $this->input->post('id_draft_bucket');
        // $idb = $this->input->get('id_draft_bucket');
        // echo $idb;
        $bucket = null;
        $draft_bucket = $this->session->userdata('draft_bucket');
        // echo json_encode($draft_bucket);exit;
        foreach ($draft_bucket as $pro) {
            if ($pro->id_draft_bucket == $idb) {
                $bucket = $pro;
                break;   
            }
        }
        $id = $bucket->segment->id_segment;
        // echo $id;
        $rsp = $this->Server->GET("input-fl/segment/$id");
        echo json_encode($rsp);
    }

    function input_fl () {

        $segment = $this->input->post('segment');
        $tmpFile = $_FILES['file_c']['tmp_name'];
        $typeFile = $_FILES['file_c']['type'];
        $nameFile = $_FILES['file_c']['name']; 

        $url = 'input-fl/upload';
        $data = array('id_segment' => $segment,'file'=> new CURLFile($tmpFile,$typeFile,$nameFile));

        $result = $this->Server->UPLOAD($url, $data );

        if ($result->status == '1' || $result->status == 1) {
            $this->session->set_flashdata("message", $result->message);
            $data['data'] = $result;

            $plugin['plugin'] = "datatable_anggota";
            $this->load->view("template/header");
            $this->load->view("template/nav", $data);
            $this->load->view("pages/upload/report-fl", $data);
            $this->load->view("template/footer", $plugin);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
            redirect('model/fl');
        }
    }

    function draft_bucket () {

        $data['draft'] = $this->Server->GET("draft");
        $data['segment'] = $this->Server->GET("segment");
        $data['data'] = $this->Server->GET("draft-bucket");
        $plugin['plugin'] = "datatable_anggota";
        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/model/draft_bucket", $data);
        $this->load->view("template/footer", $plugin);
    }

    function comment_draft(){
        $data['draft'] = $this->Server->GET("draft");
        $data['segment'] = $this->Server->GET("segment");
        $data['data'] = $this->Server->GET("draft-bucket");
        $plugin['plugin'] = "datatable_anggota";
        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/model/comment_draft", $data);
        $this->load->view("template/footer", $plugin);
    }

    function addkomen_draft_bucket(){
        $idb = $this->input->get("idb");
        if ($idb != null || $idb != "") {
            $idb = base64_encode($idb);
        }
        
        $type_coment = $this->input->post('type_coment');
        $flag_coment = $this->input->post('flag_coment');
        $id_bucket = $this->input->post('id_bucket');
        $komentar = $this->input->post('komentar');
        $id_coment = $this->input->post('id_coment');
        
        $arr = array (
            "type" => $type_coment,
            "flag" => $flag_coment,
            "bucket" => $id_bucket,
            "komentar" => $komentar,
            "created_by" => $this->session->userdata("ids")
        );
       if ($id_coment == 0 || $id_coment == "") {
            $url = "/komentar";
            $add = $this->Server->POST($url, $arr);
        } else {
            $url = "/komentar/$id_coment";
            $add = $this->Server->PUT($url, $arr);
        }
        
        if($add->status == '1' || $add->status == 1){
            $this->session->set_flashdata("message", $add->message);
        } else {
            $this->session->set_flashdata("err-message", $add->message);
        }

        if (($idb != null || $idb != "") && $type_coment == 1) {
            redirect("model/view_draft_bucket?idb=$idb");
        } else  if ($type_coment == 1) {
            redirect("model/draft_bucket");
        } else if ($type_coment == 2) {
            if ($flag_coment == 1) {
                redirect("ecl/view_mape_ecl?idb=$idb");
            } else if ($flag_coment == 2) {
                redirect("ecl/view_model_ecl?idb=$idb");
            } else {
                redirect("ecl/draft");
            }
        } else if ($type_coment == 3) {
            if ($flag_coment != 0) {
                redirect("vasicek/view_draft_ecl?idb=$idb");
            } else {
                redirect("vasicek/draft_ecl");
            }
        }
    }

    function deletekomen_draft_bucket(){
        $idb = $this->input->get("idb");
        if ($idb != null || $idb != "") {
            $idb = base64_encode($idb);
        }
        
        $idk = $this->input->get('idk');
        $type_coment = $this->input->get('typ');
        $flag_coment = $this->input->get('flg');
        $url = "/komentar/$idk";
        $add = $this->Server->DELETE($url);
       
        if($add->status == '1' || $add->status == 1){
            $this->session->set_flashdata("message", $add->message);
        } else {
            $this->session->set_flashdata("err-message", $add->message);
        }

        if (($idb != null || $idb != "") && $type_coment == 1) {
            redirect("model/view_draft_bucket?idb=$idb");
        } else  if ($type_coment == 1) {
            redirect("model/draft_bucket");
        } else if ($type_coment == 2) {
            if ($flag_coment == 1) {
                redirect("ecl/view_mape_ecl?idb=$idb");
            } else if ($flag_coment == 2) {
                redirect("ecl/view_model_ecl?idb=$idb");
            } else {
                redirect("ecl/draft");
            }
        } else if ($type_coment == 3) {
            if ($flag_coment != 0) {
                redirect("vasicek/view_draft_ecl?idb=$idb");
            } else {
                redirect("vasicek/draft_ecl");
            }
        }
    }

    public function update_draft_bucket () {
        $ids = $this->input->post("ids");
        $nama = $this->input->post("nama");

        $url = "/draft-bucket/$ids";
        $data = array(
            "nama" => $nama,
        );


        $result = $this->Server->PUT($url, $data );

        if ($result->status == '1') {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }

        redirect('model/draft_bucket');
    }

    function view_draft_bucket () {

        $irb = base64_decode($this->input->get('idb'));

        $data['data'] = $this->Server->GET("draft-bucket/$irb");
        $plugin['plugin'] = "datatable_anggota";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/model/view_draft_bucket", $data);
        $this->load->view("template/footer", $plugin);
    }

    function get_draft_bucket () {

        $idb = $this->input->post('idb');
        // $idb = '6';

        $result = $this->Server->GET("draft-bucket/$idb");

        //list bucket terpilih
            $terpilih = 
                "<p>:".$result->data->kode_draft_bucket."</p>"
                ."<p>:".$result->data->nama_draft_bucket."</p>"
                ."<p>:".$result->data->segment->nama_segment."</p>"
                ."<p>:".$result->data->flag_normalisasi."</p>"
                ."<p>:".$result->data->moving_titik."</p>"
                ."<p>:".date("d M Y",strtotime($result->data->start_date))."</p>"
                ."<p>:".date("d M Y",strtotime($result->data->end_date))."</p>"
                ;

            $terpilih2 = 
                "<p>:".$result->data->created_user->nama."</p>"
                ."<p>:".date("d M Y H:i:s",strtotime($result->data->created_date))."</p>"
                ;
        //list bucket terpilih

        $ar = $this->get_table_ar($result->data->araging);
        $rollrate_belum_normal = $this->get_table_rollrate($result->data->rollrate_belum_normal);
        $rollrate_normal = $this->get_table_rollrate($result->data->rollrate_normal);
        $average_rollrate_lossrate = $this->get_table_rollrate($result->data->average_rollrate_lossrate);
        $rollrate_normalisasi_average = $this->get_table_rollrate($result->data->rollrate_normalisasi_average);
        $lossrate = $this->get_table_lossrate($result->data->lossrate);
        $moving_average = $this->get_table_lossrate($result->data->moving_average);
        $sum_delta_lossrate = $this->get_table_lossrate($result->data->sum_delta_lossrate);
        $odr = $this->get_table_odr($result->data->odr);
        
        $data = array(
            'tbl_ar' => $ar,
            'rollrate_belum_normal' => $rollrate_belum_normal,
            'rollrate_normal' => $rollrate_normal,
            'average_rollrate_lossrate' => $average_rollrate_lossrate,
            'rollrate_normalisasi_average' => $rollrate_normalisasi_average,
            'lossrate' => $lossrate,
            'moving_average' => $moving_average,
            'sum_delta_lossrate' => $sum_delta_lossrate,
            'odr' => $odr,
            'terpilih' => $terpilih,
            'terpilih2' => $terpilih2,
        );

        $data = json_encode($data);
        echo $data;
    }

    function get_table_ar ( $ar ) {
        $header_bucket = "";
        $list_bucket = "";

        foreach ( $ar->header_bucket as $r1) {
            $header_bucket .= '<th class="all" width=200px>'.$r1.' Hari</th>';
        }
        
        $thead = 
            '<thead>'
            .'<th class="all" width=20px>#</th>'
            .'<th class="all" width=200px>Periode</th>'
            .$header_bucket
            .'<th class="min-tablet" width=200px>Jumlah Outstanding</h>'
            .'</thead>';

        $t = 1;
        foreach ( $ar->list_bucket as $r2) {

            $no = '<td>'.$t++.'</td>';
            $periode = '<td>'.$r2->periode.'</td>';
            $nilai = "";
            $outstanding = '<td>'."Rp. " . number_format( $r2->jumlah_outstanding, 2, ',', '.').'</td>';
            foreach ( $r2->nilai as $an ) {
                
                $nilai .= '<td>'."Rp. " . number_format( $an, 2, ',', '.').'</td>';
            }

            $list_bucket .= '<tr>'.$no . $periode . $nilai . $outstanding.'</tr>';
        }


        $tbody = 
            '<tbody>'
            .$list_bucket
            .'</tbody>';
        
        $ar = $thead . $tbody;

        return $ar;

        //araging
    }

    function get_table_rollrate ( $ar ) {
        $header_bucket = "";
        $list_bucket = "";

        foreach ( $ar->header_bucket as $r1) {
            $header_bucket .= '<th class="all" width=200px>'.$r1.' Hari</th>';
        }
        
        $thead = 
            '<thead>'
            .'<th class="all" width=20px>#</th>'
            .'<th class="all" width=200px>Periode</th>'
            .$header_bucket
            .'</thead>';

        $t = 1;
        foreach ( $ar->list_bucket as $r2) {

            $no = '<td>'.$t++.'</td>';
            $periode = '<td>'.$r2->periode.'</td>';
            $nilai = "";
            foreach ( $r2->nilai as $an ) {
                if ( $an == 'Infinity') {
                    $v = $an;
                } else {
                    $v = $an *100;
                    $v = round($v, 2);
                }
                $nilai .= '<td>'.$v.' %</td>';
            }

            $list_bucket .= '<tr>'.$no . $periode . $nilai .'</tr>';
        }


        $tbody = 
            '<tbody>'
            .$list_bucket
            .'</tbody>';
        
        $ar = $thead . $tbody;

        return $ar;

        //araging
    }

    function get_table_lossrate ( $ar ) {
        $header_bucket = "";
        $list_bucket = "";

        foreach ( $ar->header_bucket as $r1) {
            $header_bucket .= '<th class="all" width=200px>'.$r1.' Hari</th>';
        }
        
        $thead = 
            '<thead>'
            .'<th class="all" width=20px>#</th>'
            .'<th class="all" width=200px>Periode</th>'
            .$header_bucket
            .'<th class="all" width=200px>Simple Average</th>'
            .'<th class="all" width=200px>Sum Loss Rate</th>'
            .'</thead>';

        $t = 1;
        foreach ( $ar->list_bucket as $r2) {

            $no = '<td>'.$t++.'</td>';
            $periode = '<td>'.$r2->periode.'</td>';
            $nilai = "";
            $average = '<td>'.round($r2->average, 4) * 100 . " %" .'</td>';
            $sum = '<td>'.round($r2->sum, 4) * 100 . " %" .'</td>';
            foreach ( $r2->nilai as $an ) {
                if ( $an == 'Infinity') {
                    $v = $an;
                } else {
                    $v = $an *100;
                    $v = round($v, 2);
                }
                $nilai .= '<td>'.$v.' %</td>';
            }

            $list_bucket .= '<tr>'.$no . $periode . $nilai . $average . $sum. '</tr>';
        }


        $tbody = 
            '<tbody>'
            .$list_bucket
            .'</tbody>';
        
        $ar = $thead . $tbody;

        return $ar;

        //araging
    }

    function get_table_odr ( $ar ) {
        $header_bucket = "";
        $list_bucket = "";

        foreach ( $ar->header_bucket as $r1) {
            $header_bucket .= '<th class="all" width=200px>'.$r1.' Hari</th>';
        }
        
        $thead = 
            '<thead>'
            .'<th class="all" width=20px>#</th>'
            .'<th class="all" width=200px>Periode</th>'
            .$header_bucket
            .'<th class="all" width=200px>Jumlah ODR</th>'
            .'<th class="all" width=200px>Persentase ODR</th>'
            .'</thead>';

        $t = 1;
        foreach ( $ar->list_bucket as $r2) {

            $no = '<td>'.$t++.'</td>';
            $periode = '<td>'.$r2->periode.'</td>';
            $nilai = "";
            $average = '<td>'."Rp. " . number_format( $r2->sum, 2, ',', '.').'</td>';
            $sum = '<td>'.round($r2->persentase_odr, 4) * 100 . " %" .'</td>';
            foreach ( $r2->nilai as $an ) {
                $nilai .= '<td>'."Rp. " . number_format( $an, 2, ',', '.').'</td>';
            }

            $list_bucket .= '<tr>'.$no . $periode . $nilai . $average . $sum. '</tr>';
        }


        $tbody = 
            '<tbody>'
            .$list_bucket
            .'</tbody>';
        
        $ar = $thead . $tbody;

        return $ar;

        //araging
    }

    function get_model_fl () {

        $ifl = $this->input->post('ifl');
        echo json_encode($this->Server->GET("input-fl/$ifl"));
        
    }

    function get_mape() {

        $id_draft_bucket = $this->input->post('id_draft_bucket');
        $id_model = $this->input->post('id_model');
        $id_regresi = $this->input->post('id_regresi');
        $periode_forecast = $this->input->post('periode_forecast');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        
        $sds = explode("/",$start_date);
        $sde = explode("/",$end_date);
        // echo $sds[0]."01".$sds[1]."<br />";
        // echo $sde[0]."01".$sde[1]."<br />";
        $dts = date("d/m/Y",strtotime($sds[0]."/01/".$sds[1]));
        $dte = date("t/m/Y",strtotime($sde[0]."/01/".$sde[1]));
        $data = array(
            "id_draft_bucket" => $id_draft_bucket,
            "id_model" => $id_model,
            "id_regresi" => $id_regresi,
            "periode_forecast" => $periode_forecast,
            "start_date" => $dts,
            "end_date" => $dte
        );

        // $data = array(
        //     "id_draft_bucket" => "6",
        //     "id_model" => "1",
        //     "id_regresi" => "1",
        //     "periode_forecast" => "12",
        //     "start_date" => "01/06/2019",
        //     "end_date" => "01/12/2019"
        // );
    
        $url = "report/mape";
    
        $result = $this->Server->POST($url, $data );
        // echo json_encode($result);
        // exit;

        if ( $result->status == 1 || $result->status == '1' ) {
            $model = $this->table_model_mape($result->data->model);
            $list = $this->table_list_mape($result->data->id_regresi,$result->data->list_mape, $result->data->average_mape); 
            $model_mape_terpilih = $this->table_mape_terpilih($result);
            $mape_header = $this->get_mape_header($result->data->id_regresi,$result->data->header);
            $average_mape = $result->data->average_mape;
        } else {
            $model = "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
            $list = "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
            $model_mape_terpilih = "<tr><td></td></tr>";
            $mape_header = "
                <th class='all' width=200px>Periode</th>
                <th class='all' width=200px>Nilai Var 1</th>
                <th class='all' width=200px>Nilai Var 2</th>
                <th cclass='all' width=200px>Logit Expected</th>
                <th cclass='all' width=200px>Expected</th>
                <th cclass='all' width=200px>Actual</th>
                <th class='all' width=200px>Mape</th>
                ";
            $average_mape = "0";
        }

        $data = array(
            'model_mape' => $model,
            'list_mape' => $list,
            'mape_message' => $result->message,
            'model_mape_terpilih' => $model_mape_terpilih,
            'mape_header' => $mape_header,
            'mape' => $result->data,
            'average_mape' => $average_mape,
            'status' => $result->status,
        );
        $this->session->set_userdata($data);

        $data = json_encode($data);
        echo $data;
    }

    function get_mape_header ( $regresi ,$result ) {
        $th = "";
        if ( $result == null) {
            $th = "
            <th class='all' width=200px>Periode</th>
            <th class='all' width=200px>Nilai Var 1</th>
            <th class='all' width=200px>Nilai Var 2</th>
            <th cclass='all' width=200px>Logit Expected</th>
            <th cclass='all' width=200px>Expected</th>
            <th cclass='all' width=200px>Actual</th>
            <th class='all' width=200px>Mape</th>
            ";
        } 
        // else if ($regresi == '4' || $regresi == 4) { //hanif sengaja buat disini untuk sum delta
        //     $th = "
        //     <th class='all' width=200px>Periode</th>
        //     <th class='all' width=200px>Nilai Var 1</th>
        //     <th class='all' width=200px>Nilai Var 2</th>
        //     <th cclass='all' width=200px>Expected Sum Delta Lossrate</th>
        //     <th cclass='all' width=200px>Actual Sum Delta Lossrate</th>
        //     <th class='all' width=200px>Mape</th>
        //     ";
        // } 
        else {
            foreach ( $result as $r ) {
                $th .= "<th class='all' width=200px>".$r."</th>";
            }
        }

        return $th;
    }

    function table_mape_terpilih ($result) {
        $model_mape_terpilih = 
        "<p>: ".$result->data->draft_bucket->kode_draft_bucket."</p>"
        ."<p>: ".$result->data->draft_bucket->nama_draft_bucket."</p>"
        ."<p>: ".$result->data->draft_bucket->segment->nama_segment."</p>"
        ."<p>: ".$result->data->input_regresi."</p>"
        ."<p>: ".$result->data->periode_forecast."</p>"
        ."<p>: ".date("M Y",strtotime($result->data->periode_start))."</p>"
        ."<p>: ".date("M Y",strtotime($result->data->periode_end))."</p>"
        ;

        return $model_mape_terpilih;
    }

    function table_model_mape ( $m ) {
        $model_mape = "";
        $model_mape .= "<td>".$m->nomor."</td>"
        . "<td>".$m->var1."</td>"
        . "<td>".$m->var2."</td>" 
        . "<td>".$m->lag1."</td>" 
        . "<td>".$m->lag2."</td>" 
        . "<td>".$m->c_coef."</td>" 
        . "<td>".$m->koef_var1."</td>" 
        . "<td>".$m->koef_var2."</td>" ;

        $model_mape = "<tr>".$model_mape."</tr>";
        return $model_mape;
    }

    function table_list_mape ($regresi,$body ,$average) {
        $list_mape = "";
        if ($regresi == '1' || $regresi == '2') {
            foreach ( $body as $b ) {
                $list_mape .= 
                '<tr>'
                .'<td>'.$b->periode.'</td>'
                .'<td>'.$b->nilai_var1.'</td>'
                .'<td>'.$b->nilai_var2.'</td>'
                .'<td>'.$b->logit_expected.'</td>'
                .'<td>'.round(($b->expected*100), 2).' %</td>'
                .'<td>'.round(($b->actual*100), 2).' %</td>'
                .'<td>'.round(($b->mape*100), 2).' %</td>'
                .'</tr>';
            }
    
            $average_tbl = 
            '<tr>'
                .'<td></td>'
                .'<td></td>'
                .'<td></td>'
                .'<td></td>'
                .'<td></td>'
                .'<td>Average MAPE</td>'
                .'<td>'.round(($average*100), 2).' %</td>'
                .'</tr>'; 
        } else {
            foreach ( $body as $b ) {
                $list_mape .= 
                '<tr>'
                .'<td>'.$b->periode.'</td>'
                .'<td>'.$b->nilai_var1.'</td>'
                .'<td>'.$b->nilai_var2.'</td>'
                .'<td>'.round(($b->expected*100), 2).' %</td>'
                .'<td>'.round(($b->actual*100), 2).' %</td>'
                .'<td>'.round(($b->mape*100), 2).' %</td>'
                .'</tr>';
            }
    
            $average_tbl = 
            '<tr>'
                .'<td></td>'
                .'<td></td>'
                .'<td></td>'
                .'<td></td>'
                .'<td>Average MAPE</td>'
                .'<td>'.round(($average*100), 2).' %</td>'
                .'</tr>'; 
        }
        $tbody = $list_mape.$average_tbl;
        return $tbody;
    }

    function get_ecl() {
        $id_draft_bucket = $this->input->post('id_draft_bucket');
        $id_model = $this->input->post('id_model');
        $id_regresi = $this->input->post('id_regresi');
        $periode_forecast = $this->input->post('periode_forecast');
        $periode = $this->input->post('periode');
        $lgd = $this->input->post('lgd');
        $normal = $this->input->post('normal');
        $optimis = $this->input->post('optimis');
        $pesimis = $this->input->post('pesimis');
        $scaling = $this->input->post('scaling');

        $datas = array(
            "id_draft_bucket" => $id_draft_bucket,
            "id_model" => $id_model,
            "id_regresi" => $id_regresi,
            "periode_forecast" => $periode_forecast,
            "periode" => date('d/m/Y', strtotime($periode)),
            "lgd" => $lgd,
            "normal" => $normal,
            "optimis" => $optimis,
            "pesimis" => $pesimis,
            "scaling" => $scaling,
        );

        

        // $datas = array(
        //     "id_draft_bucket" => "6",
        //     "id_model" => "3",
        //     "id_regresi" => "2",
        //     "periode_forecast" => "12",
        //     "periode" => "31/12/2019",
        //     "lgd" => "45",
        //     "normal" => "60",
        //     "optimis" => "20",
        //     "pesimis" => "20",
        //     "scaling" => "6.39194424666944"
        // );

        $url = "report/ecl";

        $result = $this->Server->POST($url, $datas );

        $message = $result->message;

        // echo $message;
        // exit;


        if ( $result->status == 1 || $result->status == '1' ) {
            $no_modal_list = $this->no_modal_ecl($result->data->ecl_list[0]);
            $model_tittle = $this->model_tittle($result->data->regresi,$result->data->ecl_list[0]);
            if (count($result->data->ecl_list) > 1) {  

                $modal_list= $this->modal_list($result->data->ecl_list[1]);
                $proporsional_list= $this->proporsional_list($result->data->ecl_list[1]);
                $solver_list= $this->solver_list($result->data->ecl_list[1]);
                $skenario = $this->skenario($result->data->ecl_list[1]);
                $psak71 = $this->eclPsak71($result->data->input_regresi,$result->data->ecl_list[1]);
                $MEVS = $this->eclMEVS($result->data->input_regresi,$result->data->ecl_list[1]);
                $model_mevs_solver = $this->eclMEVSolver($result->data->input_regresi,$result->data->ecl_list[1]);
                $model_scaling_target = $this->eclScalling($result->data->input_regresi,$result->data->ecl_list[1]);

            } else {

                $modal_list= "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
                $proporsional_list= "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
                $solver_list= "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
                $skenario = "<tr><td></td><td></td>td></td><td></td><td></td></tr>";
                $psak71 =  
                "<thead>
                <tr>
                <th class = 'all' width = 800px>Logit Expected PD</th>
                <th class = 'all' width = 800px>-</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                <td>Expected PD</td>
                <td>-</td>
                </tr>
                </tbody>";
                $MEVS =  
                "<thead>
                <tr>
                <th class = 'all' width = 800px>Expected ODR</th>
                <th class = 'all' width = 800px>-</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                <td>Last ODR</td>
                <td>-</td>
                </tr>
                <tr>
                <td>MEV Effect</td>
                <td>-</td>
                </tr>
                </tbody>";
                $model_mevs_solver = "";
                $model_scaling_target = "";

            }
            

            $table_model_jurnal = $this->getModelJurnal($result->data->input_regresi, $result->data);
            $jurnal_periode_ecl = $result->data->ecl_list[0]->periode;
            $jurnal_model_pilihan = $this->getModelPilihan($result->data->input_regresi);
            
            $ecl_no_model = "0";
            $ecl_proporsional = "0";
            $ecl_solver = "0";
            if (count($result->data->ecl_list) > 0) {  
                if ($result->data->ecl_list[0]->no_model_sum_ecl != null) {
                    $ecl_no_model = $result->data->ecl_list[0]->no_model_sum_ecl;
                }
            }
            if (count($result->data->ecl_list) > 1) {
                if ($result->data->ecl_list[1]->proporsional_sum_ecl_forward_looking != null) {
                    $ecl_proporsional = $result->data->ecl_list[1]->proporsional_sum_ecl_forward_looking;
                }
            }
            if (count($result->data->ecl_list) > 1) {
                if ($result->data->ecl_list[1]->solver_sum_ecl_forward_looking != null) {
                    $ecl_solver = $result->data->ecl_list[1]->solver_sum_ecl_forward_looking;
                }
            }

        } else {
            $no_modal_list = "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
            $modal_list= "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
            $proporsional_list= "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
            $solver_list= "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
            $skenario = "<tr><td></td><td></td>td></td><td></td><td></td></tr>";
            $model_tittle = "";
            $psak71 =  
            "<thead>
            <tr>
            <th class = 'all' width = 800px>Logit Expected PD</th>
            <th class = 'all' width = 800px>-</th>
            </tr>
            </thead>
            <tbody>
            <tr>
            <td>Expected PD</td>
            <td>-</td>
            </tr>
            </tbody>";
            $MEVS =  
            "<thead>
            <tr>
            <th class = 'all' width = 800px>Expected ODR</th>
            <th class = 'all' width = 800px>-</th>
            </tr>
            </thead>
            <tbody>
            <tr>
            <td>Last ODR</td>
            <td>-</td>
            </tr>
            <tr>
            <td>MEV Effect</td>
            <td>-</td>
            </tr>
            </tbody>";
            $model_mevs_solver = "";
            $model_scaling_target = "";
            $table_model_jurnal = "";
            $jurnal_periode_ecl = "";
            $jurnal_model_pilihan = "";
            $ecl_no_model = "0";
            $ecl_proporsional = "0";
            $ecl_solver = "0";
        }

        $hasil = array(
            'no_modal_list' => $no_modal_list,
            'model_tittle' => $model_tittle,
            'modal_list' => $modal_list,
            'proporsional_list' => $proporsional_list,
            'solver_list' => $solver_list,
            'ecl_message' => $message,
            'skenario' => $skenario,
            'psak71' => $psak71,
            'Mevs' => $MEVS,
            'model_mevs_solver' => $model_mevs_solver,
            'model_scaling_target' => $model_scaling_target,
            'table_model_jurnal' => $table_model_jurnal,
            'jurnal_periode_ecl' => $jurnal_periode_ecl,
            'jurnal_model_pilihan' => $jurnal_model_pilihan,
            'ecl_no_model' => $ecl_no_model,
            'ecl_proporsional' => $ecl_proporsional,
            'ecl_solver' => $ecl_solver,
        );

        $decl = array(
            'ecl' => $result->data->ecl_list
        );
        $this->session->set_userdata($decl);

        $hasil = json_encode($hasil);
        echo $hasil;  
    }

    function model_tittle ($regresi , $dt) {
        $modal_list = "";
        $modal_list .= 
        "<tr>"
        ."<td>Input Regresi</td>"
        ."<td>".$regresi."</td>"
        ."</tr>";
        $modal_list .= 
        "<tr>"
        ."<td>Periode</td>"
        ."<td>".$dt->periode."</td>"
        ."</tr>";
        $modal_list .= 
        "<tr>"
        ."<td>Month Forecast</td>"
        ."<td>".$dt->periode_forecast."</td>"
        ."</tr>";
        return $modal_list;
    }

    function eclPsak71($regresi, $result) {
        if ($regresi == '1') {
            $body = 
            "<thead>
            <tr>
            <th class = 'all' width = 800px>Logit Expected PD</th>
            <th class = 'all' width = 800px>".round(($result->logit_expected_psak), 2)."</th>
            </tr>
            </thead>
            <tbody>
            <tr>
            <td>Expected PD</td>
            <td>".round(($result->expected_psak*100), 2)." %</td>
            </tr>
            </tbody>";
        } elseif ($regresi == '2') {
            $body = 
            "<thead>
            <tr>
            <th class = 'all' width = 800px>Logit Expected Average Lossrate</th>
            <th class = 'all' width = 800px>".round(($result->logit_expected_psak), 2)."</th>
            </tr>
            </thead>
            <tbody>
            <tr>
            <td>Expected Average Lossrate</td>
            <td>".round(($result->expected_psak*100), 2)." %</td>
            </tr>
            </tbody>";
        } elseif ($regresi == '3') {
            $body = 
            "<thead>
            <tr>
            <th class = 'all' width = 800px>Expected Sum Lossrate</th>
            <th class = 'all' width = 800px>".round(($result->expected_psak*100), 2)." %</th>
            </tr>
            </thead>";
        } elseif ($regresi == '4') {
            $body = 
            "<thead>
            <tr>
            <th class = 'all' width = 800px>Expected Sum delta lossrate</th>
            <th class = 'all' width = 800px>".round(($result->expected_psak*100), 2)." %</th>
            </tr>
            </thead>";
        }
        
        return $body;
    }

    function eclMEVS($regresi,$result) {
        if ($regresi == '1') {
            $body = 
            "<thead>
            <tr>
            <th class = 'all' width = 800px>Expected ODR</th>
            <th class = 'all' width = 800px>".round(($result->expected*100), 2)." %</th>
            </tr>
            </thead>
            <tbody>
            <tr>
            <td>Last ODR</td>
            <td>".round(($result->last*100), 2)." %</td>
            </tr>
            <tr>
            <td>MEV Effect</td>
            <td>".round(($result->mev*100), 2)." %</td>
            </tr>
            </tbody>";
        } elseif ($regresi == '2') {
            $body = 
            "<thead>
            <tr>
            <th class = 'all' width = 800px>Expected Average Lossrate</th>
            <th class = 'all' width = 800px>".round(($result->expected*100), 2)." %</th>
            </tr>
            </thead>
            <tbody>
            <tr>
            <td>Last Average Lossrate</td>
            <td>".round(($result->last*100), 2)." %</td>
            </tr>
            <tr>
            <td>MEV Effect</td>
            <td>".round(($result->mev*100), 2)." %</td>
            </tr>
            </tbody>";
        } elseif ($regresi == '3') {
            $body = 
            "<thead>
            <tr>
            <th class = 'all' width = 800px>Expected Sum Lossrate</th>
            <th class = 'all' width = 800px>".round(($result->expected*100), 2)." %</th>
            </tr>
            </thead>
            <tbody>
            <tr>
            <td>Last Sum Lossrate</td>
            <td>".round(($result->last*100), 2)." %</td>
            </tr>
            <tr>
            <td>MEV Effect</td>
            <td>".round(($result->mev*100), 2)." %</td>
            </tr>
            </tbody>";
        } elseif ($regresi == '4') {
            $body = "";
        }

        return $body;
    }

    function eclMEVSolver($regresi,$result) {
        if ($regresi == '1') {
            $body = "";
        } elseif ($regresi == '2') {
            $body = 
            "<thead>
            <tr>
            <th class = 'all' width = 800px>Expected Average Lossrate</th>
            <th class = 'all' width = 800px>".round(($result->expected*100), 2)." %</th>
            </tr>
            </thead>";
        } elseif ($regresi == '3') {
            $body = "";
        } elseif ($regresi == '4') {
            $body = 
            "<thead>
            <tr>
            <th class = 'all' width = 800px>Penyesuaian Forward Looking</th>
            <th class = 'all' width = 800px>".round(($result->expected*100), 2)." %</th>
            </tr>
            </thead>
            <tbody>
            <tr>
            <td>Agregat PD Base</td>
            <td>".round(($result->last*100), 2)." %</td>
            </tr>
            <tr>
            <td>PD Forward Looking</td>
            <td>".round(($result->pd_forward_looking*100), 2)." %</td>
            </tr>
            </tbody>";
        }

        return $body;
    }

    function eclScalling($regresi,$result) {
        if ($regresi == '1') {
            $body = "";
        } elseif ($regresi == '2') {
            $body = 
            "<thead>
            <tr>
            <th class = 'all' width = 800px>Target</th>
            <th class = 'all' width = 800px>".round(($result->target), 6)."</th>
            </tr>
            </thead>
            <tbody>
            <tr>
            <td>Scalling</td>
            <td>".round(($result->scaling), 2)."</td>
            </tr>
            </tbody>";
        } elseif ($regresi == '3') {
            $body = "";
        } elseif ($regresi == '4') {
            $body = 
            "<thead>
            <tr>
            <th class = 'all' width = 800px>Target</th>
            <th class = 'all' width = 800px>".round(($result->target), 6)."</th>
            </tr>
            </thead>
            <tbody>
            <tr>
            <td>Scalling</td>
            <td>".round(($result->scaling), 2)."</td>
            </tr>
            </tbody>";
        }

        return $body;
    }

    function modal_list ( $modal ) {
        $modal_list = "";

        if (  $modal->model == null ||  $modal->model  == " ") {
            $modal_list = "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
        } else {
            $modal_list = 
            "<tr>"
            ."<td>".$modal->model->nomor."</td>"
            ."<td>".$modal->model->var1."</td>"
            ."<td>".$modal->model->var2."</td>"
            ."<td>".$modal->model->lag1."</td>"
            ."<td>".$modal->model->lag2."</td>"
            ."<td>".$modal->model->c_coef."</td>"
            ."<td>".$modal->model->koef_var1."</td>"
            ."<td>".$modal->model->koef_var2."</td>"
            ."</tr>";
        }

        return $modal_list;
    }

    function no_modal_ecl ($no_model) {
        $no_modal_list = "";

        if ( $no_model->no_model_list == null || $no_model->no_model_list == "") {
            $no_modal_list = "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
        } else {
            foreach ( $no_model->no_model_list as $nm ) {
                $no_modal_list .=
                "<tr>"
                ."<td>".$nm->bucket." Hari</td>"
                ."<td>".round(($nm->pd_base*100), 2)." %</td>"
                ."<td>".round(($nm->lgd*100), 2)." %</td>"
                ."<td>".number_format( $nm->ead,2, ',', '.')."</td>"
                ."<td>".number_format( $nm->ecl_base,2, ',', '.')."</td>"
                ."</tr>";
            }
            //."Rp. " . number_format( $r2->jumlah_outstanding, 2, ',', '.').
            $no_modal_list .=
                "<tr>"
                ."<td></td>"
                ."<td></td>"
                ."<td>Jumlah</td>"
                ."<td>".number_format( $no_model->no_model_sum_ead,2, ',', '.')."</td>"
                ."<td>".number_format( $no_model->no_model_sum_ecl,2, ',', '.')."</td>"
                ."</tr>";
        }

        return $no_modal_list;
    }

    function proporsional_list ($propo) {
        $proporsional_list = "";

        if ( $propo->proporsional_list == null || $propo->proporsional_list == "") {
            $proporsional_list = "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
        } else {
            foreach ( $propo->proporsional_list as $nm ) {
                $proporsional_list .=
                "<tr>"
                ."<td>".$nm->bucket." Hari</td>"
                ."<td>".round(($nm->pd_base*100), 2)." %</td>"
                ."<td>".round(($nm->mev_effect*100), 2)." %</td>"
                ."<td>".round(($nm->pd_forward_looking*100), 2)." %</td>"
                ."<td>".round(($nm->lgd*100), 2)." %</td>"
                ."<td>".number_format( $nm->ead,2, ',', '.')."</td>"
                ."<td>".number_format( $nm->ecl_base,2, ',', '.')."</td>"
                ."<td>".number_format( $nm->ecl_forward_looking,2, ',', '.')."</td>"
                ."</tr>";
            }

            $proporsional_list .=
                "<tr>"
                ."<td></td>"
                ."<td></td>"
                ."<td></td>"
                ."<td></td>"
                ."<td>Jumlah</td>"
                ."<td>".number_format( $propo->proporsional_sum_ead,2, ',', '.')."</td>"
                ."<td>".number_format( $propo->proporsional_sum_ecl,2, ',', '.')."</td>"
                ."<td>".number_format( $propo->proporsional_sum_ecl_forward_looking,2, ',', '.')."</td>"
                ."</tr>";
        }

        return $proporsional_list;
    }

    function solver_list ($solver) {
        $solver_list = "";

        if ( $solver->solver_list == null || $solver->solver_list == "") {
            $solver_list = "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
        } else {
            foreach ( $solver->solver_list as $nm ) {
                $solver_list .=
                "<tr>"
                ."<td>".$nm->bucket." Hari</td>"
                ."<td>".round(($nm->pd_base*100), 2)." %</td>"
                ."<td>".round(($nm->logit_pd), 6)."</td>"
                ."<td>".round(($nm->pd_forward_looking*100), 2)." %</td>"
                ."<td>".round(($nm->lgd*100), 2)." %</td>"
                ."<td>".number_format( $nm->ead,2, ',', '.')."</td>"
                ."<td>".number_format( $nm->ecl_base,2, ',', '.')."</td>"
                ."<td>".number_format( $nm->ecl_forward_looking,2, ',', '.')."</td>"
                ."</tr>";
            }

            $solver_list .=
            "<tr>"
            ."<td>Average</td>"
            ."<td>".round(($solver->solver_average_pd_base*100), 2)." %</td>"
            ."<td></td>"
            ."<td>".round(($solver->solver_average_pd_forward_looking*100), 2)." %</td>"
            ."<td>Jumlah</td>"
            ."<td>".number_format( $solver->solver_sum_ead,2, ',', '.')."</td>"
            ."<td>".number_format( $solver->solver_sum_ecl,2, ',', '.')."</td>"
            ."<td>".number_format( $solver->solver_sum_ecl_forward_looking,2, ',', '.')."</td>"
            ."</tr>";
        }

        return $solver_list;
    }

    function skenario ($skenarios) {

        $skenario = "";
        if ( $skenarios->skenario == null || $skenarios->skenario == " ") {
            $skenario = "<tr><td></td><td></td>td></td><td></td><td></td></tr>";
        } else {
            $skenario = 
            '<tr>'
            ."<td>STDEV</td>"
            .'<td>'.$skenarios->skenario->stdev_var1.'</td>'
            .'<td>'.$skenarios->skenario->stdev_var2.'</td>'
            .'<td>-</td>'
            .'</tr>'
            .'<tr>'
            ."<td>Normal</td>"
            .'<td>'.$skenarios->skenario->normal_var1.'</td>'
            .'<td>'.$skenarios->skenario->normal_var2.'</td>'
            .'<td>'.$skenarios->skenario->normal_probability *100 .'%</td>'
            .'</tr>'
            ."<td>Optimis</td>"
            .'<td>'.$skenarios->skenario->optimis_var1.'</td>'
            .'<td>'.$skenarios->skenario->optimis_var2.'</td>'
            .'<td>'.$skenarios->skenario->optimis_probability * 100 .'%</td>'
            .'</tr>'
            ."<td>Pesimis</td>"
            .'<td>'.$skenarios->skenario->pesimis_var1.'</td>'
            .'<td>'.$skenarios->skenario->pesimis_var2.'</td>'
            .'<td>'.$skenarios->skenario->pesimis_probability * 100 .'%</td>'
            .'</tr>'
            ."<td>Weighted MEV</td>"
            .'<td>'.$skenarios->skenario->weigthed_var1.'</td>'
            .'<td>'.$skenarios->skenario->weigthed_var2.'</td>'
            .'<td>-</td>'
            .'</tr>'
            ;
        }

       

        return $skenario;
    }

    function get_proposional_list_jurnal ( $model, $segment, $total_mape ) {
        $proposional_list = 
        '<tr>'
        .'<td>'.$segment.'</td>'
        .'<td>'.'ODR/ Sum Delta/ Average'.'</td>'
        .'<td>'.$model->model->nomor.'</td>'
        .'<td>'.$model->model->var1.'</td>'
        .'<td>'.$model->model->var2.'</td>'
        .'<td>'.$model->model->lag1.'</td>'
        .'<td>'.$model->model->lag2.'</td>'
        .'<td>'.$total_mape.'</td>'
        .'<td>'.round($model->proporsional_sum_ecl, 2) .'</td>'
        .'</tr>'
        ;
        return $proposional_list;
    }

    function get_solver_list_jurnal ( $model, $segment, $total_mape ) {
        $solver_list = 
        '<tr>'
        .'<td>'.$segment.'</td>'
        .'<td>'.'ODR/ Sum Delta/ Average'.'</td>'
        .'<td>'.$model->model->nomor.'</td>'
        .'<td>'.$model->model->var1.'</td>'
        .'<td>'.$model->model->var2.'</td>'
        .'<td>'.$model->model->lag1.'</td>'
        .'<td>'.$model->model->lag2.'</td>'
        .'<td>'.$total_mape.'</td>'
        .'<td>'.round($model->solver_sum_ecl, 2) .'</td>'
        .'</tr>'
        ;
        return $solver_list;
    }

    function get_no_modal_list_jurnal ( $no_modal, $segment ) {
        $no_modal_list = 
        '<tr>'
        .'<td>'.$segment.'</td>'
        .'<td>'.'ODR/ Sum Delta/ Average'.'</td>'
        .'<td>'.'No Model'.'</td>'
        .'<td>'.'-'.'</td>'
        .'<td>'.'-'.'</td>'
        .'<td>'.'-'.'</td>'
        .'<td>'.'-'.'</td>'
        .'<td>'.'-'.'</td>'
        .'<td>'.round($no_modal->no_model_sum_ecl, 2) .'</td>'
        .'</tr>'
        ;
        return $no_modal_list;
    }

    function get_total_mape ( $total_mape ) {

        if ( $total_mape == null || $total_mape = " ") {
            $total_mape = " ";
            return $total_mape;
        } else {
            $total_mape = $total_mape *100;
            $total_mape = round($total_mape, 2)." %";
            return $total_mape;
        }

    }

    function getModelJurnal($regresi, $data){
        $mape = $this->session->userdata('mape');
        $av_mape = 0;
        if ($mape != null && $mape->average_mape != null) {
            $av_mape = $mape->average_mape;
        }

        $body = "<tr>"
        ."<td>".$data->draft_bucket->segment->nama_segment."</td>"
        ."<td>".$data->regresi."</td>"
        ."<td>No Model</td>"
        ."<td> - </td>"
        ."<td></td>"."<td></td>"."<td></td>"."<td></td>"."<td></td>"
        ."<td>".round(($data->ecl_list[0]->no_model_sum_ecl), 2)."</td>"
        ."</tr>";

        if (count($data->ecl_list) > 1) {
            if ($regresi == '1') {
                $body .= "<tr>"
                ."<td>".$data->draft_bucket->segment->nama_segment."</td>"
                ."<td>".$data->regresi."</td>"
                ."<td>".$data->ecl_list[1]->model->nomor."</td>"
                ."<td>Proporsional</td>"
                ."<td>".$data->ecl_list[1]->model->var1."</td>"
                ."<td>".$data->ecl_list[1]->model->var2."</td>"
                ."<td>".$data->ecl_list[1]->model->lag1."</td>"
                ."<td>".$data->ecl_list[1]->model->lag2."</td>"
                ."<td>".round(($av_mape*100), 2)." %</td>"
                ."<td>".round(($data->ecl_list[1]->proporsional_sum_ecl_forward_looking), 2)."</td>"
                ."</tr>";
    
            } else if ($regresi == '2') {
                $body .= "<tr>"
                ."<td>".$data->draft_bucket->segment->nama_segment."</td>"
                ."<td>".$data->regresi."</td>"
                ."<td>".$data->ecl_list[1]->model->nomor."</td>"
                ."<td>Proporsional</td>"
                ."<td>".$data->ecl_list[1]->model->var1."</td>"
                ."<td>".$data->ecl_list[1]->model->var2."</td>"
                ."<td>".$data->ecl_list[1]->model->lag1."</td>"
                ."<td>".$data->ecl_list[1]->model->lag2."</td>"
                ."<td>".round(($av_mape*100), 2)." %</td>"
                ."<td>".round(($data->ecl_list[1]->proporsional_sum_ecl_forward_looking), 2)."</td>"
                ."</tr>";
    
                $body .= "<tr>"
                ."<td>".$data->draft_bucket->segment->nama_segment."</td>"
                ."<td>".$data->regresi."</td>"
                ."<td>".$data->ecl_list[1]->model->nomor."</td>"
                ."<td>Solver</td>"
                ."<td>".$data->ecl_list[1]->model->var1."</td>"
                ."<td>".$data->ecl_list[1]->model->var2."</td>"
                ."<td>".$data->ecl_list[1]->model->lag1."</td>"
                ."<td>".$data->ecl_list[1]->model->lag2."</td>"
                ."<td>".round(($av_mape*100), 2)." %</td>"
                ."<td>".round(($data->ecl_list[1]->solver_sum_ecl_forward_looking), 2)."</td>"
                ."</tr>";
    
            } else if ($regresi == '2') {
                $body .= "<tr>"
                ."<td>".$data->draft_bucket->segment->nama_segment."</td>"
                ."<td>".$data->regresi."</td>"
                ."<td>".$data->ecl_list[1]->model->nomor."</td>"
                ."<td>Proporsional</td>"
                ."<td>".$data->ecl_list[1]->model->var1."</td>"
                ."<td>".$data->ecl_list[1]->model->var2."</td>"
                ."<td>".$data->ecl_list[1]->model->lag1."</td>"
                ."<td>".$data->ecl_list[1]->model->lag2."</td>"
                ."<td>".round(($av_mape*100), 2)." %</td>"
                ."<td>".round(($data->ecl_list[1]->proporsional_sum_ecl_forward_looking), 2)."</td>"
                ."</tr>";
    
            } else {
                $body .= "<tr>"
                ."<td>".$data->draft_bucket->segment->nama_segment."</td>"
                ."<td>".$data->regresi."</td>"
                ."<td>".$data->ecl_list[1]->model->nomor."</td>"
                ."<td>Solver</td>"
                ."<td>".$data->ecl_list[1]->model->var1."</td>"
                ."<td>".$data->ecl_list[1]->model->var2."</td>"
                ."<td>".$data->ecl_list[1]->model->lag1."</td>"
                ."<td>".$data->ecl_list[1]->model->lag2."</td>"
                ."<td>".round(($av_mape*100), 2)." %</td>"
                ."<td>".round(($data->ecl_list[1]->solver_sum_ecl_forward_looking), 2)."</td>"
                ."</tr>";
            }
        }
        
        return $body;
    }

    function getModelPilihan($regresi){
        $select = '<option value="0">No Model</option>';
        if ($regresi == '1') {
            $select .= '<option value="1">Model penyesuaian Proporsional</option>';
        } elseif ($regresi == '2') {
            $select .= '<option value="1">Model penyesuaian Proporsional</option>
            <option value="2">Model penyesuaian Solver</option>';
        } elseif ($regresi == '3') {
            $select .= '<option value="1">Model penyesuaian Proporsional</option>';
        } else {
            $select .= '<option value="2">Model penyesuaian Solver</option>';
        }

        return $select;
    }

    function get_jurnal_modal () {
        $periode = $this->input->post('periode');
        $saldo_exiting = $this->input->post('saldo');
        $pilihan = $this->input->post('pilihan'); //0 no_model , //1 proporsional , //2 solver.
        $saldo_psak = 0;
        $body = "";

        $ecl = $this->session->userdata('ecl');
        if ($pilihan == '0') {
            $saldo_psak = (int) $ecl[0]->no_model_sum_ecl;
        } else if ($pilihan == '1') {
            $saldo_psak = (int) $ecl[1]->proporsional_sum_ecl_forward_looking;
        } else if ($pilihan == '2') {
            $saldo_psak = (int) $ecl[1]->solver_sum_ecl_forward_looking;
        }

        $body .= "<tr><td>Periode</td><td>".$periode."</td><td>-</td></tr>";
        $body .= "<tr><td>Saldo Penyisihan Exiting</td><td>"."Rp. " . number_format( $saldo_exiting, 2, ',', '.')."</td><td>-</td></tr>";
        $body .= "<tr><td>Saldo Penyisihan PSAK 71</td><td>"."Rp. " . number_format( $saldo_psak, 2, ',', '.')."</td><td>-</td></tr>";

        $saldo_selisih = $saldo_psak - $saldo_exiting;
        $body .= "<tr><td>Selesih Saldo Penyisihan</td><td>"."Rp. " . number_format( $saldo_selisih, 2, ',', '.')."</td><td>-</td></tr>";

        if ($saldo_selisih >= 0 ) {
            $body .= "<tr><td>Dr</td><td>Retained Earnings</td><td>"."Rp. " . number_format( $saldo_selisih, 2, ',', '.')."</td></tr>";
            $body .= "<tr><td>Cr</td><td>Allowance for Doubtful Accounts</td><td>"."Rp. " . number_format( $saldo_selisih, 2, ',', '.')."</td></tr>";
        } else {
            $body .= "<tr><td>Dr</td><td>Allowance for Doubtful Accounts</td><td>"."Rp. " . number_format( $saldo_selisih, 2, ',', '.')."</td></tr>";
            $body .= "<tr><td>Cr</td><td>Retained Earnings</td><td>"."Rp. " . number_format( $saldo_selisih, 2, ',', '.')."</td></tr>";
        }
        echo $body;
    }

    function get_jurnal_modal2 () {
        $periode = $this->input->post('periode');
        $saldo_exiting = $this->input->post('saldo');
        $pilihan = $this->input->post('pilihan'); //0 no_model , //1 proporsional , //2 solver.
        $saldo_psak = (int) $this->input->post('saldo71');

        $body = "";
        $body .= "<tr><td>Periode</td><td>".$periode."</td><td>-</td></tr>";
        $body .= "<tr><td>Saldo Penyisihan Exiting</td><td>"."Rp. " . number_format( $saldo_exiting, 2, ',', '.')."</td><td>-</td></tr>";
        $body .= "<tr><td>Saldo Penyisihan PSAK 71</td><td>"."Rp. " . number_format( $saldo_psak, 2, ',', '.')."</td><td>-</td></tr>";

        $saldo_selisih = $saldo_psak - $saldo_exiting;
        $body .= "<tr><td>Selesih Saldo Penyisihan</td><td>"."Rp. " . number_format( $saldo_selisih, 2, ',', '.')."</td><td>-</td></tr>";

        $saldo_non = 0;
        if ($saldo_selisih < 0) {
            $saldo_non = $saldo_selisih * -1 ;
        } else {
            $saldo_non = $saldo_selisih; 
        }

        if ($periode == "2019-12-31") {
            if ($saldo_selisih >= 0 ) {
                $body .= "<tr><td>Dr</td><td>Retained Earnings</td><td>"."Rp. " . number_format( $saldo_non, 2, ',', '.')."</td></tr>";
                $body .= "<tr><td>Cr</td><td>Allowance for Doubtful Accounts</td><td>"."Rp. " . number_format( $saldo_non, 2, ',', '.')."</td></tr>";
            } else {
                $body .= "<tr><td>Dr</td><td>Allowance for Doubtful Accounts</td><td>"."Rp. " . number_format( $saldo_non, 2, ',', '.')."</td></tr>";
                $body .= "<tr><td>Cr</td><td>Retained Earnings</td><td>"."Rp. " . number_format( $saldo_non, 2, ',', '.')."</td></tr>";
            }
        } else {
            if ($saldo_selisih >= 0 ) {
                $body .= "<tr><td>Dr</td><td>Bad Debt Expense</td><td>"."Rp. " . number_format( $saldo_non, 2, ',', '.')."</td></tr>";
                $body .= "<tr><td>Cr</td><td>Allowance for Doubtful Accounts</td><td>"."Rp. " . number_format( $saldo_non, 2, ',', '.')."</td></tr>";
            } else {
                $body .= "<tr><td>Dr</td><td>Allowance for Doubtful Accounts</td><td>"."Rp. " . number_format( $saldo_non, 2, ',', '.')."</td></tr>";
                $body .= "<tr><td>Cr</td><td>Bad Debt Expense</td><td>"."Rp. " . number_format( $saldo_non, 2, ',', '.')."</td></tr>";
            }
        }
        echo $body;
    }

    public function inputBucket () {
        $default = $this->input->post("default");
        $draft = $this->input->post("draft");
        $titik = $this->input->post("titik");
        $segment = $this->input->post("segment");
        $nama = $this->input->post("nama");
        $first = $this->input->post("first");
        $interval = $this->input->post("interval");
        $bucket = $this->input->post("bucket");
        $start = $this->input->post("start");
        $end = $this->input->post("end");
        $arr = explode("|",$draft);
        $id_draft = $arr[0];
        $flag_draft = $arr[1];

        if ( $flag_draft == '1' || $flag_draft == 1 ) {
            $id_draft = $id_draft;
            $titik = $titik;
            $id_segment = $segment;
            $hari_first_bucket = $first;
            $interval = $interval;
            $jumlah_bucket = $bucket;
            $start_date = $start ;
            $end_date = $end;
        } else {
            $id_draft = $id_draft;
            $titik = $titik;
            $id_segment = $segment;
            $hari_first_bucket = "";
            $interval = "";
            $jumlah_bucket = "";
            $start_date = $start ;
            $end_date = $end;
        }

        $data = array(
            "id_draft" => $id_draft,
            "id_segment" => $id_segment,
            "nama_bucket" => $nama,
            "hari_first_bucket" => $hari_first_bucket,
            "interval" => $interval,
            "jumlah_bucket" => $jumlah_bucket,
            "start_date" => $start_date,
            "end_date" => $end_date
        );

        if ( $default == "" || $default == "1") {
            $url = "draft-bucket/default/$titik";
        } else {
            $url = "draft-bucket/average/$titik";
        }

        $result = $this->Server->POST($url, $data );

        if ($result->status == '1' || $result->status == 1) {
            $this->session->set_flashdata("message", $result->message);
            $data['data'] = $result;
            $plugin['plugin'] = "datatable_anggota";
    
            $this->load->view("template/header");
            $this->load->view("template/nav", $data);
            $this->load->view("pages/model/view_draft_bucket", $data);
            $this->load->view("template/footer", $plugin);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
            redirect('model/draft_bucket');
        }        
    }

    public function del_draft_bucket($ids) {
        $url = "draft-bucket/$ids";
        $result = $this->Server->DELETE($url);
        if ($result->status == '1') {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }
        redirect('model/draft_bucket');
    }
}