<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ecl extends CI_Controller
{
    function __Construct() {
        parent::__Construct();
        $this->load->model("Server");
    }

    function index () {

        $irb = base64_decode($this->input->get('idb'));
        $draft_bucket = $this->Server->GET("draft-bucket");
        $data['bucket'] = $draft_bucket;
        $segment = [];
        
        if ($draft_bucket->status == 1) {
            foreach ($draft_bucket->data as $key => $value) {
                $id = $value->id_segment;
                $segment = $this->Server->GET("input-fl/segment/$id");
                if ($segment->status == 1) {
                    $segment = $segment->data;
                }
                break;
            }
        }

        // echo json_encode($segment); exit;
    
        $data['fl'] =  $segment;
        $plugin['plugin'] = "datatable_anggota";

        $session = array("draft_bucket" => $data['bucket']->data);
        $this->session->set_userdata($session);

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/ecl/simulate_ecl", $data);
        $this->load->view("template/footer", $plugin);
    }

    function draft () {
        
        $data['data'] = $this->Server->GET("draft-ecl");
        $plugin['plugin'] = "datatable_anggota";
        
        if ($data['data']->status == '1') {
            $session = array("draft_ecl" => $data['data']->data);
            $this->session->set_userdata($session);
        }
        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/ecl/draft_ecl", $data);
        $this->load->view("template/footer", $plugin);
    }

    function createDraft() {
        $id_draft_bucket = $this->input->post("draf");
        $id_model = $this->input->post("fl");
        $id_regresi = $this->input->post("regresi");
        $month_forecast = $this->input->post("forecast");
        $mape_start = $this->input->post("start");
        $mape_end = $this->input->post("end");
        $periode = $this->input->post("eperiode");
        $lgd = $this->input->post("elgd");
        $normal = $this->input->post("enormal");
        $optimis = $this->input->post("eoptimis");
        $pesimis = $this->input->post("epesimis");
        $scaling = $this->input->post("escalling");
        $average_mape = $this->input->post("average_mape");
        $ecl_no_model = $this->input->post("ecl_no_model");
        $ecl_proporsional = $this->input->post("ecl_proporsional");
        $ecl_solver = $this->input->post("ecl_solver");
    
        $flagChange = $this->input->post("flagChange");

        $sds = explode("/",$mape_start);
        $sde = explode("/",$mape_end);
        // echo $sds[0]."01".$sds[1]."<br />";
        // echo $sde[0]."01".$sde[1]."<br />";
        $dts = date("d/m/Y",strtotime($sds[0]."/01/".$sds[1]));
        $dte = date("t/m/Y",strtotime($sde[0]."/01/".$sde[1]));

        if ($flagChange == '0') {
            $id_model = "0";
            $month_forecast = "0";
            $average_mape = "0";
            $scaling = "0";
            $ecl_proporsional = "0";
            $ecl_solver = "0";
            $dts = "";
            $dte = "";
        }

        $url = "draft-ecl";
        $data = array(
            "id_draft_bucket" => $id_draft_bucket,
            "id_model" => $id_model,
            "id_regresi" => $id_regresi,
            "month_forecast" => $month_forecast,
            "periode" => date('d/m/Y', strtotime($periode)),
            "lgd" => $lgd,
            "normal" => $normal,
            "optimis" => $optimis,
            "pesimis" => $pesimis,
            "average_mape" => $average_mape,
            "ecl_no_model" => $ecl_no_model,
            "ecl_proporsional" => $ecl_proporsional,
            "ecl_solver" => $ecl_solver,
            "start_date_mape" => $dts,
            "end_date_mape" => $dte,
            "scaling" => $scaling,
        );

        // echo json_encode($data);
        // exit;

        $result = $this->Server->POST($url, $data );
        $this->session->set_flashdata("message", $result->message);
        redirect("Ecl/draft");
    }

    function getDraftECL(){
        $id = $this->input->post('id');
        // $drafts = $this->session->userdata('draft_ecl');
        // $data = null;
        // foreach ($drafts as $draft) {
        //     if($draft->id_draft_ecl == $id){
        //         $data = $draft;
        //         break;
        //     } 
        // }
        $data = $this->Server->GET("draft-ecl/$id");
        if ($data != null && $data->status == 1) {
            $data = $data->data;
        }
        echo json_encode($data);
    }

    function del_draft_ecl($ids) {
        $url = "draft-ecl/$ids";
        $result = $this->Server->DELETE($url);
        if ($result->status == '1') {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }
        redirect('Ecl/draft');
    }

    function mape () {
        $draf = $this->input->post("draf");
        $fl = $this->input->post("fl");
        $regresi = $this->input->post("regresi");
        $forecast = $this->input->post("forecast");
        $start = $this->input->post("start");
        $end = $this->input->post("end");

        echo $draf;
    }

    function view_mape_ecl () {

        $irb = base64_decode($this->input->get('idb'));
        $data['data'] = $this->Server->GET("draft-ecl/$irb");
        $plugin['plugin'] = "datatable_anggota";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/ecl/view_mape", $data);
        $this->load->view("template/footer", $plugin);
    }

    function view_model_ecl () {

        $irb = base64_decode($this->input->get('idb'));
        $data['data'] = $this->Server->GET("draft-ecl/$irb");
        $plugin['plugin'] = "datatable_anggota";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/ecl/view_ecl_model", $data);
        $this->load->view("template/footer", $plugin);
    }

    function updateDraftECL(){
        $id_draft_ecl = $this->input->post("id_draft_ecl");
        $result_flag_model = $this->input->post("result_flag_model");
        $result_saldo_exiting = $this->input->post("result_saldo_exiting");

        $url = "draft-ecl/$id_draft_ecl";
        $data = array(
            "result_flag_model" => $result_flag_model,
            "result_saldo_exiting" => $result_saldo_exiting,
        );

        $result = $this->Server->PUT($url, $data );
        echo json_encode($result);
    }
}