<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Segment extends CI_Controller
{
    function __Construct() {
        parent::__Construct();
        $this->load->model("Server");
    }

    public function index () {
        $data['data'] = $this->Server->GET("segment");
        $plugin['plugin'] = "datatable_anggota";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/segment/segment", $data);
        $this->load->view("template/footer", $plugin);
    }

    public function create () {
        $kode = $this->input->post("kode");
        $nama = $this->input->post("nama");

        $url = "/segment";
        $data = array(
            "kode" => $kode,
            "nama" => $nama,
        );

        $result = $this->Server->POST($url, $data );

        if ($result->status == '1' || $result->status == 1) {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }
        redirect('segment');
    }

    public function update () {
        $ids = $this->input->post("ids");
        $kode = $this->input->post("kode");
        $nama = $this->input->post("nama");

        $url = "/segment/$ids";
        $data = array(
            "kode" => $kode,
            "nama" => $nama,
        );


        $result = $this->Server->PUT($url, $data );
        if ($result->status == '1' || $result->status == 1) {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }
        redirect('segment');
    }

    public function delete ($ids) {
        $url = "segment/$ids";
        $result = $this->Server->DELETE($url);
        if ($result->status == '1') {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }
        redirect('draft');
    }
}