<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recover extends CI_Controller
{
    function __Construct() {
        parent::__Construct();
        $this->load->model("Server");
        date_default_timezone_set("Asia/Jakarta");
    }

    function password($uuid="") {
        if ($uuid == "") {
            $this->session->set_flashdata("message", "Parameter Salah");
            redirect("auth");
        } else {
            $result = $this->Server->GET("password/log/$uuid");

            if ($result->data == NULL) {
                $this->session->set_flashdata("message", "Halaman tidak valid");
                redirect("auth");
            } else {
                $now = date("Y-m-d H:i:s");
                $dds['data'] = $result->data;

                if ($now > $result->data->Expired_date) {
                    $this->session->set_flashdata("message", "Halaman kadaluarsa, silahkan lakukan proses ulang");
                    redirect("auth");
                } else {
                    $this->load->view("login/recover", $dds);
                }
            }
        }
    }

    function execrecover() {
        $param['password'] = $this->input->post("passwordnew");
        $param['uuid'] = $this->input->post("uuid");

        $result = $this->Server->POST_JSON("password/recover", $param);

        if($result->status == '0') {
            $this->session->set_flashdata("message", $result->message);
            redirect("auth");
        } else {
            $this->session->set_flashdata("message", $result->message);
            redirect("auth");
        }
    }
}
