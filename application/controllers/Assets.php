<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assets extends CI_Controller
{
    function __Construct() {
        parent::__Construct();
        $this->load->model("Server");
    }

    function index() {
        if($this->Server->permision_validate() == 0) {
            $this->session->set_flashdata("message", "Silahkan login terlebih dahulu untuk mengakses halaman ini.");
            redirect("auth");
        }

        $id_cutomer = $this->session->userdata('id_customer');
        $data['data'] = $this->Server->GET("asset/customer/$id_cutomer");
        $plugin['plugin'] = "datatable_assets";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/assets/assets", $data);
        $this->load->view("template/footer", $plugin);
    }

    function create() {
        $id_cutomer = $this->session->userdata('id_customer');
        $created_by = $this->session->userdata('ids');
        $bahan = $this->input->post('bahan');
        $harga_beli = $this->input->post('harga');
        $id_type_asset = $this->input->post('tipe');
        $image_path = "";
        $jumlah = $this->input->post('jum');
        $keterangan = $this->input->post('ket');
        $kode_asset_manual =$this->input->post('kode');
        $kondisi = $this->input->post('kon');
        $merk = $this->input->post('merk');
        $nama_asset = $this->input->post('assets');
        $no_seri = $this->input->post('seri');
        $tahun = $this->input->post('tahun');
        $tanggal_masuk = $this->input->post('tanggal');
        $ukuran = $this->input->post('ukuran');

        $url = "asset";
        $data = array(
            "id_customer" => $id_cutomer,
            "created_by" => $created_by,
            "bahan" => $bahan,
            "harga_beli" => $harga_beli,
            "id_type_asset" => $id_type_asset,
            "image_path" => $image_path,
            "jumlah" => $jumlah,
            "keterangan" => $keterangan,
            "kode_asset_manual" => $kode_asset_manual,
            "kondisi" => $kondisi,
            "merk" => $merk,
            "nama_asset" => $nama_asset,
            "no_seri" => $no_seri,
            "tahun" => $tahun,
            "tanggal_masuk" => $tanggal_masuk,
            "ukuran" => $ukuran
        );
        $result = $this->Server->POST($url, $data );
        $this->session->set_flashdata("message", $result->message);
        redirect('assets');
    }


    function update() {
        $id_cutomer = $this->session->userdata('id_customer');
        $created_by = $this->session->userdata('ids');
        $id_assets = $this->input->post('id_assets');
        $bahan = $this->input->post('bahan');
        $harga_beli = $this->input->post('harga');
        $id_type_asset = $this->input->post('tipe');
        $image_path = "";
        $jumlah = $this->input->post('jum');
        $keterangan = $this->input->post('ket');
        $kode_asset_manual =$this->input->post('kode');
        $kondisi = $this->input->post('kon');
        $merk = $this->input->post('merk');
        $nama_asset = $this->input->post('assets');
        $no_seri = $this->input->post('seri');
        $tahun = $this->input->post('tahun');
        $tanggal_masuk = $this->input->post('tanggal');
        $tanggal_masuk = date("Y-m-d", strtotime("$tanggal_masuk"));
        $ukuran = $this->input->post('ukuran');

        $url = "asset/$id_assets";
        $data = array(
            "id_customer" => $id_cutomer,
            "created_by" => $created_by,
            "bahan" => $bahan,
            "harga_beli" => $harga_beli,
            "id_type_asset" => $id_type_asset,
            "image_path" => $image_path,
            "jumlah" => $jumlah,
            "keterangan" => $keterangan,
            "kode_asset_manual" => $kode_asset_manual,
            "kondisi" => $kondisi,
            "merk" => $merk,
            "nama_asset" => $nama_asset,
            "no_seri" => $no_seri,
            "tahun" => $tahun,
            "tanggal_masuk" => $tanggal_masuk,
            "ukuran" => $ukuran
        );

        $result = $this->Server->PUT($url, $data );
        $this->session->set_flashdata("message", $result->message);
        redirect('assets');
       

    }

    function delete ( $id ) {

        $url = "asset/$id";
        $result = $this->Server->DELETE($url);
        $this->session->set_flashdata("message", $result->message);
        redirect('assets');
    }

}