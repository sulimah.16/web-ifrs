<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Input extends CI_Controller
{
    function __Construct() {
        parent::__Construct();
        $this->load->model("Server");
    }

    
    public function index () {

        $draft = $this->input->post("draft");

        if ( $draft == '' || $draft == '0' ) {
            $id_draft = '0';
        } else {
            $id_draft = $draft;
        }

        $data['data'] = $this->Server->GET("input-data/draft/$id_draft");

        $data['draft'] = $this->Server->GET("draft/flag/1");
        $data['draft2'] = $this->Server->GET("draft/flag/2");
        $data['segment'] = $this->Server->GET("segment");
        $data['id_draft'] = $id_draft;
        $plugin['plugin'] = "datatable_anggota";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/input/input", $data);
        $this->load->view("template/footer", $plugin);
    }

    public function create () {
        $tipe = $this->input->post("tipe");
        if ( $tipe == '0' ) {
            $draft = $this->input->post("drafts");
            $segment = $this->input->post("segment");
            $tanggal_piutang = $this->input->post("tanggal_piutang");
            $id_piutang = $this->input->post("id_piutang");
            $kode_cus = $this->input->post("kode_cus");
            $nama_cus = $this->input->post("nama_cus");
            $kode_pro = $this->input->post("kode_pro");
            $nama_pro = $this->input->post("nama_pro");
            $pemberi_kerja = $this->input->post("pemberi_kerja");
            $doc_no = $this->input->post("doc_no");
            $doc_tipe = $this->input->post("doc_tipe");
            $jenis_piutang = $this->input->post("jenis_piutang");
            $umur_piutang = $this->input->post("umur_piutang");
            $saldo_piutang = $this->input->post("saldo_piutang");

            $url = "input-data";
            $data = array(
                "id_draft" => $draft,
                "id_segment" => $segment,
                "tanggal_piutang" => $tanggal_piutang,
                "id_piutang" => $id_piutang,
                "kode_customer" => $kode_cus,
                "nama_customer" => $nama_cus,
                "kode_proyek" => $kode_pro,
                "nama_proyek" => $nama_pro,
                "pemberi_kerja" => $pemberi_kerja,
                "doc_number" => $doc_no,
                "doc_type" => $doc_tipe,
                "jenis_piutang" => $jenis_piutang,
                "umur_piutang" => $umur_piutang,
                "saldo_piutang" => $saldo_piutang
            );

          


            $result = $this->Server->POST($url, $data );
            $this->session->set_flashdata("message", $result->message);
            redirect('input');
        } elseif ( $tipe == '1' ) {
            $segment = $this->input->post("segment");
            $drafte = $this->input->post("draft1");
            $tmpFile = $_FILES['file_b']['tmp_name'];
            $typeFile = $_FILES['file_b']['type'];
            $nameFile = $_FILES['file_b']['name']; 
            
            $url = 'input-data/upload';
            $data = array('file'=> new CURLFile($tmpFile,$typeFile,$nameFile),'id_draft' => $drafte, 'id_segment' => $segment);

            // echo json_encode($data);exit;
            $result = $this->Server->UPLOAD($url, $data );

          

            if ($result->status == '1' || $result->status == 1) {
                $this->session->set_flashdata("message", $result->message);
                $plugin['plugin'] = "datatable_anggota";
                $data['data'] = $result;

                $this->load->view("template/header");
                $this->load->view("template/nav", $data);
                $this->load->view("pages/upload/report", $data);
                $this->load->view("template/footer", $plugin);
            } else {
                $this->session->set_flashdata("err-message", $result->message);
                redirect('input');
            }
            // $this->session->set_flashdata("message", $result->message);
            // redirect('input');

        } else {
            $tmpFile = $_FILES['file_c']['tmp_name'];
            $typeFile = $_FILES['file_c']['type'];
            $nameFile = $_FILES['file_c']['name']; 
            $draft1 = $this->input->post("draft2");
            $jumlah_bucket = $this->input->post("jumlah_bucket");
            $segment = $this->input->post("segment");
            $url = 'input-data/upload-bucket';
            $data = array('file'=> new CURLFile($tmpFile,$typeFile,$nameFile),'id_draft' => $draft1, 'id_segment' => $segment, 'jumlah_bucket' => $jumlah_bucket);
            // echo json_encode($data);exit;
            $result = $this->Server->UPLOAD($url, $data );

            if ($result->status == '1' || $result->status == 1) {
                $this->session->set_flashdata("message", $result->message);
                $data['data'] = $result;

                $plugin['plugin'] = "datatable_anggota";
                $this->load->view("template/header");
                $this->load->view("template/nav", $data);
                $this->load->view("pages/upload/report", $data);
                $this->load->view("template/footer", $plugin);
            } else {
                $this->session->set_flashdata("err-message", $result->message);
                redirect('input');
            }
            // $this->session->set_flashdata("message", $result->message);
            // redirect('input');
        }
        
    }

    public function update () {
        $id_input = $this->input->post("id_input");
        $draft = $this->input->post("draft");
        $segment = $this->input->post("segment");
        $tanggal_piutang = $this->input->post("tanggal_piutang");
        $id_piutang = $this->input->post("id_piutang");
        $kode_cus = $this->input->post("kode_cus");
        $nama_cus = $this->input->post("nama_cus");
        $kode_pro = $this->input->post("kode_pro");
        $nama_pro = $this->input->post("nama_pro");
        $pemberi_kerja = $this->input->post("pemberi_kerja");
        $doc_no = $this->input->post("doc_no");
        $doc_tipe = $this->input->post("doc_tipe");
        $jenis_piutang = $this->input->post("jenis_piutang");
        $umur_piutang = $this->input->post("umur_piutang");
        $saldo_piutang = $this->input->post("saldo_piutang");

        $url = "input-data/$id_input";
        $data = array(
            "id_draft" => $draft,
            "id_segment" => $segment,
            "tanggal_piutang" => $tanggal_piutang,
            "id_piutang" => $id_piutang,
            "kode_customer" => $kode_cus,
            "nama_customer" => $nama_cus,
            "kode_proyek" => $kode_pro,
            "nama_proyek" => $nama_pro,
            "pemberi_kerja" => $pemberi_kerja,
            "doc_number" => $doc_no,
            "doc_type" => $doc_tipe,
            "jenis_piutang" => $jenis_piutang,
            "umur_piutang" => $umur_piutang,
            "saldo_piutang" => $saldo_piutang
        );

        $result = $this->Server->PUT($url, $data );

        if ($result->status == '1' || $result->status == 1) {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }
        
        redirect('input');

        

    }

    public function delete ($ids) {
        $url = "input-data/$ids";
        $result = $this->Server->DELETE($url);
        $this->session->set_flashdata("message", $result->message);
        redirect('input');
    }
}