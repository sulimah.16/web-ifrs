<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Draft extends CI_Controller
{
    function __Construct() {
        parent::__Construct();
        $this->load->model("Server");
    }

    public function index () {
        $data['data'] = $this->Server->GET("draft");
        $plugin['plugin'] = "datatable_anggota";

       
        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/draft/draft", $data);
        $this->load->view("template/footer", $plugin);
    }

    public function update () {
        $ids = $this->input->post("ids");
        $nama = $this->input->post("nama");

        $url = "/draft/$ids";
        $data = array(
            "nama" => $nama,
        );


        $result = $this->Server->PUT($url, $data );

        if ($result->status == '1') {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }

        redirect('draft');
    }

    public function delete ($ids) {
        $url = "draft/$ids";
        $result = $this->Server->DELETE($url);
        if ($result->status == '1') {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }
        redirect('draft');
    }
}