<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank extends CI_Controller
{
    function __Construct() {
        parent::__Construct();
        $this->load->model("Server");
    }

    function create() {
        $id_cutomer = $this->session->userdata('id_customer');
        $created_by = $this->session->userdata('ids');
        $id_member = $this->input->post("id_member");
        $nama_bank = $this->input->post("bank");
        $nama_pemilik = $this->input->post("pemilik");
        $no_rekening = $this->input->post("rek");

        $url = "rek-bank/member";
        $data = array(
            "created_by" => $created_by,
            "id_customer" => $id_cutomer,
            "id_member" => $id_member,
            "nama_bank" => $nama_bank,
            "nama_pemilik" => $nama_pemilik,
            "no_rekening" => $no_rekening,
        );

        $result = $this->Server->POST($url, $data );
        $this->session->set_flashdata("message", $result->message);
        redirect("anggota/profile/$id_member");
    }

    function update() {
        $id_cutomer = $this->session->userdata('id_customer');
        $created_by = $this->session->userdata('ids');
        $id_bank = $this->input->post("id_bank");
        $id_member = $this->input->post("id_member");
        $nama_bank = $this->input->post("bank");
        $nama_pemilik = $this->input->post("pemilik");
        $no_rekening = $this->input->post("rek");

        $url = "rek-bank/member/$id_bank";
        $data = array(
            "created_by" => $created_by,
            "id_customer" => $id_cutomer,
            "nama_bank" => $nama_bank,
            "nama_pemilik" => $nama_pemilik,
            "no_rekening" => $no_rekening,
        );   

        $result = $this->Server->PUT($url, $data );
        $this->session->set_flashdata("message", $result->message);
        redirect("anggota/profile/$id_member");
    }

    function delete( $id, $id_member ) {
        $url = "rek-bank/member/$id";
        $result = $this->Server->DELETE($url);
        $this->session->set_flashdata("message", $result->message);
        redirect("anggota/profile/$id_member");
    }
}