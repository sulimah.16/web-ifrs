<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info extends CI_Controller
{
    
    function __Construct() {
        parent::__Construct();
        $this->load->model("Server");
    }

    function index() {

        $plugin = "";
        $this->load->view("template/header");
        $this->load->view("template/nav");
        $this->load->view("pages/info/tentang");
        $this->load->view("template/footer", $plugin);
    }

    function index2() {

        $plugin = "";
        $this->load->view("template/header");
        $this->load->view("template/nav");
        $this->load->view("pages/info/info");
        $this->load->view("template/footer", $plugin);
    }

    function tutorial() {

        $plugin = "";
        $this->load->view("template/header");
        $this->load->view("template/nav");
        $this->load->view("pages/info/tata_cara");
        $this->load->view("template/footer", $plugin);
    }

    function kontak() {

        $plugin = "";
        $this->load->view("template/header");
        $this->load->view("template/nav");
        $this->load->view("pages/info/kontak");
        $this->load->view("template/footer", $plugin);
    }


}