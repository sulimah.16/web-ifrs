<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
    
    function __Construct() {
        parent::__Construct();
        $this->load->model("Server");
    }

    function index() {

        $draft = $this->Server->GET("dashboard/data-draft");
        $activity = $this->Server->GET("dashboard/data-login");

        $perse_hari = 0;
        $perse_bucket = 0;

        $total_draft = $draft->data->total_draft;
        $draft_perhari = $draft->data->total_draft_hari;
        $draft_perbucket = $draft->data->total_draft_bucket;

        if ($draft_perhari > 0) {
            $perse_hari = ($draft_perhari/$total_draft)*100;
        }

        if ($draft_perbucket > 0) {
            $perse_bucket = ($draft_perbucket/$total_draft)*100;
        }
      
        // echo json_encode($activity);exit;

        $acts = [];
        foreach ( $activity->data as $a ) {
            $date = "new Date ($a->tahun, $a->bulan, 19)";
            $value = $a->jumlah_aktivitas;
            $acts[] = array(
                'date' => $date,
                'value' => $value,
            );
        }

        $acts = json_encode($acts);
       


        $data['master'] = $this->Server->GET("dashboard/data-master");
        $data['perse_hari'] = $perse_hari;
        $data['parse_bucket'] = $perse_bucket;
        $data['activity'] = $acts;
    
        $plugin = "";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/home/home", $data);
        $this->load->view("template/footer", $plugin);
    }


}