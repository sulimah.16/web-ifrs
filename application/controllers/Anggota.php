<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota extends CI_Controller
{
    function __Construct() {
        parent::__Construct();
        $this->load->model("Server");
    }

    

    function index() {
        
        $rsp =  $this->Server->GET("user");

        if ($rsp->status != '1') {
            $this->session->set_flashdata("message", $rsp->message);
            redirect('home');
        }

        $data['data'] = $rsp;
        $plugin['plugin'] = "datatable_anggota";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/anggota/anggota", $data);
        $this->load->view("template/footer", $plugin);
    }

    public function profile ( $ids ) {
        if($this->Server->permision_validate() == 0) {
            $this->session->set_flashdata("message", "Silahkan login terlebih dahulu untuk mengakses halaman ini.");
            redirect("auth");
        }

        $id_cutomer = $this->session->userdata('id_customer');
        $saldo = "";
        $saldos = $this->Server->GET("report-saldo/$id_cutomer/$ids")->data;

        foreach ( $saldos as $ss ) {
            $saldo = $ss->total_saldo;
        }

        $pokok = [];
        $wajib = [];
        $sukarela = [];
        $years = date('Y');
      
        $pokok = $this->Server->GET("simpanan-iuran/member/$ids/1/0/0")->data;
        $wajib = $this->Server->GET("simpanan-iuran/member/$ids/2/$years/0")->data;
        $sukarela = $this->Server->GET("simpanan-iuran/member/$ids/3/$years/0")->data;

        $data['data'] = $this->Server->GET("member/$ids");
        $data['province'] = $this->Server->GET("wilayah/provinsi");
        $data['bank'] = $this->Server->GET("rek-bank/member/member/$ids");
        $data['agama'] = $this->Server->GET("agama");
        $data['pokok'] = $pokok;
        $data['wajib'] = $wajib;
        $data['sukarela'] = $sukarela;
        $data['saldo'] = $saldo;
        $data['pokoks'] = $this->Server->GET("type-simpanan-customer/customer/$id_cutomer/1");
        $data['wajibs'] = $this->Server->GET("type-simpanan-customer/customer/$id_cutomer/2");
        $plugin['plugin'] = "";
        
        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/anggota/profile", $data);
        $this->load->view("template/footer", $plugin);
    }

    function calon() {
        if($this->Server->permision_validate() == 0) {
            $this->session->set_flashdata("message", "Silahkan login terlebih dahulu untuk mengakses halaman ini.");
            redirect("auth");
        }

        $id_cutomer = $this->session->userdata('id_customer');
        $data['data'] = $this->Server->GET("member/customer/$id_cutomer");
        $data['province'] = $this->Server->GET("wilayah/provinsi");
        $plugin['plugin'] = "datatable_anggota";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/anggota/calon", $data);
        $this->load->view("template/footer", $plugin);
    }

    function create () {

        $tmpFile = $_FILES['add_image']['tmp_name'];
        $typeFile = $_FILES['add_image']['type'];
        $nameFile = $_FILES['add_image']['name']; 
        
        if ( $nameFile == "" ) {
            $image_path = "";
        } else {
            $url = 'upload_file';
            $data = array('file'=> new CURLFile($tmpFile,$typeFile,$nameFile),'type' => 'profile');


            $upload = $this->Server->UPLOAD($url, $data );

            $image_path = $upload->data->path_name;
        }

        $name = $this->input->post("add_fullname");
        $gender = $this->input->post("gender");
        $role = $this->input->post("role");
        // $alamat = $this->input->post("add_address");
        $alamat = "no";
        $phone = $this->input->post("add_phone");
        $email = $this->input->post("add_email");
        $status = $this->input->post("add_status");
        $divisi = $this->input->post("add_divisi");
        $jabatan = $this->input->post("add_jabatan");
        $username = $this->input->post("add_username");
        $password = $this->input->post("add_password");

        
        $url = "/user";
        $datas = array(
            "username" => $username,
            "password" => $password,
            "id_role" => $role,
            "status" => $status,
            "email" => $email,
            "nama" => $name,
            "no_telp" => $phone,
            "alamat" => $alamat,
            "jk" => $gender,
            "divisi" => $divisi,
            "jabatan" => $jabatan,
            "path_profile" => $image_path
        );

        $result = $this->Server->POST($url, $datas );

        if ($result->status == '1' || $result->status == 1) {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }
        redirect('anggota');
    }

    function update() {

        $path = $this->input->post("path");

       
        $tmpFile = $_FILES['add_image']['tmp_name'];
        $typeFile = $_FILES['add_image']['type'];
        $nameFile = $_FILES['add_image']['name']; 

        if ( $nameFile == "" ) {
            $image_path = $path;
        } else {
            $url = 'upload_file';
            $data = array('file'=> new CURLFile($tmpFile,$typeFile,$nameFile),'type' => 'profile');


            $upload = $this->Server->UPLOAD($url, $data );

            $image_path = $upload->data->path_name;
        }

        $ids = $this->input->post("ids");
        $name = $this->input->post("add_fullname");
        $gender = $this->input->post("gender");
        $role = $this->input->post("role");
        // $alamat = $this->input->post("add_address");
        $alamat = "no";
        $phone = $this->input->post("add_phone");
        $email = $this->input->post("add_email");
        $status = $this->input->post("add_status");
        $divisi = $this->input->post("add_divisi");
        $jabatan = $this->input->post("add_jabatan");
        $username = $this->input->post("add_username");
        $password = $this->input->post("add_password");

        
        $url = "/user/$ids";
        $datas = array(
            "username" => $username,
            "password" => $password,
            "id_role" => $role,
            "status" => $status,
            "email" => $email,
            "nama" => $name,
            "no_telp" => $phone,
            "alamat" => $alamat,
            "jk" => $gender,
            "divisi" => $divisi,
            "jabatan" => $jabatan,
            "path_profile" => $image_path
        );

        $result = $this->Server->PUT($url, $datas );

        if ($result->status == '1' || $result->status == 1) {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }
        redirect('anggota');

    }

    function gantiPass () {
        $ids = $this->input->post("ids");
        $new_pass = $this->input->post("new_pass");
        $conf_pass = $this->input->post("conf_pass");

        if ( $new_pass != $conf_pass ) {
            $this->session->set_flashdata("err-message", 'Password tidak cocok');
            redirect('anggota');
        }

        $data = array(
            "password_new" => $new_pass,
            "password_confirm" => $conf_pass 
        );

        $url = "change_password/$ids";

        $result = $this->Server->PUT($url, $data );

        if ($result->status == '1' || $result->status == 1) {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }
        redirect('anggota');

    }

  
    function aktifasi ($id, $val) {
       $url = "user_status/$id/$val";
       $data = array();
       $result = $this->Server->PUT($url, $data );

       if ($result->status == '1' || $result->status == 1) {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }
        redirect('anggota');
    }

}