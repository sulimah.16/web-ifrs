<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File extends CI_Controller
{
    
    function __Construct() {
        parent::__Construct();
        $this->load->model("Server");
    }

    function index() {

        $data['data'] = $this->Server->GET("upload_file");
        $plugin['plugin'] = "datatable_file";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/file/file", $data);
        $this->load->view("template/footer", $plugin);
    }

    public function delete ($ids) {
        $url = "upload_file/$ids";
        $result = $this->Server->DELETE($url);
        $this->session->set_flashdata("message", $result->message);
        redirect('file');
    }

}