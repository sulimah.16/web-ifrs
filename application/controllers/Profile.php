<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller
{
    function __Construct() {
        parent::__Construct();
        $this->load->model("Server");
    }

    function index() {
        if($this->Server->permision_validate() == 0) {
            $this->session->set_flashdata("message", "Silahkan login terlebih dahulu untuk mengakses halaman ini.");
            redirect("auth");
        }

        $id = $this->session->userdata('ids');
        $data['data'] = $this->Server->GET("user/$id");
        $data['payroll'] = $this->Server->GET("payroll/employee/$id");
        $data['education'] = $this->Server->GET("employee/education/employee/$id");
        $data['family'] = $this->Server->GET("employee/family/employee/$id");
        $data['job'] = $this->Server->GET("employee/job/employee/$id");
        $data['card'] = $this->Server->GET("employee/card/employee/$id");
        $data['saldoleave'] = $this->Server->GET("leave/quota/$id");
        $data['ids'] = $id;
        $data['active'] = "";

        $plugin['plugin'] = "datatable_employee";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("page/profile/profile", $data);
        $this->load->view("template/footer", $plugin);
    }

    function personalInformation() {
        if($this->Server->permision_validate() == 0) {
            $this->session->set_flashdata("message", "Silahkan login terlebih dahulu untuk mengakses halaman ini.");
            redirect("auth");
        }

        $id = $this->session->userdata('ids');
        $data['data'] = $this->Server->GET("user/$id");
        $data['payroll'] = $this->Server->GET("payroll/employee/$id");
        $data['education'] = $this->Server->GET("employee/education/employee/$id");
        $data['family'] = $this->Server->GET("employee/family/employee/$id");
        $data['job'] = $this->Server->GET("employee/job/employee/$id");
        $data['card'] = $this->Server->GET("employee/card/employee/$id");
        $data['saldoleave'] = $this->Server->GET("leave/quota/$id");
        $data['ids'] = $id;
        $data['active'] = "";

        $plugin['plugin'] = "datatable_employee";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("page/profile/personal_information", $data);
        $this->load->view("template/footer", $plugin);
    }

    function accountInformation() {
        if($this->Server->permision_validate() == 0) {
            $this->session->set_flashdata("message", "Silahkan login terlebih dahulu untuk mengakses halaman ini.");
            redirect("auth");
        }

        $id = $this->session->userdata('ids');
        $data['data'] = $this->Server->GET("user/$id");
        $data['payroll'] = $this->Server->GET("payroll/employee/$id");
        $data['education'] = $this->Server->GET("employee/education/employee/$id");
        $data['family'] = $this->Server->GET("employee/family/employee/$id");
        $data['job'] = $this->Server->GET("employee/job/employee/$id");
        $data['card'] = $this->Server->GET("employee/card/employee/$id");
        $data['saldoleave'] = $this->Server->GET("leave/quota/$id");
        $data['ids'] = $id;
        $data['active'] = "";

        $plugin['plugin'] = "datatable_employee";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("page/profile/account_information", $data);
        $this->load->view("template/footer", $plugin);
    }

    function changePassword() {
        if($this->Server->permision_validate() == 0) {
            $this->session->set_flashdata("message", "Silahkan login terlebih dahulu untuk mengakses halaman ini.");
            redirect("auth");
        }
        $id = $this->session->userdata('ids');
        $data['data'] = $this->Server->GET("user/$id");
        $data['payroll'] = $this->Server->GET("payroll/employee/$id");
        $data['education'] = $this->Server->GET("employee/education/employee/$id");
        $data['family'] = $this->Server->GET("employee/family/employee/$id");
        $data['job'] = $this->Server->GET("employee/job/employee/$id");
        $data['card'] = $this->Server->GET("employee/card/employee/$id");
        $data['saldoleave'] = $this->Server->GET("leave/quota/$id");
        $data['ids'] = $id;
        $data['active'] = "";

        $plugin['plugin'] = "datatable_employee";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("page/profile/change_password", $data);
        $this->load->view("template/footer", $plugin);
    }
}