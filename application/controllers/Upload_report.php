<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload_report extends CI_Controller
{
    function __Construct() {
        parent::__Construct();
        $this->load->model("Server");
    }

    

    function index() {
        

        $data['data'] = $this->Server->GET("user");

        

        $plugin['plugin'] = "datatable_anggota";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/upload/report", $data);
        $this->load->view("template/footer", $plugin);
    }
}