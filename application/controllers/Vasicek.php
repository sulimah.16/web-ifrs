<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vasicek extends CI_Controller
{
    function __Construct() {
        parent::__Construct();
        $this->load->model("Server");
    }

    public function draft () {
        $data['data'] = $this->Server->GET("vasicek/draft");
        $plugin['plugin'] = "datatable_anggota";

       
        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/vasicek/draft", $data);
        $this->load->view("template/footer", $plugin);
    }

    public function draft_update () {
        $ids = $this->input->post("ids");
        $nama = $this->input->post("nama");

        $url = "/vasicek/draft/$ids";
        $data = array(
            "nama" => $nama,
        );


        $result = $this->Server->PUT($url, $data );

        if ($result->status == '1') {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }

        redirect('vasicek/draft');
    }

    public function draft_delete ($ids) {
        $url = "vasicek/draft/$ids";
        $result = $this->Server->DELETE($url);
        if ($result->status == '1') {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }
        redirect('vasicek/draft');
    }

    function gdp() {
        $data['data'] = $this->Server->GET("vas-gdp");
        $plugin['plugin'] = "datatable_anggota";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/vasicek/gdp", $data);
        $this->load->view("template/footer", $plugin);
    }

    function createGDP () {
        
        $tipe = $this->input->post("tipe");

        if ( $tipe == '0' || $tipe == 0 ) {
            $bulan = $this->input->post("bulan");
            $tahun = $this->input->post("tahun");
            $gdp = $this->input->post("gdp");
            $flag = $this->input->post("flag");
            $url = "/vas-gdp";
            $data = array(
                "bulan" => $bulan,
                "tahun" => $tahun,
                "value" => $gdp,
                "flag" => $flag
            );

           

            $result = $this->Server->POST($url, $data );

            if ($result->status == '1' || $result->status == 1) {
                $this->session->set_flashdata("message", $result->message);
            } else {
                $this->session->set_flashdata("err-message", $result->message);
            }
            redirect('Vasicek/gdp');
        } else {
            $tmpFile = $_FILES['file']['tmp_name'];
            $typeFile = $_FILES['file']['type'];
            $nameFile = $_FILES['file']['name']; 
            $url = 'vas-gdp/upload';

            $data = array(
                'file'=> new CURLFile($tmpFile,$typeFile,$nameFile),
            );

            $result = $this->Server->UPLOAD($url, $data );

            if ($result->status == '1' || $result->status == 1) {
                $this->session->set_flashdata("message", $result->message);
                $data['data'] = $result;

                $plugin['plugin'] = "datatable_anggota";
                $this->load->view("template/header");
                $this->load->view("template/nav", $data);
                $this->load->view("pages/upload/report-gdp", $data);
                $this->load->view("template/footer", $plugin);
            } else {
                $this->session->set_flashdata("err-message", $result->message);
                redirect('input');
            }
        }

        

    }

    function editGDP () {
        $id = $this->input->post("id");
        $bulan = $this->input->post("bulan");
        $tahun = $this->input->post("tahun");
        $gdp = $this->input->post("gdp");
        $flag = $this->input->post("flag");
        $url = "/vas-gdp/$id";
        $data = array(
            "bulan" => $bulan,
            "tahun" => $tahun,
            "value" => $gdp,
            "flag" => $flag
        );

        $result = $this->Server->PUT($url, $data );

        if ($result->status == '1' || $result->status == 1) {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }
        redirect('Vasicek/gdp');
    }

    function deleteGDP ($ids) {
        $url = "/vas-gdp/$ids";
        $result = $this->Server->DELETE($url);
        $this->session->set_flashdata("message", $result->message);
        redirect('Vasicek/gdp');
    }

    function rating () {
        $data['data'] = $this->Server->GET("vas-rating");
        $plugin['plugin'] = "datatable_anggota";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/vasicek/rating", $data);
        $this->load->view("template/footer", $plugin);
    }

    function createRating () {
        $kode = $this->input->post("kode");
        $rating = $this->input->post("rating");
        $url = "/vas-rating";
        $data = array(
            "kode" => $kode,
            "value" => round(($rating / 100 ), 4),
        );

        $result = $this->Server->POST($url, $data );


        if ($result->status == '1' || $result->status == 1) {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }
        redirect('Vasicek/rating');

    }

    function editRating () {
        $id = $this->input->post("id");
        $kode = $this->input->post("kode");
        $rating = $this->input->post("rating");
        $status = $this->input->post("status");
        $url = "/vas-rating/$id";
        $data = array(
            "kode" => $kode,
            "value" => round(($rating / 100 ), 4),
            "status" => $status,
        );

        $result = $this->Server->PUT($url, $data );

        if ($result->status == '1' || $result->status == 1) {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }
        redirect('Vasicek/rating');

    }

    function bank () {
        $bank = $this->Server->GET("vas-bank");
        $grup_bank = [];
        $grup_name =  [];
        $grup_akronim =  [];
        if ($bank->status == '1') {
            foreach ($bank->data as $key => $dt) {
                $ada = false;
                for ($i=0; $i < count($grup_name) ; $i++) { 
                    if ($dt->nama_bank == $grup_name[$i]) {
                        $ada = true; break;
                    }
                }

                if ($ada) {
                    continue;
                } else {
                    $grup_bank [] = $dt;
                    $grup_name [] = $dt->nama_bank;
                    $grup_akronim [] = $dt->akronim;
                }
            }
        }

        $session = array("bank" => $bank->data);
        $this->session->set_userdata($session);
    
        $data['grup_bank'] = $grup_bank;
        $data['grup_name'] = $grup_name;
        $data['grup_akronim'] = $grup_akronim;
        $data['data'] = $bank;
        $data['rating'] = $this->Server->GET("vas-rating");
        $plugin['plugin'] = "datatable_anggota";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/vasicek/bank", $data);
        $this->load->view("template/footer", $plugin);
    }

    function getFilterBank(){
        $bank = [];
        $nama = $this->input->post('nama_bank');
        $data = $this->session->userdata('bank');
        foreach ($data as $key => $dt) {
            if ($dt->nama_bank == $nama) {
                $bank[] = $dt;
            }
        }
        echo json_encode($bank);
    }

    function createBank () {
        $nama = $this->input->post("nama");
        $akronim = $this->input->post("akronim");
        $rating = $this->input->post("rating");
        $tahun = $this->input->post("tahun");
        $referensi = $this->input->post("referensi");
        $url = "/vas-bank";
        $data = array(
            "nama" => $nama,
            "akronim" => $akronim,
            "tahun" => $tahun,
            "id_rating" => $rating,
            "referensi" => $referensi
        );

        $result = $this->Server->POST($url, $data );


        if ($result->status == '1' || $result->status == 1) {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }
        redirect('Vasicek/bank');

    }

    function editBank () {
        $id = $this->input->post("id");
        $nama = $this->input->post("nama");
        $akronim = $this->input->post("akronim");
        $tahun = $this->input->post("tahun");
        $rating = $this->input->post("rating");
        $referensi = $this->input->post("referensi");
        $url = "/vas-bank/$id";
        $data = array(
            "nama" => $nama,
            "akronim" => $akronim,
            "tahun" => $tahun,
            "id_rating" => $rating,
            "referensi" => $referensi
        );

        $result = $this->Server->PUT($url, $data );

        if ($result->status == '1' || $result->status == 1) {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }
        redirect('Vasicek/bank');

    }

    function aset () {
        $data['data'] = $this->Server->GET("vas-aset");
        $plugin['plugin'] = "datatable_anggota";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/vasicek/aset", $data);
        $this->load->view("template/footer", $plugin);
    }

    function list_aset () {

        $draft = $this->input->post("draft");
        $irb = base64_decode($this->input->get('idb'));

        if ($irb != null && $irb != '') {
            $id_draft = $irb;
        } else if ( $draft == '' || $draft == '0' ) {
            $id_draft = '0';
        } else {
            $id_draft = $draft;
        }

        $bank = $this->Server->GET("vas-bank");
        $grup_bank = [];
        $grup_name =  [];
        if ($bank->status == '1') {
            foreach ($bank->data as $key => $dt) {
                $ada = false;
                for ($i=0; $i < count($grup_name) ; $i++) { 
                    if ($dt->nama_bank == $grup_name[$i]) {
                        $ada = true; break;
                    }
                }

                if ($ada) {
                    continue;
                } else {
                    $grup_bank [] = $dt;
                    $grup_name [] = $dt->nama_bank;
                }
            }
        }

        $session = array("bank" => $bank->data);
        $this->session->set_userdata($session);

        $data['data'] = $this->Server->GET("aset-list/draft/$id_draft");
        $data['jenis'] = $this->Server->GET("vas-aset");
        $data['bank'] = $bank;
        $data['grup_bank'] = $grup_bank;
        $data['draft'] = $this->Server->GET("vasicek/draft");
        $data['id_draft'] = $id_draft;
        
        $plugin['plugin'] = "datatable_anggota";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/vasicek/list_aset", $data);
        $this->load->view("template/footer", $plugin);
    }

    function createAset () {
        $nama = $this->input->post("nama");
        $url = "/vas-aset";
        $data = array(
            "nama" => $nama,
        );

        $result = $this->Server->POST($url, $data );


        if ($result->status == '1' || $result->status == 1) {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }
        redirect('Vasicek/aset');

    }

    function editAset () {
        $id = $this->input->post("id");
        $nama = $this->input->post("nama");
        $url = "/vas-aset/$id";
        $data = array(
            "nama" => $nama,
        );

        $result = $this->Server->PUT($url, $data );
        if ($result->status == '1' || $result->status == 1) {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }
        redirect('Vasicek/aset');
    }

    function deleteAset ($ids) {
        $url = "/vas-aset/$ids";
        $result = $this->Server->DELETE($url);
        $this->session->set_flashdata("message", $result->message);
        redirect('Vasicek/aset');
    }

    function addListaset () {
        $tipe = $this->input->post("tipe");
        if ( $tipe == '0' ) {
            $draft = $this->input->post("drafts");
            $id_jenis = $this->input->post("add_jenis");
            $id_bank = $this->input->post("add_bank");
            $no_rekening = $this->input->post("add_rekening");
            $mata_uang = $this->input->post("add_mata_uang");
            $suku_bunga = $this->input->post("add_suku_bunga");
            $tgl_jatuh_tempo = $this->input->post("add_tgl_jatuh");
            $saldo = $this->input->post("add_saldo");
            $url = "/aset-list/$id";
            $data = array(
                "id_draft" => $draft,
                "id_jenis" => $id_jenis,
                "id_bank" => $id_bank,
                "no_rekening" => $no_rekening,
                "mata_uang" => $mata_uang,
                "suku_bunga" => $suku_bunga,
                "tgl_jatuh_tempo" => $tgl_jatuh_tempo,
                "saldo" => $saldo
            );
            $result = $this->Server->POST($url, $data );
            $this->session->set_flashdata("message", $result->message);
            redirect('Vasicek/list_aset');
        } else {
            
            $tmpFile = $_FILES['file_b']['tmp_name'];
            $typeFile = $_FILES['file_b']['type'];
            $nameFile = $_FILES['file_b']['name']; 
            $id_jenis = $this->input->post("add_jenis");
            $id_bank = $this->input->post("add_bank");
            $draft = $this->input->post("drafts");

            $url = 'aset-list/upload';
            $data = array('file'=> new CURLFile($tmpFile,$typeFile,$nameFile),'id_jenis' => $id_jenis, 'id_bank' => $id_bank,
            'id_draft' => $draft);
            $result = $this->Server->UPLOAD($url, $data );
            
            // echo json_encode($result); exit;
        
            if ($result->status == '1' || $result->status == 1) {
                $this->session->set_flashdata("message", $result->message);
                $plugin['plugin'] = "datatable_anggota";
                $data['data'] = $result;

                $this->load->view("template/header");
                $this->load->view("template/nav", $data);
                $this->load->view("pages/upload/report-aset", $data);
                $this->load->view("template/footer", $plugin);
            } else {
                $this->session->set_flashdata("err-message", $result->message);
                redirect('Vasicek/list_aset');
            }
        } 
    }

    function editListAset () {
        $id = $this->input->post("edit_idaset");
        $id_jenis = $this->input->post("edit_jenis");
        $id_bank = $this->input->post("edit_bank");
        $no_rekening = $this->input->post("edit_rekening");
        $mata_uang = $this->input->post("edit_mata_uang");
        $suku_bunga = $this->input->post("edit_suku_bunga");
        $tgl_jatuh_tempo = $this->input->post("edit_tgl_jatuh");
        $saldo = $this->input->post("edit_saldo");
        $url = "/aset-list/$id";
        $data = array(
            "id_jenis" => $id_jenis,
            "id_bank" => $id_bank,
            "no_rekening" => $no_rekening,
            "mata_uang" => $mata_uang,
            "suku_bunga" => $suku_bunga,
            "tgl_jatuh_tempo" => $tgl_jatuh_tempo,
            "saldo" => $saldo
        );
        $result = $this->Server->PUT($url, $data );
        if ($result->status == '1' || $result->status == 1) {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }
        redirect('Vasicek/list_aset');
    }

    function listAset_del ($ids) {
        $url = "aset-list/$ids";
        $result = $this->Server->DELETE($url);
        $this->session->set_flashdata("message", $result->message);
        redirect('Vasicek/list_aset');
    }

    function simulate () {
        $data['draft'] = $this->Server->GET("vasicek/draft");
        $data['data'] = $this->Server->GET("vas-bank");
        $data['rating'] = $this->Server->GET("vas-rating");
        $plugin['plugin'] = "datatable_anggota";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/vasicek/simulate", $data);
        $this->load->view("template/footer", $plugin);
    }

    function getAsetAll(){
        $data = $this->Server->GET("vas-aset");
        $data = json_encode($data);
        echo $data;
    }

    function getBankAll(){
        $data = $this->Server->GET("vas-bank");
        $data = json_encode($data);
        echo $data;
    }

    function getData () {
        $data = $this->input->post("data");
        $data = json_encode($data);
        $token = 'Bearer '. $this->session->userdata("authtoken");
        $host = $this->config->item('api_host');

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "$host/vasicek/simulate",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS =>$data,
        CURLOPT_HTTPHEADER => array(
            "Authorization: $token",
            "Content-Type: application/json"
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
        // echo $data;
    }

    function getDataTes () {
        $data = $this->input->post("data");
        $data = json_encode($data);
        $curl = curl_init();
        $token = 'Bearer '. $this->session->userdata("authtoken");
        $host = $this->config->item('api_host');

        curl_setopt_array($curl, array(
        // CURLOPT_URL => "$host/vasicek/simulate",
        CURLOPT_URL => "$host/vasicek/simulate-aset",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS =>$data,
        CURLOPT_HTTPHEADER => array(
            "Authorization: $token",
            "Content-Type: application/json"
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $result = json_decode($response);
       

        // echo json_encode($result);exit;


        if ( $result->status == 1 ) {
            $result = json_decode($response);
            $persen = $this->getPersen($result);
            $micro = $this->getMicroeconomi($result);
            $history = $this->getHistory($result);
            $pd = $this->getPD($result);
            $kertas = $this->getKertas($result);
            $summary = $this->getECL($result);
            $prd = $result->data->periode_ecl;
            $periode = $result->data->periode_ecl;
        
            $periode = "Saldo per " . $periode . " dalam IDR";
            $ecl1 = "Saldo per " . $prd . " dalam IDR";
            $ecl2 = "Hasil ECL PSAK 71 per " . $prd . " (Normal Scenario)";
            $ecl3 = "Hasil ECL PSAK 71 per " . $prd . " (Weighted Scenario)";
            $status = $result->status;
            // $periode = date("F j, Y", strtotime());
        

            $data = array(
                "dataPersen" => $persen,
                "dataMicro" => $micro,
                "dataHistory" => $history,
                "dataPD" => $pd,
                "dataPeriode" => $periode,
                "dataECL1" => $ecl1,
                "dataECL2" => $ecl2,
                "dataECL3" => $ecl3,
                "dataKertas" => $kertas,
                "dataECL" => $summary,
                "status" => $status,
            );
        } else {
            $status = $result->status;
            $data = array(
                "status" => $status,
                "message" => $result->message
            );
        }

        $data = json_encode($data);
        echo $data;
    }

    function ecl_createDraft () {
        $created_by = $this->session->userdata('ids');
        $draft = $this->input->post("draft");
        $interval = $this->input->post("interval");
        $start = $this->input->post("start");
        $end = $this->input->post("end");
        $persen_normal = $this->input->post("normal");
        $persen_upturn = $this->input->post("upturn");
        $persen_downturn = $this->input->post("downturn");
        $lgd = $this->input->post("lgd");
        $periode_ecl = $this->input->post("periode_ecl");

        $list_aset_ecl = $this->input->post("list_aset_ecl");

        $url = "/vasicek/simulate-aset";
        $data = array(
            "id_vasicek_draft" => $draft,
            "interval" => $interval,
            "start_date" => $start,
            "end_date" => $end,
            "persen_normal" => $persen_normal,
            "persen_downturn" => $persen_downturn,
            "persen_upturn" => $persen_upturn,
            "list_aset_ecl" => $list_aset_ecl,
            "lgd" => $lgd,
            "periode_ecl" => $periode_ecl,
            "save" => "1",
            "created_by" => $created_by
        );

        $result = $this->Server->POST($url, $data );

        if ($result->status == '1' || $result->status == 1) {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }
        redirect('Vasicek/draft_ecl');

    }

    function draft_ecl () {
        
        $data['data'] = $this->Server->GET("vasicek/draft-ecl");
        $plugin['plugin'] = "datatable_anggota";
        
        // if ($data['data']->status == '1') {
        //     $session = array("vasicek_draft_ecl" => $data['data']->data);
        //     $this->session->set_userdata($session);
        // }
        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/vasicek/draft_ecl", $data);
        $this->load->view("template/footer", $plugin);
    }

    function getDraftECL(){
        $id = $this->input->post('id');
        // $drafts = $this->session->userdata('draft_ecl');
        // $data = null;
        // foreach ($drafts as $draft) {
        //     if($draft->id_draft_ecl == $id){
        //         $data = $draft;
        //         break;
        //     } 
        // }
        $data = $this->Server->GET("vasicek/draft-ecl/$id");
        if ($data != null && $data->status == 1) {
            $data = $data->data;
        }
        echo json_encode($data);
    }

    function updateDraftECL(){
        $id_draft_ecl = $this->input->post("id_draft_ecl");
        $result_flag_model = $this->input->post("result_flag_model");
        $result_saldo_exiting = $this->input->post("result_saldo_exiting");

        $url = "vasicek/draft-ecl/$id_draft_ecl";
        $data = array(
            "result_flag_model" => $result_flag_model,
            "result_saldo_exiting" => $result_saldo_exiting,
        );

        $result = $this->Server->PUT($url, $data );
        echo json_encode($result);
    }

    public function draft_ecl_delete ($ids) {
        $url = "vasicek/draft-ecl/$ids";
        $result = $this->Server->DELETE($url);
        if ($result->status == '1') {
            $this->session->set_flashdata("message", $result->message);
        } else {
            $this->session->set_flashdata("err-message", $result->message);
        }
        redirect('vasicek/draft');
    }

    function view_draft_ecl () {

        $irb = base64_decode($this->input->get('idb'));

        $data['data'] = $this->Server->GET("vasicek/draft-ecl/$irb");
        $plugin['plugin'] = "datatable_anggota";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/vasicek/view_draft_ecl", $data);
        $this->load->view("template/footer", $plugin);
    }

    function getPersen ( $result ) {

        if ( $result->status == 1 ) {
            $thead = "<thead>"
            ."<tr>"
            ."<td class='all' width=800px > Skenario Normal </td>"
            ."<td class='all' width=800px>" . $result->data->persen_normal * 100 . " %</td>"
            ."</tr>"
            ." </thead>";
    
            $tbody =  "<tbody>"
            ."<tr>"
            ."<td> Skenario Optimis </td>"
            ."<td>" . $result->data->persen_uptum * 100  . " %</td>"
            ."</tr>"
            ."<tr>"
            ."<td> Skenario Pesimis </td>"
            ."<td>" . $result->data->persen_downtum * 100 . " %</td>"
            ."</tr>"
            ." </tbody>";
        } else {
            $thead = "<thead>"
            ."<tr>"
            ."<th class='all' width=800px> Normal </th>"
            ."<th class='all' width=800px></th>"
            ."</tr>"
            ." </thead>";
    
            $tbody =  "<tbody>"
            ."<tr>"
            ."<td> Up Turn </td>"
            ."<td></td>"
            ."</tr>"
            ."<tr>"
            ."<td> Down Turn </td>"
            ."<td></td>"
            ."</tr>"
            ." </tbody>";
        }

        return $thead . $tbody;
    }

    function getMicroeconomi ( $result ) {
        if ( $result->status == 1 ) {
            $thead = "";
            $TH = "";
            $tbody = "";
            $TD1 = "<td>Year</td>";
            $TD2 = "";
            $projection = $result->data->list_flg_projection;
            $i = 2;
            foreach ( $projection as $p ) {

                if ( $p == 1 ) {
                    $th = "<th class='all' width=200px>"
                    . "Proyeksi"
                    ."</th>";
                } else {
                    $th = "<th class='all' width=200px>"."</th>";
                }

                $TH .= $th;
               
            }

            $head = $result->data->header_macroeconomic;

            if ( $head == null ) {
                $thead = "<thead><tr><th class='all' width=800px>History</th><th class='all' width=800px ></th>/tr></thead>";
                $tbody = "<tbody><tr><td>Kosong</td><td>Kosong</td>/tr></tbody>";
            } else {
                foreach ( $head as $h ) {
                    $TD1 .= "<td>".$h."</td>";
    
                }
    
                $body = $result->data->list_macroeconomic;
                foreach ( $body as $b ) {
                    $td1 = "<td>".$b->title."</td>";
                    $td2 = "";
                    foreach ( $b->nilai as $n ) {
                        $td2 .= "<td>".round($n, 5)."</td>";
                    }
    
                    $TD2 .= "<tr>"
                    .$td1
                    .$td2
                    ."</tr>";
                }
    
    
                $thead = "<thead><tr><th></th>".$TH."</tr></thead>";
                $tbody ="<tbody><tr>"
                .$TD1
                ."</tr>"
                .$TD2
                ."</tbody>";
            }

        } else {
            $thead = "<thead><tr><th class='all' width=800px>History</th><th class='all' width=800px ></th>/tr></thead>";
            $tbody = "<tbody><tr><td>Kosong</td><td>Kosong</td>/tr></tbody>";
        }

        return $thead . $tbody;
    }

    function getHistory ( $result ) {

        if ( $result->status == 1 ) {
            $thead = "<thead>"
            ."<tr>"
            ."<td class='all' width=800px > History Mean </td>"
            ."<td class='all' width=800px>" . round($result->data->historical_mean, 10) . "</td>"
            ."</tr>"
            ." </thead>";
    
            $tbody =  "<tbody>"
            ."<tr>"
            ."<td> History STDEV </td>"
            ."<td>" . round($result->data->historical_stdev, 10)  . "</td>"
            ."</tr>"
            ."<tr>"
            ."<td> Confidence Level </td>"
            ."<td>" . round($result->data->confidence_level, 10)  . " %</td>"
            ."</tr>"
            ."<tr>"
            ."<td> T Distribution </td>"
            ."<td>" . round($result->data->t_distribution, 10)  . "</td>"
            ."</tr>"
            ." </tbody>";
        } else {
            $thead = "<thead>"
            ."<tr>"
            ."<th class='all' width=800px > History Mean </th>"
            ."<th class='all' width=800px></th>"
            ."</tr>"
            ." </thead>";
    
            $tbody =  "<tbody>"
            ."<tr>"
            ."<td> History STDEV </td>"
            ."<td></td>"
            ."</tr>"
            ."<tr>"
            ."<td> Confidence Level </td>"
            ."<td></td>"
            ."</tr>"
            ."<tr>"
            ."<td> T Distribution </td>"
            ."<td></td>"
            ."</tr>"
            ." </tbody>";
        }

        return $thead . $tbody;
    }

    function getPD ( $result ) {
        $TD1 = "<tr>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>(Normal Scenario)</td>
        <td>(Weighted Scenario)</td>
        </tr>";
        if ( $result->status == 1 ) {
           

            $TD2 = "";

            $pd = $result->data->list_pd_vasicek;

            if ( $pd == null ) {

                $TD2 = "<tr><
                td>-</td>
                td>-</td>
                td>-</td>
                td>-</td>
                td>-</td>
                </tr>";
            } else {
                foreach( $pd as $p ) {
                    $TD2 .= "<tr>"
                    ."<td>".$p->rating->kode_rating."</td>"
                    ."<td>". round(($p->value_rating * 100), 2)." %</td>"
                    ."<td>". round(($p->value_base_formula * 100), 2)." %</td>"
                    ."<td>". round(($p->vasicek_normal * 100), 2) ." %</td>"
                    // ."<td>". round(($p->vasicek_adjusted_normal * 100), 2) ." %</td>"
                    ."<td>". round(($p->vasicek_weighted * 100), 2) ." %</td>"
                    // ."<td>". round(($p->vasicek_adjusted_weighted * 100), 2) ." %</td>"
                    ."</tr>";
                }
            }
            
        } else {
           
            $TD2 = "<tr><
            td>-</td>
            td>-</td>
            td>-</td>
            td>-</td>
            td>-</td>
            </tr>";
        }

        return $TD1 . $TD2;
    }

    function getKertas ( $result ) {
        if ( $result->status == 1 ) {
           

            $TD2 = "";

            $ecl = $result->data->list_aset;

            if ( $ecl == null ) {
                $TD2 = "<tr><
                td>-</td>
                td>-</td>
                td>-</td>
                td>-</td>
                td>-</td>
                td>-</td>
                td>-</td>
                td>-</td>
                td>-</td>
                td>-</td>
                td>-</td>
                td>-</td>
                td>-</td>
                td>-</td>
                </tr>";
            } else {
                foreach( $ecl as $e ) {

                    if ( $e->id_aset == 0 ) {
                        $TD2 .= "<tr>"
                        ."<td>-</td>"
                        ."<td>-</td>"
                        ."<td>-</td>"
                        ."<td>-</td>"
                        ."<td>-</td>"
                        ."<td>-</td>"
                        ."<td>-</td>"
                        ."<td>-</td>"
                        ."<td>-</td>"
                        ."<td>-</td>"
                        ."<td>-</td>"
                        ."<td>Jumlah</td>"
                        ."<td>". "Rp. " . number_format( $e->ecl_normal_scan, 2, ',', '.')."</td>"
                        ."<td>"."Rp. " . number_format( $e->ecl_weighted_scan, 2, ',', '.')."</td>"
                        ."</tr>";
                    } else {
                        $TD2 .= "<tr>"
                        ."<td>".$e->aset->nama_aset."</td>"
                        ."<td>".$e->no_rekening."</td>"
                        ."<td>".$e->mata_uang."</td>"
                        ."<td>".$e->bank->nama_bank."</td>"
                        ."<td>".$e->bank->akronim."</td>"
                        ."<td>".$e->bank->rating->kode_rating."</td>"
                        ."<td>". round(($e->suku_bunga * 100), 2) ." %</td>"
                        ."<td>". date("d/M/Y", strtotime($e->tgl_jatuh_tempo))."</td>"
                        ."<td>"."Rp. " . number_format( $e->saldo, 2, ',', '.')."</td>"
                        ."<td>". round(($e->pd_normal_scan * 100), 2)." %</td>"
                        ."<td>". round(($e->pd_weighted_scan * 100), 2)." %</td>"
                        ."<td>". round(($e->lgd * 100), 2)." %</td>"
                        ."<td>"."Rp. " . number_format( $e->ecl_normal_scan, 2, ',', '.')."</td>"
                        ."<td>"."Rp. " . number_format( $e->ecl_weighted_scan, 2, ',', '.')."</td>"
                        ."</tr>";
                    }
                }
            }

          
        } else {
           
            $TD2 = "<tr><
            td>-</td>
            td>-</td>
            td>-</td>
            td>-</td>
            td>-</td>
            td>-</td>
            td>-</td>
            td>-</td>
            td>-</td>
            td>-</td>
            td>-</td>
            td>-</td>
            td>-</td>
            td>-</td>
            </tr>";
        }

        return $TD2;
    }

    function getECL ( $result ) {
        if ( $result->status == 1 ) {
           

            $TD2 = "";

            $ecl = $result->data->list_summary;

            if ( $ecl == null ) {
                $TD2 = "<tr><
                td>-</td>
                td>-</td>
                td>-</td>
                td>-</td>
                </tr>";
            } else {
                foreach( $ecl as $e ) {

                    if ( $e->id_aset == 0 ) {
                        $TD2 .= "<tr>"
                        ."<td>Jumlah</td>"
                        ."<td>"."Rp. " . number_format( $e->saldo, 2, ' ,', '.')."</td>"
                        ."<td>"."Rp. " . number_format( $e->ecl_normal_scan, 2, ',', '.')."</td>"
                        ."<td>"."Rp. " . number_format( $e->ecl_weighted_scan, 2, ',', '.')."</td>"
                        ."</tr>";
                    } else {
                        $TD2 .= "<tr>"
                        ."<td>".$e->aset->nama_aset."</td>"
                        ."<td>"."Rp. " . number_format( $e->saldo, 2, ' ,', '.')."</td>"
                        ."<td>"."Rp. " . number_format( $e->ecl_normal_scan, 2, ',', '.')."</td>"
                        ."<td>"."Rp. " . number_format( $e->ecl_weighted_scan, 2, ',', '.')."</td>"
                        ."</tr>";
                    }
                }
            }
        } else {
           
            $TD2 = "<tr><
            td>-</td>
            td>-</td>
            td>-</td>
            td>-</td>
            </tr>";
        }

        return $TD2;
    }

}