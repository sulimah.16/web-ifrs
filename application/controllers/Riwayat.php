<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Riwayat extends CI_Controller
{
    function __Construct() {
        parent::__Construct();
        $this->load->model("Server");
    }

    function index() {

        $role =  $this->session->userdata('role');
        $id =  $this->session->userdata('ids');

        if ( $role == '1' ) {
            $type_role = 'admin';
        } else {
            $type_role = 'guest';
        }

        $start = $this->input->post("start");
        $end = $this->input->post("end");

        if ( $start == "" ) {
            $start_date = date('01/m/Y');
        } else {
            $start_date = $start;
        }

        
        if ( $end == "" ) {
            $end_date = date('t/m/Y');
        } else {
            $end_date = $end;
        }

        $url = "/activity/search";
        $datas = array(
            "type_role" => $type_role,
            "id" => $id,
            "start_date" => $start_date,
            "end_date" => $end_date
        );

        $result = $this->Server->POST($url, $datas );

        if ( $result->data == NULL ) {
                $results = array();
        } else {
            $results = $result->data;
        }

        $status = $result->status;
        $message = $result->message;
        
        $ids = $this->session->userdata('ids');
        $data['data'] = $results;
        $data['status'] = $status;
        $data['message'] = $message;
        $plugin['plugin'] = "";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/riwayat/riwayat", $data);
        $this->load->view("template/footer", $plugin);
    }

}