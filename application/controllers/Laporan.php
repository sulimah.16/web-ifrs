<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller
{
    function __Construct() {
        parent::__Construct();
        $this->load->model("Server");
    }

    function ar () {
        $draft = $this->input->post("draft");
        $segment = $this->input->post("segment");
        $first = $this->input->post("first");
        $interval = $this->input->post("interval");
        $bucket = $this->input->post("bucket");
        $start = $this->input->post("start");
        $end = $this->input->post("end");

        if ( $draft == '' ) {
            $id_draft = "0";
            $id_segment = "2";
            $hari_first_bucket = "12";
            $interval = "3";
            $jumlah_bucket = "10";
            $start_date = date('01/m/Y');
            $end_date = date('t/m/Y');
        } else {

            $arr = explode("|",$draft);

            $id_draft = $arr[0];
            $flag_draft = $arr[1];

            if ( $flag_draft == '1' || $flag_draft == 1 ) {
                $id_draft = $id_draft;
                $id_segment = $segment;
                $hari_first_bucket = $first;
                $interval = $interval;
                $jumlah_bucket = $bucket;
                $start_date = $start ;
                $end_date = $end;
            } else {
                $id_draft = $id_draft;
                $id_segment = $segment;
                $hari_first_bucket = "";
                $interval = "";
                $jumlah_bucket = "";
                $start_date = $start ;
                $end_date = $end;
            }
        }
       
        $plugin['plugin'] = "datatable_assets";

        $url = "/report/get-araging";
        $data = array(
            "id_draft" => $id_draft,
            "id_segment" => $id_segment,
            "hari_first_bucket" => $hari_first_bucket,
            "interval" => $interval,
            "jumlah_bucket" => $jumlah_bucket,
            "start_date" => $start_date,
            "end_date" => $end_date
        );


        $data['data'] = $this->Server->POST($url, $data );
        $data['draft'] = $this->Server->GET("draft");
        $data['segment'] = $this->Server->GET("segment");
        $plugin = "";
       
        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/laporan/ar", $data);
        $this->load->view("template/footer", $plugin);
    }

    function rolerate () {

        $draft = $this->input->post("draft");
        $segment = $this->input->post("segment");
        $first = $this->input->post("first");
        $interval = $this->input->post("interval");
        $bucket = $this->input->post("bucket");
        $start = $this->input->post("start");
        $end = $this->input->post("end");


      

        if ( $draft == '' ) {
            $id_draft = "0";
            $id_segment = "2";
            $hari_first_bucket = "12";
            $interval = "3";
            $jumlah_bucket = "10";
            $start_date = date('01/m/Y');
            $end_date = date('t/m/Y');
        } else {

            $arr = explode("|",$draft);

            $id_draft = $arr[0];
            $flag_draft = $arr[1];

            if ( $flag_draft == '1' || $flag_draft == 1 ) {
                $id_draft = $id_draft;
                $id_segment = $segment;
                $hari_first_bucket = $first;
                $interval = $interval;
                $jumlah_bucket = $bucket;
                $start_date = $start ;
                $end_date = $end;
            } else {
                $id_draft = $id_draft;
                $id_segment = $segment;
                $hari_first_bucket = "";
                $interval = "";
                $jumlah_bucket = "";
                $start_date = $start ;
                $end_date = $end;
            }
        }

        


       
        $plugin['plugin'] = "datatable_assets";

         
        $url = "/report/get-rollrate";
        $data = array(
            "id_draft" => $id_draft,
            "id_segment" => $id_segment,
            "hari_first_bucket" => $hari_first_bucket,
            "interval" => $interval,
            "jumlah_bucket" => $jumlah_bucket,
            "start_date" => $start_date,
            "end_date" => $end_date
        );


        $data['data'] = $this->Server->POST($url, $data );
        $data['draft'] = $this->Server->GET("draft");
        $data['segment'] = $this->Server->GET("segment");
        $plugin = "";

       
        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/laporan/rolerate", $data);
        $this->load->view("template/footer", $plugin);
    }

    function normalrollrate () {

        $draft = $this->input->post("draft");
        $segment = $this->input->post("segment");
        $first = $this->input->post("first");
        $interval = $this->input->post("interval");
        $bucket = $this->input->post("bucket");
        $start = $this->input->post("start");
        $end = $this->input->post("end");


      

        if ( $draft == '' ) {
            $id_draft = "0";
            $id_segment = "2";
            $hari_first_bucket = "12";
            $interval = "3";
            $jumlah_bucket = "10";
            $start_date = date('01/m/Y');
            $end_date = date('t/m/Y');
        } else {

            $arr = explode("|",$draft);

            $id_draft = $arr[0];
            $flag_draft = $arr[1];

            if ( $flag_draft == '1' || $flag_draft == 1 ) {
                $id_draft = $id_draft;
                $id_segment = $segment;
                $hari_first_bucket = $first;
                $interval = $interval;
                $jumlah_bucket = $bucket;
                $start_date = $start ;
                $end_date = $end;
            } else {
                $id_draft = $id_draft;
                $id_segment = $segment;
                $hari_first_bucket = "";
                $interval = "";
                $jumlah_bucket = "";
                $start_date = $start ;
                $end_date = $end;
            }
        }

        


       
        $plugin['plugin'] = "datatable_assets";

         
        $url = "/report/get-rollrate/normalisasi";
        $data = array(
            "id_draft" => $id_draft,
            "id_segment" => $id_segment,
            "hari_first_bucket" => $hari_first_bucket,
            "interval" => $interval,
            "jumlah_bucket" => $jumlah_bucket,
            "start_date" => $start_date,
            "end_date" => $end_date
        );


        $data['data'] = $this->Server->POST($url, $data );
        $data['draft'] = $this->Server->GET("draft");
        $data['segment'] = $this->Server->GET("segment");
        $plugin = "";

       
        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/laporan/normalisasi", $data);
        $this->load->view("template/footer", $plugin);
    }

    function normalaveragerollrate () {
        $draft = $this->input->post("draft");
        $segment = $this->input->post("segment");
        $first = $this->input->post("first");
        $interval = $this->input->post("interval");
        $bucket = $this->input->post("bucket");
        $start = $this->input->post("start");
        $end = $this->input->post("end");


      

        if ( $draft == '' ) {
            $id_draft = "0";
            $id_segment = "2";
            $hari_first_bucket = "12";
            $interval = "3";
            $jumlah_bucket = "10";
            $start_date = date('01/m/Y');
            $end_date = date('t/m/Y');
        } else {

            $arr = explode("|",$draft);

            $id_draft = $arr[0];
            $flag_draft = $arr[1];

            if ( $flag_draft == '1' || $flag_draft == 1 ) {
                $id_draft = $id_draft;
                $id_segment = $segment;
                $hari_first_bucket = $first;
                $interval = $interval;
                $jumlah_bucket = $bucket;
                $start_date = $start ;
                $end_date = $end;
            } else {
                $id_draft = $id_draft;
                $id_segment = $segment;
                $hari_first_bucket = "";
                $interval = "";
                $jumlah_bucket = "";
                $start_date = $start ;
                $end_date = $end;
            }
        }

        


       
        $plugin['plugin'] = "datatable_assets";

         
        $url = "/report/get-rollrate/normalisasi/average";
        $data = array(
            "id_draft" => $id_draft,
            "id_segment" => $id_segment,
            "hari_first_bucket" => $hari_first_bucket,
            "interval" => $interval,
            "jumlah_bucket" => $jumlah_bucket,
            "start_date" => $start_date,
            "end_date" => $end_date
        );


        $data['data'] = $this->Server->POST($url, $data );
        $data['draft'] = $this->Server->GET("draft");
        $data['segment'] = $this->Server->GET("segment");
        $plugin = "";

       
        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/laporan/normalisasiaverage", $data);
        $this->load->view("template/footer", $plugin);
    }

    function selectRollRate () {
        $role = $this->input->post("role");
        if ( $role == '2' ) {
            redirect('laporan/normalrollrate');
        } else if ( $role == '3') {
            redirect('laporan/normalaveragerollrate');
        } else {
            redirect('laporan/rolerate');
        }

    }


    function selectLossRate () {
        $role = $this->input->post("role");
        if ( $role == '1') {
            redirect('laporan/loserate');
        } else if ( $role == '2' ) {
            redirect('laporan/normalosserate');
        } else if ( $role == '3' ) {
            redirect('laporan/deltaosserate');
        } else {
            redirect('laporan/odrlosserate');
        }
    }

    function loserate () {
        $default = $this->input->post("default");
        $draft = $this->input->post("draft");
        $segment = $this->input->post("segment");
        $first = $this->input->post("first");
        $interval = $this->input->post("interval");
        $bucket = $this->input->post("bucket");
        $start = $this->input->post("start");
        $end = $this->input->post("end");

        if ( $draft == '' ) {
            $id_draft = "0";
            $id_segment = "2";
            $hari_first_bucket = "12";
            $interval = "3";
            $jumlah_bucket = "10";
            $start_date = "01/01/2019";
            $end_date = "30/12/2019";
        } else {

            $arr = explode("|",$draft);

            $id_draft = $arr[0];
            $flag_draft = $arr[1];

            if ( $flag_draft == '1' || $flag_draft == 1 ) {
                $id_draft = $id_draft;
                $id_segment = $segment;
                $hari_first_bucket = $first;
                $interval = $interval;
                $jumlah_bucket = $bucket;
                $start_date = $start ;
                $end_date = $end;
            } else {
                $id_draft = $id_draft;
                $id_segment = $segment;
                $hari_first_bucket = "";
                $interval = "";
                $jumlah_bucket = "";
                $start_date = $start ;
                $end_date = $end;
            }
        }

        $plugin['plugin'] = "datatable_assets";
        if ( $default == "" || $default == "1") {
            $url = "/report/get-lossrate/default";
        } else {
            $url = "/report/get-lossrate/average";
        }
         
        $data = array(
            "id_draft" => $id_draft,
            "id_segment" => $id_segment,
            "hari_first_bucket" => $hari_first_bucket,
            "interval" => $interval,
            "jumlah_bucket" => $jumlah_bucket,
            "start_date" => $start_date,
            "end_date" => $end_date
        );

        $data['data'] = $this->Server->POST($url, $data );
        $data['draft'] = $this->Server->GET("draft");
        $data['segment'] = $this->Server->GET("segment");
        $plugin = "";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/laporan/loserate", $data);
        $this->load->view("template/footer", $plugin);
    }

    function normalosserate () {
        $default = $this->input->post("default");
        $draft = $this->input->post("draft");
        $titik = $this->input->post("titik");
        $segment = $this->input->post("segment");
        $first = $this->input->post("first");
        $interval = $this->input->post("interval");
        $bucket = $this->input->post("bucket");
        $start = $this->input->post("start");
        $end = $this->input->post("end");

        if ( $draft == '' ) {
            $id_draft = "0";
            $titik = "3";
            $id_segment = "2";
            $hari_first_bucket = "12";
            $interval = "3";
            $jumlah_bucket = "10";
            $start_date = "01/01/2019";
            $end_date = "30/12/2019";
        } else {

            $arr = explode("|",$draft);
            $id_draft = $arr[0];
            $flag_draft = $arr[1];
            if ( $flag_draft == '1' || $flag_draft == 1 ) {
                $id_draft = $id_draft;
                $titik = $titik;
                $id_segment = $segment;
                $hari_first_bucket = $first;
                $interval = $interval;
                $jumlah_bucket = $bucket;
                $start_date = $start ;
                $end_date = $end;
            } else {
                $id_draft = $id_draft;
                $titik = $titik;
                $id_segment = $segment;
                $hari_first_bucket = "";
                $interval = "";
                $jumlah_bucket = "";
                $start_date = $start ;
                $end_date = $end;
            }           
        }
       
        $plugin['plugin'] = "datatable_assets";

        if ( $default == "" || $default == "1") {
            $url = "/report/get-lossrate/moving-average/default/$titik";
        } else {
            $url = "/report/get-lossrate/moving-average/average/$titik";
        }

        $data = array(
            "id_draft" => $id_draft,
            "id_segment" => $id_segment,
            "hari_first_bucket" => $hari_first_bucket,
            "interval" => $interval,
            "jumlah_bucket" => $jumlah_bucket,
            "start_date" => $start_date,
            "end_date" => $end_date
        );

        $data['data'] = $this->Server->POST($url, $data );
        $data['draft'] = $this->Server->GET("draft");
        $data['segment'] = $this->Server->GET("segment");
        $plugin = "";
       
        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/laporan/normalisasilossrate", $data);
        $this->load->view("template/footer", $plugin);
    }

    function deltaosserate () {
        $default = $this->input->post("default");
        $draft = $this->input->post("draft");
        $titik = $this->input->post("titik");
        $segment = $this->input->post("segment");
        $first = $this->input->post("first");
        $interval = $this->input->post("interval");
        $bucket = $this->input->post("bucket");
        $start = $this->input->post("start");
        $end = $this->input->post("end");

        if ( $draft == '' ) {
            $id_draft = "0";
            $titik = "3";
            $id_segment = "2";
            $hari_first_bucket = "12";
            $interval = "3";
            $jumlah_bucket = "10";
            $start_date = "01/01/2019";
            $end_date = "30/12/2019";
        } else {

            $arr = explode("|",$draft);
            $id_draft = $arr[0];
            $flag_draft = $arr[1];

            if ( $flag_draft == '1' || $flag_draft == 1 ) {
                $id_draft = $id_draft;
                $titik = $titik;
                $id_segment = $segment;
                $hari_first_bucket = $first;
                $interval = $interval;
                $jumlah_bucket = $bucket;
                $start_date = $start ;
                $end_date = $end;
            } else {
                $id_draft = $id_draft;
                $titik = $titik;
                $id_segment = $segment;
                $hari_first_bucket = "";
                $interval = "";
                $jumlah_bucket = "";
                $start_date = $start ;
                $end_date = $end;
            }
        }

        $plugin['plugin'] = "datatable_assets";
        if ( $default == "" || $default == "1") {
            $url = "/report/get-lossrate/delta-lossrate/default/$titik";
        } else {
            $url = "/report/get-lossrate/delta-lossrate/average/$titik";
        }
      
        $data = array(
            "id_draft" => $id_draft,
            "id_segment" => $id_segment,
            "hari_first_bucket" => $hari_first_bucket,
            "interval" => $interval,
            "jumlah_bucket" => $jumlah_bucket,
            "start_date" => $start_date,
            "end_date" => $end_date
        );

        $data['data'] = $this->Server->POST($url, $data );
        $data['draft'] = $this->Server->GET("draft");
        $data['segment'] = $this->Server->GET("segment");
        $plugin = "";
       
        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/laporan/agregasilossrate", $data);
        $this->load->view("template/footer", $plugin);
    }

    function odrlosserate () {
        $default = $this->input->post("default");
        $draft = $this->input->post("draft");
        $titik = $this->input->post("titik");
        $segment = $this->input->post("segment");
        $first = $this->input->post("first");
        $interval = $this->input->post("interval");
        $bucket = $this->input->post("bucket");
        $start = $this->input->post("start");
        $end = $this->input->post("end");

        if ( $draft == '' ) {
            $id_draft = "0";
            $titik = "3";
            $id_segment = "2";
            $hari_first_bucket = "12";
            $interval = "3";
            $jumlah_bucket = "10";
            $start_date = "01/01/2019";
            $end_date = "30/12/2019";
        } else {

            $arr = explode("|",$draft);
            $id_draft = $arr[0];
            $flag_draft = $arr[1];

            if ( $flag_draft == '1' || $flag_draft == 1 ) {
                $id_draft = $id_draft;
                $titik = $titik;
                $id_segment = $segment;
                $hari_first_bucket = $first;
                $interval = $interval;
                $jumlah_bucket = $bucket;
                $start_date = $start ;
                $end_date = $end;
            } else {
                $id_draft = $id_draft;
                $titik = $titik;
                $id_segment = $segment;
                $hari_first_bucket = "";
                $interval = "";
                $jumlah_bucket = "";
                $start_date = $start ;
                $end_date = $end;
            }
        }

        $plugin['plugin'] = "datatable_assets";
        if ( $default == "" || $default == "1") {
            $url = "/report/get-odr/default/$titik";
        } else {
            $url = "/report/get-odr/average/$titik";
        }
      
        $data = array(
            "id_draft" => $id_draft,
            "id_segment" => $id_segment,
            "hari_first_bucket" => $hari_first_bucket,
            "interval" => $interval,
            "jumlah_bucket" => $jumlah_bucket,
            "start_date" => $start_date,
            "end_date" => $end_date
        );

        $data['data'] = $this->Server->POST($url, $data );
        $data['draft'] = $this->Server->GET("draft");
        $data['segment'] = $this->Server->GET("segment");
        $plugin = "";

        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/laporan/agregasiodr", $data);
        $this->load->view("template/footer", $plugin);
    }

    function average () {

        $draft = $this->input->post("draft");
        $segment = $this->input->post("segment");
        $first = $this->input->post("first");
        $interval = $this->input->post("interval");
        $bucket = $this->input->post("bucket");
        $start = $this->input->post("start");
        $end = $this->input->post("end");

        if ( $draft == '' ) {
            $id_draft = "0";
            $id_segment = "2";
            $hari_first_bucket = "12";
            $interval = "3";
            $jumlah_bucket = "10";
            $start_date = date('01/m/Y');
            $end_date = date('t/m/Y');
        } else {

            $arr = explode("|",$draft);

            $id_draft = $arr[0];
            $flag_draft = $arr[1];

            if ( $flag_draft == '1' || $flag_draft == 1 ) {
                $id_draft = $id_draft;
                $id_segment = $segment;
                $hari_first_bucket = $first;
                $interval = $interval;
                $jumlah_bucket = $bucket;
                $start_date = $start ;
                $end_date = $end;
            } else {
                $id_draft = $id_draft;
                $id_segment = $segment;
                $hari_first_bucket = "";
                $interval = "";
                $jumlah_bucket = "";
                $start_date = $start ;
                $end_date = $end;
            }
        }


        


       
        $plugin['plugin'] = "datatable_assets";

         
        $url = "/report/get-rollrate/average";
        $data = array(
            "id_draft" => $id_draft,
            "id_segment" => $id_segment,
            "hari_first_bucket" => $hari_first_bucket,
            "interval" => $interval,
            "jumlah_bucket" => $jumlah_bucket,
            "start_date" => $start_date,
            "end_date" => $end_date
        );


        $data['data'] = $this->Server->POST($url, $data );
        $data['draft'] = $this->Server->GET("draft");
        $data['segment'] = $this->Server->GET("segment");
        $plugin = "";

       
        $this->load->view("template/header");
        $this->load->view("template/nav", $data);
        $this->load->view("pages/laporan/average", $data);
        $this->load->view("template/footer", $plugin);
    }

}