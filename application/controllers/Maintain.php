<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maintain extends CI_Controller
{
    function __Construct() {
        parent::__Construct();
        $this->load->model("Server");
    }

    function index () {
        $this->load->view("pages/maintain/maintain");
    }

}