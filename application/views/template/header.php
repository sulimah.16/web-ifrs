<!DOCTYPE html>
<html lang="en">

<head>
    <style>
    /* Absolute Center Spinner */
    .loading {
        position: fixed;
        z-index: 999;
        height: 2em;
        width: 2em;
        overflow: show;
        margin: auto;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
    }

    /* Transparent Overlay */
    .loading:before {
        content: '';
        display: block;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: radial-gradient(rgba(20, 20, 20, .8), rgba(0, 0, 0, .8));

        background: -webkit-radial-gradient(rgba(20, 20, 20, .8), rgba(0, 0, 0, .8));
    }

    /* :not(:required) hides these rules from IE9 and below */
    .loading:not(:required) {
        /* hide "loading..." text */
        font: 0/0 a;
        color: transparent;
        text-shadow: none;
        background-color: transparent;
        border: 0;
    }

    .loading:not(:required):after {
        content: '';
        display: block;
        font-size: 10px;
        width: 1em;
        height: 1em;
        margin-top: -0.5em;
        -webkit-animation: spinner 150ms infinite linear;
        -moz-animation: spinner 150ms infinite linear;
        -ms-animation: spinner 150ms infinite linear;
        -o-animation: spinner 150ms infinite linear;
        animation: spinner 150ms infinite linear;
        border-radius: 0.5em;
        -webkit-box-shadow: rgba(255, 255, 255, 0.75) 1.5em 0 0 0, rgba(255, 255, 255, 0.75) 1.1em 1.1em 0 0, rgba(255, 255, 255,
                0.75) 0 1.5em 0 0, rgba(255, 255, 255, 0.75) -1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) -1.5em 0 0 0, rgba(255, 255, 255,
                0.75) -1.1em -1.1em 0 0, rgba(255, 255, 255, 0.75) 0 -1.5em 0 0, rgba(255, 255, 255, 0.75) 1.1em -1.1em 0 0;
        box-shadow: rgba(255, 255, 255, 0.75) 1.5em 0 0 0, rgba(255, 255, 255, 0.75) 1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) 0 1.5em 0 0, rgba(255, 255, 255, 0.75) -1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) -1.5em 0 0 0, rgba(255, 255, 255, 0.75) -1.1em -1.1em 0 0, rgba(255, 255, 255, 0.75) 0 -1.5em 0 0, rgba(255, 255, 255, 0.75) 1.1em -1.1em 0 0;
    }

    /* Animation */

    @-webkit-keyframes spinner {
        0% {
            -webkit-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -ms-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @-moz-keyframes spinner {
        0% {
            -webkit-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -ms-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @-o-keyframes spinner {
        0% {
            -webkit-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -ms-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @keyframes spinner {
        0% {
            -webkit-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -ms-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    fieldset.border {
        border: 1px groove #ddd !important;
        padding: 0 1.4em 1.4em 1.4em !important;
        margin: 0 0 1.5em 0 !important;
        -webkit-box-shadow: 0px 0px 0px 0px #000;
        box-shadow: 0px 0px 0px 0px #000;
    }

    legend.border {
        font-size: 1.2em !important;
        font-weight: bold !important;
        text-align: left !important;
        width: auto;
        padding: 0 10px;
        border-bottom: none;
    }

    fieldset {
        min-width: 100%;
        background-color: #ddd;
        max-width: 500px;
        padding: 16px;
    }
    </style>
    <div class="loading" id="loading_screen">Loading&#8230;</div>

    <base href="">
    <meta charset="utf-8" />
    <!-- <title>Simply 9 Solution.Id</title> -->
    <title>Aplikasi PSAK 71</title>
    <meta name="description" content="Simply 9 Solution Dashboard">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
    <link href="<?php echo base_url(); ?>public/assets/plugins/custom/fullcalendar/fullcalendar.bundle.css"
        rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>public/assets/css/pages/invoices/invoice-1.css" rel="stylesheet"
        type="text/css" />
    <link href="<?php echo base_url(); ?>public/assets/plugins/global/plugins.bundle.css" rel="stylesheet"
        type="text/css" />
    <link href="<?php echo base_url(); ?>public/assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
    <link href="//www.amcharts.com/lib/3/plugins/export/export.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>public/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet"
        type="text/css" />
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/xy.js"></script>
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>


    <!-- INI UNTUK WIZARD -->
    <link href="<?php echo base_url(); ?>public/assets/css/pages/wizard/wizard-1.css" rel="stylesheet"
        type="text/css" />
    <link href="<?php echo base_url(); ?>public/assets/css/pages/wizard/wizard-2.css" rel="stylesheet"
        type="text/css" />

    <style>
    #chartdiv {
        width: 100%;
        height: 100%;
    }

    #chartdiv2 {
        width: 100%;
        height: 100%;
    }

    .kts-badge {
        margin-left: 5%;
        font-size: 8px;
        margin-bottom: 20%;

    }
    </style>

    <!-- Resources -->
    <!-- <script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/material.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script> -->

    <!-- <script src="//www.amcharts.com/lib/4/core.js"></script>
<script src="//www.amcharts.com/lib/4/charts.js"></script>
<script src="//www.amcharts.com/lib/4/themes/animated.js"></script> -->





    <!-- Chart code -->
    <script>
    // am4core.ready(function() {

    // // Themes begin
    // am4core.useTheme(am4themes_material);
    // am4core.useTheme(am4themes_animated);
    // // Themes end

    // // Create chart instances
    // var chart = am4core.create("chartdiv", am4charts.XYChart);

    // //

    // // Increase contrast by taking evey second color
    // chart.colors.step = 2;

    // console.log(JSON.stringify(generateChartData()));

    // // Add data
    // chart.data = [
    // {"date":"2019-12-16T17:00:00.000Z","visits":"20:00","hits":"08:00"},
    // {"date":"2019-12-17T17:00:00.000Z","visits":"21:00","hits":"09:00"},
    // {"date":"2019-12-18T17:00:00.000Z","visits":"20:00","hits":"08:00"},
    // {"date":"2019-12-19T17:00:00.000Z","visits":"21:00","hits":"09:00"},
    // {"date":"2019-12-23T17:00:00.000Z","visits":"20:00","hits":"08:00"}
    // ];

    // // Create axes
    // var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    // dateAxis.renderer.minGridDistance = 50;

    // // Create series
    // function createAxisAndSeries(field, name, opposite, bullet) {
    //   var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    //   if(chart.yAxes.indexOf(valueAxis) != 0){
    //   	valueAxis.syncWithAxis = chart.yAxes.getIndex(0);
    //   }

    //   var series = chart.series.push(new am4charts.LineSeries());
    //   series.dataFields.valueY = field;
    //   series.dataFields.dateX = "date";
    //   series.strokeWidth = 2;
    //   series.yAxis = valueAxis;
    //   series.name = name;
    //   series.tooltipText = "{name}: [bold]{valueY}[/]";
    //   series.tensionX = 0.8;
    //   series.showOnInit = true;


    //   var interfaceColors = new am4core.InterfaceColorSet();

    //   switch(bullet) {
    //     case "triangle":
    //       var bullet = series.bullets.push(new am4charts.Bullet());
    //       bullet.width = 12;
    //       bullet.height = 12;
    //       bullet.horizontalCenter = "middle";
    //       bullet.verticalCenter = "middle";

    //       var triangle = bullet.createChild(am4core.Triangle);
    //       triangle.stroke = interfaceColors.getFor("background");
    //       triangle.strokeWidth = 2;
    //       triangle.direction = "top";
    //       triangle.width = 12;
    //       triangle.height = 12;
    //       break;
    //     case "rectangle":
    //       var bullet = series.bullets.push(new am4charts.Bullet());
    //       bullet.width = 10;
    //       bullet.height = 10;
    //       bullet.horizontalCenter = "middle";
    //       bullet.verticalCenter = "middle";

    //       var rectangle = bullet.createChild(am4core.Rectangle);
    //       rectangle.stroke = interfaceColors.getFor("background");
    //       rectangle.strokeWidth = 2;
    //       rectangle.width = 10;
    //       rectangle.height = 10;
    //       break;
    //     default:
    //       var bullet = series.bullets.push(new am4charts.CircleBullet());
    //       bullet.circle.stroke = interfaceColors.getFor("background");
    //       bullet.circle.strokeWidth = 2;
    //       break;
    //   }

    //   valueAxis.renderer.line.strokeOpacity = 1;
    //   valueAxis.renderer.line.strokeWidth = 2;
    //   valueAxis.renderer.line.stroke = series.stroke;
    //   valueAxis.renderer.labels.template.fill = series.stroke;
    //   valueAxis.renderer.opposite = opposite;
    // }

    // createAxisAndSeries("visits", "Visits", false, "circle");
    // createAxisAndSeries("hits", "Hits", true, "rectangle");

    // // Add legend
    // chart.legend = new am4charts.Legend();

    // // Add cursor
    // chart.cursor = new am4charts.XYCursor();

    // // generate some random data, quite different range
    // function generateChartData() {
    //   var chartData = [];
    //   var firstDate = new Date();
    //   firstDate.setDate(firstDate.getDate() - 100);
    //   firstDate.setHours(0, 0, 0, 0);

    //   var visits = 1600;
    //   var hits = 2900;

    //   for (var i = 0; i < 15; i++) {
    //     // we create date objects here. In your data, you can have date strings
    //     // and then set format of your dates using chart.dataDateFormat property,
    //     // however when possible, use date objects, as this will speed up chart rendering.
    //     var newDate = new Date(firstDate);
    //     newDate.setDate(newDate.getDate() + i);

    //     visits += Math.round((Math.random()<0.5?1:-1)*Math.random()*10);
    //     hits += Math.round((Math.random()<0.5?1:-1)*Math.random()*10);

    //     chartData.push({
    //       date: newDate,
    //       visits: visits,
    //       hits: hits,
    //     });
    //   }
    //   return chartData;
    // }

    // }); // end am4core.ready()
    </script>



    <!-- <link rel="shortcut icon" href="<?php echo base_url(); ?>public/assets/teratur/teratur_favicon_2.ico" /> -->
</head>

<body
    class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--left kt-aside--fixed kt-page--loading">
    <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
        <div class="kt-header-mobile__logo">
            <a href="index.html">
                <img alt="Logo" src="<?php echo base_url(); ?>public/assets/teratur/Dash-60x60@3x.png" />
            </a>
        </div>
        <div class="kt-header-mobile__toolbar">
            <button class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left"
                id="kt_aside_mobile_toggler"><span></span></button>
            <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
            <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i
                    class="flaticon-more-1"></i></button>
        </div>
    </div>

    <div class="modal fade" id="warningmodal" tabindex="1" role="basic" aria-hidden="true">
        <div class="modal-dialog kt-ribbon kt-ribbon--dark kt-ribbon--ver kt-ribbon--shadow ">
            <div class="modal-content">
                <div class="kt-ribbon__target teratur-ribbon" style="top: -2px; right: 12px;">
                    <i class="fa flaticon2-bell-alarm-symbol"></i>
                </div>
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Berhasil</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body"> <?php echo $this->session->flashdata("message"); ?> </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    </div>


    <div class="modal fade" id="errmodal" tabindex="1" role="basic" aria-hidden="true">
        <div class="modal-dialog kt-ribbon kt-ribbon--dark kt-ribbon--ver kt-ribbon--shadow ">
            <div class="modal-content">
                <div class="kt-ribbon__target err-ribbon" style="top: -2px; right: 12px;">
                    <i class="fa flaticon2-bell-alarm-symbol"></i>
                </div>
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Peringatan !</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body"> <?php echo $this->session->flashdata("err-message"); ?> </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    </div>