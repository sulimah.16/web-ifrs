<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <!-- begin:: Header -->
            <div id="kt_header" class="kt-header  kt-header--fixed " data-ktheader-minimize="on">
                <div class="kt-container  kt-container--fluid ">

                    <!-- begin: Header Menu -->
                    <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i
                            class="la la-close"></i></button>
                    <div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_header_menu_wrapper">
                        <!-- end: Header Menu -->

                        <!-- begin:: Brand -->
                        <!-- <div class="kt-header__brand   kt-grid__item teratur-add-margin" id="kt_header_brand" >
                            <a class="kt-header__brand-logo" href="?page=index">
                                <img alt="Logo" src="<?php echo base_url(); ?>public/assets/teratur/teratur_3.png"
                                    height=40px />
                            </a>
                        </div> -->

                        <!-- end:: Brand -->

                        <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
                            <ul class="kt-menu__nav ">
                                <!-- <li class="kt-menu__item" aria-haspopup="true"><a href="<?php echo site_url("home"); ?>"
                                        class="kt-menu__link "><i
                                            class="kt-menu__link-icon flaticon-imac teratur-nav"></i><span
                                            class="kt-menu__link-text teratur-nav">Dashboard</span></a></li> -->



                                <li class="kt-menu__item" aria-haspopup="true"><a href="<?php echo site_url("home"); ?>"
                                        class="kt-menu__link "><i
                                            class="kt-menu__link-icon flaticon-home-1 teratur-nav"></i><span
                                            class="kt-menu__link-text teratur-nav">Beranda</span></a></li>

                                <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel"
                                    data-ktmenu-submenu-toggle="hover" aria-haspopup="true"><a href="javascript:;"
                                        class="kt-menu__link kt-menu__toggle teratur-nav"><i
                                            class="kt-menu__link-icon flaticon-layer teratur-nav"></i><span
                                            class="kt-menu__link-text teratur-nav">Data&nbspMaster</span><i
                                            class="kt-menu__ver-arrow la la-angle-right teratur-nav"></i></a>
                                    <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
                                        <ul class="kt-menu__subnav">

                                            <li class="kt-menu__item teratur-nav " aria-haspopup="true"><a
                                                    href="<?php echo site_url("input"); ?>"
                                                    class="kt-menu__link teratur-nav"><i
                                                        class="kt-menu__link-icon flaticon-download-1 teratur-nav"></i><span></span><span
                                                        class="kt-menu__link-text teratur-nav"> Input Data </span></a>
                                            </li>

                                            <li class="kt-menu__item teratur-nav" aria-haspopup="true"><a
                                                    href="<?php echo site_url("segment"); ?>"
                                                    class="kt-menu__link teratur-nav"><i
                                                        class="kt-menu__link-icon flaticon2-box teratur-nav"></i><span></span><span
                                                        class="kt-menu__link-text teratur-nav">Segmentasi</span></a>
                                            </li>

                                            <li class="kt-menu__item teratur-nav" aria-haspopup="true"><a
                                                    href="<?php echo site_url("draft"); ?>"
                                                    class="kt-menu__link teratur-nav"><i
                                                        class="kt-menu__link-icon flaticon-file-2 teratur-nav"></i><span></span><span
                                                        class="kt-menu__link-text teratur-nav">Draf Input
                                                        Data</span></a></li>

                                            <li class="kt-menu__item teratur-nav " aria-haspopup="true"><a
                                                    href="<?php echo site_url("laporan/ar"); ?>"
                                                    class="kt-menu__link teratur-nav"><i
                                                        class="kt-menu__link-icon flaticon-web teratur-nav"></i><span></span><span
                                                        class="kt-menu__link-text teratur-nav"> Struktur Data
                                                    </span></a></li>
                                            <li class="kt-menu__item teratur-nav " aria-haspopup="true"><a
                                                    href="<?php echo site_url("laporan/average"); ?>"
                                                    class="kt-menu__link teratur-nav"><i
                                                        class="kt-menu__link-icon flaticon-interface-6 teratur-nav"></i><span></span><span
                                                        class="kt-menu__link-text teratur-nav"> Average </span></a></li>
                                            <li class="kt-menu__item teratur-nav " aria-haspopup="true"><a
                                                    href="<?php echo site_url("laporan/rolerate"); ?>"
                                                    class="kt-menu__link teratur-nav"><i
                                                        class="kt-menu__link-icon flaticon-interface-7 teratur-nav"></i><span></span><span
                                                        class="kt-menu__link-text teratur-nav"> Roll Rate </span></a>
                                            </li>
                                            <!-- <li class="kt-menu__item teratur-nav " aria-haspopup="true"><a
                                                    href="<?php echo site_url("laporan/normalisasi"); ?>"
                                                    class="kt-menu__link teratur-nav"><i
                                                        class="kt-menu__link-icon flaticon2-percentage teratur-nav"></i><span></span><span
                                                        class="kt-menu__link-text teratur-nav"> Normalisasi </span></a></li> -->
                                            <li class="kt-menu__item teratur-nav " aria-haspopup="true"><a
                                                    href="<?php echo site_url("laporan/loserate"); ?>"
                                                    class="kt-menu__link teratur-nav"><i
                                                        class="kt-menu__link-icon flaticon-interface-8 teratur-nav"></i><span></span><span
                                                        class="kt-menu__link-text teratur-nav"> Loss Rate </span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>

                                <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel"
                                    data-ktmenu-submenu-toggle="hover" aria-haspopup="true"><a href="javascript:;"
                                        class="kt-menu__link kt-menu__toggle teratur-nav"><i
                                            class="kt-menu__link-icon flaticon-squares-4 teratur-nav"></i><span
                                            class="kt-menu__link-text teratur-nav">Data&nbspModel</span><i
                                            class="kt-menu__ver-arrow la la-angle-right teratur-nav"></i></a>
                                    <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
                                        <ul class="kt-menu__subnav">

                                            <li class="kt-menu__item teratur-nav " aria-haspopup="true"><a
                                                    href="<?php echo site_url("model/mev"); ?>"
                                                    class="kt-menu__link teratur-nav"><i
                                                        class="kt-menu__link-icon flaticon-list-2 teratur-nav"></i><span></span><span
                                                        class="kt-menu__link-text teratur-nav"> Rekap Mev </span></a>
                                            </li>

                                            <li class="kt-menu__item teratur-nav " aria-haspopup="true"><a
                                                    href="<?php echo site_url("model/fl"); ?>"
                                                    class="kt-menu__link teratur-nav"><i
                                                        class="kt-menu__link-icon flaticon-interface-10 teratur-nav"></i><span></span><span
                                                        class="kt-menu__link-text teratur-nav"> Rekap Model FL
                                                    </span></a></li>

                                            <li class="kt-menu__item teratur-nav " aria-haspopup="true"><a
                                                    href="<?php echo site_url("model/draft_bucket"); ?>"
                                                    class="kt-menu__link teratur-nav"><i
                                                        class="kt-menu__link-icon flaticon-list-1 teratur-nav"></i><span></span><span
                                                        class="kt-menu__link-text teratur-nav"> Model Bucket Simulate
                                                    </span></a></li>
                                        </ul>
                                    </div>
                                </li>

                                <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel"
                                    data-ktmenu-submenu-toggle="hover" aria-haspopup="true"><a href="javascript:;"
                                        class="kt-menu__link kt-menu__toggle teratur-nav"><i
                                            class="kt-menu__link-icon flaticon-interface-4 teratur-nav"></i><span
                                            class="kt-menu__link-text teratur-nav">ECL</span><i
                                            class="kt-menu__ver-arrow la la-angle-right teratur-nav"></i></a>
                                    <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
                                        <ul class="kt-menu__subnav">
                                            <?php if ( $this->session->userdata('role') == '1' || $this->session->userdata('role') == 1 || $this->session->userdata('role') == '2' || $this->session->userdata('role') == 2 ) { ?>
                                            <li class="kt-menu__item teratur-nav " aria-haspopup="true"><a
                                                    href="<?php echo site_url("ecl"); ?>"
                                                    class="kt-menu__link teratur-nav"><i
                                                        class="kt-menu__link-icon flaticon2-reload teratur-nav"></i><span></span><span
                                                        class="kt-menu__link-text teratur-nav"> Simulasi ECL </span></a>
                                            </li>
                                            <?php } ?>
                                            <li class="kt-menu__item teratur-nav " aria-haspopup="true"><a
                                                    href="<?php echo site_url("ecl/draft"); ?>"
                                                    class="kt-menu__link teratur-nav"><i
                                                        class="kt-menu__link-icon flaticon-interface-10 teratur-nav"></i><span></span><span
                                                        class="kt-menu__link-text teratur-nav"> Daftar ECL
                                                    </span></a></li>
                                        </ul>
                                    </div>
                                </li>

                                <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel"
                                    data-ktmenu-submenu-toggle="hover" aria-haspopup="true"><a href="javascript:;"
                                        class="kt-menu__link kt-menu__toggle teratur-nav"><i
                                            class="kt-menu__link-icon flaticon2-graphic-1 teratur-nav"></i><span
                                            class="kt-menu__link-text teratur-nav">Vasicek</span><i
                                            class="kt-menu__ver-arrow la la-angle-right teratur-nav"></i></a>
                                    <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
                                        <ul class="kt-menu__subnav">

                                            <li class="kt-menu__item teratur-nav " aria-haspopup="true"><a
                                                    href="<?php echo site_url("Vasicek/gdp"); ?>"
                                                    class="kt-menu__link teratur-nav"><i
                                                        class="kt-menu__link-icon flaticon-graphic-2 teratur-nav"></i><span></span><span
                                                        class="kt-menu__link-text teratur-nav"> GDP </span></a>
                                            </li>

                                            <li class="kt-menu__item teratur-nav " aria-haspopup="true"><a
                                                    href="<?php echo site_url("Vasicek/rating"); ?>"
                                                    class="kt-menu__link teratur-nav"><i
                                                        class="kt-menu__link-icon flaticon-star teratur-nav"></i><span></span><span
                                                        class="kt-menu__link-text teratur-nav"> PD Pefindo </span></a>
                                            </li>

                                            <li class="kt-menu__item teratur-nav " aria-haspopup="true"><a
                                                    href="<?php echo site_url("Vasicek/bank"); ?>"
                                                    class="kt-menu__link teratur-nav"><i
                                                        class="kt-menu__link-icon flaticon2-list teratur-nav"></i><span></span><span
                                                        class="kt-menu__link-text teratur-nav"> Daftar Bank
                                                    </span></a></li>

                                            <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel"
                                                data-ktmenu-submenu-toggle="hover" aria-haspopup="true"><a
                                                    href="javascript:;"
                                                    class="kt-menu__link kt-menu__toggle teratur-nav"><i
                                                        class="kt-menu__link-icon flaticon2-box-1 teratur-nav"></i><span
                                                        class="kt-menu__link-text teratur-nav">Asset</span><i
                                                        class="kt-menu__ver-arrow la la-angle-right teratur-nav"></i></a>
                                                <div
                                                    class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--right">
                                                    <ul class="kt-menu__subnav">
                                                        <li class="kt-menu__item teratur-nav" aria-haspopup="true"><a
                                                                href="<?php echo site_url("Vasicek/aset"); ?>"
                                                                class="kt-menu__link teratur-nav"><i
                                                                    class="kt-menu__link-icon flaticon-list teratur-nav"></i><span></span><span
                                                                    class="kt-menu__link-text teratur-nav">Jenis
                                                                    Aset</span></a>
                                                        </li>
                                                        <li class="kt-menu__item teratur-nav" aria-haspopup="true"><a
                                                                href="<?php echo site_url("Vasicek/list_aset"); ?>"
                                                                class="kt-menu__link teratur-nav"><i
                                                                    class="kt-menu__link-icon flaticon-list-2 teratur-nav"></i><span></span><span
                                                                    class="kt-menu__link-text teratur-nav">Daftar
                                                                    Aset</span></a>
                                                        </li>
                                                        <li class="kt-menu__item teratur-nav" aria-haspopup="true"><a
                                                                href="<?php echo site_url("Vasicek/draft"); ?>"
                                                                class="kt-menu__link teratur-nav"><i
                                                                    class="kt-menu__link-icon flaticon-file-2 teratur-nav"></i><span></span><span
                                                                    class="kt-menu__link-text teratur-nav">Draf Input
                                                                    Aset</span></a></li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel"
                                                data-ktmenu-submenu-toggle="hover" aria-haspopup="true"><a
                                                    href="javascript:;"
                                                    class="kt-menu__link kt-menu__toggle teratur-nav"><i
                                                        class="kt-menu__link-icon flaticon-interface-4  teratur-nav"></i><span
                                                        class="kt-menu__link-text teratur-nav">Simulasi Vasicek</span><i
                                                        class="kt-menu__ver-arrow la la-angle-right teratur-nav"></i></a>
                                                <div
                                                    class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--right">
                                                    <ul class="kt-menu__subnav">
                                                        <?php if ( $this->session->userdata('role') == '1' || $this->session->userdata('role') == 1 || $this->session->userdata('role') == '2' || $this->session->userdata('role') == 2 ) { ?>
                                                        <li class="kt-menu__item teratur-nav " aria-haspopup="true"><a
                                                                href="<?php echo site_url("Vasicek/simulate"); ?>"
                                                                class="kt-menu__link teratur-nav"><i
                                                                    class="kt-menu__link-icon flaticon2-refresh teratur-nav"></i><span></span><span
                                                                    class="kt-menu__link-text teratur-nav"> Simulasi
                                                                    Vasicek
                                                                </span></a></li>
                                                        <?php } ?>
                                                        <li class="kt-menu__item teratur-nav " aria-haspopup="true"><a
                                                                href="<?php echo site_url("Vasicek/draft_ecl"); ?>"
                                                                class="kt-menu__link teratur-nav"><i
                                                                    class="kt-menu__link-icon flaticon-interface-10 teratur-nav"></i><span></span><span
                                                                    class="kt-menu__link-text teratur-nav"> Daftar ECL
                                                                </span></a></li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </li>



                                <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel"
                                    data-ktmenu-submenu-toggle="hover" aria-haspopup="true"><a href="javascript:;"
                                        class="kt-menu__link kt-menu__toggle teratur-nav"><i
                                            class="kt-menu__link-icon flaticon-settings-1 teratur-nav"></i><span
                                            class="kt-menu__link-text teratur-nav">Pengaturan</span><i
                                            class="kt-menu__ver-arrow la la-angle-right teratur-nav"></i></a>
                                    <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
                                        <ul class="kt-menu__subnav">
                                            <li class="kt-menu__item teratur-nav" aria-haspopup="true"><a
                                                    href="<?php echo site_url("riwayat"); ?>"
                                                    class="kt-menu__link teratur-nav"><i
                                                        class="kt-menu__link-icon flaticon-stopwatch teratur-nav"></i><span></span><span
                                                        class="kt-menu__link-text teratur-nav">Aktifitas</span></a></li>

                                            <?php if ( $this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' ) { ?>
                                            <li class="kt-menu__item teratur-nav" aria-haspopup="true"><a
                                                    href="<?php echo site_url("anggota"); ?>"
                                                    class="kt-menu__link teratur-nav"><i
                                                        class="kt-menu__link-icon flaticon-network teratur-nav"></i><span></span><span
                                                        class="kt-menu__link-text teratur-nav">User</span></a></li>
                                            <li class="kt-menu__item teratur-nav" aria-haspopup="true"><a
                                                    href="<?php echo site_url("file"); ?>"
                                                    class="kt-menu__link teratur-nav"><i
                                                        class="kt-menu__link-icon flaticon2-file-1 teratur-nav"></i><span></span><span
                                                        class="kt-menu__link-text teratur-nav">Daftar File</span></a>
                                            </li>
                                            <?php } ?>

                                        </ul>
                                    </div>
                                </li>

                                <!-- <li class="kt-menu__item" aria-haspopup="true"><a href="<?php echo site_url("info"); ?>"
                                        class="kt-menu__link "><i
                                            class="kt-menu__link-icon flaticon-information teratur-nav"></i><span
                                            class="kt-menu__link-text teratur-nav">Info</span></a></li> -->
                                <!-- <li class="kt-menu__item" aria-haspopup="false"><a href="#" class="kt-menu__link "><i
                                            class="kt-menu__link-icon flaticon-information teratur-nav"></i><span
                                            class="kt-menu__link-text teratur-nav">Info</span></a></li> -->
                            </ul>
                        </div>
                    </div>



                    <!-- begin:: Header Topbar -->
                    <div class="kt-header__topbar kt-grid__item">

                        <!--begin: User bar -->
                        <div class="kt-header__topbar-item kt-header__topbar-item--user">
                            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                                <!-- <span class="kt-header__topbar-welcome kt-visible-desktop teratur-nav">Hai,</span> -->
                                <span class="kt-header__topbar-username kt-visible-desktop teratur-nav">Hai,
                                    <?php 
                                        echo $this->session->userdata('username');
                                    ?>
                                </span>

                                <?php 
                                if ($this->session->userdata("image") != "") {
                                ?>
                                <img alt="Pic"
                                    src="<?php echo $this->config->item('api_host') .'/view_file'. '/' .  $this->session->userdata("image"); ?>"
                                    width="40px" height="40px" />
                                <?php
                                } else {
                                ?>
                                <img alt="Pic"
                                    src="<?php echo base_url(); ?>public/assets/custom/default_profile.png" />
                                <?php } ?>
                                <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                                <span
                                    class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden">S</span>
                            </div>
                            <div
                                class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">

                                <!--begin: Head -->
                                <div class="kt-user-card kt-user-card--skin-light kt-notification-item-padding-x">
                                    <div class="kt-user-card__avatar">
                                        <?php 
                                    if ($this->session->userdata("image") != "") {
                                    ?>
                                        <img alt="Pic"
                                            src="<?php echo $this->config->item('api_host') .'/view_file'. '/' .  $this->session->userdata("image"); ?>"
                                            width="40px" height="40px" />
                                        <?php
                                    } else {
                                    ?>
                                        <img alt="Pic"
                                            src="<?php echo base_url(); ?>public/assets/custom/default_profile.png" />
                                        <?php } ?>

                                        <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                                        <span
                                            class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden">S</span>
                                    </div>
                                    <div class="kt-user-card__name">
                                        <?php echo $this->session->userdata('username'); ?> <br />
                                        <div><small>
                                                <?php echo $this->session->userdata('name_role'); ?>
                                            </small>
                                        </div>
                                    </div>
                                    <div class="kt-user-card__badge">
                                        <!-- <span class="btn btn-label-primary btn-sm btn-bold btn-font-md">23
                                            messages</span> -->
                                    </div>
                                </div>

                                <!--end: Head -->

                                <!--begin: Navigation -->
                                <div class="kt-notification">
                                    <a href="#" class="kt-notification__item" data-toggle="modal"
                                        data-target="#edit_password">
                                        <div class="kt-notification__item-icon">
                                            <i class="flaticon-security kt-font-success"></i>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title kt-font-bold">
                                                Ubah Kata Sandi
                                            </div>
                                            <div class="kt-notification__item-time">
                                                Pengaturan perubahan Kata Sandi
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="kt-notification__item" data-toggle="modal"
                                        data-target="#edit_profiles" onClick="return EditUserProfile(
                                        '<?php echo $this->session->userdata("ids") ?>',
                                        '<?php echo $this->session->userdata("nama") ?>',
                                        '<?php echo $this->session->userdata("email") ?>',
                                        '<?php echo $this->session->userdata("alamat") ?>',
                                        '<?php echo $this->session->userdata("jk") ?>',
                                        '<?php echo $this->session->userdata("image") ?>',
                                        '<?php echo $this->session->userdata("role") ?>',
                                        '<?php echo $this->session->userdata("telepon") ?>',
                                        '<?php echo $this->session->userdata("divisi") ?>',
                                        '<?php echo $this->session->userdata("jabatan") ?>',
                                    )">

                                        <div class="kt-notification__item-icon">
                                            <i class="flaticon2-user-1 kt-font-success"></i>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title kt-font-bold">
                                                Ubah Profil
                                            </div>
                                            <div class="kt-notification__item-time">
                                                Pengaturan perubahan Profil user
                                            </div>
                                        </div>
                                    </a>
                                    <div class="kt-notification__custom kt-space-between">
                                        <a href="<?php echo site_url("auth/signout"); ?>"
                                            class="btn btn-label btn-label-brand btn-sm btn-bold teratur-button">Keluar</a>

                                    </div>
                                </div>

                                <!--end: Navigation -->
                            </div>
                        </div>


                    </div>

                    <!-- end:: Header Topbar -->
                </div>
            </div>


            <div class="modal fade" id="edit_password" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Ubah Kata Sandi</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            </button>
                        </div>
                        <!--begin::Form-->
                        <form class="kt-form kt-form--fit kt-form--label-center" method="post"
                            enctype="multipart/form-data" action="<?php echo site_url('auth/updatePass'); ?>">
                            <div class="modal-body">
                                <div class="kt-portlet__body">
                                    <div class="kt-section kt-section--first" id='section_id'>
                                        <div class="kt-section__body">
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Kata Sandi Lama</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="password" class="form-control" value=""
                                                        placeholder="Masukan kata sandi lama" id="old_password"
                                                        name="old_password" required>
                                                    <!-- <a href="#"
                                            class="kt-link kt-font-sm kt-font-bold kt-margin-t-5">Forgot
                                            password ?</a> -->
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Kata Sandi Baru</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="password" class="form-control" value=""
                                                        placeholder="Masukan kata sandi baru" id="new_password"
                                                        name="new_password" required>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-last row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Konfirmasi
                                                    Kata Sandi</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="password" class="form-control" value=""
                                                        placeholder="Konfirmasi kata sandi baru" id="confirm_password"
                                                        name="confirm_password" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                                <button type="submit" class="btn btn-brand btn-icon-sm teratur-button">Ubah
                                    Kata Sandi</button>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                </div>
            </div>

            <div class="modal fade" id="edit_profiles" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Ubah Data Profil</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            </button>
                        </div>
                        <!-- Star Form -->
                        <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                            action="<?php echo site_url('auth/profile'); ?>">
                            <div class="modal-body">
                                <div class="kt-portlet__body">
                                    <div class="kt-section kt-section--first">
                                        <div class="kt-section__body">
                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Foto</label>
                                                <div class="col-lg-9 col-xl-9">
                                                    <div class="kt-avatar kt-avatar--outline" id="kt_user_avatar">
                                                        <div class="kt-avatar__holder" id="avatar_image_edit" style="background-image: url(<?php 
                                                            if ($this->session->userdata("image") != "") {
                                                                echo $this->config->item('api_host') .'/view_file'.'/'.$this->session->userdata('image');
                                                            } else {
                                                                echo base_url()."public/assets/custom/default_profile.png";
                                                            }
                                                            ?>)">
                                                        </div>
                                                        <label class="kt-avatar__upload" data-toggle="kt-tooltip"
                                                            title="" data-original-title="Change avatar">
                                                            <i class="fa fa-pen"></i>
                                                            <input type="file" name="add_image" id="edit_image"
                                                                accept=".png, .jpg, .jpeg" onchange="readURL(this);">
                                                        </label>
                                                        <span class="kt-avatar__cancel" data-toggle="kt-tooltip"
                                                            title="" data-original-title="Cancel avatar">
                                                            <i class="fa fa-times"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <input class="form-control" type="hidden" id="idss" name="ids" required>
                                            <input class="form-control" type="hidden" id="paths" name="path" required>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Nama</label>
                                                <div class="col-lg-9 col-xl-8">
                                                    <input class="form-control" type="text" id="fullnames"
                                                        name="add_fullname" required>
                                                </div>
                                            </div>


                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Jenis Kelamin</label>
                                                <div class="col-lg-9 col-xl-4">

                                                    <select name="gender" id="genders" class="form-control" required>
                                                        <option value="1">Laki Laki</option>
                                                        <option value="0">Perempuan</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Role</label>
                                                <div class="col-lg-9 col-xl-4">

                                                    <select name="role" id="roles" class="form-control" required>
                                                        <option value="1">Administrator</option>
                                                        <option value="2">Finance</option>
                                                        <option value="3">Reviewer</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Divisi</label>
                                                <div class="col-lg-9 col-xl-8">
                                                    <input class="form-control" type="text" id="add_divisi"
                                                        name="add_divisi" required>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Jabatan</label>
                                                <div class="col-lg-9 col-xl-8">
                                                    <input class="form-control" type="text" id="add_jabatan"
                                                        name="add_jabatan" required>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Alamat Detail</label>
                                                <div class="col-lg-9 col-xl-8">
                                                    <input class="form-control" type="text" id="addresss"
                                                        name="add_address" required>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Nomor Telepon</label>
                                                <div class="col-lg-9 col-xl-8">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span
                                                                class="input-group-text"><i
                                                                    class="la la-phone"></i></span></div>
                                                        <input type="text" class="form-control" id="phones"
                                                            name="add_phone"
                                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                            placeholder="Phone" aria-describedby="basic-addon1"
                                                            required>
                                                    </div>
                                                    <!-- <span class="form-text text-muted"></span> -->
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-xl-3 col-lg-3 col-form-label">Alamat Email</label>
                                                <div class="col-lg-9 col-xl-8">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span
                                                                class="input-group-text"><i class="la la-at"></i></span>
                                                        </div>
                                                        <input type="text" class="form-control" id="emails"
                                                            name="add_email" placeholder="Email"
                                                            aria-describedby="basic-addon1" required>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                                <button type="submit" class="btn btn-primary teratur-button">Ubah Data</button>
                            </div>
                        </form>
                        <!-- End Form -->
                    </div>
                </div>
            </div>


            <script>
            function EditUserProfile(id, nama, email, alamat, jk, image, role, phone, divisi, jabatan) {
                document.getElementById("idss").value = id;
                document.getElementById("fullnames").value = nama;
                document.getElementById("emails").value = email;
                document.getElementById("addresss").value = alamat;
                document.getElementById("genders").value = jk;
                document.getElementById("roles").value = role;
                document.getElementById("paths").value = image;
                document.getElementById("phones").value = phone;
                document.getElementById("add_divisi").value = divisi;
                document.getElementById("add_jabatan").value = jabatan;
            }
            </script>

            <script type="text/javascript">
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        // $('#blah')
                        //     .attr('src', e.target.result);
                        document.getElementById('avatar_image_edit').style.backgroundImage = "url(" + e.target
                            .result + ")";
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }

            function readURLAdd(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        // $('#blah')
                        //     .attr('src', e.target.result);
                        document.getElementById('avatar_image_add').style.backgroundImage = "url(" + e.target
                            .result + ")";
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }

            function EditPenarikan(id, name, rek) {
                document.getElementById("add_name").value = name;
                document.getElementById("add_id").value = id;
                document.getElementById("add_rek").value = rek;
            }
            </script>