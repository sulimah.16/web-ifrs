       <!-- begin:: Footer -->
       <div class="kt-footer kt-grid__item" id="kt_footer">
           <div class="kt-container ">
               <div class="kt-footer__wrapper">
                   <div class="kt-footer__copyright">
                       2020&nbsp;&copy;&nbsp;<a href="http://teratur.id" target="_blank" class="kt-link">Simply 9
                           Solution.Id</a>
                   </div>
                   <div class="kt-footer__menu">
                   </div>
               </div>
           </div>
       </div>

       <!-- end:: Footer -->
       </div>
       </div>
       </div>
       <div id="kt_scrolltop" class="kt-scrolltop">
           <i class="fa fa-arrow-up"></i>
       </div>


       <script>
var KTAppOptions = {
    "colors": {
        "state": {
            "brand": "#591df1",
            "light": "#ffffff",
            "dark": "#282a3c",
            "primary": "#5867dd",
            "success": "#34bfa3",
            "info": "#36a3f7",
            "warning": "#ffb822",
            "danger": "#fd3995"
        },
        "base": {
            "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
            "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
        }
    }
};
       </script>

       <!-- end::Global Config -->

       <!--begin::Global Theme Bundle(used by all pages) -->
       <script src="<?php echo base_url(); ?>public/assets/plugins/global/plugins.bundle.js" type="text/javascript">
       </script>
       <script src="<?php echo base_url(); ?>public/assets/js/scripts.bundle.js" type="text/javascript"></script>

       <!--end::Global Theme Bundle -->

       <!--begin::Page Vendors(used by this page) -->
       <script src="<?php echo base_url(); ?>public/assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"
           type="text/javascript"></script>
       <!-- <script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM" type="text/javascript"></script> -->
       <script src="<?php echo base_url(); ?>public/assets/plugins/custom/gmaps/gmaps.js" type="text/javascript">
       </script>

       <!--end::Page Vendors -->

       <!--begin::Page Scripts(used by this page) -->

       <!--begin::Page Vendors(used by this page) -->
       <script src="//www.amcharts.com/lib/3/amcharts.js" type="text/javascript"></script>
       <script src="//www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>
       <script src="//www.amcharts.com/lib/3/radar.js" type="text/javascript"></script>
       <script src="//www.amcharts.com/lib/3/pie.js" type="text/javascript"></script>
       <script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js" type="text/javascript">
       </script>
       <script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js" type="text/javascript"></script>
       <script src="//www.amcharts.com/lib/3/plugins/export/export.min.js" type="text/javascript"></script>
       <script src="//www.amcharts.com/lib/3/themes/light.js" type="text/javascript"></script>
       <!--end::Page Vendors -->

       <!--begin::Page Scripts(used by this page) -->
       <script src="<?php echo base_url(); ?>public/assets/js/pages/components/charts/amcharts/charts.js"
           type="text/javascript"></script>

       <script src="<?php echo base_url(); ?>public/assets/js/pages/components/charts/flotcharts.js"
           type="text/javascript"></script>
       <script src="<?php echo base_url(); ?>public/assets/js/pages/components/charts/morris-charts.js"
           type="text/javascript"></script>
       <!--end::Page Scripts -->

       <script src="<?php echo base_url(); ?>public/assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js"
           type="text/javascript"></script>
       <script src="<?php echo base_url(); ?>public/assets/js/pages/crud/forms/widgets/bootstrap-select.js"
           type="text/javascript"></script>
       <script src="<?php echo base_url(); ?>public/assets/js/pages/crud/forms/widgets/bootstrap-timepicker.js"
           type="text/javascript"></script>
       <script src="<?php echo base_url(); ?>public/assets/js/pages/crud/forms/widgets/select2.js"
           type="text/javascript"></script>
       <script src="<?php echo base_url(); ?>public/assets/plugins/custom/datatables/datatables.bundle.js"
           type="text/javascript"></script>

       <script src="//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>
       <script src="https://cdn.jsdelivr.net/alasql/0.3/alasql.min.js"></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.7.12/xlsx.core.min.js"></script>


       <?php if($this->session->flashdata('message') != NULL) { ?>
       <script type="text/javascript">
$(window).on('load', function() {
    $('#warningmodal').modal('show');
});
       </script>
       <?php } ?>

       <?php if($this->session->flashdata('err-message') != NULL) { ?>
       <script type="text/javascript">
$(window).on('load', function() {
    $('#errmodal').modal('show');
});
       </script>
       <?php } ?>

       <script>
function del_confirm() {
    var x = confirm("Apakah anda akan menghapus data ini?");
    if (x) {
        localStorage.clear();
        return true;
    } else {
        return false;
    }
}

function act_confirm() {
    var x = confirm("Apakah anda akan mengerjakan perintah tersebut?");
    if (x) {
        localStorage.clear();
        return true;
    } else {
        return false;
    }
}
       </script>

       <script>
$(document).ready(function() {
    $.ajax({
        type: 'post',
        url: "<?php echo site_url('pesan/count'); ?>",
        data: {
            message: '1'
        },
        success: function(html) {
            $("#badges_count").html(html);
        }
    });
});

$('#dateStartMape').datepicker({
    format: "mm/yyyy",
    autoclose: true,
    viewMode: "months",
    minViewMode: "months"
});

$('#dateEndMape').datepicker({
    format: "mm/yyyy",
    autoclose: true,
    viewMode: "months",
    minViewMode: "months"
});

$('#dateStart').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
});

$('#eperiode').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});

$('#dateStart2').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
});

$('#dateStart3').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});

$('#years').datepicker({
    format: 'yyyy',
    autoclose: true,
    viewMode: "years",
    minViewMode: "years"
});

$('#edit_years').datepicker({
    format: 'yyyy',
    autoclose: true,
    viewMode: "years",
    minViewMode: "years"
});

$('#edit_month').datepicker({
    format: 'm',
    autoclose: true,
    viewMode: "months",
    minViewMode: "months"
});

$('#tambahBulan').datepicker({
    format: 'm',
    autoclose: true,
    viewMode: "months",
    minViewMode: "months"
});

$('#tambahTahun').datepicker({
    format: 'yyyy',
    autoclose: true,
    viewMode: "years",
    minViewMode: "years"
});


$('#edit_monthe').datepicker({
    format: 'm',
    autoclose: true,
    viewMode: "months",
    minViewMode: "months"
});

$('#edit_monthee').datepicker({
    format: 'm',
    autoclose: true,
    viewMode: "months",
    minViewMode: "months"
});

$('#edit_tanggal').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});

$('#dateEnd').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});

$('#edit_join_at').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});
       </script>

       <script>
$('#date_from_att').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});
$('#date_to_att').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});
       </script>

       <script>
$('#taskDate').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});
       </script>

       <script>
$('#eduStart').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});
       </script>
       <script>
$('#eduEnd').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});
       </script>

       <script>
$('#add_cmp_start').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});
       </script>
       <script>
$('#add_cmp_end').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});
       </script>

       <script>
$('#add_card_start').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});
       </script>
       <script>
$('#add_card_end').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});
       </script>

       <script type="text/javascript">
$("#generate_years").datepicker({
    format: "yyyy",
    viewMode: "years",
    autoclose: true,
    minViewMode: "years"
});

$("#generate_month").datepicker({
    format: "mm",
    viewMode: "months",
    autoclose: true,
    minViewMode: "months"

});

$("#month_cicilan").datepicker({
    format: "m",
    viewMode: "months",
    autoclose: true,
    minViewMode: "months"
});

$("#generate_years_edit").datepicker({
    format: "yyyy",
    viewMode: "years",
    autoclose: true,
    minViewMode: "years"
});

$('#edit_dateHoliday').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});

$('#edit_dob').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});
$('#edit_join').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});

$('#edit_dob_profiles').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});
$('#edit_join_profiles').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});

$('#dobe').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});
       </script>

       <script type="text/javascript">
$(document).ready(function() {
    localStorage.setItem('create-wizard-simulate', 0);
    $('#select_m').select2({
        width: '100%'
    });
});

$(document).ready(function() {
    $('#edit_division_profiles').select2({
        width: '100%'
    });
});
$(document).ready(function() {
    $('#edit_position_profiles').select2({
        width: '100%'
    });
});
$(document).ready(function() {
    $('#edit_grade_profiles').select2({
        width: '100%'
    });
});
$(document).ready(function() {
    $('#edit_jobtype_profiles').select2({
        width: '100%'
    });
});

function EditKoperasi(id, name, provinsi, kota, kecamatan, desa, address, phone, email, web, npwp, status) {

    document.getElementById("edit_id_pemilik").value = id;
    document.getElementById("edit_name").value = name;
    document.getElementById("edit_address").value = address;
    document.getElementById("edit_phone").value = phone;
    document.getElementById("edit_email").value = email;
    document.getElementById("edit_web").value = web;
    document.getElementById("edit_npwp").value = npwp;
    document.getElementById("edit_status").value = status;

    if (provinsi != 0) {
        $("#provinceee").val(provinsi).trigger('change');
    }
    //

    $.ajax({
        type: 'post',
        url: "<?php echo site_url('regency/get_spc_city_form'); ?>",
        data: {
            message: provinsi
        },
        success: function(html) {
            $('#cityee').html(html);
            $('#cityee').selectpicker('refresh');
            $('#cityee').val(kota).trigger('change');
        }
    });

    $(function() {

        $('#provinceee').on('change', function() {
            var selected = $(this).val();

            $.ajax({
                type: 'post',
                url: "<?php echo site_url('regency/get_spc_city_form'); ?>",
                data: {
                    message: selected
                },
                success: function(html) {
                    $('#cityee').html(html);
                    $('#cityee').selectpicker('refresh');
                }
            });
        });
    });


    $('#cityee').on('change', function() {
        var selecteds = $(this).val();

        $.ajax({
            type: 'post',
            url: "<?php echo site_url('regency/get_spc_kec_form'); ?>",
            data: {
                message: selecteds
            },
            success: function(html) {
                $('#kecee').html(html);
                $('#kecee').selectpicker('refresh');
                if (kota == selecteds) {
                    $('#kecee').val(kecamatan).trigger('change');
                }
            }
        });
    });

    $('#kecee').on('change', function() {
        var selecteds = $(this).val();

        $.ajax({
            type: 'post',
            url: "<?php echo site_url('regency/get_spc_des_form'); ?>",
            data: {
                message: selecteds
            },
            success: function(html) {
                $('#desse').html(html);
                $('#desse').selectpicker('refresh');
                if (kecamatan == selecteds) {
                    $('#desse').val(desa).trigger('change');
                }
            }
        });
    });


}



function EditProfiles(ids, nama, gender, role, phone, email, status, path, divisi, jabatan) {

    document.getElementById("edit_ids").value = ids;
    document.getElementById("edit_fullname").value = nama;
    document.getElementById("edit_gender").value = gender;
    document.getElementById("edit_role").value = role;
    // document.getElementById("edit_address").value = alamat;
    document.getElementById("edit_phone").value = phone;
    document.getElementById("edit_email").value = email;
    document.getElementById("edit_status").value = status;
    document.getElementById("edit_path").value = path;
    document.getElementById("edit_divisi").value = divisi;
    document.getElementById("edit_jabatan").value = jabatan;

    let img = "http://128.199.65.184:8000/core-ifrs/view_file/" + path;
    document.getElementById('avatar_image_edit').style.backgroundImage = "url(" + img + ")";

}
       </script>


       <script>
$("#formGDP").show()
$("#uploadGDP").hide()

$('#tipeInput').on('change', function(e) {
    var value = this.value;
    if (value == 0) {
        $("#formGDP").show()
        $("#uploadGDP").hide()
    } else {
        $("#formGDP").hide()
        $("#uploadGDP").show()
    }
});
       </script>


       <script>
// $("#a").show()
// $("#b").hide()
// $("#c").hide()

$("#umur_piutang").prop('required', true);
$("#saldo_piutang").prop('required', true);

$('#tipe_input').on('change', function(e) {
    var value = this.value;

    if (value == 0) {
        $("#a").show()
        $("#b").hide()
        $("#c").hide()
        $("#umur_piutang").prop('required', true);
        $("#saldo_piutang").prop('required', true);
        $("#jumlah_bucket").prop('required', false);
    } else if (value == 1) {
        $("#a").hide()
        $("#b").show()
        $("#c").hide()
        $("#umur_piutang").prop('required', false);
        $("#saldo_piutang").prop('required', false);
        $("#jumlah_bucket").prop('required', false);
    } else {
        $("#a").hide()
        $("#b").hide()
        $("#c").show()
        $("#umur_piutang").prop('required', false);
        $("#saldo_piutang").prop('required', false);
        $("#jumlah_bucket").prop('required', true);


    }

});
       </script>

       <script>
$.get("<?php echo site_url("notification/notif"); ?>", function(data) {
    $("#notif").html(data);
});

$.get("<?php echo site_url("notification/countNotif"); ?>", function(data) {
    $("#span_notif").html(data);
});
       </script>


       <script>
$('#add_asset_start_time').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});
$('#add_asset_end_time').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});
$('#dateStartLeave').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});
$('#dateEndLeave').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});
$('#tanggal_bod').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});
$('#tanggal_join').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});
       </script>

       <script>
$('#timeStartOv').timepicker({
    use24hours: true
});
$('#timeStart').timepicker();
$('#timeEnd').timepicker();
$('#add_start').timepicker();
$('#add_end').timepicker();
       </script>

       <script type="text/javascript">
function EditJobType(ids, job_type_name, sun, mon, tue, wed, thu, fri, sat, start, end) {
    document.getElementById("edit_id").value = ids;
    document.getElementById("edit_job_type_name").value = job_type_name;
    document.getElementById("edit_sun").value = sun;
    document.getElementById("edit_mon").value = mon;
    document.getElementById("edit_tue").value = tue;
    document.getElementById("edit_wed").value = wed;
    document.getElementById("edit_thu").value = thu;
    document.getElementById("edit_fri").value = fri;
    document.getElementById("edit_sat").value = sat;
    $('#timeStartEdit').timepicker({
        defaultTime: start
    });
    $('#endStartEdit').timepicker({
        defaultTime: end
    });

}

function EditConfigAbsanse(ids, type, loc_name, loc_addr, lat, lon, tol, str, end, tmz, tmz_name) {
    document.getElementById("edit_con_id").value = ids;
    //$('#edit_con_id').val(ids).trigger('change');
    $('#edit_jobtype').val(type).trigger('change');
    document.getElementById("edit_location_name").value = loc_name;
    document.getElementById("edit_location_addr").value = loc_addr;
    document.getElementById("edit_lat").value = lat;
    document.getElementById("edit_long").value = lon;
    document.getElementById("edit_tol").value = tol;
    $('#edit_start').timepicker({
        defaultTime: str
    });
    $('#edit_end').timepicker({
        defaultTime: end
    });
    document.getElementById("edit_timezone").value = tmz;
    document.getElementById("edit_timezone_name").value = tmz_name;

}
       </script>

       <script type="text/javascript">
function EditEdu(employee, ids, place, gelar, start, end, gpa, cert_number, cert) {
    document.getElementById("edit_edu_employee").value = employee;
    document.getElementById("edit_id").value = ids;
    document.getElementById("edit_edu_place").value = place;
    document.getElementById("edit_edu_gelar").value = gelar;
    document.getElementById("eduEditStart").value = start;
    document.getElementById("eduEditEnd").value = end;
    document.getElementById("edit_edu_gpa").value = gpa;
    document.getElementById("edit_cert_number").value = cert_number;
    document.getElementById("edit_cert").value = cert;
    $('#eduEditStart').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
    });
    $('#eduEditEnd').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
    });
}
       </script>

       <script type="text/javascript">
function EditAsset(employee, id, name, spec, serial, str, end, photo, status, image) {
    document.getElementById("edit_asset_employee").value = employee;
    document.getElementById("edit_asset_ids").value = id;
    document.getElementById("edit_asset_name").value = name;
    document.getElementById("edit_asset_spesification").value = spec;
    document.getElementById("edit_asset_serial_number").value = serial;
    document.getElementById("edit_asset_start_time").value = str;
    document.getElementById("edit_asset_end_time").value = end;
    document.getElementById("edit_asset_photo_2").value = photo;
    document.getElementById("edit_asset_status").value = status;
    // document.getElementById('avatar_image_edit').style.backgroundImage = "url(" + image + ")"; 
    $('#edit_asset_start_time').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
    });
    $('#edit_asset_end_time').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
    });
}
       </script>

       <script type="text/javascript">
function EditCard(employee, ids, type, number, vendor, name, address, start, end) {
    document.getElementById("edit_employee_card_id").value = employee;
    document.getElementById("edit_card_id").value = ids;
    document.getElementById("edit_card_type").value = type;
    document.getElementById("edit_card_number").value = number;
    document.getElementById("edit_card_vendor").value = vendor;
    document.getElementById("edit_card_name").value = name;
    document.getElementById("edit_card_address").value = address;
    document.getElementById("edit_card_start").value = start;
    document.getElementById("edit_card_end").value = end;
    $('#edit_card_start').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
    });
    $('#edit_card_end').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
    });
}
       </script>

       <script type="text/javascript">
function EditJob(employee, ids, company_name, company_address, start, end, position, salary, exp) {
    document.getElementById("edit_employee_job_id").value = employee;
    document.getElementById("edit_job_id").value = ids;
    document.getElementById("edit_cmp_history_name").value = company_name;
    document.getElementById("edit_cmp_history_address").value = company_address;
    document.getElementById("edit_cmp_start").value = start;
    document.getElementById("edit_cmp_end").value = end;
    document.getElementById("edit_cmp_last_position").value = position;
    document.getElementById("edit_cmp_last_salary").value = salary;
    document.getElementById("edit_cmp_explanation").value = exp;
    $('#edit_cmp_start').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
    });
    $('#edit_cmp_end').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
    });
}
       </script>

       <script type="text/javascript">
function EditFam(employee, ids, name, dob, pob, gender, nationality, religion, address, status) {
    document.getElementById("edit_fam_employee").value = employee;
    document.getElementById("edit_fam_id").value = ids;
    document.getElementById("edit_family_name").value = name;
    document.getElementById("edit_dob").value = dob;
    document.getElementById("edit_pob").value = pob;
    document.getElementById("edit_gender").value = gender;
    document.getElementById("edit_nationality").value = nationality;
    document.getElementById("edit_religion").value = religion;
    document.getElementById("edit_address").value = address;
    document.getElementById("edit_familiy_status").value = status;
    $('#edit_dob').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
    });
}
       </script>

       <script type="text/javascript">
$('#add_dob').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd',
});
       </script>

       <script type="text/javascript">
$(document).ready(function() {

    $('#add_type').change(function() {

        // $('#laoding').modal('show');
        // $('#add_claim_modal').modal('hide');
        $('#section_id').hide();
        var employee = $("#add_employee").val();
        var type = $("#add_type").val();
        var ids = employee + '/' + type;


        $.post("<?php echo site_url('benefit/getBenefitId');?>", {
                id: ids
            })
            .done(function(data) {
                // alert( "Data Loaded: " + data );
                $('#benefit_id').show();
                $('#section_id').show();
                $('#add_benefit').html(data);
            });
    });

    $('#add_benefit_type').change(function() {

        // $('#laoding').modal('show');
        // $('#add_claim_modal').modal('hide');
        var employee = $("#add_benefit_employee").val();
        var type = $("#add_benefit_type").val();
        var ids = employee + '/' + type;


        $.post("<?php echo site_url('benefit/getBenefitIdBenefit');?>", {
                id: type
            })
            .done(function(data) {
                // alert( "Data Loaded: " + data );
                $('#benefit_id_2').show();
                $('#add_benefit_category').html(data);
                $('#add_benefit_category').selectpicker('refresh');
            });



    });

    $('#edit_benefit_type').change(function() {

        // $('#laoding').modal('show');
        // $('#add_claim_modal').modal('hide');
        var employee = $("#edit_benefit_employee").val();
        var type = $("#edit_benefit_type").val();
        var ids = employee + '/' + type;


        $.post("<?php echo site_url('benefit/getBenefitIdBenefit');?>", {
                id: type
            })
            .done(function(data) {
                // alert( "Data Loaded: " + data );
                $('#benefit_id_3').show();
                $('#edit_benefit_category').html(data);
                $('#edit_benefit_category').selectpicker('refresh');

            });



    });

});
       </script>

       <script>
function EditBenefit(ids, employee, type, cat, cl, ammount, payroll, claim) {
    document.getElementById("edit_benefit_employee").value = employee;
    document.getElementById("edit_benefit_ids").value = ids;
    document.getElementById("edit_benefit_type").value = type;
    var ids = employee + '/' + type;
    $.post("<?php echo site_url('benefit/getBenefitIdBenefitSelect');?>", {
            id: ids,
            cat: cat
        })
        .done(function(data) {
            // alert( "Data Loaded: " + data );
            $('#benefit_id_3').show();
            $('#edit_benefit_category').html(data);
            $('#edit_benefit_category').selectpicker('refresh');
        });
    document.getElementById("edit_benefit_class").value = cl;
    document.getElementById("edit_benefit_ammount").value = ammount;
    document.getElementById("edit_benefit_payroll").value = payroll;
    document.getElementById("edit_benefit_cliam").value = claim;
}
       </script>

       <!-- <script type="text/javascript">
            $(document).ready(function() {

                $('#add_employee_leave').change(function() {
                    var employee = $("#add_employee_leave").val();
                    var ids = employee;

                    $.post("<?php echo site_url('leave/getLeaveType');?>", {
                            id: ids
                        })
                        .done(function(data) {
                            // alert( "Data Loaded: " + data );
                            $('#add_type_leave').html(data);
                            $('#add_type_leave').selectpicker('refresh');
                        });
                });

            });
       </script> -->

       <script type="text/javascript">
$(document).ready(function() {
    $('#dateStartLeave').change(function() {
        var start = $("#dateStartLeave").val();
        var end = $("#dateEndLeave").val();

        if (end == "") {
            document.getElementById("dateEndLeave").value = start;
            end = start;
        }

        $.post("<?php echo site_url('leave/getLeaveWorkday');?>", {
                st: start,
                ed: end
            })
            .done(function(data) {
                document.getElementById("workday").value = data;
            });
    });
});

$(document).ready(function() {
    $('#dateEndLeave').change(function() {
        var start = $("#dateStartLeave").val();
        var end = $("#dateEndLeave").val();

        if (start == "") {
            document.getElementById("dateEndLeave").value = start;
            start = end;
        }

        $.post("<?php echo site_url('leave/getLeaveWorkday');?>", {
                st: start,
                ed: end
            })
            .done(function(data) {
                document.getElementById("workday").value = data;
            });
    });
});
       </script>

       <script type="text/javascript">
function generateCutiData() {
    var employee = $("#add_employee_leave").val();
    var ids = employee;
    $.post("<?php echo site_url('leave/getLeaveType');?>", {
            id: ids
        })
        .done(function(data) {
            // alert( "Data Loaded: " + data );
            $('#add_type_leave').html(data);
            $('#add_type_leave').selectpicker('refresh');
        });
}
       </script>

       <?php 
		if(isset($plugin)) {
			if(strpos($plugin, 'datatable_employee') !== false) { ?>
       <!-- <script src="<?php echo base_url(); ?>public/assets/js/pages/crud/metronic-datatable/base/html-table.js" type="text/javascript"></script> -->

       <script type="text/javascript">
var datatable = $('.kt-datatable').KTDatatable({
    layout: {
        scroll: false,
        footer: false,
    },
    sortable: true,
    pagination: true,
    search: {
        input: $('#generalSearch'),
    },
    columns: [{
            field: 'Nama Karyawan',
            width: 300,
        }, {
            field: 'NIK',
            width: 60,
        },
        {
            field: 'Gender',
            width: 60,
        },
        {
            field: 'Status',
            width: 80,
        },
        {
            field: 'Action',
            title: 'Action',
            sortable: false,
            width: 50,
            overflow: 'visible',
            textAlign: 'center',
        }
    ],
});
       </script>





       <?php }
		}
		?>



       <?php if(isset($plugin)) { if(strpos($plugin, 'datatable_anggota') !== false) { ?>
       <script type="text/javascript">
var datatable = $('.kt-datatable').KTDatatable({
    layout: {
        scroll: false,
        footer: false,
    },
    sortable: true,
    pagination: true,
    search: {
        input: $('#generalSearch'),
    },
    columns: [{
            field: 'Nama Karyawan',
            width: 300,
        }, {
            field: 'NIK',
            width: 60,
        },
        {
            field: 'Gender',
            width: 60,
        },
        {
            field: 'Status',
            width: 80,
        },
    ],
});
       </script>
       <?php } } ?>

       <?php if(isset($plugin)) { if(strpos($plugin, 'datatable_assets') !== false) { ?>
       <script type="text/javascript">
var datatable = $('.kt-datatable').KTDatatable({
    layout: {
        scroll: false,
        footer: false,
    },
    sortable: true,
    pagination: true,
    search: {
        input: $('#generalSearch'),
    },
    columns: [{
            field: 'Nama Assets',
            width: 300,
        }, {
            field: 'Jumlah',
            width: 100,
        },
        {
            field: 'Tipe Assets',
            width: 100,
        },
        {
            field: 'Kadaluarsa',
            width: 100,
        },
    ],
});
       </script>
       <?php } } ?>







       <?php 
		if(isset($plugin)) {
			if(strpos($plugin, 'datatable_jobtype') !== false) { ?>
       <!-- <script src="<?php echo base_url(); ?>public/assets/js/pages/crud/metronic-datatable/base/html-table.js" type="text/javascript"></script> -->

       <script type="text/javascript">
var datatable = $('.kt-datatable').KTDatatable({
    layout: {
        scroll: false,
        footer: false,
    },
    sortable: true,
    pagination: true,
    search: {
        input: $('#generalSearch'),
    },
    columns: [{
            field: 'Tipe Pekerjaan',
            textAlign: 'left',
            width: 200,
        },
        {
            field: 'Minggu',
            textAlign: 'center',
            width: 50,
        },
        {
            field: 'Senin',
            textAlign: 'center',
            width: 50,
        },
        {
            field: 'Selasa',
            textAlign: 'center',
            width: 50,
        },
        {
            field: 'Rabu',
            textAlign: 'center',
            width: 50,
        },
        {
            field: 'Kamis',
            textAlign: 'center',
            width: 50,
        },
        {
            field: 'Jumat',
            width: 50,
            textAlign: 'center',
        },
        {
            field: 'Sabtu',
            width: 50,
            textAlign: 'center',
        },
        {
            field: 'Waktu',
            textAlign: 'center',
            width: 100,
        },
        {
            field: 'Action',
            title: 'Action',
            sortable: false,
            width: 50,
            overflow: 'visible',
            textAlign: 'left',
        }
    ],
});
       </script>





       <?php }
		}
		?>


       <?php 
		if(isset($plugin)) {
			if(strpos($plugin, 'datatable_approve') !== false) { ?>
       <!-- <script src="<?php echo base_url(); ?>public/assets/js/pages/crud/metronic-datatable/base/html-table.js" type="text/javascript"></script> -->

       <script type="text/javascript">
var datatable = $('.kt-datatable').KTDatatable({
    layout: {
        scroll: false,
        footer: false,
    },
    sortable: true,
    pagination: true,
    search: {
        input: $('#generalSearch'),
    },
    columns: [{
            field: 'Employee',
            width: 200,
        }, {
            field: 'NIK',
            width: 60,
        },
        {
            field: 'Status',
            width: 80,
        },
        {
            field: 'Zona Waktu',
            width: 100,
        },
        {
            field: 'Jam Masuk',
            width: 150,
        },
        {
            field: 'In/Out',
            width: 50,
            textAlign: 'center',
        },
        {
            field: 'Action',
            title: 'Action',
            sortable: false,
            width: 50,
            overflow: 'visible',
            textAlign: 'left',
        }
    ],
});
       </script>





       <?php }
		}
		?>

       <?php 
		if(isset($plugin)) {
			if(strpos($plugin, 'datatable_approve_overtime') !== false) { ?>
       <!-- <script src="<?php echo base_url(); ?>public/assets/js/pages/crud/metronic-datatable/base/html-table.js" type="text/javascript"></script> -->

       <script type="text/javascript">
var datatable = $('.kt-datatable').KTDatatable({
    layout: {
        scroll: false,
        footer: false,
    },
    sortable: true,
    pagination: true,
    search: {
        input: $('#generalSearch'),
    },
    columns: [{
            field: 'Employee',
            width: 200,
        }, {
            field: 'NIK',
            width: 60,
        },
        {
            field: 'Status',
            width: 80,
        },
        {
            field: 'Zona Waktu',
            width: 100,
        },
        {
            field: 'Jam Masuk',
            width: 150,
        },
        {
            field: 'In/Out',
            width: 50,
            textAlign: 'center',
        },
        {
            field: 'Action',
            title: 'Action',
            sortable: false,
            width: 50,
            overflow: 'visible',
            textAlign: 'left',
        }
    ],
});
       </script>





       <?php }
		}
		?>

       <?php 
		if(isset($plugin)) {
			if(strpos($plugin, 'datatable_leave') !== false) { ?>
       <!-- <script src="<?php echo base_url(); ?>public/assets/js/pages/crud/metronic-datatable/base/html-table.js" type="text/javascript"></script> -->

       <script type="text/javascript">
var datatable = $('.kt-datatable').KTDatatable({
    layout: {
        scroll: false,
        footer: false,
    },
    sortable: true,
    pagination: true,
    search: {
        input: $('#generalSearch'),
    },
    columns: [{
            field: 'Employee',
            width: 200,
        }, {
            field: 'NIK',
            width: 60,
        },
        {
            field: 'Status',
            width: 80,
        },
        {
            field: 'Waktu Cuti',
            width: 150,
        },
        {
            field: 'Total Hari',
            width: 80,
            textAlign: 'center',
        },
        {
            field: 'Action',
            title: 'Action',
            sortable: false,
            width: 50,
            overflow: 'visible',
            textAlign: 'center',
        }
    ],
});
       </script>





       <?php }
		}
		?>

       <?php 
		if(isset($plugin)) {
			if(strpos($plugin, 'datatable_approval_leave') !== false) { ?>
       <!-- <script src="<?php echo base_url(); ?>public/assets/js/pages/crud/metronic-datatable/base/html-table.js" type="text/javascript"></script> -->

       <script type="text/javascript">
var datatable = $('.kt-datatable').KTDatatable({
    layout: {
        scroll: false,
        footer: false,
    },
    sortable: true,
    pagination: true,
    search: {
        input: $('#generalSearch'),
    },
    columns: [{
            field: 'Employee',
            width: 200,
        }, {
            field: 'NIK',
            width: 60,
        },
        {
            field: 'Status',
            width: 80,
        },
        {
            field: 'Mulai',
            width: 120,
        },
        {
            field: 'Sampai',
            width: 120,
        },
        {
            field: 'Penjelasan',
            width: 200,
        },
        {
            field: 'Total Hari',
            width: 80,
            textAlign: 'center',
        },
        {
            field: 'Action',
            title: 'Action',
            sortable: false,
            width: 50,
            overflow: 'visible',
            textAlign: 'left',
        }
    ],
});
       </script>





       <?php }
		}
		?>

       <?php 
		if(isset($plugin)) {
			if(strpos($plugin, 'datatable_task') !== false) { ?>
       <!-- <script src="<?php echo base_url(); ?>public/assets/js/pages/crud/metronic-datatable/base/html-table.js" type="text/javascript"></script> -->

       <script type="text/javascript">
var datatable = $('.kt-datatable').KTDatatable({
    layout: {
        scroll: false,
        footer: false,
    },
    sortable: true,
    pagination: true,
    search: {
        input: $('#generalSearch'),
    },
    columns: [{
            field: 'Employee',
            width: 200,
        }, {
            field: 'NIK',
            width: 60,
        },
        {
            field: 'Status',
            width: 80,
        },
        {
            field: 'Mulai',
            width: 120,
        },
        {
            field: 'Sampai',
            width: 120,
        },
        {
            field: 'Location',
            width: 100,
        },
        {
            field: 'Action',
            title: 'Action',
            sortable: false,
            width: 50,
            overflow: 'visible',
            textAlign: 'left',
        }
    ],
});
       </script>





       <?php }
		}
		?>


       <?php 
		if(isset($plugin)) {
			if(strpos($plugin, 'datatable_config_leave') !== false) { ?>
       <!-- <script src="<?php echo base_url(); ?>public/assets/js/pages/crud/metronic-datatable/base/html-table.js" type="text/javascript"></script> -->

       <script type="text/javascript">
var datatable = $('.kt-datatable').KTDatatable({
    layout: {
        scroll: false,
        footer: false,
    },
    sortable: true,
    pagination: true,
    search: {
        input: $('#generalSearch'),
    },
    columns: [{
            field: 'Jenis Cuti',
            width: 100,
        }, {
            field: 'Periode',
            textAlign: 'center',
            width: 100,
        },
        {
            field: 'Nilai Saldo Default',
            width: 100,
            textAlign: 'center',
        },
        {
            field: 'Gender',
            width: 100,
            textAlign: 'left',
        },
        {
            field: 'Action',
            title: 'Action',
            sortable: false,
            width: 50,
            overflow: 'visible',
            textAlign: 'left',
        }
    ],
});
       </script>





       <?php }
		}
		?>

       <?php 
		if(isset($plugin)) {
			if(strpos($plugin, 'datatable_absence') !== false) { ?>
       <!-- <script src="<?php echo base_url(); ?>public/assets/js/pages/crud/metronic-datatable/base/html-table.js" type="text/javascript"></script> -->

       <script type="text/javascript">
var datatable = $('.kt-datatable').KTDatatable({
    layout: {
        scroll: false,
        footer: false,
    },
    sortable: true,
    pagination: true,
    search: {
        input: $('#generalSearch'),
    },
    columns: [{
            field: 'Employee',
            width: 200,
        }, {
            field: 'NIK',
            width: 60,
        },
        {
            field: 'In/Out',
            width: 60,
        },
        {
            field: 'Action',
            title: 'Action',
            sortable: false,
            width: 50,
            overflow: 'visible',
            textAlign: 'left',
        }
    ],
});
       </script>





       <?php }
		}
		?>

       <?php 
		if(isset($plugin)) {
			if(strpos($plugin, 'datatable_emp_attendance') !== false) { ?>

       <script type="text/javascript">
var datatable = $('.kt-datatable').KTDatatable({
    layout: {
        scroll: false,
        footer: false,
    },
    sortable: true,
    pagination: true,
    search: {
        input: $('#generalSearch'),
    },
    columns: [{
            field: 'Employee',
            width: 200,
        }, {
            field: 'NIK',
            width: 60,
        },
        {
            field: 'In/Out',
            width: 40,
        },
    ],
});
       </script>





       <?php }
		}
		?>

       <?php 
		if(isset($plugin)) {
			if(strpos($plugin, 'datatable_claim') !== false) { ?>
       <!-- <script src="<?php echo base_url(); ?>public/assets/js/pages/crud/metronic-datatable/base/html-table.js" type="text/javascript"></script> -->

       <script type="text/javascript">
var datatable = $('.kt-datatable').KTDatatable({
    layout: {
        scroll: false,
        footer: false,
    },
    sortable: true,
    pagination: true,
    search: {
        input: $('#generalSearch'),
    },
    columns: [{
            field: 'Employee',
            width: 200,
        }, {
            field: 'NIK',
            width: 60,
        },
        {
            field: 'Gender',
            width: 60,
        },
        {
            field: 'Employee Status',
            width: 60,
        },
        {
            field: 'Action',
            title: 'Action',
            sortable: false,
            width: 50,
            overflow: 'visible',
            textAlign: 'center',
        }
    ],
});
       </script>

       <?php }
            }
            ?>


       <?php 
		if(isset($plugin)) {
			if(strpos($plugin, 'datatable_bank') !== false) { ?>
       <!-- <script src="<?php echo base_url(); ?>public/assets/js/pages/crud/metronic-datatable/base/html-table.js" type="text/javascript"></script> -->

       <script type="text/javascript">
var datatable = $('.kt-datatable_bank').KTDatatable({
    layout: {
        scroll: false,
        footer: false,
    },
    sortable: true,
    pagination: true,
    search: {
        input: $('#generalSearch'),
    },
    columns: [{
            field: 'Employee',
            width: 150,
        },
        {
            field: 'NIK',
            width: 60,
        },
        {
            field: 'Bank',
            width: 80,
        },
        {
            field: 'Branch',
            width: 150,
        },
        {
            field: 'No Rekening',
            width: 100,
        },
        {
            field: 'Status',
            width: 60,
        },
        {
            field: 'Action',
            title: 'Action',
            sortable: false,
            width: 50,
            overflow: 'visible',
            textAlign: 'center',
        }
    ],
});
       </script>

       <?php }
            }
            ?>


       <?php 
		if(isset($plugin)) {
			if(strpos($plugin, 'datatable_rule') !== false) { ?>
       <!-- <script src="<?php echo base_url(); ?>public/assets/js/pages/crud/metronic-datatable/base/html-table.js" type="text/javascript"></script> -->

       <script type="text/javascript">
var datatable = $('.kt-datatable').KTDatatable({
    layout: {
        scroll: false,
        footer: false,
    },
    sortable: true,
    pagination: true,
    search: {
        input: $('#generalSearch'),
    },
    columns: [{
            field: 'Pembuat',
            width: 200,
        }, {
            field: 'NIK',
            width: 60,
        },
        {
            field: 'Peraturan',
            width: 300,
        },
        {
            field: 'File Rule',
            width: 60,
        },
        {
            field: 'Status',
            width: 60,
        },
        {
            field: 'Action',
            title: 'Action',
            sortable: false,
            width: 50,
            overflow: 'visible',
            textAlign: 'left',
        }
    ],
});
       </script>

       <?php }
            }
            ?>

       <?php 
            if(isset($plugin)) {
                if(strpos($plugin, 'datatable_asset') !== false) { ?>
       <!-- <script src="<?php echo base_url(); ?>public/assets/js/pages/crud/metronic-datatable/base/html-table.js" type="text/javascript"></script> -->

       <script type="text/javascript">
var datatable = $('.kt-datatable').KTDatatable({
    layout: {
        scroll: false,
        footer: false,
    },
    sortable: true,
    pagination: true,
    search: {
        input: $('#generalSearch'),
    },
    columns: [{
            field: 'Nama Barang',
            width: 120,
        }, {
            field: 'Nomor Serial',
            width: 120,
        },
        {
            field: 'Photo',
            width: 50,
        },
        {
            field: 'Spesification',
            width: 150,
        },
        {
            field: 'Periode',
            width: 100,
        },
        {
            field: 'Status',
            width: 60,
        },
        {
            field: 'Action',
            title: 'Action',
            sortable: false,
            width: 50,
            overflow: 'visible',
            textAlign: 'left',
        }
    ],
});
       </script>





       <?php }
                    }
                ?>

       <?php 
                    if(isset($plugin)) {
                        if(strpos($plugin, 'datatable_payroll') !== false) { ?>

       <script type="text/javascript">
var datatable = $('.kt-datatable').KTDatatable({
    layout: {
        scroll: false,
        footer: false,
    },
    sortable: true,
    pagination: true,
    search: {
        input: $('#generalSearch'),
    },
    columns: [{
        field: 'Category',
        width: 400,
    }, {
        field: 'Amount',
        width: 300,
    }, {
        field: 'Action',
        title: 'Action',
        sortable: false,
        width: 50,
        overflow: 'visible',
        textAlign: 'left',
    }],
});
       </script>





       <?php }
                    }
                    ?>

       <?php 
		if(isset($plugin)) {
			if(strpos($plugin, 'datatable_document') !== false) { ?>
       <!-- <script src="<?php echo base_url(); ?>public/assets/js/pages/crud/metronic-datatable/base/html-table.js" type="text/javascript"></script> -->

       <script type="text/javascript">
var datatable = $('.kt-datatable').KTDatatable({
    layout: {
        scroll: false,
        footer: false,
    },
    sortable: true,
    pagination: true,
    search: {
        input: $('#generalSearch'),
    },
    columns: [{
            field: 'Employee',
            width: 200,
        }, {
            field: 'NIK',
            width: 60,
        },
        {
            field: 'Gender',
            width: 60,
        },
        {
            field: 'Employee Status',
            width: 60,
        },
        {
            field: 'Action',
            title: 'Action',
            sortable: false,
            width: 50,
            overflow: 'visible',
            textAlign: 'center',
        }
    ],
});
       </script>

       <?php }
            }
            ?>



       <?php 
		if(isset($plugin)) {
			if(strpos($plugin, 'datatable_income') !== false) { ?>
       <!-- <script src="<?php echo base_url(); ?>public/assets/js/pages/crud/metronic-datatable/base/html-table.js" type="text/javascript"></script> -->

       <script type="text/javascript">
var datatable = $('.kt-datatable').KTDatatable({
    layout: {
        scroll: false,
        footer: false,
    },
    sortable: true,
    pagination: true,
    search: {
        input: $('#generalSearch'),
    },
    columns: [{
            field: 'Employee',
            width: 180,
        }, {
            field: 'NIK',
            width: 60,
        },
        {
            field: 'Amount',
            width: 100,
        },
        {
            field: 'Payrol Date',
            width: 50,
        },
        {
            field: 'Prosess Date',
            width: 50,
        },
        {
            field: 'Action',
            title: 'Action',
            sortable: false,
            width: 50,
            overflow: 'visible',
            textAlign: 'center',
        }
    ],
});
       </script>

       <?php }
            }
            ?>

       <script type="text/javascript">
var yLabels = [];
var rawData = [{
        "id": "March 1",
        "in_time": "08:12:14",
        "out_time": "18:12:14"
    },
    {
        "id": "March 2",
        "in_time": "07:12:14",
        "out_time": "17:12:14"
    },
    {
        "id": "March 3",
        "in_time": "08:12:14",
        "out_time": "18:12:14"
    },
    {
        "id": "March 4",
        "in_time": "07:12:14",
        "out_time": "17:12:14"
    },
    {
        "id": "March 5",
        "in_time": "08:12:14",
        "out_time": "18:12:14"
    },
    {
        "id": "March 6",
        "in_time": "07:12:14",
        "out_time": "17:12:14"
    },
    {
        "id": "March 7",
        "in_time": "08:12:14",
        "out_time": "18:12:14"
    },
    {
        "id": "March 8",
        "in_time": "07:12:14",
        "out_time": "17:12:14"
    },
    {
        "id": "March 9",
        "in_time": "08:12:14",
        "out_time": "18:12:14"
    },
    {
        "id": "March 10",
        "in_time": "07:12:14",
        "out_time": "17:12:14"
    },
];
var data = rawData.map(function(row) {
    yLabels.push('');
    return {
        id: row.id,
        yIn: ((new Date('01/01/2018 ' + row.in_time).getTime()) - (new Date('01/01/2018 00:00:00').getTime())) /
            1000,
        yOut: ((new Date('01/01/2018 ' + row.out_time).getTime()) - (new Date('01/01/2018 00:00:00')
            .getTime())) / 1000
    };
});

new Morris.Line({
    element: "chart-att",
    data: data,
    xkey: 'id',
    ykeys: ['yIn', 'yOut'],
    yLabelFormat: function(y) {
        var date = new Date(new Date('01/01/2018').getTime() + (y * 1000));
        var hour = '00' + date.getHours().toString();
        hour = hour.substr(hour.length - 2);
        var min = '00' + date.getMinutes().toString();
        min = min.substr(min.length - 2);
        var second = '00' + date.getSeconds().toString();
        second = second.substr(second.length - 2);
        return hour + ':' + min;
    },
    // labels :yLabels,
    labels: ["In", "Out"],
    parseTime: false,
    numLines: 12,
    // hideHover: true,
    lineWidth: '6px',
    lineColors: ["#6e4ff5", "#FF3A3A"]
    // stacked: true
})
       </script>

       <script>
$('#scroll1').dataTable({
    language: {
        aria: {
            sortAscending: ": activate to sort column ascending",
            sortDescending: ": activate to sort column descending"
        },
        emptyTable: "No data available in table",
        info: "Showing _START_ to _END_ of _TOTAL_ entries",
        infoEmpty: "No entries found",
        infoFiltered: "(filtered1 from _MAX_ total entries)",
        lengthMenu: "_MENU_ entries",
        search: "Cari Data:",
        zeroRecords: "Data tidak ditemukan"
    },
    buttons: [{
            extend: "excel",
            className: "btn dark btn-outline"
        },
        {
            extend: "pdf",
            className: "btn green btn-outline"
        },
        {
            extend: "csv",
            className: "btn purple btn-outline "
        }
    ],
    responsive: {
        details: {}
    },
    order: [
        [0, "asc"]
    ],
    lengthMenu: [
        [5, 10, 15, 20, -1],
        [5, 10, 15, 20, "All"]
    ],
    scrollX: true,
    pageLength: 10,
    dom: "<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>"
});
$('#scroll2').dataTable({
    language: {
        aria: {
            sortAscending: ": activate to sort column ascending",
            sortDescending: ": activate to sort column descending"
        },
        emptyTable: "No data available in table",
        info: "Showing _START_ to _END_ of _TOTAL_ entries",
        infoEmpty: "No entries found",
        infoFiltered: "(filtered1 from _MAX_ total entries)",
        lengthMenu: "_MENU_ entries",
        search: "Cari Data:",
        zeroRecords: "Data tidak ditemukan"
    },
    buttons: [{
            extend: "excel",
            className: "btn dark btn-outline"
        },
        {
            extend: "pdf",
            className: "btn green btn-outline"
        },
        {
            extend: "csv",
            className: "btn purple btn-outline "
        }
    ],
    responsive: {
        details: {}
    },
    order: [
        [0, "asc"]
    ],
    lengthMenu: [
        [5, 10, 15, 20, -1],
        [5, 10, 15, 20, "All"]
    ],
    scrollX: true,
    pageLength: 10,
    dom: "<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>"
});
$('#scroll3').dataTable({
    language: {
        aria: {
            sortAscending: ": activate to sort column ascending",
            sortDescending: ": activate to sort column descending"
        },
        emptyTable: "No data available in table",
        info: "Showing _START_ to _END_ of _TOTAL_ entries",
        infoEmpty: "No entries found",
        infoFiltered: "(filtered1 from _MAX_ total entries)",
        lengthMenu: "_MENU_ entries",
        search: "Cari Data:",
        zeroRecords: "Data tidak ditemukan"
    },
    buttons: [{
            extend: "excel",
            className: "btn dark btn-outline"
        },
        {
            extend: "pdf",
            className: "btn green btn-outline"
        },
        {
            extend: "csv",
            className: "btn purple btn-outline "
        }
    ],
    responsive: {
        details: {}
    },
    order: [
        [0, "asc"]
    ],
    lengthMenu: [
        [5, 10, 15, 20, -1],
        [5, 10, 15, 20, "All"]
    ],
    scrollX: true,
    pageLength: 10,
    dom: "<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>"
});
$('#scroll4').dataTable({
    language: {
        aria: {
            sortAscending: ": activate to sort column ascending",
            sortDescending: ": activate to sort column descending"
        },
        emptyTable: "No data available in table",
        info: "Showing _START_ to _END_ of _TOTAL_ entries",
        infoEmpty: "No entries found",
        infoFiltered: "(filtered1 from _MAX_ total entries)",
        lengthMenu: "_MENU_ entries",
        search: "Cari Data:",
        zeroRecords: "Data tidak ditemukan"
    },
    buttons: [{
            extend: "excel",
            className: "btn dark btn-outline"
        },
        {
            extend: "pdf",
            className: "btn green btn-outline"
        },
        {
            extend: "csv",
            className: "btn purple btn-outline "
        }
    ],
    responsive: {
        details: {}
    },
    order: [
        [0, "asc"]
    ],
    lengthMenu: [
        [5, 10, 15, 20, -1],
        [5, 10, 15, 20, "All"]
    ],
    scrollX: true,
    pageLength: 10,
    dom: "<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>"
});
$('#scroll5').dataTable({
    language: {
        aria: {
            sortAscending: ": activate to sort column ascending",
            sortDescending: ": activate to sort column descending"
        },
        emptyTable: "No data available in table",
        info: "Showing _START_ to _END_ of _TOTAL_ entries",
        infoEmpty: "No entries found",
        infoFiltered: "(filtered1 from _MAX_ total entries)",
        lengthMenu: "_MENU_ entries",
        search: "Cari Data:",
        zeroRecords: "Data tidak ditemukan"
    },
    buttons: [{
            extend: "excel",
            className: "btn dark btn-outline"
        },
        {
            extend: "pdf",
            className: "btn green btn-outline"
        },
        {
            extend: "csv",
            className: "btn purple btn-outline "
        }
    ],
    responsive: {
        details: {}
    },
    order: [
        [0, "asc"]
    ],
    lengthMenu: [
        [5, 10, 15, 20, -1],
        [5, 10, 15, 20, "All"]
    ],
    scrollX: true,
    pageLength: 10,
    dom: "<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>"
});
$('#scroll6').dataTable({
    language: {
        aria: {
            sortAscending: ": activate to sort column ascending",
            sortDescending: ": activate to sort column descending"
        },
        emptyTable: "No data available in table",
        info: "Showing _START_ to _END_ of _TOTAL_ entries",
        infoEmpty: "No entries found",
        infoFiltered: "(filtered1 from _MAX_ total entries)",
        lengthMenu: "_MENU_ entries",
        search: "Cari Data:",
        zeroRecords: "Data tidak ditemukan"
    },
    buttons: [{
            extend: "excel",
            className: "btn dark btn-outline"
        },
        {
            extend: "pdf",
            className: "btn green btn-outline"
        },
        {
            extend: "csv",
            className: "btn purple btn-outline "
        }
    ],
    responsive: {
        details: {}
    },
    order: [
        [0, "asc"]
    ],
    lengthMenu: [
        [5, 10, 15, 20, -1],
        [5, 10, 15, 20, "All"]
    ],
    scrollX: true,
    pageLength: 10,
    dom: "<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>"
});
$('#scroll7').dataTable({
    language: {
        aria: {
            sortAscending: ": activate to sort column ascending",
            sortDescending: ": activate to sort column descending"
        },
        emptyTable: "No data available in table",
        info: "Showing _START_ to _END_ of _TOTAL_ entries",
        infoEmpty: "No entries found",
        infoFiltered: "(filtered1 from _MAX_ total entries)",
        lengthMenu: "_MENU_ entries",
        search: "Cari Data:",
        zeroRecords: "Data tidak ditemukan"
    },
    buttons: [{
            extend: "excel",
            className: "btn dark btn-outline"
        },
        {
            extend: "pdf",
            className: "btn green btn-outline"
        },
        {
            extend: "csv",
            className: "btn purple btn-outline "
        }
    ],
    responsive: {
        details: {}
    },
    order: [
        [0, "asc"]
    ],
    lengthMenu: [
        [5, 10, 15, 20, -1],
        [5, 10, 15, 20, "All"]
    ],
    scrollX: true,
    pageLength: 10,
    dom: "<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>"
});
$('#scroll8').dataTable({
    language: {
        aria: {
            sortAscending: ": activate to sort column ascending",
            sortDescending: ": activate to sort column descending"
        },
        emptyTable: "No data available in table",
        info: "Showing _START_ to _END_ of _TOTAL_ entries",
        infoEmpty: "No entries found",
        infoFiltered: "(filtered1 from _MAX_ total entries)",
        lengthMenu: "_MENU_ entries",
        search: "Cari Data:",
        zeroRecords: "Data tidak ditemukan"
    },
    buttons: [{
            extend: "excel",
            className: "btn dark btn-outline"
        },
        {
            extend: "pdf",
            className: "btn green btn-outline"
        },
        {
            extend: "csv",
            className: "btn purple btn-outline "
        }
    ],
    responsive: {
        details: {}
    },
    order: [
        [0, "asc"]
    ],
    lengthMenu: [
        [5, 10, 15, 20, -1],
        [5, 10, 15, 20, "All"]
    ],
    scrollX: true,
    pageLength: 10,
    dom: "<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>"
});
$('#scroll9').dataTable({
    language: {
        aria: {
            sortAscending: ": activate to sort column ascending",
            sortDescending: ": activate to sort column descending"
        },
        emptyTable: "No data available in table",
        info: "Showing _START_ to _END_ of _TOTAL_ entries",
        infoEmpty: "No entries found",
        infoFiltered: "(filtered1 from _MAX_ total entries)",
        lengthMenu: "_MENU_ entries",
        search: "Cari Data:",
        zeroRecords: "Data tidak ditemukan"
    },
    buttons: [{
            extend: "excel",
            className: "btn dark btn-outline"
        },
        {
            extend: "pdf",
            className: "btn green btn-outline"
        },
        {
            extend: "csv",
            className: "btn purple btn-outline "
        }
    ],
    responsive: {
        details: {}
    },
    order: [
        [0, "asc"]
    ],
    lengthMenu: [
        [5, 10, 15, 20, -1],
        [5, 10, 15, 20, "All"]
    ],
    scrollX: true,
    pageLength: 10,
    dom: "<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>"
});
$('#scroll10').dataTable({
    language: {
        aria: {
            sortAscending: ": activate to sort column ascending",
            sortDescending: ": activate to sort column descending"
        },
        emptyTable: "No data available in table",
        info: "Showing _START_ to _END_ of _TOTAL_ entries",
        infoEmpty: "No entries found",
        infoFiltered: "(filtered1 from _MAX_ total entries)",
        lengthMenu: "_MENU_ entries",
        search: "Cari Data:",
        zeroRecords: "Data tidak ditemukan"
    },
    buttons: [{
            extend: "excel",
            className: "btn dark btn-outline"
        },
        {
            extend: "pdf",
            className: "btn green btn-outline"
        },
        {
            extend: "csv",
            className: "btn purple btn-outline "
        }
    ],
    responsive: {
        details: {}
    },
    order: [
        [0, "asc"]
    ],
    lengthMenu: [
        [5, 10, 15, 20, -1],
        [5, 10, 15, 20, "All"]
    ],
    scrollX: true,
    pageLength: 10,
    dom: "<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>"
});
       </script>

       <script type="text/javascript">
var TableDatatablesResponsive = function() {
    var e = function() {
        var e = $("#tbl_cust");
        e.dataTable({
            language: {
                aria: {
                    sortAscending: ": activate to sort column ascending",
                    sortDescending: ": activate to sort column descending"
                },
                emptyTable: "No data available in table",
                info: "Showing _START_ to _END_ of _TOTAL_ entries",
                infoEmpty: "No entries found",
                infoFiltered: "(filtered1 from _MAX_ total entries)",
                lengthMenu: "_MENU_ entries",
                search: "Cari Data:",
                zeroRecords: "Data tidak ditemukan"
            },
            buttons: [{
                    extend: "excel",
                    className: "btn dark btn-outline"
                },
                {
                    extend: "pdf",
                    className: "btn green btn-outline"
                },
                {
                    extend: "csv",
                    className: "btn purple btn-outline "
                }
            ],
            responsive: {
                details: {}
            },
            order: [
                [0, "asc"]
            ],
            bSort: false,
            bPaginate: false,
            paging: false,
            lengthMenu: [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],
            scrollX: true,
            pageLength: 10,
            dom: "<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>"
        })
    };

    n = function() {
            var n = $("#tbl_cust3");
            n.dataTable({
                language: {
                    aria: {
                        sortAscending: "",
                        sortDescending: ""
                    },
                    emptyTable: "No data available in table",
                    info: "Showing _START_ to _END_ of _TOTAL_ entries",
                    infoEmpty: "No entries found",
                    infoFiltered: "(filtered1 from _MAX_ total entries)",
                    lengthMenu: "_MENU_ entries",
                    search: "Search:",
                    zeroRecords: "No matching records found"
                },
                buttons: [{
                        extend: "excel",
                        className: "btn dark btn-outline"
                    },
                    {
                        extend: "pdf",
                        className: "btn green btn-outline"
                    },
                    {
                        extend: "csv",
                        className: "btn purple btn-outline "
                    }
                ],
                responsive: {
                    details: {}
                },
                order: [
                    [5, "desc"]
                ],
                lengthMenu: [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],
                scrollX: true,
                pageLength: 10,
                dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
            })
        },

        t = function() {
            var t = $("#tbl_list_default2");
            t.dataTable({
                language: {
                    aria: {
                        sortAscending: ": activate to sort column ascending",
                        sortDescending: ": activate to sort column descending"
                    },
                    emptyTable: "No data available in table",
                    info: "Showing _START_ to _END_ of _TOTAL_ entries",
                    infoEmpty: "No entries found",
                    infoFiltered: "(filtered1 from _MAX_ total entries)",
                    lengthMenu: "_MENU_ entries",
                    search: "Cari Data:",
                    zeroRecords: "Data tidak ditemukan"
                },
                buttons: [{
                        extend: "excel",
                        className: "btn dark btn-outline"
                    },
                    {
                        extend: "pdf",
                        className: "btn green btn-outline"
                    },
                    {
                        extend: "csv",
                        className: "btn purple btn-outline "
                    }
                ],
                responsive: {
                    details: {}
                },
                order: [
                    [0, "asc"]
                ],
                lengthMenu: [
                    [5, 10, 15, 20, -1],
                    [5, 10, 15, 20, "All"]
                ],
                pageLength: 10,
                dom: "<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>"
            })
        };

    xt = function() {
        var t = $("#tbl_list_default56");
        t.dataTable({
            language: {
                aria: {
                    sortAscending: ": activate to sort column ascending",
                    sortDescending: ": activate to sort column descending"
                },
                emptyTable: "No data available in table",
                info: "Showing _START_ to _END_ of _TOTAL_ entries",
                infoEmpty: "No entries found",
                infoFiltered: "(filtered1 from _MAX_ total entries)",
                lengthMenu: "_MENU_ entries",
                search: "Cari Data:",
                zeroRecords: "Data tidak ditemukan"
            },
            buttons: [
                // {
                //     extend: "excel",
                //     className: "btn dark btn-outline"
                // },
                // {
                //     extend: "pdf",
                //     className: "btn green btn-outline"
                // },
                // {
                //     extend: "csv",
                //     className: "btn purple btn-outline "
                // }
            ],
            responsive: {
                details: {}
            },
            order: [
                [0, "asc"]
            ],
            lengthMenu: [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],
            pageLength: 10,
            dom: "<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>"
        })
    };

    o = function() {
        var o = $("#tbl_list_default");
        o.dataTable({
            language: {
                aria: {
                    sortAscending: ": activate to sort column ascending",
                    sortDescending: ": activate to sort column descending"
                },
                emptyTable: "No data available in table",
                info: "Showing _START_ to _END_ of _TOTAL_ entries",
                infoEmpty: "No entries found",
                infoFiltered: "(filtered1 from _MAX_ total entries)",
                lengthMenu: "_MENU_ entries",
                search: "Cari Data:",
                zeroRecords: "Data tidak ditemukan"
            },
            buttons: [{
                    extend: "excel",
                    className: "btn dark btn-outline"
                },
                {
                    extend: "pdf",
                    className: "btn green btn-outline"
                },
                {
                    extend: "csv",
                    className: "btn purple btn-outline "
                }
            ],
            responsive: {
                details: {}
            },
            order: [
                [0, "asc"]
            ],
            lengthMenu: [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],
            scrollX: true,
            pageLength: 10,
            dom: "<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>"
        })
    };

    p = function() {
        var p = $("#tbl_list_default3");
        p.dataTable({
            language: {
                aria: {
                    sortAscending: ": activate to sort column ascending",
                    sortDescending: ": activate to sort column descending"
                },
                emptyTable: "No data available in table",
                info: "Showing _START_ to _END_ of _TOTAL_ entries",
                infoEmpty: "No entries found",
                infoFiltered: "(filtered1 from _MAX_ total entries)",
                lengthMenu: "_MENU_ entries",
                search: "Cari Data:",
                zeroRecords: "Data tidak ditemukan"
            },
            buttons: [{
                    extend: "excel",
                    className: "btn dark btn-outline"
                },
                {
                    extend: "pdf",
                    className: "btn green btn-outline"
                },
                {
                    extend: "csv",
                    className: "btn purple btn-outline "
                }
            ],
            responsive: {
                details: {}
            },
            order: [
                [0, "asc"]
            ],
            scrollX: true,
            lengthMenu: [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"]
            ],
            pageLength: 10,
            dom: "<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>"
        })
    };

    return {
        init: function() {
            jQuery().dataTable && (e(), t(), n(), p(), o(), xt())
        }
    }
}();
jQuery(document).ready(function() {
    TableDatatablesResponsive.init()
});
       </script>

       <script type="text/javascript">
$(function() {

    $('#member').on('change', function() {
        var selected = $(this).val();

        $.ajax({
            type: 'post',
            url: "<?php echo site_url('Rekening/get_spc_rek_member'); ?>",
            data: {
                message: selected
            },
            success: function(html) {
                $('#rek').html(html);
                $('#rek').selectpicker('refresh');
            }
        });
    });
});

$(function() {

    $('#membere').on('change', function() {
        var selected = $(this).val();

        $.ajax({
            type: 'post',
            url: "<?php echo site_url('Rekening/get_spc_rek_member'); ?>",
            data: {
                message: selected
            },
            success: function(html) {
                $('#reke').html(html);
                $('#reke').selectpicker('refresh');
            }
        });
    });
});


$(function() {

    $('#memberee').on('change', function() {
        var selected = $(this).val();

        $.ajax({
            type: 'post',
            url: "<?php echo site_url('Rekening/get_spc_rek_member'); ?>",
            data: {
                message: selected
            },
            success: function(html) {
                $('#rekee').html(html);
                $('#rekee').selectpicker('refresh');
            }
        });
    });
});

$(function() {

    $('#province').on('change', function() {
        var selected = $(this).val();

        $.ajax({
            type: 'post',
            url: "<?php echo site_url('regency/get_spc_city_form'); ?>",
            data: {
                message: selected
            },
            success: function(html) {
                $('#city').html(html);
                $('#city').selectpicker('refresh');
            }
        });
    });
});

$(function() {

    $('#city').on('change', function() {
        var selected = $(this).val();

        $.ajax({
            type: 'post',
            url: "<?php echo site_url('regency/get_spc_kec_form'); ?>",
            data: {
                message: selected
            },
            success: function(html) {
                $('#kec').html(html);
                $('#kec').selectpicker('refresh');
            }
        });
    });
});

$(function() {

    $('#kec').on('change', function() {
        var selected = $(this).val();

        $.ajax({
            type: 'post',
            url: "<?php echo site_url('regency/get_spc_des_form'); ?>",
            data: {
                message: selected
            },
            success: function(html) {
                $('#des').html(html);
                $('#des').selectpicker('refresh');
            }
        });
    });
});
       </script>

       <script type="text/javascript">
$("#year_form_dashboard").datepicker({
    format: "yyyy",
    viewMode: "years",
    minViewMode: "years",
    startView: 'decade',
    minView: 'decade',
    viewSelect: 'decade',
    autoclose: true,
});

$("#year_form_dashboard_leave").datepicker({
    format: "yyyy",
    viewMode: "years",
    minViewMode: "years",
    startView: 'decade',
    minView: 'decade',
    viewSelect: 'decade',
    autoclose: true,
});

$("#month_form_dashboard").datepicker({
    format: "mm",
    viewMode: "months",
    minViewMode: "months",
    startView: 'months',
    minView: 'months',
    viewSelect: 'months',
    autoclose: true,
    maxViewMode: 0,
});
       </script>

       <script type=text/javascript>
let val = "";
if (document.getElementById("pick_draft")) {
    val = document.getElementById("pick_draft").value;
}
var arr = val.split("|");
let flag = arr[1]
if (flag == '1' || flag == 1) {
    $("#f1").show()
    $("#f2").show()
    $("#f3").show()
} else {
    $("#f1").hide()
    $("#f2").hide()
    $("#f3").hide()
}
       </script>

       <script>
$("#prdTgl").hide()

function getValue(radio) {

    if (radio.value == 1 || radio.value == "1") {
        $("#prdTgl").hide()
    } else {
        $("#prdTgl").show()
    }

}
       </script>

       <script type=text/javascript>
$('#pick_draft').on('change', (event) => {
    let values = event.target.value
    var arr = values.split("|");
    let flag = arr[1]
    if (flag == '1' || flag == 1) {
        $("#f1").show()
        $("#f2").show()
        $("#f3").show()
    } else {
        $("#f1").hide()
        $("#f2").hide()
        $("#f3").hide()
    }
});

// $(function() {
//     var availableTags = [
//         "ActionScript",
//         "AppleScript",
//         "Asp",
//         "BASIC",
//         "C",
//         "C++",
//         "Clojure",
//         "COBOL",
//         "ColdFusion",
//         "Erlang",
//         "Fortran",
//         "Groovy",
//         "Haskell",
//         "Java",
//         "JavaScript",
//         "Lisp",
//         "Perl",
//         "PHP",
//         "Python",
//         "Ruby",
//         "Scala",
//         "Scheme"
//     ];
//     $("#tambahNama").autocomplete({
//         source: availableTags
//     });
// });
       </script>

       <script type=text/javascript>
function next() {
    let value = parseInt(localStorage.getItem('create-wizard-simulate'));
    // alert(value);

    //mape
    var draf = document.getElementById("draf").value;
    var fl = document.getElementById("fl").value;
    var regresi = document.getElementById("regresi").value;
    var forecast = document.getElementById("forecast").value;
    var start = document.getElementById("dateStartMape").value;
    var end = document.getElementById("dateEndMape").value;

    //ecl
    var eperiode = document.getElementById("eperiode").value;
    var elgd = document.getElementById("elgd").value;
    var enormal = document.getElementById("enormal").value;
    var eoptimis = document.getElementById("eoptimis").value;
    var epesimis = document.getElementById("epesimis").value;
    var escalling = document.getElementById("escalling").value;

    var flagChange = $("input[name='flagChange']:checked").val();

    if (value == 0) {

        if (draf == null || regresi == null) {
            return alert('Pilih Model');
        }

        if (flagChange == '1') {

            if (start == null || end == null || start == "" || end == "") {
                return alert('Lengkapi Tanggal Mape');
            }

            if (fl == null || fl == '0') {
                return alert('Pilih Model');
            }

            if (forecast == null || forecast == "") {
                return alert('Pilih Periode Forecast');
            }

            $("#tittle_simulate_konfirmasi_model").text("Konfirmasi Model Simulate dan Model FL Terpilih");

        } else {
            $("#tittle_simulate_konfirmasi_model").text("Konfirmasi Model Simulate");
        }

        //INI API BUAT DAPETIN DRAFT BUCKET -> ada 9 , araging , dll
        draft_bucket = $.ajax({
            data: {
                idb: draf
            },
            type: "POST",
            url: "<?php echo site_url('Model/get_draft_bucket');?>",
            async: false
        }).responseText;
        let db = JSON.parse(draft_bucket);

        $('#tbl_ar').html(db.tbl_ar);
        $('#rollrate_belum_normal').html(db.rollrate_belum_normal);
        $('#rollrate_normal').html(db.rollrate_normal);
        $('#average_rollrate_lossrate').html(db.average_rollrate_lossrate);
        $('#rollrate_normalisasi_average').html(db.rollrate_normalisasi_average);
        $('#lossrate').html(db.lossrate);
        $('#moving_average').html(db.moving_average);
        $('#sum_delta_lossrate').html(db.sum_delta_lossrate);
        $('#odr').html(db.odr);
        $('#terpilih').html(db.terpilih);
        $('#terpilih2').html(db.terpilih2);

        //INI API BUAT DAPETIN MODEL FL NYA -> var1 , var2 , koef , dll
        if (flagChange == '1') {
            model_fl = $.ajax({
                data: {
                    ifl: fl
                },
                type: "POST",
                url: "<?php echo site_url('Model/get_model_fl');?>",
                async: false
            }).responseText;
            let mfl = JSON.parse(model_fl);
            let table_fl =
                "<tr>" +
                "<td>" + mfl.data.nomor + "</td>" +
                "<td>" + mfl.data.var1 + "</td>" +
                "<td>" + mfl.data.var2 + "</td>" +
                "<td>" + mfl.data.lag1 + "</td>" +
                "<td>" + mfl.data.lag2 + "</td>" +
                "<td>" + mfl.data.c_coef + "</td>" +
                "<td>" + mfl.data.koef_var1 + "</td>" +
                "<td>" + mfl.data.koef_var2 + "</td>" +
                "</tr>";
            $('#table_model_fl').html(table_fl);
            $("#div_model_fl").show();
            $("#div_scalling_factor").show();
        } else {
            $("#div_model_fl").hide();
            $("#div_scalling_factor").hide();
        }

        // maping tampilin ke html.
        localStorage.setItem('create-wizard-simulate', value + 1);
        $("#nav-1").attr("data-ktwizard-state", "current");
        $("#content-0").attr("data-ktwizard-state", "");
        $("#content-1").attr("data-ktwizard-state", "current");


    } else if (value == 1) {

        //INI API BUAT DAPETIN MAPE NYA
        if (flagChange == '1') {

            mape = $.ajax({
                data: {
                    id_draft_bucket: draf,
                    id_model: fl,
                    id_regresi: regresi,
                    periode_forecast: forecast,
                    start_date: start,
                    end_date: end
                },
                type: "POST",
                url: "<?php echo site_url('Model/get_mape');?>",
                async: false
            }).responseText;
            console.log(mape);
            let mp = JSON.parse(mape);

            if (mp.mape == "" || mp.mape == null) {
                alert(mp.mape_message);
                return;
            }

            if (mp.status != 1 || mp.status != '1') {
                alert(mp.mape_message);
                return;
            }

            $('#model_mape').html(mp.model_mape);
            $('#list_mape').html(mp.list_mape);
            $('#model_mape_terpilih').html(mp.model_mape_terpilih);
            $('#mape_headers').html(mp.mape_header);
            $('#average_mape').val(mp.average_mape);

            localStorage.setItem('create-wizard-simulate', value + 1);
            $("#nav-2").attr("data-ktwizard-state", "current");
            $("#content-1").attr("data-ktwizard-state", "");
            $("#content-2").attr("data-ktwizard-state", "current");

        } else {
            //karena ini no model longkap langsung ke pengaturan ECL
            localStorage.setItem('create-wizard-simulate', value + 2);
            $("#nav-2").attr("data-ktwizard-state", "current");
            $("#nav-3").attr("data-ktwizard-state", "current");
            $("#content-1").attr("data-ktwizard-state", "");
            $("#content-2").attr("data-ktwizard-state", "");
            $("#content-3").attr("data-ktwizard-state", "current");

        }

    } else if (value == 2) {

        localStorage.setItem('create-wizard-simulate', value + 1);
        $("#nav-3").attr("data-ktwizard-state", "current");
        $("#content-2").attr("data-ktwizard-state", "");
        $("#content-3").attr("data-ktwizard-state", "current");
        // }
    } else if (value == 3) {

        if (eperiode == null || eperiode == "") {
            return alert('Masukan tanggal periode');
        }

        if (flagChange != '1') {
            fl = "0";
            forecast = "0";
            escalling = "0";
        }

        ecls = $.ajax({
            data: {
                id_draft_bucket: draf,
                id_model: fl,
                id_regresi: regresi,
                periode_forecast: forecast,
                periode: eperiode,
                lgd: elgd,
                normal: enormal,
                optimis: eoptimis,
                pesimis: epesimis,
                scaling: escalling,
            },
            type: "POST",
            url: "<?php echo site_url('Model/get_ecl');?>",
            async: false
        }).responseText;
        console.log(ecls);
        let ecl = JSON.parse(ecls);

        if (ecl.no_modal_list == "" || ecl.no_modal_list == null) {
            alert(ecl.ecl_message);
        }

        $('#no_modal_list').html(ecl.no_modal_list);
        $('#model_title').html(ecl.model_tittle);
        $('#modal_list').html(ecl.modal_list);
        $('#proporsional_list').html(ecl.proporsional_list);
        $('#solver_list').html(ecl.solver_list);
        $('#skenario').html(ecl.skenario);
        $('#model_psak71_ecl').html(ecl.psak71);
        $('#model_mevs_ecl').html(ecl.Mevs);
        $('#model_mevs_solver').html(ecl.model_mevs_solver);
        $('#model_scaling_target').html(ecl.model_scaling_target);
        $('#table_model_jurnal').html(ecl.table_model_jurnal);
        $('#jurnal_periode_ecl').val(ecl.jurnal_periode_ecl);
        $('#jurnal_model_pilihan').html(ecl.jurnal_model_pilihan);

        $('#ecl_no_model').val(ecl.ecl_no_model);
        $('#ecl_proporsional').val(ecl.ecl_proporsional);
        $('#ecl_solver').val(ecl.ecl_solver);

        localStorage.setItem('create-wizard-simulate', value + 1);
        $("#nav-4").attr("data-ktwizard-state", "current");
        $("#content-3").attr("data-ktwizard-state", "");
        $("#content-4").attr("data-ktwizard-state", "current");
        $("#ecl_simulate_next").hide();
        $('#ecl_simulate_submit').show();

        if (flagChange == '0') {
            $("#result_with_model").hide();
        } else {
            $("#result_with_model").show();
        }

    } else if (value == 4) {

        localStorage.setItem('create-wizard-simulate', value + 1);
        $("#nav-5").attr("data-ktwizard-state", "current");
        $("#content-4").attr("data-ktwizard-state", "");
        $("#content-5").attr("data-ktwizard-state", "current");
        $("#ecl_simulate_next").hide();

    } else {
        console.log("tinggal di submit");
    }
}

function prosesJurnal() {
    $('#title_jurnal_terpilih').show();
    let periode = document.getElementById('jurnal_periode_ecl').value;
    let saldo = document.getElementById('jurnal_saldo_ecl').value;
    let pilihan = document.getElementById('jurnal_model_pilihan').value;

    console.log("periode: " + periode);
    console.log("saldo: " + saldo);
    console.log("pilihan: " + pilihan);

    jurnal = $.ajax({
        data: {
            periode: periode,
            saldo: saldo,
            pilihan: pilihan,
        },
        type: "POST",
        url: "<?php echo site_url('Model/get_jurnal_modal');?>",
        async: false
    }).responseText;
    console.log("jurnal: " + jurnal);
    $('#table_penerapan_awal').html(jurnal);
}

function back() {
    let value = parseInt(localStorage.getItem('create-wizard-simulate'));
    var flagChange = $("input[name='flagChange']:checked").val();
    // alert(value);
    if (value > 0) {
        value = value - 1;
        localStorage.setItem('create-wizard-simulate', value);
        if (value == 0) {

            $("#nav-0").attr("data-ktwizard-state", "current");
            $("#content-0").attr("data-ktwizard-state", "current");
            $("#nav-1").attr("data-ktwizard-state", "");
            $("#content-1").attr("data-ktwizard-state", "");

        } else if (value == 1) {

            $("#nav-1").attr("data-ktwizard-state", "current");
            $("#content-1").attr("data-ktwizard-state", "current");
            $("#nav-2").attr("data-ktwizard-state", "");
            $("#content-2").attr("data-ktwizard-state", "");

        } else if (value == 2) {

            if (flagChange == '1') {

                $("#nav-2").attr("data-ktwizard-state", "current");
                $("#content-2").attr("data-ktwizard-state", "current");
                $("#nav-3").attr("data-ktwizard-state", "");
                $("#content-3").attr("data-ktwizard-state", "");
                $('#ecl_simulate_submit').hide();

            } else {

                value = value - 1;
                localStorage.setItem('create-wizard-simulate', value);
                $("#nav-1").attr("data-ktwizard-state", "current");
                $("#content-1").attr("data-ktwizard-state", "current");
                $("#nav-2").attr("data-ktwizard-state", "");
                $("#nav-3").attr("data-ktwizard-state", "");
                $("#content-3").attr("data-ktwizard-state", "");
                $("#content-2").attr("data-ktwizard-state", "");

            }



        } else if (value == 3) {

            $("#nav-3").attr("data-ktwizard-state", "current");
            $("#content-3").attr("data-ktwizard-state", "current");
            $("#nav-4").attr("data-ktwizard-state", "");
            $("#content-4").attr("data-ktwizard-state", "");
            $('#ecl_simulate_submit').hide();
            $("#ecl_simulate_next").show();

        } else if (value == 4) {

            $("#nav-4").attr("data-ktwizard-state", "current");
            $("#content-4").attr("data-ktwizard-state", "current");
            $("#nav-5").attr("data-ktwizard-state", "");
            $("#content-5").attr("data-ktwizard-state", "");
            $("#ecl_simulate_next").show();
            $('#ecl_simulate_submit').hide();

        }
    }
}

function nextVesicek() {
    let value = parseInt(localStorage.getItem('create-wizard-simulate'));

    //config
    var interval = document.getElementById("interval").value;
    var normal = document.getElementById("persenNormal").value;
    var upturn = document.getElementById("persenUpturn").value;
    var downturn = document.getElementById("persenDownturn").value;
    var start = document.getElementById("dateStart").value;
    var end = document.getElementById("dateStart2").value;
    start = start.split('/')[1] + '/' + start.split('/')[0] + '/' + start.split('/')[2];
    let d_start = new Date(start);
    d_start.setDate(d_start.getDate() - 1);
    var dd = d_start.getDate();
    var mm = d_start.getMonth() + 1;
    var yyyy = d_start.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    start = dd + '/' + mm + '/' + yyyy;

    var normal = document.getElementById("persenNormal").value;
    var upturn = document.getElementById("persenUpturn").value;
    var downturn = document.getElementById("persenDownturn").value;

    var a = parseInt(normal)
    var b = parseInt(upturn)
    var c = parseInt(downturn)

    var count = a + b + c;

    //lgd
    var lgd = document.getElementById("lgd").value;

    //eperiode
    var eperiode = document.getElementById("eperiode").value;


    //aset

    if (start == "" || end == "") {
        alert('Lengkapi Form yang kosong');
    } else {
        if (count != 100) {
            alert('Jumlah persentase didapat dari penjumlahan normal, up turn, dan down turn wajib 100');
        } else {
            if (value == 0) {
                localStorage.setItem('create-wizard-simulate', value + 1);
                $("#nav-1").attr("data-ktwizard-state", "current");
                $("#content-0").attr("data-ktwizard-state", "");
                $("#content-1").attr("data-ktwizard-state", "current");
            } else if (value == 1) {
                var data = [];
                var myTab = document.getElementById('empTable');
                var arrValues = new Array();


                if (eperiode == "") {
                    alert('Isi Form Periode');
                } else {

                    if (myTab.rows.length - 1 == 0) {
                        alert('Isi Form Aset');
                    } else {
                        //
                        for (row = 1; row < myTab.rows.length - 1; row++) {

                            var arrayItem = {};
                            for (c = 0; c < myTab.rows[row].cells.length; c++) {
                                var element = myTab.rows.item(row).cells[c];
                                if (element.childNodes[0].getAttribute('type') == 'text') {
                                    var headers = ['id_aset', 'id_bank', 'no_rekening', 'mata_uang', 'suku_bunga',
                                        'tgl_jatuh_tempo', 'saldo'
                                    ]
                                    if (element.childNodes[0].value != "" || element.childNodes[0].value != null) {
                                        arrayItem[headers[c]] = element.childNodes[0].value;
                                    }
                                }
                            }

                            if (arrayItem.id_aset || arrayItem.id_bank || arrayItem.no_rekening || arrayItem
                                .no_rekening || arrayItem.suku_bunga || arrayItem.tgl_jatuh_tempo || arrayItem.saldo) {
                                data.push(arrayItem);
                            }
                        }





                        var datas = {
                            'interval': interval,
                            'start_date': start,
                            'end_date': end,
                            'persen_normal': normal,
                            'persen_upturn': upturn,
                            'persen_downturn': downturn,
                            'list_aset_ecl': data,
                            'lgd': lgd,
                            'periode_ecl': eperiode,
                        };


                        values = $.ajax({
                            data: {
                                data: datas
                            },
                            type: "POST",
                            url: "<?php echo site_url('Vasicek/getDataTes');?>",
                            async: false
                        }).responseText;
                        let val = JSON.parse(values);

                        if (val.status == 3) {
                            alert('Langkapi Forn yang masih kosong ');
                        } else {
                            $('#tablePersen').html(val.dataPersen);
                            $('#tableMicro').html(val.dataMicro);
                            $('#tableHistory').html(val.dataHistory);

                            localStorage.setItem('create-wizard-simulate', value + 1);
                            $("#nav-2").attr("data-ktwizard-state", "current");
                            $("#content-1").attr("data-ktwizard-state", "");
                            $("#content-2").attr("data-ktwizard-state", "current");

                        }


                        // 


                    }

                }

            } else if (value == 2) {
                var data = [];
                var myTab = document.getElementById('empTable');
                var arrValues = new Array();


                if (eperiode == "") {
                    alert('Isi Form Periode');
                } else {

                    if (myTab.rows.length - 1 == 0) {
                        alert('Isi Form Aset');
                    } else {
                        //
                        for (row = 1; row < myTab.rows.length - 1; row++) {

                            var arrayItem = {};
                            for (c = 0; c < myTab.rows[row].cells.length; c++) {
                                var element = myTab.rows.item(row).cells[c];
                                if (element.childNodes[0].getAttribute('type') == 'text') {
                                    var headers = ['id_aset', 'id_bank', 'no_rekening', 'mata_uang', 'suku_bunga',
                                        'tgl_jatuh_tempo', 'saldo'
                                    ]
                                    if (element.childNodes[0].value != "" || element.childNodes[0].value != null) {
                                        arrayItem[headers[c]] = element.childNodes[0].value;
                                    }
                                }
                            }


                            if (arrayItem.id_aset || arrayItem.id_bank || arrayItem.no_rekening || arrayItem
                                .no_rekening || arrayItem.suku_bunga || arrayItem.tgl_jatuh_tempo || arrayItem.saldo) {
                                data.push(arrayItem);
                            }
                        }


                        var datas = {
                            'interval': interval,
                            'start_date': start,
                            'end_date': end,
                            'persen_normal': normal,
                            'persen_upturn': upturn,
                            'persen_downturn': downturn,
                            'list_aset_ecl': data,
                            'lgd': lgd,
                            'periode_ecl': eperiode,
                        };

                        values = $.ajax({
                            data: {
                                data: datas
                            },
                            type: "POST",
                            url: "<?php echo site_url('Vasicek/getDataTes');?>",
                            async: false
                        }).responseText;
                        let val = JSON.parse(values);
                        console.log(val);

                        $('#listPD').html(val.dataPD);

                        localStorage.setItem('create-wizard-simulate', value + 1);
                        $("#nav-3").attr("data-ktwizard-state", "current");
                        $("#content-2").attr("data-ktwizard-state", "");
                        $("#content-3").attr("data-ktwizard-state", "current");

                    }

                }
            } else if (value == 3) {
                var data = [];
                var myTab = document.getElementById('empTable');
                var arrValues = new Array();


                if (eperiode == "") {
                    alert('Isi Form Periode');
                } else {

                    if (myTab.rows.length - 1 == 0) {
                        alert('Isi Form Aset');
                    } else {
                        //
                        for (row = 1; row < myTab.rows.length - 1; row++) {

                            var arrayItem = {};
                            for (c = 0; c < myTab.rows[row].cells.length; c++) {
                                var element = myTab.rows.item(row).cells[c];
                                if (element.childNodes[0].getAttribute('type') == 'text') {
                                    var headers = ['id_aset', 'id_bank', 'no_rekening', 'mata_uang', 'suku_bunga',
                                        'tgl_jatuh_tempo', 'saldo'
                                    ]
                                    if (element.childNodes[0].value != "" || element.childNodes[0].value != null) {
                                        arrayItem[headers[c]] = element.childNodes[0].value;
                                    }
                                }
                            }

                            if (arrayItem.id_aset || arrayItem.id_bank || arrayItem.no_rekening || arrayItem
                                .no_rekening || arrayItem.suku_bunga || arrayItem.tgl_jatuh_tempo || arrayItem.saldo) {
                                data.push(arrayItem);
                            }
                        }
                        var datas = {
                            'interval': interval,
                            'start_date': start,
                            'end_date': end,
                            'persen_normal': normal,
                            'persen_upturn': upturn,
                            'persen_downturn': downturn,
                            'list_aset_ecl': data,
                            'lgd': lgd,
                            'periode_ecl': eperiode,
                        };

                        values = $.ajax({
                            data: {
                                data: datas
                            },
                            type: "POST",
                            url: "<?php echo site_url('Vasicek/getDataTes');?>",
                            async: false
                        }).responseText;
                        let val = JSON.parse(values);
                        $('#eclperiode').html(val.dataPeriode);
                        $('#listKertas').html(val.dataKertas);

                        localStorage.setItem('create-wizard-simulate', value + 1);
                        $("#nav-4").attr("data-ktwizard-state", "current");
                        $("#content-3").attr("data-ktwizard-state", "");
                        $("#content-4").attr("data-ktwizard-state", "current");
                        //
                    }

                }
            } else if (value == 4) {
                var data = [];
                var myTab = document.getElementById('empTable');
                var arrValues = new Array();


                if (eperiode == "") {
                    alert('Isi Form Periode');
                } else {

                    if (myTab.rows.length - 1 == 0) {
                        alert('Isi Form Aset');
                    } else {
                        //
                        for (row = 1; row < myTab.rows.length - 1; row++) {

                            var arrayItem = {};
                            for (c = 0; c < myTab.rows[row].cells.length; c++) {
                                var element = myTab.rows.item(row).cells[c];
                                if (element.childNodes[0].getAttribute('type') == 'text') {
                                    var headers = ['id_aset', 'id_bank', 'no_rekening', 'mata_uang', 'suku_bunga',
                                        'tgl_jatuh_tempo', 'saldo'
                                    ]
                                    if (element.childNodes[0].value != "" || element.childNodes[0].value != null) {
                                        arrayItem[headers[c]] = element.childNodes[0].value;
                                    }
                                }
                            }

                            if (arrayItem.id_aset || arrayItem.id_bank || arrayItem.no_rekening || arrayItem
                                .no_rekening || arrayItem.suku_bunga || arrayItem.tgl_jatuh_tempo || arrayItem.saldo) {
                                data.push(arrayItem);
                            }
                        }
                        var datas = {
                            'interval': interval,
                            'start_date': start,
                            'end_date': end,
                            'persen_normal': normal,
                            'persen_upturn': upturn,
                            'persen_downturn': downturn,
                            'list_aset_ecl': data,
                            'lgd': lgd,
                            'periode_ecl': eperiode,
                        };

                        values = $.ajax({
                            data: {
                                data: datas
                            },
                            type: "POST",
                            url: "<?php echo site_url('Vasicek/getDataTes');?>",
                            async: false
                        }).responseText;
                        let val = JSON.parse(values);
                        $('#ecl1').html(val.dataECL1);
                        $('#ecl2').html(val.dataECL2);
                        $('#ecl3').html(val.dataECL3);
                        $('#listECL').html(val.dataECL);

                        localStorage.setItem('create-wizard-simulate', value + 1);
                        $("#nav-5").attr("data-ktwizard-state", "current");
                        $("#content-4").attr("data-ktwizard-state", "");
                        $("#content-5").attr("data-ktwizard-state", "current");
                        //
                        $("#vasicek_simulate_next").hide();
                    }

                }
            }
        }
    }

}

function nextVesicek2() { //hanif buat
    let value = parseInt(localStorage.getItem('create-wizard-simulate'));

    //config
    var draft = document.getElementById("draft").value;
    var interval = document.getElementById("interval").value;
    var normal = document.getElementById("persenNormal").value;
    var upturn = document.getElementById("persenUpturn").value;
    var downturn = document.getElementById("persenDownturn").value;
    var start = document.getElementById("dateStart").value;
    var end = document.getElementById("dateStart2").value;
    start = start.split('/')[1] + '/' + start.split('/')[0] + '/' + start.split('/')[2];
    let d_start = new Date(start);
    d_start.setDate(d_start.getDate() - 1);
    var dd = d_start.getDate();
    var mm = d_start.getMonth() + 1;
    var yyyy = d_start.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    start = dd + '/' + mm + '/' + yyyy;

    var normal = document.getElementById("persenNormal").value;
    var upturn = document.getElementById("persenUpturn").value;
    var downturn = document.getElementById("persenDownturn").value;
    var a = parseInt(normal)
    var b = parseInt(upturn)
    var c = parseInt(downturn)
    var count = a + b + c;
    //lgd
    var lgd = document.getElementById("lgd").value;
    //eperiode
    var eperiode = document.getElementById("eperiode").value;

    if (value == 0) {

        if (start == "" || end == "") {
            return alert('Lengkapi Form yang kosong');
        }
        if (count != 100) {
            return alert('Jumlah persentase didapat dari penjumlahan normal, up turn, dan down turn wajib 100');
        }

        localStorage.setItem('create-wizard-simulate', value + 1);
        $("#nav-1").attr("data-ktwizard-state", "current");
        $("#content-0").attr("data-ktwizard-state", "");
        $("#content-1").attr("data-ktwizard-state", "current");

    } else if (value == 1) {

        if (lgd == "" || lgd == "0") {
            lgd = "45"; //default lgd
        }
        if (eperiode == "") {
            return alert('Isi Form Periode');
        }

        var datas = {
            'id_vasicek_draft': draft,
            'interval': interval,
            'start_date': start,
            'end_date': end,
            'persen_normal': normal,
            'persen_upturn': upturn,
            'persen_downturn': downturn,
            'list_aset_ecl': data,
            'lgd': lgd,
            'periode_ecl': eperiode,
            'save': '0',
        };

        values = $.ajax({
            data: {
                data: datas
            },
            type: "POST",
            url: "<?php echo site_url('Vasicek/getDataTes');?>",
            async: false
        }).responseText;
        console.log(values);
        let val = JSON.parse(values);

        if (val.status == 3) {
            alert('Ada kesalahan Server. Lengkapi Form yang masih kosong');
        } else {
            $('#tablePersen').html(val.dataPersen);
            $('#tableMicro').html(val.dataMicro);
            $('#tableHistory').html(val.dataHistory);
            $('#listPD').html(val.dataPD);
            $('#eclperiode').html(val.dataPeriode);
            $('#listKertas').html(val.dataKertas);
            $('#ecl1').html(val.dataECL1);
            $('#ecl2').html(val.dataECL2);
            $('#ecl3').html(val.dataECL3);
            $('#listECL').html(val.dataECL);

            localStorage.setItem('create-wizard-simulate', value + 1);
            $("#nav-2").attr("data-ktwizard-state", "current");
            $("#content-1").attr("data-ktwizard-state", "");
            $("#content-2").attr("data-ktwizard-state", "current");
        }

    } else if (value == 2) {

        // var datas = {
        //     'interval': interval,
        //     'start_date': start,
        //     'end_date': end,
        //     'persen_normal': normal,
        //     'persen_upturn': upturn,
        //     'persen_downturn': downturn,
        //     'list_aset_ecl': data,
        //     'lgd': lgd,
        //     'periode_ecl': eperiode,
        // };

        // values = $.ajax({
        //     data: {
        //         data: datas
        //     },
        //     type: "POST",
        //     url: "<?php echo site_url('Vasicek/getDataTes');?>",
        //     async: false
        // }).responseText;
        // let val = JSON.parse(values);
        // console.log(val);

        // $('#listPD').html(val.dataPD);

        localStorage.setItem('create-wizard-simulate', value + 1);
        $("#nav-3").attr("data-ktwizard-state", "current");
        $("#content-2").attr("data-ktwizard-state", "");
        $("#content-3").attr("data-ktwizard-state", "current");

    } else if (value == 3) {
        // var datas = {
        //     'interval': interval,
        //     'start_date': start,
        //     'end_date': end,
        //     'persen_normal': normal,
        //     'persen_upturn': upturn,
        //     'persen_downturn': downturn,
        //     'list_aset_ecl': data,
        //     'lgd': lgd,
        //     'periode_ecl': eperiode,
        // };

        // values = $.ajax({
        //     data: {
        //         data: datas
        //     },
        //     type: "POST",
        //     url: "<?php echo site_url('Vasicek/getDataTes');?>",
        //     async: false
        // }).responseText;
        // let val = JSON.parse(values);
        // $('#eclperiode').html(val.dataPeriode);
        // $('#listKertas').html(val.dataKertas);

        localStorage.setItem('create-wizard-simulate', value + 1);
        $("#nav-4").attr("data-ktwizard-state", "current");
        $("#content-3").attr("data-ktwizard-state", "");
        $("#content-4").attr("data-ktwizard-state", "current");

    } else if (value == 4) {

        // var datas = {
        //     'interval': interval,
        //     'start_date': start,
        //     'end_date': end,
        //     'persen_normal': normal,
        //     'persen_upturn': upturn,
        //     'persen_downturn': downturn,
        //     'list_aset_ecl': data,
        //     'lgd': lgd,
        //     'periode_ecl': eperiode,
        // };

        // values = $.ajax({
        //     data: {
        //         data: datas
        //     },
        //     type: "POST",
        //     url: "<?php echo site_url('Vasicek/getDataTes');?>",
        //     async: false
        // }).responseText;
        // let val = JSON.parse(values);
        // $('#ecl1').html(val.dataECL1);
        // $('#ecl2').html(val.dataECL2);
        // $('#ecl3').html(val.dataECL3);
        // $('#listECL').html(val.dataECL);

        localStorage.setItem('create-wizard-simulate', value + 1);
        $("#nav-5").attr("data-ktwizard-state", "current");
        $("#content-4").attr("data-ktwizard-state", "");
        $("#content-5").attr("data-ktwizard-state", "current");
        //
        $("#vasicek_simulate_next").hide();
        $("#ecl_simulate_submit").show();

    }


}

function backVesicek() {
    // alert('back');
    let value = parseInt(localStorage.getItem('create-wizard-simulate'));
    // alert(value);
    if (value > 0) {
        value = value - 1;
        localStorage.setItem('create-wizard-simulate', value);
        if (value == 0) {
            $("#nav-0").attr("data-ktwizard-state", "current");
            $("#content-0").attr("data-ktwizard-state", "current");
            $("#nav-1").attr("data-ktwizard-state", "");
            $("#content-1").attr("data-ktwizard-state", "");
        } else if (value == 1) {

            $("#nav-1").attr("data-ktwizard-state", "current");
            $("#content-1").attr("data-ktwizard-state", "current");
            $("#nav-2").attr("data-ktwizard-state", "");
            $("#content-2").attr("data-ktwizard-state", "");

        } else if (value == 2) {

            $("#nav-2").attr("data-ktwizard-state", "current");
            $("#content-2").attr("data-ktwizard-state", "current");
            $("#nav-3").attr("data-ktwizard-state", "");
            $("#content-3").attr("data-ktwizard-state", "");

        } else if (value == 3) {

            $("#nav-3").attr("data-ktwizard-state", "current");
            $("#content-3").attr("data-ktwizard-state", "current");
            $("#nav-4").attr("data-ktwizard-state", "");
            $("#content-4").attr("data-ktwizard-state", "");
            $("#vasicek_simulate_next").show();

        } else if (value == 4) {

            $("#nav-4").attr("data-ktwizard-state", "current");
            $("#content-4").attr("data-ktwizard-state", "current");
            $("#nav-5").attr("data-ktwizard-state", "");
            $("#content-5").attr("data-ktwizard-state", "");
            $("#vasicek_simulate_next").show();
            $("#ecl_simulate_submit").hide();

        }
    }

}

function ExportMicro() {


    //model Persentase
    var data1 = [];
    var headers1 = [];
    $('#tablePersen th').each(function(index, item) {
        headers1[index] = $(item).html();
    });
    $('#tablePersen tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers1[index]] = $(item).html();
        });
        data1.push(arrayItem);
    });



    //model Micro
    var data2 = [];
    var headers2 = [];
    $('#tableMicro th').each(function(index, item) {
        headers2[index] = $(item).html();
    });
    $('#tableMicro tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers2[index]] = $(item).html();
        });
        data2.push(arrayItem);
    });

    //model History
    var data3 = [];
    var headers3 = [];
    $('#tableHistory th').each(function(index, item) {
        headers3[index] = $(item).html();
    });
    $('#tableHistory tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers3[index]] = $(item).html();
        });
        data3.push(arrayItem);
    });

    var opts = [{
            sheetid: 'Model Persentase',
            header: true
        },
        {
            sheetid: 'Model Microeconomi',
            header: false
        },
        {
            sheetid: 'Model History',
            header: false
        }
    ];

    var res = alasql('SELECT INTO XLSX("ReportMicro.xlsx",?) FROM ?', [opts, [data1, data2, data3]]);
}

function ExportPD() {


    //model PD
    var data1 = [];
    var headers1 = [];
    $('#tablePD th').each(function(index, item) {
        headers1[index] = $(item).html();
    });
    $('#tablePD tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers1[index]] = $(item).html();
        });
        data1.push(arrayItem);
    });

    var opts = [{
        sheetid: 'Model PD',
        header: true
    }, ];

    var res = alasql('SELECT INTO XLSX("ReportPD.xlsx",?) FROM ?', [opts, [data1]]);
}

function ExportKertas() {


    //model Kertas
    var data1 = [];
    var headers1 = [];
    $('#tableKertas th').each(function(index, item) {
        headers1[index] = $(item).html();
    });
    $('#tableKertas tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers1[index]] = $(item).html();
        });
        data1.push(arrayItem);
    });

    var opts = [{
        sheetid: 'Model Kertas Kerja ECL',
        header: true
    }, ];

    var res = alasql('SELECT INTO XLSX("ReportKertasKerjaEcl.xlsx",?) FROM ?', [opts, [data1]]);
}

function ExportSumECL() {

    //model ECL
    var data1 = [];
    var headers1 = [];
    $('#tableECL th').each(function(index, item) {
        headers1[index] = $(item).html();
    });
    $('#tableECL tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers1[index]] = $(item).html();
        });
        data1.push(arrayItem);
    });



    var opts = [{
        sheetid: 'Model Summary ECL',
        header: true
    }, ];

    var res = alasql('SELECT INTO XLSX("ReportSummaryEcl.xlsx",?) FROM ?', [opts, [data1]]);
}

function ExportJurnalEcl() {


    //model Persentase
    var data1 = [];
    var headers1 = [];
    $('#modalJurnalEcl th').each(function(index, item) {
        headers1[index] = $(item).html();
    });
    $('#modalJurnalEcl tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers1[index]] = $(item).html();
        });
        data1.push(arrayItem);
    });



    //model Micro
    var data2 = [];
    var headers2 = [];
    $('#modalPSAK71 th').each(function(index, item) {
        headers2[index] = $(item).html();
    });
    $('#modalPSAK71 tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers2[index]] = $(item).html();
        });
        data2.push(arrayItem);
    });



    var opts = [{
            sheetid: 'Model Jurnal Ecl',
            header: true
        },
        {
            sheetid: 'Model PSAK 71',
            header: false
        }
    ];

    var res = alasql('SELECT INTO XLSX("ReportJurnalEcl.xlsx",?) FROM ?', [opts, [data1, data2]]);
}

function ExportDrafBucket() {


    //model araging
    var data1 = [];
    var headers1 = [];
    $('#draf_ar th').each(function(index, item) {
        headers1[index] = $(item).html();
    });
    $('#draf_ar tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers1[index]] = $(item).html();
        });
        data1.push(arrayItem);
    });



    //model Micro
    var data2 = [];
    var headers2 = [];
    $('#draf_rollrate_bn th').each(function(index, item) {
        headers2[index] = $(item).html();
    });
    $('#draf_rollrate_bn tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers2[index]] = $(item).html();
        });
        data2.push(arrayItem);
    });

    //
    var data3 = [];
    var headers3 = [];
    $('#draf_rollrate_n th').each(function(index, item) {
        headers3[index] = $(item).html();
    });
    $('#draf_rollrate_bn tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers3[index]] = $(item).html();
        });
        data3.push(arrayItem);
    });

    //
    var data4 = [];
    var headers4 = [];
    $('#draf_average th').each(function(index, item) {
        headers4[index] = $(item).html();
    });
    $('#draf_average tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers4[index]] = $(item).html();
        });
        data4.push(arrayItem);
    });

    //
    var data5 = [];
    var headers5 = [];
    $('#draf_rollrate_na th').each(function(index, item) {
        headers5[index] = $(item).html();
    });
    $('#draf_rollrate_na tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers5[index]] = $(item).html();
        });
        data5.push(arrayItem);
    });

    //
    var data6 = [];
    var headers6 = [];
    $('#draf_lossrate th').each(function(index, item) {
        headers6[index] = $(item).html();
    });
    $('#draf_lossrate tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers6[index]] = $(item).html();
        });
        data6.push(arrayItem);
    });

    //
    var data7 = [];
    var headers7 = [];
    $('#draf_moving_avg th').each(function(index, item) {
        headers7[index] = $(item).html();
    });
    $('#draf_moving_avg tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers7[index]] = $(item).html();
        });
        data7.push(arrayItem);
    });

    //
    var data8 = [];
    var headers8 = [];
    $('#draf_sum_delta th').each(function(index, item) {
        headers8[index] = $(item).html();
    });
    $('#draf_sum_delta tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers8[index]] = $(item).html();
        });
        data8.push(arrayItem);
    });

    //
    var data9 = [];
    var headers9 = [];
    $('#draf_odr th').each(function(index, item) {
        headers9[index] = $(item).html();
    });
    $('#draf_odr tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers9[index]] = $(item).html();
        });
        data9.push(arrayItem);
    });



    var opts = [{
            sheetid: 'Model Araging',
            header: true
        },
        {
            sheetid: 'Model Rollrate Bukan Normal',
            header: false
        },
        {
            sheetid: 'Model Rollrate Normal',
            header: false
        },
        {
            sheetid: 'Model Average',
            header: false
        },
        {
            sheetid: 'Model Normalisasi Average',
            header: false
        },
        {
            sheetid: 'Model Lossrate',
            header: false
        },
        {
            sheetid: 'Model Moving Average',
            header: false
        },
        {
            sheetid: 'Model Sum Delta',
            header: false
        },
        {
            sheetid: 'Model Odr',
            header: false
        }
    ];

    var res = alasql('SELECT INTO XLSX("ReportDrafBucket.xlsx",?) FROM ?', [opts, [data1, data2, data3, data4, data5,
        data6, data7, data8, data9
    ]]);
}





$(document).ready(function() {
    console.log("this running");
    localStorage.setItem('create-wizard-simulate', 0);
});
       </script>

       <script>
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}
arrHead = ['Jenis Aset', 'Bank', 'No Rek', 'Mata Uang', 'Suku Bunga (%)', 'Jatuh Tempo', 'Saldo', 'Action'];

function addRow() {
    var empTab = document.getElementById('empTable');
    var rowCnt = empTab.rows.length; // get the number of rows.
    // console.log('row', rowCnt);
    var tr = empTab.insertRow(rowCnt); // table row.
    tr = empTab.insertRow(rowCnt);


    asets = $.ajax({
        type: "GET",
        url: "<?php echo site_url('Vasicek/getAsetAll');?>",
        async: false
    }).responseText;
    let aset = JSON.parse(asets);

    banks = $.ajax({
        type: "GET",
        url: "<?php echo site_url('Vasicek/getBankAll');?>",
        async: false
    }).responseText;
    let bank = JSON.parse(banks);

    let countasset = aset.data;
    let countbank = bank.data;

    if (countasset == 0 || countbank == 0) {
        alert('data kosong')
    } else {

        //
        for (var c = 0; c < arrHead.length; c++) {
            var td = document.createElement('td'); // TABLE DEFINITION.
            td = tr.insertCell(c);



            if (c == 7) { // if its the first column of the table.
                // add a button control.
                var button = document.createElement('button');

                // set the attributes.
                button.setAttribute('type', 'button');
                button.setAttribute('class', "btn btn-sm btn-danger teratur-button");

                //icon
                var icon = document.createElement('i');
                icon.setAttribute('class', "flaticon2-cancel-music");
                button.appendChild(icon);

                // add button's "onclick" event.
                button.setAttribute('onclick', 'removeRow(this)');
                td.appendChild(button);
            } else if (c == 0) {

                var arr = aset.data
                // console.log('result arr', arr);

                var sel = document.createElement("select");
                sel.setAttribute('class', 'form-control');
                arr.map((item, index) => {
                    var idx = index + 1;
                    var opt = "opt" + 1;
                    console.log(opt);
                    var opt = document.createElement("option");
                    opt.value = item.id_vas_aset;
                    opt.text = item.nama_aset;
                    sel.add(opt, null);
                    sel.setAttribute('type', 'text');
                    td.appendChild(sel);
                });
            } else if (c == 1) {
                var arr = bank.data
                // console.log('result arr', arr);

                var sel = document.createElement("select");
                sel.setAttribute('class', 'form-control');
                arr.map((item, index) => {
                    var idx = index + 1;
                    var opt = "opt" + 1;
                    console.log(opt);
                    var opt = document.createElement("option");
                    opt.value = item.id_vas_bank;
                    opt.text = item.nama_bank;
                    sel.add(opt, null);
                    sel.setAttribute('type', 'text');
                    td.appendChild(sel);
                });
            } else if (c == 2) {
                var sel = document.createElement("input");
                sel.setAttribute('class', 'form-control');
                sel.setAttribute('onkeypress', "return isNumberKey(event)");
                sel.setAttribute('type', 'text');
                sel.setAttribute('required', '');
                td.appendChild(sel);
            } else if (c == 3) {
                var arr = [{
                        "title": "Indonesia Rupiah",
                        "value": "IDR"
                    },
                    {
                        "title": "US Dollar",
                        "value": "USD"
                    }
                ];

                var sel = document.createElement("select");
                sel.setAttribute('class', 'form-control');
                arr.map((item, index) => {
                    var idx = index + 1;
                    var opt = "opt" + 1;
                    console.log(opt);
                    var opt = document.createElement("option");
                    opt.value = item.value;
                    opt.text = item.title;
                    sel.add(opt, null);
                    sel.setAttribute('type', 'text');
                    td.appendChild(sel);
                });
            } else if (c == 4) {
                var sel = document.createElement("input");
                sel.setAttribute('class', 'form-control');
                sel.setAttribute('onkeypress', "return isNumberKey(event)");
                sel.setAttribute('type', 'text');
                sel.setAttribute('required', '');
                td.appendChild(sel);
            } else if (c == 5) {
                var idx = 'jatuhtempo' + rowCnt;
                var idxx = '#jatuhtempo' + rowCnt;
                var sel = document.createElement("input");
                sel.setAttribute('class', 'form-control');
                sel.setAttribute('id', idx);
                sel.setAttribute('required', '');
                sel.setAttribute('type', 'text');
                td.appendChild(sel);

                $(idxx).datepicker({
                    format: 'dd/mm/yyyy',
                    autoclose: true,
                });
            } else if (c == 6) {
                var sel = document.createElement("input");
                sel.setAttribute('class', 'form-control');
                sel.setAttribute('onkeypress', "return isNumberKey(event)");
                sel.setAttribute('type', 'text');
                sel.setAttribute('required', '');
                td.appendChild(sel);
            } else {
                var arr = [{
                        "title": "option satu",
                        "value": "1"
                    },
                    {
                        "title": "option dua",
                        "value": "2"
                    }
                ];

                var sel = document.createElement("select");
                sel.setAttribute('class', 'form-control');
                arr.map((item, index) => {
                    var idx = index + 1;
                    var opt = "opt" + 1;
                    console.log(opt);
                    var opt = document.createElement("option");
                    opt.value = item.value;
                    opt.text = item.title;
                    sel.add(opt, null);
                    sel.setAttribute('type', 'text');
                    td.appendChild(sel);
                });
            }
        }

        //

    }

}

function removeRow(oButton) {
    var empTab = document.getElementById('empTable');
    empTab.deleteRow(oButton.parentNode.parentNode.rowIndex); // buttton -> td -> tr
}
       </script>

       <script>
function ExportBucket() {



    //model FL
    var data1 = [];
    var headers1 = [];
    $('#table_model_fl_bucket th').each(function(index, item) {
        headers1[index] = $(item).html();
    });
    $('#table_model_fl_bucket tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers1[index]] = $(item).html();
        });
        data1.push(arrayItem);
    });



    //Araging
    var data2 = [];
    var headers2 = [];
    $('#tbl_ar th').each(function(index, item) {
        headers2[index] = $(item).html();
    });
    $('#tbl_ar tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers2[index]] = $(item).html();
        });
        data2.push(arrayItem);
    });

    //RollRate Belum Noraml
    var data3 = [];
    var headers3 = [];
    $('#rollrate_belum_normal th').each(function(index, item) {
        headers3[index] = $(item).html();
    });
    $('#rollrate_belum_normal tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers3[index]] = $(item).html();
        });
        data3.push(arrayItem);
    });

    //RollRate Noraml
    var data4 = [];
    var headers4 = [];
    $('#rollrate_normal th').each(function(index, item) {
        headers4[index] = $(item).html();
    });
    $('#rollrate_normal tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers4[index]] = $(item).html();
        });
        data4.push(arrayItem);
    });

    //Average
    var data5 = [];
    var headers5 = [];
    $('#average_rollrate_lossrate th').each(function(index, item) {
        headers5[index] = $(item).html();
    });
    $('#average_rollrate_lossrate tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers5[index]] = $(item).html();
        });
        data5.push(arrayItem);
    });

    //Roll Rate Normalisasi Average 
    var data6 = [];
    var headers6 = [];
    $('#rollrate_normalisasi_average th').each(function(index, item) {
        headers6[index] = $(item).html();
    });
    $('#rollrate_normalisasi_average tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers6[index]] = $(item).html();
        });
        data6.push(arrayItem);
    });

    //Loass Rate
    var data7 = [];
    var headers7 = [];
    $('#lossrate th').each(function(index, item) {
        headers7[index] = $(item).html();
    });
    $('#lossrate tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers7[index]] = $(item).html();
        });
        data7.push(arrayItem);
    });

    //Moving Average
    var data8 = [];
    var headers8 = [];
    $('#moving_average th').each(function(index, item) {
        headers8[index] = $(item).html();
    });
    $('#moving_average tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers8[index]] = $(item).html();
        });
        data8.push(arrayItem);
    });

    //Moving Average
    var data9 = [];
    var headers9 = [];
    $('#sum_delta_lossrate th').each(function(index, item) {
        headers9[index] = $(item).html();
    });
    $('#sum_delta_lossrate tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers9[index]] = $(item).html();
        });
        data9.push(arrayItem);
    });

    //ODR
    var data10 = [];
    var headers10 = [];
    $('#odr th').each(function(index, item) {
        headers10[index] = $(item).html();
    });
    $('#odr tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers10[index]] = $(item).html();
        });
        data10.push(arrayItem);
    });

    var opts = [{
            sheetid: 'Model Fl',
            header: true
        },
        {
            sheetid: 'Araging',
            header: false
        },
        {
            sheetid: 'Roll Rate Belum Normalisasi',
            header: false
        },
        {
            sheetid: 'Roll Rate Normalisasi',
            header: false
        },
        {
            sheetid: 'Average',
            header: false
        },
        {
            sheetid: 'Roll Rate Normalisasi Average',
            header: false
        },
        {
            sheetid: 'Loss Rate',
            header: false
        },
        {
            sheetid: 'Moving Average',
            header: false
        },
        {
            sheetid: 'Sum Delta',
            header: false
        },
        {
            sheetid: 'ODR',
            header: false
        }
    ];
    var res = alasql('SELECT INTO XLSX("ReportBucket.xlsx",?) FROM ?', [opts, [data1, data2, data3, data4, data5, data6,
        data7, data8, data9, data10
    ]]);
}

function ExportMAPE() {


    //model FL
    var data1 = [];
    var headers1 = [];
    $('#model_fl_mape th').each(function(index, item) {
        headers1[index] = $(item).html();
    });
    $('#model_fl_mape tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers1[index]] = $(item).html();
        });
        data1.push(arrayItem);
    });



    //model MAPE
    var data2 = [];
    var headers2 = [];
    $('#table_mape2 th').each(function(index, item) {
        headers2[index] = $(item).html();
    });
    $('#table_mape2 tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers2[index]] = $(item).html();
        });
        data2.push(arrayItem);
    });

    var opts = [{
            sheetid: 'Model Fl',
            header: true
        },
        {
            sheetid: 'Model MAPE',
            header: false
        }
    ];

    var res = alasql('SELECT INTO XLSX("ReportMAPE.xlsx",?) FROM ?', [opts, [data1, data2]]);
}

function ExportECL() {


    var data1 = [];
    var headers1 = [];
    $('#nomodel_ecl th').each(function(index, item) {
        headers1[index] = $(item).html();
    });
    $('#nomodel_ecl tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers1[index]] = $(item).html();
        });
        data1.push(arrayItem);
    });

    console.log("data1 : ", data1);

    //Model
    var data2 = [];
    var headers2 = [];
    $('#model_ecl_fl th').each(function(index, item) {
        headers2[index] = $(item).html();
    });
    $('#model_ecl_fl tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers2[index]] = $(item).html();
        });
        data2.push(arrayItem);
    });

    console.log("data2 : ", data2);

    //Proposional
    var data3 = [];
    var headers3 = [];
    $('#model_proposional_ecl th').each(function(index, item) {
        headers3[index] = $(item).html();
    });
    $('#model_proposional_ecl tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers3[index]] = $(item).html();
        });
        data3.push(arrayItem);
    });

    console.log("data3 : ", data3);

    //Solver
    var data4 = [];
    var headers4 = [];
    $('#model_solver_ecl th').each(function(index, item) {
        headers4[index] = $(item).html();
    });
    $('#model_solver_ecl tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers4[index]] = $(item).html();
        });
        data4.push(arrayItem);
    });

    console.log("data4 : ", data4);

    //model FL
    var data5 = [];
    var headers5 = [];
    $('#model_skenario_ecl th').each(function(index, item) {
        headers5[index] = $(item).html();
    });
    $('#model_skenario_ecl tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers5[index]] = $(item).html();
        });
        data5.push(arrayItem);
    });

    //model FL
    var data6 = [];
    var headers6 = [];
    $('#model_psak71_ecl th').each(function(index, item) {
        headers6[index] = $(item).html();
    });
    $('#model_psak71_ecl tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers6[index]] = $(item).html();
        });
        data6.push(arrayItem);
    });

    //model FL
    var data7 = [];
    var headers7 = [];
    $('#model_mevs_ecl th').each(function(index, item) {
        headers7[index] = $(item).html();
    });
    $('#model_mevs_ecl tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers7[index]] = $(item).html();
        });
        data7.push(arrayItem);
    });

    console.log("data5 : ", data5);

    var opts = [{
            sheetid: 'No Model',
            header: true
        },
        {
            sheetid: 'Model',
            header: false
        },
        {
            sheetid: 'Model Proposional',
            header: false
        },
        {
            sheetid: 'Model Solver',
            header: false
        },
        {
            sheetid: 'Model Skenario',
            header: false
        },
        {
            sheetid: 'Model PSAK 71',
            header: false
        },
        {
            sheetid: 'Model Effect',
            header: false
        },

    ];

    var res = alasql('SELECT INTO XLSX("ReportECL.xlsx",?) FROM ?', [opts, [data1, data2, data3, data4, data5, data6,
        data7
    ]]);
}
       </script>
       <script>
$("#loading_screen").hide();
       </script>

       </body>

       </html>