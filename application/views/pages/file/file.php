<style>
table {
    table-layout: fixed;
}

td {
    overflow: hidden;
    text-overflow: ellipsis;
}
</style>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Daftar File </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-file-1"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            File </a>

                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-container   kt-grid__item--fluid">
            <?php if ( $data->status == '1' || $data->status == 1 ) { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Berhasil!&nbsp;&nbsp;</strong><?php echo "Jumlah File : ".count($data->data); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } else { ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Perhatian!&nbsp;&nbsp;</strong> <?php echo "Tidak ada File"; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } ?>
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                <strong>Info!&nbsp;&nbsp;</strong><?php echo "Menghapus data file tidak mempengaruhi data yang sudah di impor."; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">

                    </div>

                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">

                            </div>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">


                    <table class="table table-striped table-bordered table-hover" id="tbl_list_default56" width="100%">
                        <thead>
                            <tr>
                                <th class="all" width=10px>#</th>
                                <th class="all" width=100px>Nama File</th>
                                <!-- <th class="min-tablet" width=30>Tipe Folder</th> -->
                                <th class="min-tablet" width=100px>Path File</th>
                                <th class="min-tablet" width=30px>Size File </th>
                                <th class="min-tablet" width=50>Tipe File </th>
                                <th class="min-tablet" width=60>Tanggal Upload</th>
                                <th class="min-tablet" width=90px>Action </th>
                            </tr>
                        </thead>
                        <tbody class="kt-datatable__body">
                            <?php foreach ($data->data as $key => $file): ?>
                            <tr>
                                <td><?php echo ++$key; ?></td>
                                <td><?php echo $file->true_name; ?></td>
                                <!-- <td><?php echo $file->type_folder; ?></td> -->
                                <td><?php echo $file->path_name; ?></td>
                                <td><?php 
                                $size = $file->size_file / 1024 ;
                                echo round($size,2)." KB"; ?></td>
                                <td><?php echo $file->type_file; ?></td>
                                <td><?php echo date("d M Y H:i:s", strtotime($file->tanggal_upload)); ?></td>
                                <td>
                                    <a onclick='return del_confirm()'
                                        href="<?php echo site_url('file/delete/'.$file->id_upload); ?>">
                                        <button type="button" class="btn btn-sm btn-danger teratur-button"><i
                                                class="flaticon2-trash"></i>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>