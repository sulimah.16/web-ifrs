<style>
table {
    table-layout: fixed;
}

td {
    overflow: hidden;
    text-overflow: ellipsis;
}
</style>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Daftar Assets </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon-open-box"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                        Assets </a>

                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container   kt-grid__item--fluid">
            <?php if ( $data->status == '1' || $data->status == 1 ) { ?>

            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Success!&nbsp;</strong> <?php echo $data->message; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <?php } else { ?>

            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Warning!&nbsp;</strong> <?php echo $data->message; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <?php } ?>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        
                    </div>

                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <?php if ( $this->session->userdata('role') == 'RL002' || $this->session->userdata('role') == 'RL005' ) { ?>
                                <a href="#" class="btn btn-brand btn-icon-sm teratur-button" data-toggle="modal"
                                    data-target="#add_assets">
                                    <i class="flaticon-plus"></i>
                                    Tambah
                                </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">

                  
                    <table class="table table-striped table-bordered table-hover" id="tbl_list_default" width="100%">
                        <thead>
                            <tr>
                                <th class="all" width=10px>#</th>
                                <th class="min-tablet" width=100px>Nama Assets</th>
                                <th class="min-tablet" width=80px>Merk</th>
                                <th class="min-tablet" width=80px>Kode</th>
                                <th class="min-tablet" width=80px>No Seri</th>
                                <th class="min-tablet" width=80p>Jumlah</th>
                                <th class="min-tablet" width=100px>Harga Beli</th>
                                <th class="min-tablet" width=100px>Keternagan</th>
                                <th class="min-tablet" width=80p>Kondisi</th>

                                <th class="none">Tanggal</th>
                                <th class="none">Tahun</th>
                                <th class="none">Ukuran</th>
                                <th class="none">Bahan</th>
                                <th class="none">Tipe</th>
                                <th class="none">Kode Sistem</th>
                                <?php if ( $this->session->userdata('role') == 'RL002' || $this->session->userdata('role') == 'RL005' ) { ?>
                                <th class="min-tablet" width=50px>Action</th>
                                <?php } ?>

                                
                            </tr>
                        </thead>
                        <tbody class="kt-datatable__body">
                            <?php $i = 1; foreach( $data->data as $d ): ?>
                            <tr>
                                <td><?php echo $i; $i++;  ?></td>
                                <td><?php echo $d->nama_asset; ?></td>
                                <td><?php echo $d->merk; ?></td>
                                <td><?php echo $d->kode_asset_manual; ?></td>
                                <td><?php echo $d->no_seri; ?></td>
                                <td><?php echo $d->jumlah; ?></td>
                                <td><?php echo  number_format($d->harga_beli,2,",","."); ?></td>
                                <td><?php echo $d->keterangan; ?></td>
                                <td><?php echo $d->kondisi; ?></td>
                                <td><?php echo date("d F Y", strtotime("$d->tanggal_masuk ")) ?></td>
                                <td><?php echo $d->tahun; ?></td>
                                <td><?php echo $d->ukuran; ?></td>
                                <td><?php echo $d->bahan; ?></td>
                                <td><?php if ($d->id_type_asset == "1" ) { echo "Barang Berwujud";}else{echo "Barang Tidak Berwujud";}; ?></td>
                                <td><?php echo $d->kode_asset_sistem; ?></td>
                                <?php if ( $this->session->userdata('role') == 'RL002' || $this->session->userdata('role') == 'RL005' ) { ?>
                                <td>
                                    <div class="dropdown">
                                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                            data-toggle="dropdown">
                                            <i class="la la-cog"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-sm">
                                            <ul class="kt-nav">
                                                <li class="kt-nav__item">
                                                    <a href="<?php echo site_url("#"); ?>"
                                                        data-toggle="modal"
                                                        data-target="#edit_assets"
                                                        onClick="return EditAssets(
                                                            '<?php echo $d->id_asset; ?>',
                                                            '<?php echo $d->nama_asset; ?>',
                                                            '<?php echo $d->kode_asset_manual; ?>',
                                                            '<?php echo $d->bahan; ?>',
                                                            '<?php echo $d->ukuran; ?>',
                                                            '<?php echo $d->jumlah; ?>',
                                                            '<?php echo $d->id_type_asset; ?>',
                                                            '<?php echo $d->kondisi; ?>',
                                                            '<?php echo $d->merk; ?>',
                                                            '<?php echo $d->no_seri; ?>',
                                                            '<?php echo $d->harga_beli; ?>',
                                                            '<?php echo $d->keterangan; ?>',
                                                            '<?php echo $d->tanggal_masuk; ?>',
                                                            '<?php echo $d->tahun; ?>',
                                                        )"
                                                        class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon-edit"></i>
                                                        <span class="kt-nav__link-text">Edit Data</span>
                                                    </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <a href="<?php echo site_url("assets/delete/$d->id_asset"); ?>"
                                                        class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                        <span class="kt-nav__link-text">Delete Data</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                                <?php } ?>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add_assets" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Assets</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('assets/create'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Foto Asset</label>
                                    <div class="col-lg-9 col-xl-9">
                                        <div class="kt-avatar kt-avatar--outline" id="kt_user_avatar">
                                            <div class="kt-avatar__holder" id="avatar_image_add"
                                                style="background-image: url(/metronic/themes/metronic/theme/default/demo9/dist/assets/media/users/100_13.jpg)">
                                            </div>
                                            <label class="kt-avatar__upload" data-toggle="kt-tooltip" title=""
                                                data-original-title="Change avatar">
                                                <i class="fa fa-pen"></i>
                                                <input type="file" name="add_image" id="add_image"
                                                    accept=".png, .jpg, .jpeg" onchange="readURLAdd(this);">
                                            </label>
                                            <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title=""
                                                data-original-title="Cancel avatar">
                                                <i class="fa fa-times"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nama Assets</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_assets" name="assets"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Kode Barang</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_kode" name="kode"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Bahan</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_bahan" name="bahan"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Ukuran</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_ukuran" name="ukuran"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Jumlah</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_jum" name="jum" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tipe Branag</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="tipe" id="add_tipe" class="form-control" required>
                                            <option value="0">Tidak Berwujud</option>
                                            <option value="1">Berwujud</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Kondisi</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="kon" id="add_kon" class="form-control" required>
                                            <option value="Baik">Baik</option>
                                            <option value="Rusak">Rusak</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Merk</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_merk" name="merk"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">No Barang</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_seri" name="seri"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Harga</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="la la-dollar"></i></span></div>
                                            <input type="text" class="form-control" id="add_harga" name="harga" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                placeholder="Harga" aria-describedby="basic-addon1" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tanggal Masuk</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="dateStart" name="tanggal"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tahun</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="years" name="tahun"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Keterangan</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_ket" name="ket"
                                            required>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button">Prosess Data</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>

<div class="modal fade" id="edit_assets" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Assets</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('assets/update'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Foto Asset</label>
                                    <div class="col-lg-9 col-xl-9">
                                        <div class="kt-avatar kt-avatar--outline" id="kt_user_avatar">
                                            <div class="kt-avatar__holder" id="avatar_image_add"
                                                style="background-image: url(/metronic/themes/metronic/theme/default/demo9/dist/assets/media/users/100_13.jpg)">
                                            </div>
                                            <label class="kt-avatar__upload" data-toggle="kt-tooltip" title=""
                                                data-original-title="Change avatar">
                                                <i class="fa fa-pen"></i>
                                                <input type="file" name="add_image" id="add_image"
                                                    accept=".png, .jpg, .jpeg" onchange="readURLAdd(this);">
                                            </label>
                                            <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title=""
                                                data-original-title="Cancel avatar">
                                                <i class="fa fa-times"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <input class="form-control" type="hidden" id="edit_ids" name="id_assets" required>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nama Assets</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="edit_asset" name="assets"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Kode Barang</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="edit_kode" name="kode"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Bahan</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="edit_bahan" name="bahan"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Ukuran</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="edit_ukuran" name="ukuran"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Jumlah</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="edit_jum" name="jum" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tipe Branag</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="tipe" id="edit_tipe" class="form-control" required>
                                            <option value="0">Tidak Berwujud</option>
                                            <option value="1">Berwujud</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Kondisi</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="kon" id="edit_kon" class="form-control" required>
                                            <option value="Baik">Baik</option>
                                            <option value="Rusak">Rusak</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Merk</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="edit_merk" name="merk"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">No Barang</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="edit_seri" name="seri"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Harga</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="la la-dollar"></i></span></div>
                                            <input type="text" class="form-control" id="edit_harga" name="harga" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                placeholder="Harga" aria-describedby="basic-addon1" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tanggal Masuk</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="edit_tanggal" name="tanggal"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tahun</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="edit_years" name="tahun"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Keterangan</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="edit_kets" name="ket"
                                            required>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button">Prosess Data</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>


<script type="text/javascript">
    function EditAssets(ids, assets, kode, bahan, ukuran, jumlah, tipe, kondisi, merk, seri, harga, ket, tanggal, years ) {

        document.getElementById("edit_ids").value = ids;
        document.getElementById("edit_asset").value = assets;
        document.getElementById("edit_kode").value = kode;
        document.getElementById("edit_bahan").value = bahan;
        document.getElementById("edit_ukuran").value = ukuran;
        document.getElementById("edit_jum").value = jumlah;
        document.getElementById("edit_tipe").value = tipe;
        document.getElementById("edit_kon").value = kondisi;
        document.getElementById("edit_merk").value = merk;
        document.getElementById("edit_seri").value = seri;
        document.getElementById("edit_harga").value = harga;
        document.getElementById("edit_kets").value = ket;
        document.getElementById("edit_tanggal").value = tanggal;
        document.getElementById("edit_years").value = years;
    }
</script>

<script type="text/javascript">

    

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                // $('#blah')
                //     .attr('src', e.target.result);
                document.getElementById('avatar_image').style.backgroundImage = "url(" + e.target.result + ")";
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURLAdd(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                // $('#blah')
                //     .attr('src', e.target.result);
                document.getElementById('avatar_image_add').style.backgroundImage = "url(" + e.target.result + ")";
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
