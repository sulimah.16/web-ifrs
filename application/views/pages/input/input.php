<style>
table {
    table-layout: fixed;
}

td {
    overflow: hidden;
    text-overflow: ellipsis;
}

#tbdy {
    margin-top: 25px;
    width: 550px;
}

#url {
    color: blue;
    text-decoration: underline;
}
</style>

<?php 

// echo json_encode($draft);

?>


<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Input Data </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon-download-1"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Input Data </a>

                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container   kt-grid__item--fluid">
            <?php if (($data->status == '1' || $data->status == 1) && $id_draft != 0) { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Berhasil!&nbsp;&nbsp;</strong><?php echo "Jumlah Data : ".count($data->data); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } else { ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Perhatian!&nbsp;&nbsp;</strong> <?php echo "Tidak ada Data"; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } ?>
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                <strong>Info!&nbsp;&nbsp;</strong><?php echo "Pilih draft anda setelah itu tekan button 'ubah data' untuk melakukan pencarian data."; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h2 class="kt-portlet__head-title">
                            <?php 
                            if ($id_draft != 0 && count($data->data) > 0) { 
                                echo $data->data[0]->draft->nama_draft;
                            } ?>
                        </h2>
                    </div>

                    <?php if ( $this->session->userdata('role') == '1' || $this->session->userdata('role') == 1 || $this->session->userdata('role') == '2' || $this->session->userdata('role') == 2 ) { ?>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <div id='tbdy'>
                                    <form method="post" enctype="multipart/form-data"
                                        action="<?php echo site_url('input'); ?>">
                                        <div class="form-group row">
                                            <div class="col-lg-6">
                                                <select name="draft" id="draft" class="form-control" required>
                                                    <option value="0"><?php echo "Pilih Draft" ?></option>
                                                    <?php foreach( $draft->data as $dr ):?>
                                                    <option value="<?php echo $dr->id_draft; ?>">
                                                        <?php echo $dr->nama_draft; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-6">
                                                <button type="submit" class="btn btn-primary teratur-button"><i
                                                        class="flaticon-file-2 icon-xs"></i>Lihat Data</button>
                                                <span></span>
                                                <button type="button" class="btn btn-primary teratur-button"
                                                    data-toggle="modal" data-target="#input"><i
                                                        class="flaticon-plus icon-xs"></i>Input Data</button>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } else { ?>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <div id='tbdy'>
                                    <form method="post" enctype="multipart/form-data"
                                        action="<?php echo site_url('input'); ?>">
                                        <div class="form-group row">
                                            <div class="col-lg-8">
                                                <select name="draft" id="draft" class="form-control" required>
                                                    <option value="0"><?php echo "Pilih Draft" ?></option>
                                                    <?php foreach( $draft->data as $dr ):?>
                                                    <option value="<?php echo $dr->id_draft; ?>">
                                                        <?php echo $dr->nama_draft; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-4">
                                                <button type="submit" class="btn btn-primary teratur-button"><i
                                                        class="flaticon-file-2 icon-xs"></i>Lihat Data</button>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>

                <div class="kt-portlet__body">
                    <table class="table table-striped table-bordered table-hover" id="tbl_list_default" width="100%">
                        <thead>
                            <tr>
                                <th class="all" width=20px>#</th>
                                <th class="all" width=100px>Periode</th>
                                <th class="all" width=100px>Kode Customer</th>
                                <th class="all" width=100px>Nama Customer</th>
                                <th class="all" width=100px>Nomor Dokumen</th>
                                <th class="all" width=100px>Tipe Dokumen</th>
                                <th class="all" width=100px>Umur Piutang</th>
                                <th class="all" width=200px>Saldo Piutang</th>
                                <th class="all" width=100px>Segmen</th>
                                <th class="min-tablet" width=50px>Action</th>
                            </tr>
                        </thead>
                        <tbody class="kt-datatable__body">
                            <?php $i = 1; foreach ( $data->data as $d ) : ?>
                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $d->periode; ?></td>
                                <td><?php echo $d->kode_customer; ?></td>
                                <td><?php echo $d->nama_customer; ?></td>
                                <td><?php echo $d->doc_number; ?></td>
                                <td><?php echo $d->doc_type; ?></td>
                                <td><?php echo $d->umur_piutang_hari." Hari"; ?></td>
                                <td><?php echo "Rp. " . number_format( $d->saldo_piutang,2, ',', '.');?></td>
                                <td><?php echo $d->segment->nama_segment; ?></td>
                                <td>
                                    <div class="dropdown">
                                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                            data-toggle="dropdown">
                                            <i class="la la-cog"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-sm">
                                            <ul class="kt-nav">
                                                <li class="kt-nav__item">
                                                    <a href="<?php echo site_url("#"); ?>" data-toggle="modal"
                                                        data-target="#update" onClick="return EditInputs(
                                                                '<?php echo $d->id_input; ?>',
                                                                '<?php echo $d->id_segment; ?>',
                                                                '<?php echo $d->id_draft; ?>',
                                                                '<?php echo date("m/d/Y", strtotime($d->tanggal_piutang)); ?>',
                                                                '<?php echo $d->id_piutang; ?>',
                                                                '<?php echo $d->kode_customer; ?>',
                                                                '<?php echo $d->nama_customer; ?>',
                                                                '<?php echo $d->kode_proyek; ?>',
                                                                '<?php echo $d->nama_proyek; ?>',
                                                                '<?php echo $d->pemberi_kerja; ?>',
                                                                '<?php echo $d->doc_number; ?>',
                                                                '<?php echo $d->doc_type; ?>',
                                                                '<?php echo $d->jenis_piutang; ?>',
                                                                '<?php echo $d->umur_piutang_hari; ?>',
                                                                '<?php echo $d->saldo_piutang; ?>',
                                                            )" class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon-edit"></i>
                                                        <span class="kt-nav__link-text">Ubah Data</span>
                                                    </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <a onclick='return del_confirm()'
                                                        href="<?php echo site_url('input/delete/'.$d->id_input); ?>"
                                                        class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                        <span class="kt-nav__link-text">Hapus Data</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="input" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Input Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>

            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('input/create'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tipe Input Data</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <select name="tipe" id="tipe_input" class="form-control" required>
                                            <option value="<?php echo '0'; ?>"><?php echo 'Input Data Manual'; ?>
                                            </option>
                                            <option value="<?php echo '1'; ?>"><?php echo 'Upload File Per Hari'; ?>
                                            </option>
                                            <option value="<?php echo '2'; ?>">
                                                <?php echo 'Upload Listing Piutang Per Bucket'; ?>
                                            </option>
                                        </select>
                                    </div>
                                </div>


                                <div id="a1" class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Segmentasi</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="segment" id="segment" class="form-control">
                                            <?php foreach( $segment->data as $s ):?>
                                            <option value="<?php echo $s->id_segment; ?>">
                                                <?php echo $s->nama_segment." (".$s->kode_segment.")"; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <span class="form-text text-muted">Pilih Segmentasi.</span>
                                    </div>
                                </div>

                                <div id="a">
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Draf</label>
                                        <div class="col-lg-9 col-xl-4">
                                            <select name="drafts" id="draft" class="form-control">
                                                <option value="<?php echo ""; ?>"><?php echo "Buat Draf Baru"; ?>
                                                </option>
                                                <?php foreach( $draft->data as $dr ):?>
                                                <option value="<?php echo $dr->id_draft; ?>">
                                                    <?php echo "Relasi dengan: ".$dr->nama_draft; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <span class="form-text text-muted">Buat Draf baru atau tambahkan ke draf
                                                yang sudah ada.</span>
                                        </div>
                                    </div>

                                    <div id="a2" class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Tanggal Piutang</label>
                                        <div class="col-lg-9 col-xl-4">
                                            <input class="form-control" type="text" id="dateStart2"
                                                name="tanggal_piutang">
                                            <span class="form-text text-muted">Pilih tanggal Piutang / Periode
                                                Piutang</span>
                                        </div>
                                    </div>

                                    <div id="a3" class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">ID Piutang</label>
                                        <div class="col-lg-9 col-xl-4">
                                            <input class="form-control" type="text" id="id_piutang" name="id_piutang">
                                            <span class="form-text text-muted">Contoh: "PUP"</span>
                                        </div>
                                    </div>

                                    <div id="a4" class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Kode Customer</label>
                                        <div class="col-lg-9 col-xl-4">
                                            <input class="form-control" type="text" id="kode_cus" name="kode_cus">
                                            <span class="form-text text-muted">Contoh: "8129363"</span>
                                        </div>
                                    </div>

                                    <div id="a5" class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Nama Customer</label>
                                        <div class="col-lg-9 col-xl-4">
                                            <input class="form-control" type="text" id="nama_cus" name="nama_cus">
                                            <span class="form-text text-muted">Contoh: "YAYASAN ALBAAITS"</span>
                                        </div>
                                    </div>

                                    <div id="a6" class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Kode Proyek</label>
                                        <div class="col-lg-9 col-xl-4">
                                            <input class="form-control" type="text" id="kode_pro" name="kode_pro">
                                            <span class="form-text text-muted">Contoh: "2015400102"</span>
                                        </div>
                                    </div>

                                    <div id="a7" class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Nama Proyek</label>
                                        <div class="col-lg-9 col-xl-4">
                                            <input class="form-control" type="text" id="nama_pro" name="nama_pro">
                                            <span class="form-text text-muted">Contoh: "PROYEK PEMBANGUNAN A"</span>
                                        </div>
                                    </div>

                                    <div id="a8" class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Pemberi Kerja</label>
                                        <div class="col-lg-9 col-xl-4">
                                            <input class="form-control" type="text" id="pemberi_kerja"
                                                name="pemberi_kerja">
                                            <span class="form-text text-muted">Contoh: "Kementrian X"</span>
                                        </div>
                                    </div>

                                    <div id="a9" class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Nomer Dokumen</label>
                                        <div class="col-lg-9 col-xl-4">
                                            <input class="form-control" type="text" id="doc_no" name="doc_no">
                                            <span class="form-text text-muted">Contoh: "1600000283"</span>
                                        </div>
                                    </div>

                                    <div id="a10" class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Tipe Dokumen</label>
                                        <div class="col-lg-9 col-xl-4">
                                            <input class="form-control" type="text" id="doc_tipe" name="doc_tipe">
                                            <span class="form-text text-muted">Contoh: "DA"</span>
                                        </div>
                                    </div>

                                    <div id="a11" class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Jenis Piutang</label>
                                        <div class="col-lg-9 col-xl-4">
                                            <input class="form-control" type="text" id="jenis_piutang"
                                                name="jenis_piutang">
                                            <span class="form-text text-muted">Contoh: "Usaha , Prestasi , dll"</span>
                                        </div>
                                    </div>

                                    <div id="a12" class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Umur Piutang (Hari)</label>
                                        <div class="col-lg-9 col-xl-4">
                                            <input class="form-control" type="text" id="umur_piutang"
                                                name="umur_piutang"
                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                            <span class="form-text text-muted">Umur Piutang dalam Hari</span>
                                        </div>
                                    </div>

                                    <div id="a13" class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Saldo Piutang (Rupiah)</label>
                                        <div class="col-lg-9 col-xl-4">
                                            <input class="form-control" type="text" id="saldo_piutang"
                                                name="saldo_piutang"
                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                        </div>
                                    </div>
                                </div>

                                <div id="b" style="display:none">

                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Draf</label>
                                        <div class="col-lg-9 col-xl-4">
                                            <div class="input-group">
                                                <select name="draft1" id="draft" class="form-control">
                                                    <option value="<?php echo ""; ?>"><?php echo "Buat Draf Baru"; ?>
                                                    </option>
                                                    <?php foreach( $draft->data as $dr ):?>
                                                    <option value="<?php echo $dr->id_draft; ?>">
                                                        <?php echo "Relasi dengan: ".$dr->nama_draft; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <span class="form-text text-muted">Buat Draf baru atau tambahkan ke draf
                                                yang sudah ada.</span>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">File Upload</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="customFile"
                                                        name="file_b">
                                                    <label class="custom-file-label" for="customFile">Choose
                                                        file</label>
                                                </div>
                                            </div>
                                            <span class="form-text text-muted">contoh untuk upload file per hari. <a
                                                    id='url'
                                                    href="<?php echo base_url(); ?>public/example/UPLOAD_INPUT_HARIAN.csv"
                                                    download>klik disini </a></span>
                                        </div>
                                    </div>
                                </div>


                                <div id="c" style="display:none">

                                    <!-- <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Draf</label>
                                        <div class="col-lg-9 col-xl-4">

                                            <select name="draft2" id="draft" class="form-control">
                                                <option value="<?php echo ""; ?>"><?php echo "Buat Draf Baru"; ?>
                                                </option>
                                                <?php foreach( $draft->data as $dr ):?>
                                                <option value="<?php echo $dr->id_draft; ?>">
                                                    <?php echo "Relasi dengan: ".$dr->nama_draft; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <span class="form-text text-muted">Buat Draf baru atau tambahkan ke draf
                                                yang sudah ada.</span>
                                        </div>
                                    </div> -->

                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">File Upload</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="customFile"
                                                        name="file_c">
                                                    <label class="custom-file-label" for="customFile">Choose
                                                        file</label>
                                                </div>
                                            </div>
                                            <span class="form-text text-muted">contoh untuk upload file per bucket. <a
                                                    id='url'
                                                    href="<?php echo base_url(); ?>public/example/UPLOAD_INPUT_BUCKET.csv"
                                                    download>klik disini</a></span>
                                        </div>
                                    </div>

                                    <div id="c1" class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Jumlah Bucket</label>
                                        <div class="col-lg-9 col-xl-4">
                                            <input class="form-control" type="text" id="jumlah_bucket"
                                                name="jumlah_bucket"
                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                            <span class="form-text text-muted">Jumlah Bucket dalam file.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button" onclick="return act_confirm()">Prosess
                        Data</button>
                </div>
            </form>

        </div>
    </div>
</div>

<div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ubah Data Input</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>

            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('input/update'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">


                                <input class="form-control" type="hidden" id="edit_id_input" name="id_input">


                                <div id="a1" class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Segmen</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="segment" id="edit_segment" class="form-control">
                                            <?php foreach( $segment->data as $s ):?>
                                            <option value="<?php echo $s->id_segment; ?>">
                                                <?php echo $s->nama_segment." (".$s->kode_segment.")"; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <span class="form-text text-muted">Pilih Segmentasi.</span>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Draft</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="draft" id="edit_draft" class="form-control">
                                            <option value="<?php echo ""; ?>"><?php echo "Draft Baru"; ?></option>
                                            <?php foreach( $draft->data as $dr ):?>
                                            <option value="<?php echo $dr->id_draft; ?>"><?php echo $dr->nama_draft; ?>
                                            </option>
                                            <?php endforeach; ?>
                                        </select>
                                        <span class="form-text text-muted">Buat Draf baru atau tambahkan ke draf
                                            yang sudah ada.</span>
                                    </div>
                                </div>

                                <div id="d3" class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tanggal Piutang</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="dateStart" name="tanggal_piutang">
                                        <span class="form-text text-muted">Pilih tanggal Piutang / Periode
                                            Piutang</span>
                                    </div>
                                </div>

                                <div id="d4" class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">ID Piutang</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="edit_id_piutang" name="id_piutang">
                                        <span class="form-text text-muted">Contoh: "PUP"</span>
                                    </div>
                                </div>

                                <div id="d5" class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Kode Customer</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="edit_kode_cus" name="kode_cus">
                                        <span class="form-text text-muted">Contoh: "8129363"</span>
                                    </div>
                                </div>

                                <div id="d6" class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nama Customer</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="edit_nama_cus" name="nama_cus">
                                        <span class="form-text text-muted">Contoh: "YAYASAN ALBAAITS"</span>
                                    </div>
                                </div>

                                <div id="d7" class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Kode Proyek</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="edit_kode_pro" name="kode_pro">
                                        <span class="form-text text-muted">Contoh: "2015400102"</span>
                                    </div>
                                </div>

                                <div id="d8" class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nama Proyek</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="edit_nama_pro" name="nama_pro">
                                        <span class="form-text text-muted">Contoh: "PROYEK PEMBANGUNAN A"</span>
                                    </div>
                                </div>

                                <div id="d9" class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Pemberi Kerja</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="edit_pemberi_kerja"
                                            name="pemberi_kerja">
                                        <span class="form-text text-muted">Contoh: "Kementrian X"</span>
                                    </div>
                                </div>

                                <div id="d10" class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nomer Dokumen</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="edit_doc_no" name="doc_no">
                                        <span class="form-text text-muted">Contoh: "1600000283"</span>
                                    </div>
                                </div>

                                <div id="d11" class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tipe Dokumen</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="edit_doc_tipe" name="doc_tipe">
                                        <span class="form-text text-muted">Contoh: "DA"</span>
                                    </div>
                                </div>

                                <div id="d12" class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Jenis Piutang</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="edit_jenis_piutang"
                                            name="jenis_piutang">
                                        <span class="form-text text-muted">Contoh: "Usaha , Prestasi , dll"</span>
                                    </div>
                                </div>

                                <div id="d13" class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Umur Piutang (Hari)</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="edit_umur_piutang"
                                            name="umur_piutang"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                        <span class="form-text text-muted">Umur Piutang dalam Hari</span>
                                    </div>
                                </div>

                                <div id="d14" class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Saldo Piutang (Rupiah)</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="edit_saldo_piutang"
                                            name="saldo_piutang"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button" onclick="return act_confirm()">Prosess
                        Data</button>
                </div>
            </form>

        </div>
    </div>
</div>




<script type="text/javascript">
// document.getElementById("draft").value = <?php echo $id_draft; ?>;

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            // $('#blah')
            //     .attr('src', e.target.result);
            document.getElementById('avatar_image').style.backgroundImage = "url(" + e.target.result + ")";
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURLAdd(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            // $('#blah')
            //     .attr('src', e.target.result);
            document.getElementById('avatar_image_add').style.backgroundImage = "url(" + e.target.result + ")";
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function EditPenarikan(id, name, rek) {
    document.getElementById("add_name").value = name;
    document.getElementById("add_id").value = id;
    document.getElementById("add_rek").value = rek;
}

function EditInputs(ids, segment, draft, tanggal, id_piutang, kode_cos, nama_cos, kod_pro, nam_pro, pem_kerja, no_dok,
    type_dok, jenis_piutang, umur_piutang, saldo_piutang) {
    document.getElementById("edit_id_input").value = ids;
    document.getElementById("edit_segment").value = segment;
    document.getElementById("edit_draft").value = draft;
    document.getElementById("dateStart").value = tanggal;
    document.getElementById("edit_id_piutang").value = id_piutang;
    document.getElementById("edit_kode_cus").value = kode_cos;
    document.getElementById("edit_nama_cus").value = nama_cos;
    document.getElementById("edit_kode_pro").value = kod_pro;
    document.getElementById("edit_nama_pro").value = nam_pro;
    document.getElementById("edit_pemberi_kerja").value = pem_kerja;
    document.getElementById("edit_doc_no").value = no_dok;
    document.getElementById("edit_doc_tipe").value = type_dok;
    document.getElementById("edit_jenis_piutang").value = jenis_piutang;
    document.getElementById("edit_umur_piutang").value = umur_piutang;
    document.getElementById("edit_saldo_piutang").value = saldo_piutang;
}
</script>