<style>
table {
    table-layout: fixed;
}

td {
    overflow: hidden;
    text-overflow: ellipsis;
}
</style>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Average </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon-interface-6"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Rollrate dan Lossrate </a>

                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container   kt-grid__item--fluid">
            <?php if (($data->status == '1' || $data->status == 1) && $id_draft != 0) { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Berhasil!&nbsp;&nbsp;</strong><?php echo "Jumlah Data : ".count($data->data->list_bucket); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } else { ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Perhatian!&nbsp;&nbsp;</strong> <?php echo "Tidak ada Data"; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } ?>
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                <strong>Info!&nbsp;&nbsp;</strong><?php echo "Tekan button pencarian data untuk melakukan pencarian average data yang di inginkan."; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <?php if ($id_draft != "" && $id_draft != 0) { ?>
                        <h2 class="kt-portlet__head-title">
                            <?php echo $data->data->draft->nama_draft." (".$data->data->title_bucket.")  "; ?>
                        </h2>
                        <small><?php  if ($data->data->draft->flag_draft == 2) {
                            echo "- Data Piutang per Bucket";
                        } else {
                            echo "- Data Piutang per Aging";
                        }
                        ?> </small>
                        <?php } ?>
                    </div>

                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <button type="button" class="btn btn-primary teratur-button" data-toggle="modal"
                                    data-target="#filter"><i class="flaticon-search-1 icon-xs"></i>Pencarian</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">
                    <table class="table table-striped table-bordered table-hover" id="tbl_list_default" width="100%">
                        <thead>
                            <tr>
                                <th class="all" width=20px>#</th>
                                <th class="all" width=150px>Periode</th>
                                <?php foreach ( $data->data->header_bucket as $h ) : ?>
                                <th class="min-tablet" width=100px><?php echo $h." hari"; ?></th>
                                <?php endforeach ?>
                                <th class="min-tablet" width=100px>Simple Average</th>
                                <th class="min-tablet" width=100px>Sum Loss Rate Average</th>
                            </tr>
                        </thead>
                        <tbody class="kt-datatable__body">
                            <?php $t = 1; foreach ( $data->data->list_bucket as $l ) : ?>
                            <tr>
                                <td><?php echo $t++; ?></td>
                                <td><?php echo $l->periode; ?></td>
                                <?php foreach ( $l->nilai as $n ) : ?>
                                <td><?php if ( $n == 'Infinity' || $n == '-Infinity') {
                                            $v = $n;
                                        } else {
                                            $v = $n *100;
                                            $v = round($v, 2)." %";
                                        }
                                        echo $v;?></td>
                                <?php endforeach; ?>
                                <td><?php echo round($l->average * 100, 2)." %";?></td>
                                <td><?php echo round($l->sum * 100, 2)." %";?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="filter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pencarian</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('laporan/average'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">


                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Draf</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="draft" id="pick_draft" class="form-control" required>
                                            <?php foreach( $draft->data as $dr ):?>
                                            <option value="<?php echo $dr->id_draft . '|' . $dr->flag_draft; ?>">
                                                <?php echo $dr->nama_draft; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Segmentasi</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="segment" id="segment" class="form-control" required>
                                            <?php foreach( $segment->data as $s ):?>
                                            <option value="<?php echo $s->id_segment; ?>">
                                                <?php echo $s->nama_segment." (".$s->kode_segment.")"; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div id='f1' class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Jumlah hari bucket pertama
                                        (hari)</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <!-- <select name="first" id="first" class="form-control">
                                            <?php for ( $x = 0; $x <= 20; $x += 2 ) { ?>
                                            <option value="<?php echo $x; ?>"><?php echo $x; ?></option>
                                            <?php } ?>
                                        </select> -->
                                        <input class="form-control" type="number" id="first" name="first" min="0"
                                            max="200">
                                    </div>
                                </div>
                                <div id='f2' class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Jarak hari antar bucket
                                        (hari)</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <!-- <select name="interval" id="interval" class="form-control">
                                            <?php for ( $x = 1; $x <= 100; $x ++ ) { ?>
                                            <option value="<?php echo $x; ?>"><?php echo $x; ?></option>
                                            <?php } ?>
                                        </select> -->
                                        <input class="form-control" type="number" id="interval" name="interval" min="1"
                                            max="100">
                                    </div>
                                </div>
                                <div id='f3' class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Jumlah Bucket</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <!-- <select name="bucket" id="bucket" class="form-control">
                                            <?php for ( $x = 1; $x <= 20; $x++) { ?>
                                            <option value="<?php echo $x; ?>"><?php echo $x; ?></option>
                                            <?php } ?>
                                        </select> -->
                                        <input class="form-control" type="number" id="bucket" name="bucket" min="1"
                                            max="50">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label"></label>
                                    <div class="col-lg-9 col-xl-4">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="inlineRadioOptions"
                                                id="inlineRadio1" value="1" checked onchange="getValue(this)">
                                            <label class="form-check-label" for="inlineRadio1">Semua</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="inlineRadioOptions"
                                                id="inlineRadio2" value="2" onchange="getValue(this)">
                                            <label class="form-check-label" for="inlineRadio2">Periode</label>
                                        </div>
                                    </div>

                                </div>

                                <div id="prdTgl">

                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Tanggal Awal</label>
                                        <div class="col-lg-9 col-xl-4">
                                            <input class="form-control" type="text" id="dateStart" name="start">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Tanggal Akhir</label>
                                        <div class="col-lg-9 col-xl-4">
                                            <input class="form-control" type="text" id="dateStart2" name="end">
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button">Prosess Data</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>



<script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            // $('#blah')
            //     .attr('src', e.target.result);
            document.getElementById('avatar_image').style.backgroundImage = "url(" + e.target.result + ")";
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURLAdd(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            // $('#blah')
            //     .attr('src', e.target.result);
            document.getElementById('avatar_image_add').style.backgroundImage = "url(" + e.target.result + ")";
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function EditPenarikan(id, name, rek) {
    document.getElementById("add_name").value = name;
    document.getElementById("add_id").value = id;
    document.getElementById("add_rek").value = rek;
}
</script>