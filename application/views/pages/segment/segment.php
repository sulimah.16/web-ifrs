<style>
table {
    table-layout: fixed;
}

td {
    overflow: hidden;
    text-overflow: ellipsis;
}
</style>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Daftar Segmentasi </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-box"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Segmentasi </a>

                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container   kt-grid__item--fluid">
            <?php if ( $data->status == '1' || $data->status == 1 ) { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Berhasil!&nbsp;&nbsp;</strong><?php echo "Jumlah Segment : ".count($data->data); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } else { ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Perhatian!&nbsp;&nbsp;</strong> <?php echo "Tidak ada segment"; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } ?>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">

                    </div>

                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <?php if ( $this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' ) { ?>
                                <a href="#" class="btn btn-brand btn-icon-sm teratur-button" data-toggle="modal"
                                    data-target="#add_profile">
                                    <i class="flaticon-plus"></i>
                                    Tambah Data
                                </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">


                    <table class="table table-striped table-bordered table-hover" id="tbl_list_default2" width="100%">
                        <thead>
                            <tr>
                                <th class="all" width=10px>#</th>
                                <th class="min-tablet" width=300px>Kode Segmentasi</th>
                                <th class="min-tablet" width=300px>Nama Segmentasi</th>
                                <?php if ( $this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' ) { ?>
                                <th class="min-tablet" width=50px>Action</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody class="kt-datatable__body">
                            <?php $i = 1; foreach( $data->data as $d ): ?>
                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $d->kode_segment; ?></td>
                                <td><?php echo $d->nama_segment; ?></td>
                                <?php if ( $this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' ) { ?>
                                <td>
                                    <div class="dropdown">
                                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                            data-toggle="dropdown">
                                            <i class="la la-cog"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-sm">
                                            <ul class="kt-nav">
                                                <li class="kt-nav__item">
                                                    <a href="<?php echo site_url("#"); ?>" data-toggle="modal"
                                                        data-target="#edit_profile" onClick="return EditSegment(
                                                            '<?php echo  $d->id_segment; ?>', 
                                                            '<?php echo  $d->kode_segment; ?>', 
                                                            '<?php echo  $d->nama_segment; ?>', 
                                                        )" class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon-edit"></i>
                                                        <span class="kt-nav__link-text">Ubah Data</span>
                                                    </a>
                                                </li>
                                                <!-- <li class="kt-nav__item">
                                                    <a onclick='return del_confirm()'
                                                        href="<?php echo site_url("segment/delete/$d->id_draft"); ?>"
                                                        class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                        <span class="kt-nav__link-text">Hapus</span>
                                                    </a>
                                                </li> -->
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                                <?php } ?>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="add_profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('segment/create'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Kode Segmentasi</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_kode" name="kode" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nama Segmentasi</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_nama" name="nama" required>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button" onclick="return act_confirm()">Tambah
                        Data</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>

<div class="modal fade" id="edit_profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ubah Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('segment/update'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">

                                <input class="form-control" type="hidden" id="edit_ids" name="ids" required>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Kode Segmentasi</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="edit_kode" name="kode" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nama segmentasi</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="edit_nama" name="nama" required>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button" onclick="return act_confirm()">Ubah
                        Data</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>


<script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            // $('#blah')
            //     .attr('src', e.target.result);
            document.getElementById('avatar_image_edit').style.backgroundImage = "url(" + e.target.result + ")";
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURLAdd(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            // $('#blah')
            //     .attr('src', e.target.result);
            document.getElementById('avatar_image_add').style.backgroundImage = "url(" + e.target.result + ")";
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function EditSegment(id, kode, nama) {
    document.getElementById("edit_ids").value = id;
    document.getElementById("edit_kode").value = kode;
    document.getElementById("edit_nama").value = nama;
}
</script>