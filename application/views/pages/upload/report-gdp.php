<style>
table {
    table-layout: fixed;
}

td {
    overflow: hidden;
    text-overflow: ellipsis;
}
</style>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Hasil Impor File </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon-cogwheel"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Impor File </a>

                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet">
                <div class="kt-portlet__body kt-portlet__body--fit">
                    <div class="kt-invoice-1">
                        <div class="kt-invoice__head">
                            <div class="kt-invoice__container">
                                <div class="kt-invoice__brand">
                                    <h1 class="kt-invoice__title">LAPORAN UNGGAH FILE</h1>
                                    <!-- <div href="#" class="kt-invoice__logo">
                                        <a href="#"><img src="<?php echo base_url(); ?>public/assets/media/company-logos/logo_client_white.png"></a>
                                        <span class="kt-invoice__desc">
                                            <span>Cecilia Chapman, 711-2880 Nulla St, Mankato</span>
                                            <span>Mississippi 96522</span>
                                        </span>
                                    </div> -->
                                </div>
                                <div class="kt-invoice__items">
                                    <div class="kt-invoice__item">
                                        <span class="kt-invoice__subtitle">Tanggal</span>
                                        <span class="kt-invoice__text"><?php echo $data->data->tgl_import ?></span>
                                    </div>
                                    <div class="kt-invoice__item">
                                        <!-- <span class="kt-invoice__subtitle">INVOICE NO.</span>
                                        <span class="kt-invoice__text">GS 000014</span> -->
                                    </div>
                                    <div class="kt-invoice__item">
                                        <span class="kt-invoice__subtitle">Sumber File.</span>
                                        <span class="kt-invoice__text">Metode dipilih.<br>Unggah File:
                                            <?php echo $data->data->file_upload->true_name; ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <!-- -->

                <div class="kt-portlet__body">
                    <div class="row row-no-padding row-col-separator-lg">
                        <div class="col-md-12 col-lg-6 col-xl-4">

                            <!--begin::New Feedbacks-->
                            <div class="kt-widget24">
                                <div class="kt-widget24__details">
                                    <div class="kt-widget24__info">
                                        <h4 class="kt-widget24__title">
                                            Jumlah Impor File
                                        </h4>
                                        <span class="kt-widget24__desc">
                                            Impor File
                                        </span>
                                    </div>
                                    <span class="kt-widget24__stats kt-font-warning">
                                        <?php echo $data->data->count_import ?>
                                    </span>
                                </div>
                                <div class="progress progress--sm">
                                    <div class="progress-bar kt-bg-warning" role="progressbar" style="width: 100%;"
                                        aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class="kt-widget24__action">
                                    <span class="kt-widget24__change">
                                        Baris
                                    </span>
                                    <span class="kt-widget24__number">
                                        100%
                                    </span>
                                </div>
                            </div>

                            <!--end::New Feedbacks-->
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-4">
                            <?php $perse =  ($data->data->count_failed/$data->data->count_import ) *100 ; $perse = round($perse, 2); ?>
                            <!--begin::New Orders-->
                            <div class="kt-widget24">
                                <div class="kt-widget24__details">
                                    <div class="kt-widget24__info">
                                        <h4 class="kt-widget24__title">
                                            Jumlah Impor File Gagal
                                        </h4>
                                        <span class="kt-widget24__desc">
                                            Impor File Gagal
                                        </span>
                                    </div>
                                    <span class="kt-widget24__stats kt-font-danger">
                                        <?php echo $data->data->count_failed ?>
                                    </span>
                                </div>
                                <div class="progress progress--sm">
                                    <div class="progress-bar kt-bg-danger" role="progressbar"
                                        style="width: <?php echo $perse.'%' ?>;" aria-valuenow="50" aria-valuemin="0"
                                        aria-valuemax="100"></div>
                                </div>
                                <div class="kt-widget24__action">
                                    <span class="kt-widget24__change">
                                        Baris
                                    </span>
                                    <span class="kt-widget24__number">
                                        <?php echo $perse.'%' ?>
                                    </span>
                                </div>
                            </div>

                            <!--end::New Orders-->
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-4">
                            <?php $perses =  ($data->data->count_success/$data->data->count_import ) *100 ; $perses = round($perses, 2); ?>
                            <!--begin::New Users-->
                            <div class="kt-widget24">
                                <div class="kt-widget24__details">
                                    <div class="kt-widget24__info">
                                        <h4 class="kt-widget24__title">
                                            Jumlah Impor Data Berhasil
                                        </h4>
                                        <span class="kt-widget24__desc">
                                            Impor Data Berhasil
                                        </span>
                                    </div>
                                    <span class="kt-widget24__stats kt-font-success">
                                        <?php echo $data->data->count_success ?>
                                    </span>
                                </div>
                                <div class="progress progress--sm">
                                    <div class="progress-bar kt-bg-success" role="progressbar"
                                        style="width: <?php echo $perses.'%' ?>;" aria-valuenow="50" aria-valuemin="0"
                                        aria-valuemax="100"></div>
                                </div>
                                <div class="kt-widget24__action">
                                    <span class="kt-widget24__change">
                                        Baris
                                    </span>
                                    <span class="kt-widget24__number">
                                        <?php echo $perses.'%' ?>
                                    </span>
                                </div>
                            </div>

                            <!--end::New Users-->
                        </div>
                    </div>
                </div>


                <div class="kt-portlet__body">


                    <table class="table table-striped table-bordered table-hover" id="tbl_cust" width="100%">
                        <thead>
                            <tr>
                                <th class="all" width=200px> Nomer Inputan </th>
                                <th class="all" width=200px> Status </th>
                                <th class="all" width=300px> Pesan </th>
                            </tr>
                        </thead>
                        <tbody class="kt-datatable__body">
                            <?php foreach ( $data->data->list_pesan as $d ) { ?>
                            <tr>
                                <td><?php echo $d->nomor_input ?></td>
                                <td><?php echo $d->status ?></td>
                                <td><?php echo $d->pesan ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>

                <div class="kt-portlet__foot">
                    <div class="kt-chat__input">
                        <div class="kt-chat__editor">
                        </div>
                        <div class="kt-chat__toolbar">
                            <div class="kt_chat__actions">
                                <a onclick='return act_confirm()' href="<?php echo site_url("Vasicek/gdp"); ?>">
                                    <button type="button"
                                        class="btn btn-sm btn-danger teratur-button float-right">Kembali
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- -->
            </div>
        </div>




    </div>
</div>