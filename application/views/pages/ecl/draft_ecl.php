<!-- <style>
table {
    table-layout: fixed;
    display: block;
    overflow-x: auto;
    white-space: nowrap;
}

td {
    overflow: hidden;
    text-overflow: ellipsis;
}
</style> -->

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Daftar ECL</h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon-list-1"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Draft ECL</a>
                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container   kt-grid__item--fluid">
            <?php if ( $data->status == '1' || $data->status == 1 ) { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Berhasil!&nbsp;&nbsp;</strong><?php echo "Jumlah Daftar ECL: ".count($data->data); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } else { ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Perhatian!&nbsp;&nbsp;</strong> <?php echo "Tidak ada draft bucket"; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } ?>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                    </div>

                    <!-- <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <a href="#" class="btn btn-brand btn-icon-sm teratur-button" data-toggle="modal"
                                    data-target="#input">
                                    <i class="flaticon-plus"></i>
                                    Buat Bucket Simulate
                                </a>
                            </div>
                        </div>
                    </div> -->
                </div>

                <div class="kt-portlet__body">

                    <table class="table table-striped table-bordered table-hover" id="tbl_list_default" width="100%">
                        <thead>
                            <tr>
                                <th class="all" width=10px>#</th>
                                <th class="min-tablet">Kode Bucket Simulate</th>
                                <th class="min-tablet">Nama Bucket</th>
                                <th class="min-tablet">Segmentasi</th>
                                <th class="min-tablet">Input Regresi</th>
                                <th class="min-tablet">Model Terpilih</th>
                                <th class="min-tablet">Var 1</th>
                                <th class="min-tablet">Var 2</th>
                                <th class="none">Lag 1</th>
                                <th class="none">Lag 2</th>
                                <th class="min-tablet">Mape Average</th>
                                <th class="min-tablet">ECL No Model</th>
                                <th class="min-tablet">ECL Proporsional</th>
                                <th class="min-tablet">ECL Solver</th>
                                <th class="none">Dibuat tanggal</th>
                                <th class="none">Dibuat oleh</th>
                                <th class="none">Diubah tanggal</th>
                                <th class="none">Diubah oleh</th>
                                <th class="min-tablet">Action</th>
                            </tr>
                        </thead>
                        <tbody class="kt-datatable__body">
                            <?php $i = 1; foreach( $data->data as $d ): ?>
                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $d->draft_bucket->kode_draft_bucket; ?></td>
                                <td><?php echo $d->draft_bucket->nama_draft_bucket; ?></td>
                                <td><?php echo $d->segment->nama_segment; ?></td>
                                <td><?php if ($d->id_regresi == '1') {
                                    echo "ODR";
                                } elseif ($d->id_regresi == '2') {
                                    echo "Average";
                                } elseif ($d->id_regresi == '3') {
                                    echo "Sum Lossrate";
                                } elseif ($d->id_regresi == '4') {
                                    echo "Sum Delta Lossrate";
                                }?></td>
                                <td><?php echo $d->model->nomor; ?></td>
                                <td><?php echo $d->model->var1; ?></td>
                                <td><?php echo $d->model->var2; ?></td>
                                <td><?php echo $d->model->lag1; ?></td>
                                <td><?php echo $d->model->lag2; ?></td>
                                <td><?php echo round($d->average_mape * 100 , 2)." %"; ?></td>
                                <!-- <td><?php echo round($d->ecl_no_model , 2); ?></td>
                                <td><?php echo round($d->ecl_proporsional , 2); ?></td>
                                <td><?php echo round($d->ecl_solver , 2); ?></td> -->
                                <td><?php echo "Rp. " . number_format( $d->ecl_no_model,2, ',', '.');?></td>
                                <td><?php echo "Rp. " . number_format( $d->ecl_proporsional,2, ',', '.');?></td>
                                <td><?php echo "Rp. " . number_format( $d->ecl_solver,2, ',', '.');?></td>
                                <td><?php echo date("d M Y H:i:s", strtotime($d->created_date)); ?></td>
                                <td><?php echo $d->created_user->username; ?></td>
                                <td><?php 
                                if ($d->updated_date != null) {
                                    echo date("d M Y H:i:s", strtotime($d->updated_date)); 
                                }
                                ?></td>
                                <td><?php 
                                if ($d->updated_date != null) {
                                echo $d->updated_user->username; 
                                }
                                ?></td>
                                <td>
                                    <?php  $irb = base64_encode($d->id_draft_bucket); ?>
                                    <?php  $irbx = base64_encode($d->id_draft_ecl); ?>
                                    <div class="dropdown">
                                        <a href="javascript;" class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                            data-toggle="dropdown">
                                            <i class="la la-cog"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-sm">
                                            <ul class="kt-nav">
                                                <li class="kt-nav__item">
                                                    <a href="<?php echo site_url("model/view_draft_bucket?idb=".$irb); ?>"
                                                        class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon2-search"></i>
                                                        <span class="kt-nav__link-text">Lihat Bucket Simulate</span>
                                                    </a>
                                                </li>
                                                <?php if ($d->id_model != 0) { ?>
                                                <li class="kt-nav__item">

                                                    <a href="<?php echo site_url("ecl/view_mape_ecl?idb=".$irbx); ?>"
                                                        class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon-list-2"></i>
                                                        <span class="kt-nav__link-text">Lihat MAPE</span>
                                                    </a>
                                                </li>
                                                <?php } ?>
                                                <li class="kt-nav__item">
                                                    <a href="<?php echo site_url("ecl/view_model_ecl?idb=".$irbx); ?>"
                                                        class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon-edit-1"></i>
                                                        <span class="kt-nav__link-text">Lihat Perhitungan ECL</span>
                                                    </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <a href="<?php echo site_url("#"); ?>" data-toggle="modal"
                                                        data-target="#view_comment<?php echo $d->id_draft_ecl; ?>"
                                                        class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon-comment"></i>
                                                        <span class="kt-nav__link-text">Lihat Komentar</span>
                                                        <?php if ($d->list_komentar != []) { ?>
                                                        <!-- <span
                                                            class="kt-badge kt-badge--success"><?php echo count($d->list_komentar); ?></span> -->
                                                        <?php } ?>
                                                    </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <a href="<?php echo site_url("#"); ?>" data-toggle="modal"
                                                        data-target="#view_jurnal" onClick="return viewJurnal(
                                                            '<?php echo  $d->id_draft_ecl; ?>'
                                                        )" class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon-refresh"></i>
                                                        <span class="kt-nav__link-text">Lihat Jurnal</span>
                                                    </a>
                                                </li>
                                                <?php if ( $this->session->userdata('role') == '1' || $this->session->userdata('role') == 1 || $this->session->userdata('role') == '2' || $this->session->userdata('role') == 2 ) { ?>
                                                <li class="kt-nav__item">
                                                    <a onclick='return del_confirm()'
                                                        href="<?php echo site_url("Ecl/del_draft_ecl/$d->id_draft_ecl"); ?>"
                                                        class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                        <span class="kt-nav__link-text">Hapus ECL</span>
                                                    </a>
                                                </li>
                                                <?php } ?>
                                                <!-- <li class="kt-nav__item">
                                                    <a href="<?php echo site_url("#"); ?>" data-toggle="modal"
                                                        data-target="#edit_profile" onClick="return EditDraft(
                                                            '<?php echo  $d->id_draft_bucket; ?>', 
                                                            '<?php echo  $d->nama_draft_bucket; ?>', 
                                                        )" class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon-edit"></i>
                                                        <span class="kt-nav__link-text">Ganti Nama</span>
                                                    </a>
                                                </li>
                                                 -->
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $i = 1; foreach( $data->data as $d ): ?>
<div class="modal fade" id="view_comment<?php echo $d->id_draft_ecl; ?>" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Komentar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__body">
                    <div class="kt-scroll kt-scroll--pull" style="overflow:scroll; height:350px;">
                        <div class="kt-widget3">
                            <?php if ($d->list_komentar == []) { ?>
                            <div class="kt-widget3__item">
                                <h4 class="center">Tidak ada Komentar</h1>
                            </div>
                            <?php } else { ?>
                            <?php foreach ($d->list_komentar as $key => $kme) { 
                            if ($kme->flag != 0) {
                                continue;
                            }   
                            ?>
                            <div class="kt-widget3__item">
                                <div class="kt-widget3__header">
                                    <div class="kt-widget3__user-img">
                                        <?php if ( $kme->user->path_profile == "" ) { ?>
                                        <img class="kt-widget3__img"
                                            src="<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>"
                                            alt="image">
                                        <?php } else { ?>
                                        <?php $src =  $this->config->item('api_host') .'/view_file'. '/' .$kme->user->path_profile ;?>
                                        <img class="kt-widget3__img" src="<?php echo $src; ?>" alt="image" width="40px"
                                            height="40px">
                                        <?php } ?>
                                    </div>
                                    <div class="kt-widget3__info">
                                        <a href="#" class="kt-widget3__username">
                                            <?php echo $kme->user->nama; ?>
                                        </a><br>
                                        <span class="kt-widget3__time">
                                            <?php
                                            if ($kme->updated_date != null) {
                                                echo get_timeago(strtotime($kme->updated_date)); 
                                            } else {
                                                echo get_timeago(strtotime($kme->created_date)); 
                                            }
                                            ?>
                                        </span>
                                    </div>
                                    <span class="kt-widget3__status kt-font-info">
                                        <?php if ($this->session->userdata("ids") == $kme->created_by) { ?>
                                        <a id="btn_editkom<?php echo  $kme->id_komentar_bucket; ?>" href="#"
                                            class="kt-nav__link kt-font-info" onClick="return EditKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', 
                                                            '<?php echo $kme->komentar; ?>',
                                                            '<?php echo $d->id_draft_ecl; ?>' 
                                                        )">Ubah
                                        </a>
                                        &nbsp
                                        <a id="btn_batkom<?php echo  $kme->id_komentar_bucket; ?>" href="#"
                                            class="kt-nav__link kt-font-info" onClick="return batalKomentar(
                                                            '<?php echo  $kme->id_komentar_bucket; ?>', 
                                                            '<?php echo  $kme->komentar; ?>', 
                                                            '<?php echo $d->id_draft_ecl; ?>'
                                                        )" style="display:none">Batal
                                        </a>
                                        &nbsp
                                        <a href="<?php echo site_url("Model/deletekomen_draft_bucket?idk=$kme->id_komentar_bucket&typ=2"); ?>"
                                            class="kt-nav__link kt-font-info" onclick='return del_confirm()'>Hapus
                                        </a>
                                        <?php } ?>
                                    </span>
                                </div>
                                <div class="kt-widget3__body">
                                    <p class="kt-widget3__text">
                                        <?php echo $kme->komentar; ?>
                                    </p>
                                </div>
                            </div>
                            <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__foot">
                    <form method="post" enctype="multipart/form-data"
                        action="<?php echo site_url('model/addkomen_draft_bucket'); ?>">
                        <div class="form-row align-items-center">
                            <div class="col-auto">
                                <input value="2" name="type_coment" hidden></input>
                                <input value="0" name="flag_coment" hidden></input>
                                <input value="0" id="komen_id<?php echo $d->id_draft_ecl; ?>" name="id_coment"
                                    hidden></input>
                                <input value="<?php echo $d->id_draft_ecl; ?>" name="id_bucket" hidden></input>
                                <textarea id="komen_content<?php echo $d->id_draft_ecl; ?>" required
                                    style="height: 50px; width: 400px;" placeholder="Type here..."
                                    name="komentar"></textarea>
                            </div>
                            <div class="col-auto">
                                <button type="submit" class="btn btn-primary teratur-button">Kirim</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>

<?php
function get_timeago( $ptime )
{
    $estimate_time = time() - $ptime;
    if( $estimate_time < 1 )
    {
        return 'kurang dari 1 detik yang lalu';
    }
    $condition = array(
                12 * 30 * 24 * 60 * 60  =>  'tahun',
                30 * 24 * 60 * 60       =>  'bulan',
                24 * 60 * 60            =>  'hari',
                60 * 60                 =>  'jam',
                60                      =>  'menit',
                1                       =>  'detik'
    );
    foreach( $condition as $secs => $str )
    {
        $d = $estimate_time / $secs;
        if( $d >= 1 )
        {
            $r = round( $d );
            return 'sekitar ' . $r . ' ' . $str . ( $r > 1 ? '' : '' ) . ' yang lalu';
        }
    }
}
?>


<div class="modal fade" id="view_jurnal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Lihat Jurnal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data" action="#">
                <div class="modal-body">
                    <input id="id_draft_ecl" hidden></input>
                    <div class="row">
                        <div class="col-lg-6"><br />
                            <div class="column" style="float: left;width: 45%;">
                                <p><b>Kode Model Simulate</b></p>
                                <p><b>Nama Model Simulate</b></p>
                                <p><b>Segmentasi</b></p>
                                <p><b>Normalisasi</b></p>
                                <p><b>Regresi</b></p>
                            </div>
                            <div class="column" style="float: left;width: 55%;">
                                <p id="kode_draft_bucket"></p>
                                <p id="nama_draft_bucket"></p>
                                <p id="segment"></p>
                                <p id="normalisasi"></p>
                                <p id="regresi"></p>
                            </div>
                        </div>
                        <div class="col-lg-6"><br />
                            <div class="column" style="float: left;width: 45%;">
                                <p><b>Jumlah Moving</b></p>
                                <p><b>Periode Mulai</b></p>
                                <p><b>Periode Selesai</b></p>
                                <p><b>Dibuat oleh</b></p>
                                <p><b>Tanggal dibuat</b></p>
                            </div>
                            <div class="column" style="float: left;width: 55%;">
                                <p id="moving"></p>
                                <p id="periode_mulai"></p>
                                <p id="periode_selesai"></p>
                                <p id="dibuat_oleh"></p>
                                <p id="dibuat_tgl"></p>
                            </div>
                        </div>
                        <div class="col-lg-6" id="div_model_mape"><br />
                            <div class="column" style="float: left;width: 45%;">
                                <p><b>Model Nomor</b></p>
                                <p><b>Var 1</b></p>
                                <p><b>Var 2</b></p>
                                <p><b>Lag 1</b></p>
                                <p><b>Lag 2</b></p>
                            </div>
                            <div class="column" style="float: left;width: 55%;">
                                <p id="model_nomor"></p>
                                <p id="var1"></p>
                                <p id="var2"></p>
                                <p id="lag1"></p>
                                <p id="lag2"></p>
                            </div>
                        </div>
                        <div class="col-lg-6"><br />
                            <div class="column" style="float: left;width: 45%;">
                                <p><b>LGD</b></p>
                                <p class="div_model_jur"><b>Month Forecast</b></p>
                                <p class="div_model_jur"><b>Skenario Normal</b></p>
                                <p class="div_model_jur"><b>Skenario Optimis</b></p>
                                <p class="div_model_jur"><b>Skenario Pesimis</b></p>
                            </div>
                            <div class="column" style="float: left;width: 55%;">
                                <p id="lgd"></p>
                                <p id="forecast" class="div_model_jur"></p>
                                <p id="skenario_normal" class="div_model_jur"></p>
                                <p id="skenario_optimis" class="div_model_jur"></p>
                                <p id="skenario_pesimis" class="div_model_jur"></p>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <table id="table_jurnal_ecl"
                                class="table table-striped table-bordered table-hover table-responsive">
                                <thead>
                                    <tr>
                                        <th class="all" width="200px">Periode ECL</th>
                                        <th class="all" width="200px">Average Mape</th>
                                        <th class="all" width="200px">ECL No Model</th>
                                        <th class="all" width="200px">ECL Proporsional</th>
                                        <th class="all" width="200px">ECL Solver</th>
                                    </tr>
                                </thead>
                                <tbody class="kt-datatable__body" id="table_model_jurnal">
                                    <tr>
                                        <td id="periode"></td>
                                        <td id="mape"></td>
                                        <td id="v_ecl"></td>
                                        <td id="v_ecl_propo"></td>
                                        <td id="v_ecl_solver"></td>
                                    </tr>
                                    <input type="text" class="form-control" id="ecl" hidden>
                                    <input type="text" class="form-control" id="ecl_propo" hidden>
                                    <input type="text" class="form-control" id="ecl_solver" hidden>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xl-4">
                            <div class="form-group">
                                <label>Pilih Model ECL yang di inginkan</label>
                                <select id="jurnal_model_pilihan" class="form-control">
                                    <option value="1">No Model</option>
                                    <option value="2">Model penyesuaian Proporsional</option>
                                    <option value="3">Model penyesuaian Solver</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-4">
                            <div class="form-group">
                                <label>Saldo Penyisihan Eksiting</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Rp. </span>
                                    </div>
                                    <input type="number" class="form-control" id="jurnal_saldo_ecl"
                                        placeholder="masukan saldo">
                                </div>
                                <span class="form-text text-muted">Masukan saldo penyisihan saat
                                    ini</span>
                            </div>
                        </div>
                        <div class="col-xl-4">
                            <div class="form-group">
                                <label>Tekan Button untuk proses jurnal.</label>
                                <button onclick="prosesJurnal2()" type="button"
                                    class="form-control btn btn-sm btn-danger teratur-button">
                                    <i class="flaticon2-fast-next"></i> Proses Jurnal
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
                    <div class="kt-heading kt-heading--md" id="title_jurnal_terpilih" style="display:none">Summary Model
                        Terpilih dan ECL</div>
                    <div class="kt-form__section kt-form__section--first">
                        <div class="kt-wizard-v1__review">
                            <div class="row">
                                <table class="table table-striped table-bordered table-hover" id="modalPSAK71">
                                    <tbody class="kt-datatable__body" id="table_penerapan_awal"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="row">
                        <div class="col-12">
                            <button onclick="ExportJurnalEcl()" type="button"
                                class="btn btn-sm btn-danger teratur-button float-right">
                                <i class="flaticon-download-1"></i> Export Excel
                            </button>
                        </div>
                    </div> -->
                </div>
                <!-- <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button" onClick="act_confirm()">Prosess
                        Data</button>
                </div> -->
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>

<script type="text/javascript">
function viewJurnal(id) {
    $("#title_jurnal_terpilih").hide();
    $("#table_penerapan_awal").hide();
    $("#id_draft_ecl").val(id);

    draft = $.ajax({
        data: {
            id: id
        },
        type: "POST",
        url: "<?php echo site_url('Ecl/getDraftECL');?>",
        async: false
    }).responseText;
    console.log("[draft]: " + draft);
    let dataP = JSON.parse(draft);

    $("#kode_draft_bucket").text(": " + dataP.draft_bucket.kode_draft_bucket);
    $("#nama_draft_bucket").text(": " + dataP.draft_bucket.nama_draft_bucket);
    $("#segment").text(": " + dataP.segment.nama_segment);
    $("#normalisasi").text(": " + dataP.draft_bucket.flag_normalisasi);
    let reg = "";
    if (dataP.id_regresi == 1) {
        reg = "ODR";
    } else if (dataP.id_regresi == 2) {
        reg = "Average";
    } else if (dataP.id_regresi == 3) {
        reg = "Sum Lossrate";
    } else if (dataP.id_regresi == 4) {
        reg = "Sum Delta Lossrate";
    }
    $("#regresi").text(": " + reg);
    $("#moving").text(": " + dataP.draft_bucket.moving_average + " Baris");
    $("#periode_mulai").text(": " + convertDateMS(dataP.draft_bucket.start_date));
    $("#periode_selesai").text(": " + convertDateMS(dataP.draft_bucket.end_date));
    $("#dibuat_oleh").text(": " + dataP.created_user.username);
    $("#dibuat_tgl").text(": " + dataP.created_date);

    if (dataP.model != null) {

        $("#model_nomor").text(": " + dataP.model.nomor);
        $("#var1").text(": " + dataP.model.var1);
        $("#var2").text(": " + dataP.model.var2);
        $("#lag1").text(": " + dataP.model.lag1);
        $("#lag2").text(": " + dataP.model.lag2);

    } else {

        $("#model_nomor").text(": ");
        $("#var1").text(": ");
        $("#var2").text(": ");
        $("#lag1").text(": ");
        $("#lag2").text(": ");

    }

    $("#forecast").text(": " + dataP.month_forecast + " Bulan");
    $("#lgd").text(": " + (dataP.lgd * 100) + " %");
    $("#skenario_normal").text(": " + (dataP.normal * 100) + " %");
    $("#skenario_optimis").text(": " + (dataP.optimis * 100) + " %");
    $("#skenario_pesimis").text(": " + (dataP.pesimis * 100) + " %");
    $("#periode").text(convertDate(dataP.periode));

    let aveMape = (dataP.average_mape * 100);
    $("#mape").text(aveMape.toFixed(2) + " %");
    // console.log(dataP.average_mape * 100);
    // console.log(aveMape.toFixed(2));

    $("#v_ecl").text(convertToRupiah(dataP.ecl_no_model.toFixed(2)));
    $("#v_ecl_propo").text(convertToRupiah(dataP.ecl_proporsional.toFixed(2)));
    $("#v_ecl_solver").text(convertToRupiah(dataP.ecl_solver.toFixed(2)));
    document.getElementById('ecl').value = dataP.ecl_no_model;
    document.getElementById('ecl_propo').value = dataP.ecl_proporsional;
    document.getElementById('ecl_solver').value = dataP.ecl_solver;

    //ini untuk misalkan sudah di update data sebelumnya.
    if (dataP.result_flag_model != 0) {

        document.getElementById('jurnal_saldo_ecl').value = dataP.result_saldo_exiting;
        document.getElementById('jurnal_model_pilihan').value = dataP.result_flag_model;

        let saldo71 = "";
        if (dataP.result_flag_model == 1) {
            saldo71 = dataP.ecl_no_model;
        } else if (dataP.result_flag_model == 2) {
            saldo71 = dataP.ecl_proporsional;
        } else {
            saldo71 = dataP.ecl_solver;
        }

        jurnal = $.ajax({
            data: {
                periode: convertDate(dataP.periode),
                saldo: dataP.result_saldo_exiting,
                pilihan: dataP.result_flag_model,
                saldo71: saldo71
            },
            type: "POST",
            url: "<?php echo site_url('Model/get_jurnal_modal2');?>",
            async: false
        }).responseText;
        console.log("jurnal: " + jurnal);
        $('#table_penerapan_awal').html(jurnal);
        $("#title_jurnal_terpilih").show();
        $("#table_penerapan_awal").show();

    } else {
        document.getElementById('jurnal_saldo_ecl').value = "";
    }

    if (dataP.model != null) {

        $("#div_model_mape").show();
        $(".div_model_jur").show();

    } else {
        $("#mape").text("");
        $("#div_model_mape").hide();
        $(".div_model_jur").hide();
    }

}

function convertDateMS(date) {
    console.log("date: " + date);
    let res = date.split("T");
    return res[0];
}

function convertDate(date) {
    console.log("date: " + date);
    let today = new Date(date);
    today = today.getTime() + (1000 * 60 * 60 * 24);
    today = new Date(today);
    console.log("today +1day: " + today);
    let dd = today.getDate();
    let mm = today.getMonth() + 1;
    let yyyy = today.getFullYear();
    console.log("day: " + dd);
    console.log("month: " + mm);
    console.log("year: " + yyyy);
    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }
    today = yyyy + '-' + mm + '-' + dd;
    console.log("result date: " + today);
    return today;
}

function convertToRupiah(angka) {
    var res = angka.toString().replace(".", ",");
    split = res.split(','),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{1,3}/gi);

    if (ribuan) {
        separator = sisa ? ',' : '';
        rupiah += separator + ribuan.join(',');
    }
    rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
    return "Rp. " + rupiah;
}

function prosesJurnal2() {
    let periode = $('#table_jurnal_ecl #periode').text();
    let saldo = document.getElementById('jurnal_saldo_ecl').value;
    let pilihan = document.getElementById('jurnal_model_pilihan').value;
    let id_draft_ecl = document.getElementById('id_draft_ecl').value;

    let ecl_no = $('#ecl').val();
    let ecl_pro = $('#ecl_propo').val();
    let ecl_sol = $('#ecl_solver').val();
    let saldo71 = "";
    if (saldo == null || saldo == "") {
        return alert("Saldo Exiting harus di isi");
    }

    $('#title_jurnal_terpilih').show();
    console.log("periode: " + periode);
    console.log("saldo: " + saldo);
    console.log("pilihan: " + pilihan);
    console.log("ecl_no: " + ecl_no);
    console.log("ecl_pro: " + ecl_pro);
    console.log("ecl_sol: " + ecl_sol);
    if (pilihan == 1) {
        saldo71 = ecl_no;
    } else if (pilihan == 2) {
        saldo71 = ecl_pro;
    } else {
        saldo71 = ecl_sol;
    }

    //SAVE ECL UPDATE SALDO
    updateECL = $.ajax({
        data: {
            id_draft_ecl: id_draft_ecl,
            result_flag_model: pilihan,
            result_saldo_exiting: saldo
        },
        type: "POST",
        url: "<?php echo site_url('Ecl/updateDraftECL');?>",
        async: false
    }).responseText;
    console.log("[updateECL]: " + updateECL);
    // let dataP = JSON.parse(updateECL);

    jurnal = $.ajax({
        data: {
            periode: periode,
            saldo: saldo,
            pilihan: pilihan,
            saldo71: saldo71
        },
        type: "POST",
        url: "<?php echo site_url('Model/get_jurnal_modal2');?>",
        async: false
    }).responseText;
    console.log("jurnal: " + jurnal);
    $('#table_penerapan_awal').html(jurnal);
    $("#title_jurnal_terpilih").show();
    $("#table_penerapan_awal").show();
}

function EditKomentar(id, content, id_draft_bucket) {
    document.getElementById("komen_id" + id_draft_bucket).value = id;
    document.getElementById("komen_content" + id_draft_bucket).value = content;
    $("#btn_editkom" + id).hide();
    $("#btn_batkom" + id).show();
}

function batalKomentar(id, content, id_draft_bucket) {
    document.getElementById("komen_id" + id_draft_bucket).value = 0;
    document.getElementById("komen_content" + id_draft_bucket).value = "";
    $("#btn_editkom" + id).show();
    $("#btn_batkom" + id).hide();
}
</script>