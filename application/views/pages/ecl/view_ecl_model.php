<style>
table {
    table-layout: fixed;
    display: block;
    overflow-x: auto;
    white-space: nowrap;
}

td {
    overflow: hidden;
    text-overflow: ellipsis;
}
</style>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        View Draft ECL</h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon-list-1"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            No Model / Proporsional / Solver</a>
                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>

        <!-- <?php echo json_encode($data); ?> -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">

                    </div>

                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <!-- <a href="#" class="btn btn-brand btn-icon-sm teratur-button" data-toggle="modal"
                                    data-target="#add_profile">
                                    <i class="flaticon-plus"></i>
                                    Ubah Data
                                </a> -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">
                    <?php $idb = $data->data->id_draft_ecl; 
                    
                    // $ecl_no_model = "0";
                    // $ecl_proporsional = "0";
                    // $ecl_solver = "0";
                    // if ($data->data->ecl_list > 0) {  
                    //     if ($data->data->ecl_list[0]->no_model_sum_ecl != null) {
                    //         $ecl_no_model = $data->data->ecl_list[0]->no_model_sum_ecl;
                    //     }
                    // }
                    // if ($data->data->ecl_list > 1) {
                    //     if ($data->data->ecl_list[1]->proporsional_sum_ecl_forward_looking != null) {
                    //         $ecl_proporsional = $data->data->ecl_list[1]->proporsional_sum_ecl_forward_looking;
                    //     }
                    // }
                    // if ($data->data->ecl_list > 1) {
                    //     if ($data->data->ecl_list[1]->solver_sum_ecl_forward_looking != null) {
                    //         $ecl_solver = $data->data->ecl_list[1]->solver_sum_ecl_forward_looking;
                    //     }
                    // }
                    
                    ?>
                    <div class="row">
                        <!-- <h2> Hasil MAPE</h2> -->
                        <div class="col-lg-6">
                            <div class="column" style="float: left;width: 40%;">
                                <p><b>Kode Model Simulate</b></p>
                                <p><b>Nama Model Simulate</b></p>
                                <p><b>Segmentasi</b></p>
                                <p><b>Input Regresi</b></p>
                                <!-- <p><b>Periode Bulan Forecast</b></p> -->
                                <p><b>Periode Awal Piutang</b></p>
                                <p><b>Periode Akhir Piutang</b></p>
                            </div>
                            <div class="column" style="float: left;width: 60%;" id="model_mape_terpilih">
                                <p>: <?php echo $data->data->draft_bucket->kode_draft_bucket; ?></p>
                                <p>: <?php echo $data->data->draft_bucket->nama_draft_bucket; ?></p>
                                <p>: <?php echo $data->data->segment->nama_segment; ?></p>
                                <p>: <?php 
                                if ($data->data->id_regresi == '1') {
                                   echo "ODR";
                                } elseif ($data->data->id_regresi == '2') {
                                   echo "Average";
                                } elseif ($data->data->id_regresi == '3') {
                                    echo "Sum Loss Rate";
                                } else {
                                    echo "Sum Delta Loss Rate";
                                }
                                ?></p>
                                <!-- <p>: <?php echo $data->data->month_forecast; ?></p> -->
                                <p>: <?php echo date("d M Y",strtotime($data->data->draft_bucket->start_date)); ?></p>
                                <p>: <?php echo date("d M Y",strtotime($data->data->draft_bucket->end_date)); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div id="content-4" class="kt-wizard-v1__content" data-ktwizard-type="step-content">
                            <div class="kt-heading kt-heading--md">
                                <h3>Hasil Perhitungan ECL</h3>
                            </div>
                            <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed">
                            </div>
                            <div class="kt-heading kt-heading--md">No Model</div>
                            <div class="kt-form__section kt-form__section--first">
                                <div class="kt-wizard-v1__form">
                                    <div class="row">
                                        <div class="col-12">
                                            <table class="table table-striped table-bordered table-hover"
                                                id="nomodel_ecl">
                                                <thead>
                                                    <tr>
                                                        <th class="all" width=200px>Bucket</th>
                                                        <th class="all" width=200px>PD Base</th>
                                                        <!-- <th class="all" width=200px>MEV Effect</th> -->
                                                        <!-- <th class="all" width=200px>Logit PD</th> -->
                                                        <!-- <th class="all" width=200px>PD Forward Looking</th> -->
                                                        <th class="all" width=200px>LGD</th>
                                                        <th class="all" width=200px>EAD</th>
                                                        <th class="all" width=200px>ECL</th>
                                                        <!-- <th class="all" width=200px>ECL Forward Lookong</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody class="kt-datatable__body" id="no_modal_list">
                                                    <?php
                                                    $no_modal_list = "";
                                                    $no_model = $data->data->ecl_list[0];
                                                    if ( $no_model->no_model_list == null || $no_model->no_model_list == "") {
                                                        $no_modal_list = "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
                                                    } else {
                                                        foreach ( $no_model->no_model_list as $nm ) {
                                                            $no_modal_list .=
                                                            "<tr>"
                                                            ."<td>".$nm->bucket." Hari</td>"
                                                            ."<td>".round(($nm->pd_base*100), 2)." %</td>"
                                                            ."<td>".round(($nm->lgd*100), 2)." %</td>"
                                                            ."<td>".number_format( $nm->ead,2, ',', '.')."</td>"
                                                            ."<td>".number_format( $nm->ecl_base,2, ',', '.')."</td>"
                                                            ."</tr>";
                                                        }
                                                        //."Rp. " . number_format( $r2->jumlah_outstanding, 2, ',', '.').
                                                        $no_modal_list .=
                                                            "<tr>"
                                                            ."<td></td>"
                                                            ."<td></td>"
                                                            ."<td>Jumlah</td>"
                                                            ."<td>".number_format( $no_model->no_model_sum_ead,2, ',', '.')."</td>"
                                                            ."<td>".number_format( $no_model->no_model_sum_ecl,2, ',', '.')."</td>"
                                                            ."</tr>";
                                                    }
                                                    echo $no_modal_list;
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed">
                            </div>
                            <?php if ($data->data->id_model != 0) { ?>
                            <div class="kt-heading kt-heading--md">With Model</div>
                            <div class="kt-form__section kt-form__section--first">
                                <div class="kt-wizard-v1__form">
                                    <div class="row">
                                        <div class="col-12">
                                            <table class="table table-striped table-bordered table-hover">
                                                <tbody class="kt-datatable__body" id="model_title">
                                                    <?php 
                                                    $regresi = $data->data->mape_draft->input_regresi;
                                                    $dt = $data->data->ecl_list[0];
                                                    $modal_list = "";
                                                    $modal_list .= 
                                                    "<tr>"
                                                    ."<td>Input Regresi</td>"
                                                    ."<td>".$regresi."</td>"
                                                    ."</tr>";
                                                    $modal_list .= 
                                                    "<tr>"
                                                    ."<td>Periode</td>"
                                                    ."<td>".$dt->periode."</td>"
                                                    ."</tr>";
                                                    $modal_list .= 
                                                    "<tr>"
                                                    ."<td>Month Forecast</td>"
                                                    ."<td>".$dt->periode_forecast."</td>"
                                                    ."</tr>";
                                                    echo $modal_list;
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-form__section kt-form__section--first">
                                <div class="kt-wizard-v1__form">
                                    <div class="row">
                                        <div class="col-12">
                                            <table class="table table-striped table-bordered table-hover"
                                                id="model_fl_mape">
                                                <thead>
                                                    <tr>
                                                        <th class="all" width=20px>No Model</th>
                                                        <th class="all" width=200px>Var 1</th>
                                                        <th class="all" width=200px>Var 2</th>
                                                        <th class="all" width=200px>Lag 1</th>
                                                        <th class="all" width=200px>Lag 2</th>
                                                        <th class="all" width=200px>C Coef</th>
                                                        <th class="all" width=200px>Koef Var 1</th>
                                                        <th class="all" width=200px>Koef Var 2</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="kt-datatable__body" id="model_mape">
                                                    <tr>
                                                        <td><?php echo $data->data->mape_draft->model->nomor; ?></td>
                                                        <td><?php echo $data->data->mape_draft->model->var1; ?></td>
                                                        <td><?php echo $data->data->mape_draft->model->var2; ?></td>
                                                        <td><?php echo $data->data->mape_draft->model->lag1; ?></td>
                                                        <td><?php echo $data->data->mape_draft->model->lag2; ?></td>
                                                        <td><?php echo $data->data->mape_draft->model->c_coef; ?></td>
                                                        <td><?php echo $data->data->mape_draft->model->koef_var1; ?>
                                                        </td>
                                                        <td><?php echo $data->data->mape_draft->model->koef_var2; ?>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="kt-form__section kt-form__section--first">
                                <div class="kt-wizard-v1__form">
                                    <div class="row">
                                        <div class="col-12">
                                            <table class="table table-striped table-bordered table-hover"
                                                id="model_skenario_ecl" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th class="all" width=400px>Skenario</th>
                                                        <th class="all" width=400px>Var 1</th>
                                                        <th class="all" width=400px>Var 2</th>
                                                        <th class="all" width=400px>Probabilty Weighted</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="kt-datatable__body" id="skenario">
                                                    <?php 
                                                $skenario = "";
                                                $skenarios = $data->data->ecl_list[1];
                                                if ( $skenarios->skenario == null || $skenarios->skenario == " ") {
                                                    $skenario = "<tr><td></td><td></td>td></td><td></td><td></td></tr>";
                                                } else {
                                                    $skenario = 
                                                    '<tr>'
                                                    ."<td>STDEV</td>"
                                                    .'<td>'.$skenarios->skenario->stdev_var1.'</td>'
                                                    .'<td>'.$skenarios->skenario->stdev_var2.'</td>'
                                                    .'<td>-</td>'
                                                    .'</tr>'
                                                    .'<tr>'
                                                    ."<td>Normal</td>"
                                                    .'<td>'.$skenarios->skenario->normal_var1.'</td>'
                                                    .'<td>'.$skenarios->skenario->normal_var2.'</td>'
                                                    .'<td>'.$skenarios->skenario->normal_probability *100 .'%</td>'
                                                    .'</tr>'
                                                    ."<td>Optimis</td>"
                                                    .'<td>'.$skenarios->skenario->optimis_var1.'</td>'
                                                    .'<td>'.$skenarios->skenario->optimis_var2.'</td>'
                                                    .'<td>'.$skenarios->skenario->optimis_probability * 100 .'%</td>'
                                                    .'</tr>'
                                                    ."<td>Pesimis</td>"
                                                    .'<td>'.$skenarios->skenario->pesimis_var1.'</td>'
                                                    .'<td>'.$skenarios->skenario->pesimis_var2.'</td>'
                                                    .'<td>'.$skenarios->skenario->pesimis_probability * 100 .'%</td>'
                                                    .'</tr>'
                                                    ."<td>Weighted MEV</td>"
                                                    .'<td>'.$skenarios->skenario->weigthed_var1.'</td>'
                                                    .'<td>'.$skenarios->skenario->weigthed_var2.'</td>'
                                                    .'<td>-</td>'
                                                    .'</tr>'
                                                    ;
                                                }
                                                echo $skenario;
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="kt-heading kt-heading--md">Total Penyesuaian PSAK 71</div>
                            <div class="kt-form__section kt-form__section--first">
                                <div class="kt-wizard-v1__form">
                                    <div class="row">
                                        <div class="col-12">
                                            <table class="table table-striped table-bordered table-hover"
                                                id="model_psak71_ecl" width="100%">
                                                <?php 
                                                $regresi = $data->data->id_regresi;
                                                $result = $data->data->ecl_list[1];
                                                if ($regresi == '1') {
                                                    $body = 
                                                    "<thead>
                                                    <tr>
                                                    <th class = 'all' width = 800px>Logit Expected PD</th>
                                                    <th class = 'all' width = 800px>".round(($result->logit_expected_psak), 2)."</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                    <td>Expected PD</td>
                                                    <td>".round(($result->expected_psak*100), 2)." %</td>
                                                    </tr>
                                                    </tbody>";
                                                } elseif ($regresi == '2') {
                                                    $body = 
                                                    "<thead>
                                                    <tr>
                                                    <th class = 'all' width = 800px>Logit Expected Average Lossrate</th>
                                                    <th class = 'all' width = 800px>".round(($result->logit_expected_psak), 2)."</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                    <td>Expected Average Lossrate</td>
                                                    <td>".round(($result->expected_psak*100), 2)." %</td>
                                                    </tr>
                                                    </tbody>";
                                                } elseif ($regresi == '3') {
                                                    $body = 
                                                    "<thead>
                                                    <tr>
                                                    <th class = 'all' width = 800px>Expected Sum Lossrate</th>
                                                    <th class = 'all' width = 800px>".round(($result->expected_psak*100), 2)." %</th>
                                                    </tr>
                                                    </thead>";
                                                } elseif ($regresi == '4') {
                                                    $body = 
                                                    "<thead>
                                                    <tr>
                                                    <th class = 'all' width = 800px>Expected Sum delta lossrate</th>
                                                    <th class = 'all' width = 800px>".round(($result->expected_psak*100), 2)." %</th>
                                                    </tr>
                                                    </thead>";
                                                } 
                                                echo $body;
                                                
                                                ?>

                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="kt-heading kt-heading--md">Persentase MEV effect (multiplier)</div>
                            <div class="kt-form__section kt-form__section--first">
                                <div class="kt-wizard-v1__form">
                                    <div class="row">
                                        <div class="col-12">
                                            <table class="table table-striped table-bordered table-hover"
                                                id="model_mevs_ecl" width="100%">
                                                <?php 
                                                if ($regresi == '1') {
                                                    $body = 
                                                    "<thead>
                                                    <tr>
                                                    <th class = 'all' width = 800px>Expected ODR</th>
                                                    <th class = 'all' width = 800px>".round(($result->expected*100), 2)." %</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                    <td>Last ODR</td>
                                                    <td>".round(($result->last*100), 2)." %</td>
                                                    </tr>
                                                    <tr>
                                                    <td>MEV Effect</td>
                                                    <td>".round(($result->mev*100), 2)." %</td>
                                                    </tr>
                                                    </tbody>";
                                                } elseif ($regresi == '2') {
                                                    $body = 
                                                    "<thead>
                                                    <tr>
                                                    <th class = 'all' width = 800px>Expected Average Lossrate</th>
                                                    <th class = 'all' width = 800px>".round(($result->expected*100), 2)." %</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                    <td>Last Average Lossrate</td>
                                                    <td>".round(($result->last*100), 2)." %</td>
                                                    </tr>
                                                    <tr>
                                                    <td>MEV Effect</td>
                                                    <td>".round(($result->mev*100), 2)." %</td>
                                                    </tr>
                                                    </tbody>";
                                                } elseif ($regresi == '3') {
                                                    $body = 
                                                    "<thead>
                                                    <tr>
                                                    <th class = 'all' width = 800px>Expected Sum Lossrate</th>
                                                    <th class = 'all' width = 800px>".round(($result->expected*100), 2)." %</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                    <td>Last Sum Lossrate</td>
                                                    <td>".round(($result->last*100), 2)." %</td>
                                                    </tr>
                                                    <tr>
                                                    <td>MEV Effect</td>
                                                    <td>".round(($result->mev*100), 2)." %</td>
                                                    </tr>
                                                    </tbody>";
                                                } elseif ($regresi == '4') {
                                                    $body = "";
                                                }
                                        
                                                echo $body;
                                                
                                                ?>

                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="kt-heading kt-heading--md">Metode Distribusi Proporsional</div>
                            <div class="kt-form__section kt-form__section--first">
                                <div class="kt-wizard-v1__form">
                                    <div class="row">
                                        <div class="col-12">
                                            <table class="table table-striped table-bordered table-hover"
                                                id="model_proposional_ecl" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th class="all" width=200px>Bucket</th>
                                                        <th class="all" width=200px>PD Base</th>
                                                        <th class="all" width=200px>MEV Effect</th>
                                                        <!-- <th class="all" width=200px>Logit PD</th> -->
                                                        <th class="all" width=200px>PD Forward Looking</th>
                                                        <th class="all" width=200px>LGD</th>
                                                        <th class="all" width=200px>EAD</th>
                                                        <th class="all" width=200px>ECL Base</th>
                                                        <th class="all" width=200px>ECL Forward Lookong</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="kt-datatable__body" id="proporsional_list">
                                                    <?php 
                                                    $proporsional_list = "";
                                                    $propo = $data->data->ecl_list[1];
                                                    if ( $propo->proporsional_list == null || $propo->proporsional_list == "") {
                                                        $proporsional_list = "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
                                                    } else {
                                                        foreach ( $propo->proporsional_list as $nm ) {
                                                            $proporsional_list .=
                                                            "<tr>"
                                                            ."<td>".$nm->bucket." Hari</td>"
                                                            ."<td>".round(($nm->pd_base*100), 2)." %</td>"
                                                            ."<td>".round(($nm->mev_effect*100), 2)." %</td>"
                                                            ."<td>".round(($nm->pd_forward_looking*100), 2)." %</td>"
                                                            ."<td>".round(($nm->lgd*100), 2)." %</td>"
                                                            ."<td>".number_format( $nm->ead,2, ',', '.')."</td>"
                                                            ."<td>".number_format( $nm->ecl_base,2, ',', '.')."</td>"
                                                            ."<td>".number_format( $nm->ecl_forward_looking,2, ',', '.')."</td>"
                                                            ."</tr>";
                                                        }
                                            
                                                        $proporsional_list .=
                                                            "<tr>"
                                                            ."<td></td>"
                                                            ."<td></td>"
                                                            ."<td></td>"
                                                            ."<td></td>"
                                                            ."<td>Jumlah</td>"
                                                            ."<td>".number_format( $propo->proporsional_sum_ead,2, ',', '.')."</td>"
                                                            ."<td>".number_format( $propo->proporsional_sum_ecl,2, ',', '.')."</td>"
                                                            ."<td>".number_format( $propo->proporsional_sum_ecl_forward_looking,2, ',', '.')."</td>"
                                                            ."</tr>";
                                                    }
                                            
                                                    echo $proporsional_list;
                                                    
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed">
                            </div>
                            <div class="kt-heading kt-heading--md">Metode Distribusi Solver</div>
                            <div class="kt-form__section kt-form__section--first">
                                <div class="kt-wizard-v1__form">
                                    <div class="row">
                                        <div class="col-12">
                                            <table class="table table-striped table-bordered table-hover"
                                                id="model_mevs_solver" width="100%">
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-heading kt-heading--md"></div>
                            <div class="kt-form__section kt-form__section--first">
                                <div class="kt-wizard-v1__form">
                                    <div class="row">
                                        <div class="col-12">
                                            <table class="table table-striped table-bordered table-hover"
                                                id="model_solver_ecl" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th class="all" width=200px>Bucket</th>
                                                        <th class="all" width=200px>PD Base</th>
                                                        <!-- <th class="all" width=200px>MEV Effect</th> -->
                                                        <th class="all" width=200px>Logit PD</th>
                                                        <th class="all" width=200px>PD Forward Looking</th>
                                                        <th class="all" width=200px>LGD</th>
                                                        <th class="all" width=200px>EAD</th>
                                                        <th class="all" width=200px>ECL Base</th>
                                                        <th class="all" width=200px>ECL Forward Lookong</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="kt-datatable__body" id="solver_list">
                                                    <?php 
                                                    $solver_list = "";
                                                    $solver = $data->data->ecl_list[1];
                                                    if ( $solver->solver_list == null || $solver->solver_list == "") {
                                                        $solver_list = "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
                                                    } else {
                                                        foreach ( $solver->solver_list as $nm ) {
                                                            $solver_list .=
                                                            "<tr>"
                                                            ."<td>".$nm->bucket." Hari</td>"
                                                            ."<td>".round(($nm->pd_base*100), 2)." %</td>"
                                                            ."<td>".round(($nm->logit_pd), 6)."</td>"
                                                            ."<td>".round(($nm->pd_forward_looking*100), 2)." %</td>"
                                                            ."<td>".round(($nm->lgd*100), 2)." %</td>"
                                                            ."<td>".number_format( $nm->ead,2, ',', '.')."</td>"
                                                            ."<td>".number_format( $nm->ecl_base,2, ',', '.')."</td>"
                                                            ."<td>".number_format( $nm->ecl_forward_looking,2, ',', '.')."</td>"
                                                            ."</tr>";
                                                        }
                                            
                                                        $solver_list .=
                                                        "<tr>"
                                                        ."<td>Average</td>"
                                                        ."<td>".round(($solver->solver_average_pd_base*100), 2)." %</td>"
                                                        ."<td></td>"
                                                        ."<td>".round(($solver->solver_average_pd_forward_looking*100), 2)." %</td>"
                                                        ."<td>Jumlah</td>"
                                                        ."<td>".number_format( $solver->solver_sum_ead,2, ',', '.')."</td>"
                                                        ."<td>".number_format( $solver->solver_sum_ecl,2, ',', '.')."</td>"
                                                        ."<td>".number_format( $solver->solver_sum_ecl_forward_looking,2, ',', '.')."</td>"
                                                        ."</tr>";
                                                    }
                                            
                                                    echo $solver_list;
                                                    
                                                    ?>
                                                </tbody>
                                            </table>
                                            <table class="table table-striped table-bordered table-hover"
                                                id="model_target_scalling" width="100%">

                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-heading kt-heading--md"></div>
                            <div class="kt-form__section kt-form__section--first">
                                <div class="kt-wizard-v1__form">
                                    <div class="row">
                                        <div class="col-12">
                                            <table class="table table-striped table-bordered table-hover"
                                                id="model_scaling_target" width="100%">

                                                <?php  
                                                $result = $data->data->ecl_list[1];
                                                if ($regresi == '1') {
                                                $body = "";
                                                } elseif ($regresi == '2') {
                                                $body =
                                                "<thead>
                                                    <tr>
                                                        <th class='all' width=800px>Target</th>
                                                        <th class='all' width=800px>".round(($result->target), 6)."</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Scalling</td>
                                                        <td>".round(($result->scaling), 2)."</td>
                                                    </tr>
                                                </tbody>";
                                                } elseif ($regresi == '3') {
                                                $body = "";
                                                } elseif ($regresi == '4') {
                                                $body =
                                                "<thead>
                                                    <tr>
                                                        <th class='all' width=800px>Target</th>
                                                        <th class='all' width=800px>".round(($result->target), 6)."</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Scalling</td>
                                                        <td>".round(($result->scaling), 2)."</td>
                                                    </tr>
                                                </tbody>";
                                                }

                                                echo $body;
                                                ?>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="row">
                                <div class="col-12">
                                    <button onclick="ExportECL()" type="button"
                                        class="btn btn-sm btn-danger teratur-button float-right">
                                        <i class="flaticon-download-1"></i> Export Excel
                                    </button>
                                </div>
                            </div>
                            <br />
                        </div>
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Komentar</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                </button>
                            </div>
                            <div class="kt-portlet">
                                <div class="kt-portlet__body">
                                    <div class="kt-scroll kt-scroll--pull" style="overflow:scroll; height:350px;">
                                        <div class="kt-widget3">
                                            <?php if ($data->data->list_komentar == []) { ?>
                                            <div class="kt-widget3__item">
                                                <h4 class="center">Tidak ada Komentar</h1>
                                            </div>
                                            <?php } else { ?>
                                            <?php foreach ($data->data->list_komentar as $key => $kme) { 
                                                if ($kme->flag != 2) {
                                                    continue;
                                                }
                                                ?>
                                            <div class="kt-widget3__item">
                                                <div class="kt-widget3__header">
                                                    <div class="kt-widget3__user-img">
                                                        <?php if ( $kme->user->path_profile == "" ) { ?>
                                                        <img class="kt-widget3__img"
                                                            src="<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>"
                                                            alt="image">
                                                        <?php } else { ?>
                                                        <?php $src =  $this->config->item('api_host') .'/view_file'. '/' .$kme->user->path_profile ;?>
                                                        <img class="kt-widget3__img" src="<?php echo $src; ?>"
                                                            alt="image" width="40px" height="40px">
                                                        <?php } ?>
                                                    </div>
                                                    <div class="kt-widget3__info">
                                                        <a href="#" class="kt-widget3__username">
                                                            <?php echo $kme->user->nama; ?>
                                                        </a><br>
                                                        <span class="kt-widget3__time">
                                                            <?php
                                                            if ($kme->updated_date != null) {
                                                                echo get_timeago(strtotime($kme->updated_date)); 
                                                            } else {
                                                                echo get_timeago(strtotime($kme->created_date)); 
                                                            }
                                                            ?>
                                                        </span>
                                                    </div>
                                                    <span class="kt-widget3__status kt-font-info">
                                                        <?php if ($this->session->userdata("ids") == $kme->created_by) { ?>
                                                        <span id="btn_editkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                            class="kt-nav__link kt-font-info" href="#"
                                                            style="cursor:pointer"
                                                            onClick="return EditKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', '<?php echo $kme->komentar; ?>','<?php echo "2"; ?>')">Ubah
                                                        </span>
                                                        &nbsp
                                                        <span id="btn_batkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                            href="#" class="kt-nav__link kt-font-info" onClick="return batalKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', 
                                                            '<?php echo $kme->komentar; ?>', 2
                                                        )" style="display:none; cursor:pointer;">Batal
                                                        </span>
                                                        &nbsp
                                                        <a href="<?php echo site_url("Model/deletekomen_draft_bucket?idk=$kme->id_komentar_bucket&typ=2&flg=2&idb=$idb"); ?>"
                                                            class="kt-nav__link kt-font-info"
                                                            onclick='return del_confirm()'>Hapus
                                                        </a>
                                                        <?php } ?>
                                                    </span>
                                                </div>
                                                <div class="kt-widget3__body">
                                                    <p class="kt-widget3__text">
                                                        <?php echo $kme->komentar; ?>
                                                    </p>
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-portlet__foot">
                                    <form method="post" enctype="multipart/form-data"
                                        action="<?php echo site_url("model/addkomen_draft_bucket?idb=$idb"); ?>">
                                        <div class="form-row align-items-center">
                                            <div class="col-auto">
                                                <input value="2" name="type_coment" hidden></input>
                                                <input value="2" name="flag_coment" hidden></input>
                                                <input value="0" id="komen_id2" name="id_coment" hidden></input>
                                                <input value="<?php echo $idb; ?>" name="id_bucket" hidden></input>
                                                <textarea id="komen_content2" required
                                                    style="height: 50px; width: 400px;" placeholder="Type here..."
                                                    name="komentar"></textarea>
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit"
                                                    class="btn btn-primary teratur-button">Kirim</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
function get_timeago( $ptime )
{
    $estimate_time = time() - $ptime;
    if( $estimate_time < 1 )
    {
        return 'kurang dari 1 detik yang lalu';
    }
    $condition = array(
        12 * 30 * 24 * 60 * 60  =>  'tahun',
        30 * 24 * 60 * 60       =>  'bulan',
        24 * 60 * 60            =>  'hari',
        60 * 60                 =>  'jam',
        60                      =>  'menit',
        1                       =>  'detik'
    );
    foreach( $condition as $secs => $str )
    {
        $d = $estimate_time / $secs;
        if( $d >= 1 )
        {
            $r = round( $d );
            return 'sekitar ' . $r . ' ' . $str . ( $r > 1 ? '' : '' ) . ' yang lalu';
        }
    }
}
?>

<script>
// function reload_tab() {
//     alert('wwl');
//     $('#scroll1').refresh();
// }

function EditKomentar(id, content, flag) {
    // alert(id + " " + content + " " + flag);
    document.getElementById("komen_id" + flag).value = id;
    document.getElementById("komen_content" + flag).value = content;
    $("#btn_editkom" + id).hide();
    $("#btn_batkom" + id).show();
}

function batalKomentar(id, content, flag) {
    document.getElementById("komen_id" + flag).value = 0;
    document.getElementById("komen_content" + flag).value = "";
    $("#btn_editkom" + id).show();
    $("#btn_batkom" + id).hide();
}
</script>