<style>
table {
    table-layout: fixed;
    display: block;
    overflow-x: auto;
    white-space: nowrap;
}

td {
    overflow: hidden;
    text-overflow: ellipsis;
}
</style>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Simulasi ECL</h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-reload"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Simulasi ECL</a>

                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet">
                <div class="kt-portlet__body kt-portlet__body--fit">
                    <div class="kt-grid kt-wizard-v1 kt-wizard-v1--white">
                        <div class="kt-grid__item">

                            <!--begin: Form Wizard Nav -->
                            <div class="kt-wizard-v1__nav">

                                <div class="kt-wizard-v1__nav-items">
                                    <div id="nav-0" class="kt-wizard-v1__nav-item" data-ktwizard-type="step"
                                        data-ktwizard-state="current">
                                        <div class="kt-wizard-v1__nav-body">
                                            <div class="kt-wizard-v1__nav-icon">
                                                <i class="flaticon-pie-chart"></i>
                                            </div>
                                            <div class="kt-wizard-v1__nav-label">
                                                1. Pengaturan Pilihan
                                            </div>
                                        </div>
                                    </div>
                                    <div id="nav-1" class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
                                        <div class="kt-wizard-v1__nav-body">
                                            <div class="kt-wizard-v1__nav-icon">
                                                <i class="flaticon-list"></i>
                                            </div>
                                            <div class="kt-wizard-v1__nav-label">
                                                2. Konfirmasi Model
                                            </div>
                                        </div>
                                    </div>
                                    <div id="nav-2" class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
                                        <div class="kt-wizard-v1__nav-body">
                                            <div class="kt-wizard-v1__nav-icon">
                                                <i class="flaticon2-paper"></i>
                                            </div>
                                            <div class="kt-wizard-v1__nav-label">
                                                3. Perhitungan MAPE
                                            </div>
                                        </div>
                                    </div>
                                    <div id="nav-3" class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
                                        <div class="kt-wizard-v1__nav-body">
                                            <div class="kt-wizard-v1__nav-icon">
                                                <i class='flaticon-pie-chart'></i>
                                            </div>
                                            <div class="kt-wizard-v1__nav-label">
                                                4.Pengaturan Perhitungan ECL
                                            </div>
                                        </div>
                                    </div>
                                    <div id="nav-4" class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
                                        <div class="kt-wizard-v1__nav-body">
                                            <div class="kt-wizard-v1__nav-icon">
                                                <i class="flaticon-clipboard"></i>
                                            </div>
                                            <div class="kt-wizard-v1__nav-label">
                                                5. Hasil Perhitungan ECL
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div id="nav-5" class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
                                        <div class="kt-wizard-v1__nav-body">
                                            <div class="kt-wizard-v1__nav-icon">
                                                <i class="flaticon2-check-mark"></i>
                                            </div>
                                            <div class="kt-wizard-v1__nav-label">
                                                6. Hasil Jurnal
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                            </div>

                            <!--end: Form Wizard Nav -->
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin: Form Wizard Form-->
                            <form class="kt-form" id="kt_form" method="post" enctype="multipart/form-data"
                                action="<?php echo site_url('Ecl/createDraft'); ?>">
                                <!-- <?php echo json_encode($bucket->data); ?> -->
                                <!--begin: Form Wizard Step 1-->
                                <div id="content-0" class="kt-wizard-v1__content" data-ktwizard-type="step-content"
                                    data-ktwizard-state="current">
                                    <div class="kt-heading kt-heading--md">Pengaturan Pilihan</div>
                                    <div class="kt-form__section kt-form__section--first">
                                        <div class="kt-wizard-v1__form">
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Model Bucket Simulate:</label>
                                                        <select name="draf" id="draf" class="form-control"
                                                            onchange="onChangeBucketSimulate(event)">
                                                            <?php foreach ( $bucket->data as $b ) : ?>
                                                            <option value="<?php echo $b->id_draft_bucket; ?>">
                                                                <?= $b->nama_draft_bucket; ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Dengan Model:</label>
                                                        <div class="col-lg-9 col-xl-4">
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio"
                                                                    name="flagChange" id="inlineRadio1" value="1"
                                                                    checked onchange="changeFlagModel(this)">
                                                                <label class="form-check-label"
                                                                    for="inlineRadio1">Ya</label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio"
                                                                    name="flagChange" id="inlineRadio2" value="0"
                                                                    onchange="changeFlagModel(this)">
                                                                <label class="form-check-label"
                                                                    for="inlineRadio2">Tidak</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xl-6" id="div_select_model">
                                                    <div class="form-group">
                                                        <label>No. Urut Model FL:</label>
                                                        <select name="fl" id="fl" class="form-control">
                                                            <?php foreach ( $fl as $f ) : ?>
                                                            <option value="<?= $f->id_model; ?>">
                                                                <?= $f->nomor." ( Segment : ".$f->segment->kode_segment.")"; ?>
                                                            </option>
                                                            <?php endforeach; ?>

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Input Regresi:</label>
                                                        <select name="regresi" id="regresi" class="form-control">
                                                            <option value="1">ODR</option>
                                                            <option value="2">Average</option>
                                                            <option value="3">Sum Loss Rate</option>
                                                            <option value="4">Sum Delta Loss Rate</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6" id="div_periode_forecast">
                                                    <div class="form-group">
                                                        <label>Forecast Month</label>
                                                        <div class="input-group">
                                                            <input type="number" class="form-control" name="forecast"
                                                                id="forecast" placeholder="Forecast Month" value="12">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">Bulan</span>
                                                            </div>
                                                        </div>
                                                        <span class="form-text text-muted">Masukan berapa bulan forecast
                                                            yang di inginkan</span>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6" id="div_mape_start">
                                                    <div class="form-group">
                                                        <label>Periode Awal MAPE</label>
                                                        <input type="text" id="dateStartMape" class="form-control"
                                                            name="start" placeholder="Periode Mulai MAPE">
                                                        <span class="form-text text-muted">Masukan Periode awal MAPE
                                                            yang di inginkan</span>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6" id="div_mape_end">
                                                    <div class="form-group">
                                                        <label>Periode Akhir MAPE</label>
                                                        <input type="text" id="dateEndMape" class="form-control"
                                                            name="end" placeholder="Periode Akhir MAPE">
                                                        <span class="form-text text-muted">Masukan Periode akhir MAPE
                                                            yang di inginkan</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--end: Form Wizard Step 1-->

                                <!--begin: Form Wizard Step 2-->
                                <div id="content-1" class="kt-wizard-v1__content" data-ktwizard-type="step-content"
                                    data-ktwizard-state="">
                                    <div class="kt-heading kt-heading--md">
                                        <h3 id="tittle_simulate_konfirmasi_model">Konfirmasi Model Simulate dan Model FL
                                            Terpilih</h3>
                                    </div>
                                    <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed">
                                    </div>
                                    <div class="kt-form__section kt-form__section--first">
                                        <div class="kt-wizard-v1__form">
                                            <div class="row">
                                                <div class="col-lg-12" id="div_model_fl">
                                                    <h4><u>Model Terpilih</u></h4>
                                                    <table class="table table-striped table-bordered table-hover"
                                                        id="table_model_fl_bucket" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th class="all" width=50px>No Model</th>
                                                                <th class="all" width=200px>Var 1</th>
                                                                <th class="all" width=200px>Var 2</th>
                                                                <th class="all" width=200px>Lag 1</th>
                                                                <th class="all" width=200px>Lag 2</th>
                                                                <th class="all" width=200px>C Coef</th>
                                                                <th class="all" width=200px>Koef Var 1</th>
                                                                <th class="all" width=200px>Koef Var 2</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="kt-datatable__body" id="table_model_fl">
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-lg-6">
                                                    <br />
                                                    <h4><u>Draft Input Piutang</u></h4>
                                                    <div class="column" style="float: left;width: 40%;">
                                                        <p><b>Kode Model Simulate</b></p>
                                                        <p><b>Nama Model Simulate</b></p>
                                                        <p><b>Segmentasi</b></p>
                                                        <p><b>Model Normalisasi Rollrate</b></p>
                                                        <p><b>Jumlah Moving</b></p>
                                                        <p><b>Periode Awal</b></p>
                                                        <p><b>Periode Selesai</b></p>
                                                    </div>
                                                    <div class="column" style="float: left;width: 60%;" id="terpilih">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <br /><br />
                                                    <div class="column" style="float: left;width: 30%;">
                                                        <p><b>Dibuat oleh</b></p>
                                                        <p><b>Tanggal dibuat</b></p>
                                                    </div>
                                                    <div class="column" style="float: left;width: 70%;" id="terpilih2">
                                                    </div>
                                                </div>
                                            </div>
                                            <br />
                                            <ul class="nav nav-pills nav-fill" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#tab_araging">AR
                                                        Aging</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab"
                                                        href="#tab_rollrate_bn">Rollrate</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab"
                                                        href="#tab_rollrate_n">Rollrate normalisasi</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#tab_average">Agregasi
                                                        Rollrate</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab"
                                                        href="#tab_rollrate_na">Rollrate normalisasi
                                                        average</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#tab_lossrate">Lossrate
                                                        Bulanan</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#tab_moving_avg">Moving
                                                        Average Lossrate</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#tab_sum_delta">Sum
                                                        Delta Lossrate</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#tab_odr">ODR</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="tab_araging" role="tabpanel">
                                                    <table class="table table-striped table-bordered table-hover"
                                                        id="tbl_ar" width="100%">
                                                    </table>
                                                </div>
                                                <div class="tab-pane" id="tab_rollrate_bn" role="tabpanel">
                                                    <table class="table table-striped table-bordered table-hover"
                                                        id='rollrate_belum_normal' width="100%">

                                                    </table>
                                                </div>
                                                <div class="tab-pane" id="tab_rollrate_n" role="tabpanel">
                                                    <table class="table table-striped table-bordered table-hover"
                                                        id='rollrate_normal' width="100%">

                                                    </table>
                                                </div>
                                                <div class="tab-pane" id="tab_average" role="tabpanel">
                                                    <table class="table table-striped table-bordered table-hover"
                                                        id="average_rollrate_lossrate" width="100%">

                                                    </table>
                                                </div>
                                                <div class="tab-pane" id="tab_rollrate_na" role="tabpanel">
                                                    <table class="table table-striped table-bordered table-hover"
                                                        id="rollrate_normalisasi_average" width="100%">

                                                    </table>
                                                </div>
                                                <div class="tab-pane" id="tab_lossrate" role="tabpanel">
                                                    <table class="table table-striped table-bordered table-hover"
                                                        id="lossrate" width="100%">

                                                    </table>
                                                </div>
                                                <div class="tab-pane" id="tab_moving_avg" role="tabpanel">
                                                    <table class="table table-striped table-bordered table-hover"
                                                        id="moving_average" width="100%">

                                                    </table>
                                                </div>
                                                <div class="tab-pane" id="tab_sum_delta" role="tabpanel">
                                                    <table class="table table-striped table-bordered table-hover"
                                                        id="sum_delta_lossrate" width="100%">
                                                    </table>
                                                </div>
                                                <div class="tab-pane" id="tab_odr" role="tabpanel">
                                                    <table class="table table-striped table-bordered table-hover"
                                                        id="odr" width="100%">

                                                    </table>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <button onclick="ExportBucket()" type="button"
                                                        class="btn btn-sm btn-danger teratur-button float-right">
                                                        <i class="flaticon-download-1"></i> Export Excel
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--end: Form Wizard Step 2-->

                                <!--begin: Form Wizard Step 3-->
                                <div id="content-2" class="kt-wizard-v1__content" data-ktwizard-type="step-content">
                                    <div class="kt-heading kt-heading--md">
                                        <h3>Periksa hasil perhitungan MAPE untuk melanjutkan</h3>
                                    </div>
                                    <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed">
                                    </div>
                                    <div class="kt-form__section kt-form__section--first">
                                        <div class="kt-wizard-v1__form">
                                            <div class="row">
                                                <!-- <h2> Hasil MAPE</h2> -->
                                                <div class="col-lg-12">
                                                    <div class="column" style="float: left;width: 40%;">
                                                        <p><b>Kode Model Simulate</b></p>
                                                        <p><b>Nama Model Simulate</b></p>
                                                        <p><b>Segmentasi</b></p>
                                                        <p><b>Input Regresi</b></p>
                                                        <p><b>Periode Bulan Forecast</b></p>
                                                        <p><b>Periode Awal MAPE</b></p>
                                                        <p><b>Periode Akhir MAPE</b></p>
                                                    </div>
                                                    <div class="column" style="float: left;width: 60%;"
                                                        id="model_mape_terpilih">

                                                    </div>
                                                </div>
                                                <table class="table table-striped table-bordered table-hover"
                                                    id="model_fl_mape">
                                                    <thead>
                                                        <tr>
                                                            <th class="all" width=20px>No Model</th>
                                                            <th class="all" width=200px>Var 1</th>
                                                            <th class="all" width=200px>Var 2</th>
                                                            <th class="all" width=200px>Lag 1</th>
                                                            <th class="all" width=200px>Lag 2</th>
                                                            <th class="all" width=200px>C Coef</th>
                                                            <th class="all" width=200px>Koef Var 1</th>
                                                            <th class="all" width=200px>Koef Var 2</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="kt-datatable__body" id="model_mape">

                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="row">
                                                <h2> Hasil MAPE</h2>
                                                <table class="table table-striped table-bordered table-hover"
                                                    id="table_mape2">
                                                    <thead>
                                                        <tr id="mape_headers">
                                                            <!-- <th class="all" width=20px>Periode</th>
                                                            <th class="all" width=200px>Nilai Var 1</th>
                                                            <th class="all" width=200px>Nilai Var 2</th>
                                                            <th class="all" width=200px>Logit Expected</th>
                                                            <th class="all" width=200px>Expected</th>
                                                            <th class="all" width=200px>Actual</th>
                                                            <th class="all" width=200px>Mape</th> -->
                                                        </tr>
                                                    </thead>
                                                    <tbody class="kt-datatable__body" id="list_mape">

                                                    </tbody>
                                                </table>
                                                <!-- <div class="col-lg-12">
                                                    <div class="column" style="float: left;width: 40%;">
                                                        <p><b>Average MAPE</b></p>
                                                        <p><b>Threshold MAPE</b></p>
                                                        <p><b>Hasil MAPE</b></p>
                                                    </div>
                                                    <div class="column" style="float: left;width: 60%;"
                                                        id="content_mape2"></div>
                                                </div> -->
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <button onclick="ExportMAPE()" type="button"
                                                        class="btn btn-sm btn-danger teratur-button float-right">
                                                        <i class="flaticon-download-1"></i> Export Excel
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--end: Form Wizard Step 3-->

                                <!--begin: Form Wizard Step 4-->
                                <div id="content-3" class="kt-wizard-v1__content" data-ktwizard-type="step-content">
                                    <div class="kt-heading kt-heading--md">Pengaturan Perhitungan ECL</div>
                                    <div class="kt-form__section kt-form__section--first">
                                        <div class="kt-wizard-v1__form">
                                            <div class="row">

                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Periode Perhitungan</label>
                                                        <input type="text" id="eperiode" class="form-control"
                                                            name="eperiode" placeholder="Periode ECL">
                                                        <span class="form-text text-muted">Masukan Periode
                                                            yang di inginkan</span>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Persentase LGD</label>

                                                        <div class="input-group">
                                                            <input type="number" class="form-control" name="elgd"
                                                                id="elgd" placeholder="Lgd" value="45">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">%</span>
                                                            </div>
                                                        </div>

                                                        <span class="form-text text-muted">Masukan angka lgd
                                                            yang di inginkan</span>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6" id="div_skenario_normal">
                                                    <div class="form-group">
                                                        <label>Skenario Normal</label>

                                                        <div class="input-group">
                                                            <input type="number" class="form-control" name="enormal"
                                                                id="enormal" placeholder="Normal" value="60">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">%</span>
                                                            </div>
                                                        </div>
                                                        <span class="form-text text-muted">Masukan skenario normal
                                                            yang di inginkan</span>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6" id="div_skenario_optimis">
                                                    <div class="form-group">
                                                        <label>Skenario Optimis</label>
                                                        <div class="input-group">
                                                            <input type="number" class="form-control" name="eoptimis"
                                                                id="eoptimis" placeholder="Optimis" value="20">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">%</span>
                                                            </div>
                                                        </div>
                                                        <span class="form-text text-muted">Masukan skenario optimis
                                                            yang di inginkan</span>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6" id="div_skenario_pesimis">
                                                    <div class="form-group">
                                                        <label>Skenario Pesimis</label>
                                                        <div class="input-group">
                                                            <input type="number" class="form-control" name="epesimis"
                                                                id="epesimis" placeholder="Pesimis" value="20">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">%</span>
                                                            </div>
                                                        </div>
                                                        <span class="form-text text-muted">Masukan skenario pesimis
                                                            yang di inginkan</span>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6" id="div_scalling_factor">
                                                    <div class="form-group">
                                                        <label>Scalling Factor</label>
                                                        <input type="text" class="form-control" name="escalling"
                                                            id="escalling" placeholder="Scalling"
                                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                            value="6.39194424666944">
                                                        <span class="form-text text-muted">Masukan angka scalling
                                                            yang di inginkan</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--end: Form Wizard Step 4-->

                                <!--begin: Form Wizard Step 5-->
                                <!-- <div id="conte" class="kt-wizard-v1__content" data-ktwizard-type="step-content">
                                    <div class="kt-heading kt-heading--md">Summary Ecl</div>
                                    <div class="kt-form__section kt-form__section--first">
                                        <div class="kt-wizard-v1__review">

                                            <div class="row">
                                                <h2> NO MODEL</h2>
                                                <table class="table table-striped table-bordered table-hover"
                                                    id="nomodel_ecl">
                                                    <thead>
                                                        <tr>
                                                            <th class="all" width=200px>Bucket</th>
                                                            <th class="all" width=200px>PD Base</th>
                                                            <th class="all" width=200px>MEV Effect</th>
                                                            <th class="all" width=200px>Logit PD</th>
                                                            <th class="all" width=200px>PD Forward Looking</th>
                                                            <th class="all" width=200px>LGD</th>
                                                            <th class="all" width=200px>EAD</th>
                                                            <th class="all" width=200px>ECL Base</th>
                                                            <th class="all" width=200px>ECL Forward Lookong</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="kt-datatable__body" id="no_modal_list">

                                                    </tbody>
                                                </table>

                                                <br />
                                                <br />
                                                <h2> WITH MODEL</h2>

                                            </div>
                                            <br />
                                            <ul class="nav nav-pills nav-fill" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#tabs1">Model</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#tabs2">Proporsional</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#tabs3">Solver</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#tabs4">Skenario</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="tabs1" role="tabpanel">
                                                    <table class="table table-striped table-bordered table-hover"
                                                        id="model_ecl_fl">
                                                        <thead>
                                                            <tr>
                                                                <th class="all" width=200px>Nomor</th>
                                                                <th class="all" width=200px>Var 1</th>
                                                                <th class="all" width=200px>Var 2</th>
                                                                <th class="all" width=200px>Lag 1</th>
                                                                <th class="all" width=200px>Lag 2</th>
                                                                <th class="all" width=200px>C Coef</th>
                                                                <th class="all" width=200px>Koef Var 1</th>
                                                                <th class="all" width=200px>Koef Var 2</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="kt-datatable__body" id="modal_list">

                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="tab-pane" id="tabs2" role="tabpanel">
                                                    <table class="table table-striped table-bordered table-hover"
                                                        id="model_proposional_ecl" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th class="all" width=200px>Bucket</th>
                                                                <th class="all" width=200px>PD Base</th>
                                                                <th class="all" width=200px>MEV Effect</th>
                                                                <th class="all" width=200px>Logit PD</th>
                                                                <th class="all" width=200px>PD Forward Looking</th>
                                                                <th class="all" width=200px>LGD</th>
                                                                <th class="all" width=200px>EAD</th>
                                                                <th class="all" width=200px>ECL Base</th>
                                                                <th class="all" width=200px>ECL Forward Lookong</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="kt-datatable__body" id="proporsional_list">

                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="tab-pane" id="tabs3" role="tabpanel">
                                                    <table class="table table-striped table-bordered table-hover"
                                                        id="model_solver_ecl" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th class="all" width=200px>Bucket</th>
                                                                <th class="all" width=200px>PD Base</th>
                                                                <th class="all" width=200px>MEV Effect</th>
                                                                <th class="all" width=200px>Logit PD</th>
                                                                <th class="all" width=200px>PD Forward Looking</th>
                                                                <th class="all" width=200px>LGD</th>
                                                                <th class="all" width=200px>EAD</th>
                                                                <th class="all" width=200px>ECL Base</th>
                                                                <th class="all" width=200px>ECL Forward Lookong</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="kt-datatable__body" id="solver_list">

                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="tab-pane" id="tabs4" role="tabpanel">
                                                    <table class="table table-striped table-bordered table-hover"
                                                        id="model_skenario_ecl" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th class="all" width=200px>STDEV Var 1</th>
                                                                <th class="all" width=200px>STDEV Var 2</th>
                                                                <th class="all" width=200px>Normal Var 1</th>
                                                                <th class="all" width=200px>Normal Var 2</th>
                                                                <th class="all" width=200px>Normal Probability</th>
                                                                <th class="all" width=200px>Optimis Var 1</th>
                                                                <th class="all" width=200px>Optimis Var 2</th>
                                                                <th class="all" width=200px>Optimis Probability</th>
                                                                <th class="all" width=200px>Pesimis Var 1</th>
                                                                <th class="all" width=200px>Pesimis Var 2</th>
                                                                <th class="all" width=200px>Pesimis Probability</th>
                                                                <th class="all" width=200px>Weighted Var 1</th>
                                                                <th class="all" width=200px>Weighted Var 2</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="kt-datatable__body" id="skenario">

                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <button onclick="ExportECL()" type="button"
                                                        class="btn btn-sm btn-danger teratur-button float-right">
                                                        <i class="flaticon-download-1"></i> Export Excel
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->

                                <div id="content-4" class="kt-wizard-v1__content" data-ktwizard-type="step-content">
                                    <div class="kt-heading kt-heading--md">
                                        <h3>Hasil Perhitungan ECL</h3>
                                    </div>
                                    <!-- 4 hasil ecl yg dibawa ke ECL -->
                                    <input type="text" class="form-control" name="average_mape" id="average_mape"
                                        hidden>
                                    <input type="text" class="form-control" name="ecl_no_model" id="ecl_no_model"
                                        hidden>
                                    <input type="text" class="form-control" name="ecl_proporsional"
                                        id="ecl_proporsional" hidden>
                                    <input type="text" class="form-control" name="ecl_solver" id="ecl_solver" hidden>

                                    <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed">
                                    </div>
                                    <div class="kt-heading kt-heading--md">No Model</div>
                                    <div class="kt-form__section kt-form__section--first">
                                        <div class="kt-wizard-v1__form">
                                            <div class="row">
                                                <div class="col-12">
                                                    <table class="table table-striped table-bordered table-hover"
                                                        id="nomodel_ecl">
                                                        <thead>
                                                            <tr>
                                                                <th class="all" width=200px>Bucket</th>
                                                                <th class="all" width=200px>PD Base</th>
                                                                <!-- <th class="all" width=200px>MEV Effect</th> -->
                                                                <!-- <th class="all" width=200px>Logit PD</th> -->
                                                                <!-- <th class="all" width=200px>PD Forward Looking</th> -->
                                                                <th class="all" width=200px>LGD</th>
                                                                <th class="all" width=200px>EAD</th>
                                                                <th class="all" width=200px>ECL</th>
                                                                <!-- <th class="all" width=200px>ECL Forward Lookong</th> -->
                                                            </tr>
                                                        </thead>
                                                        <tbody class="kt-datatable__body" id="no_modal_list">

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed">
                                    </div>
                                    <div id="result_with_model">
                                        <div class="kt-heading kt-heading--md">With Model</div>
                                        <div class="kt-form__section kt-form__section--first">
                                            <div class="kt-wizard-v1__form">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <table class="table table-striped table-bordered table-hover">
                                                            <tbody class="kt-datatable__body" id="model_title">
                                                                <tr>
                                                                    <td class="all" width=50px>Input Regresi</td>
                                                                    <td class="all" width=50px></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="all" width=50px>Periode</td>
                                                                    <td class="all" width=50px></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="all" width=50px>Month Forecast</td>
                                                                    <td class="all" width=50px></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-form__section kt-form__section--first">
                                            <div class="kt-wizard-v1__form">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <table class="table table-striped table-bordered table-hover"
                                                            id="model_ecl_fl">
                                                            <thead>
                                                                <tr>
                                                                    <th class="all" width=200px>Nomor</th>
                                                                    <th class="all" width=200px>Var 1</th>
                                                                    <th class="all" width=200px>Var 2</th>
                                                                    <th class="all" width=200px>Lag 1</th>
                                                                    <th class="all" width=200px>Lag 2</th>
                                                                    <th class="all" width=200px>C Coef</th>
                                                                    <th class="all" width=200px>Koef Var 1</th>
                                                                    <th class="all" width=200px>Koef Var 2</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="kt-datatable__body" id="modal_list">

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="kt-form__section kt-form__section--first">
                                            <div class="kt-wizard-v1__form">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <table class="table table-striped table-bordered table-hover"
                                                            id="model_skenario_ecl" width="100%">
                                                            <thead>
                                                                <tr>
                                                                    <th class="all" width=400px>Skenario</th>
                                                                    <th class="all" width=400px>Var 1</th>
                                                                    <th class="all" width=400px>Var 2</th>
                                                                    <th class="all" width=400px>Probabilty Weighted</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="kt-datatable__body" id="skenario">

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="kt-heading kt-heading--md">Total Penyesuaian PSAK 71</div>
                                        <div class="kt-form__section kt-form__section--first">
                                            <div class="kt-wizard-v1__form">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <table class="table table-striped table-bordered table-hover"
                                                            id="model_psak71_ecl" width="100%">

                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="kt-heading kt-heading--md">Persentase MEV effect (multiplier)</div>
                                        <div class="kt-form__section kt-form__section--first">
                                            <div class="kt-wizard-v1__form">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <table class="table table-striped table-bordered table-hover"
                                                            id="model_mevs_ecl" width="100%">

                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="kt-heading kt-heading--md">Metode Distribusi Proporsional</div>
                                        <div class="kt-form__section kt-form__section--first">
                                            <div class="kt-wizard-v1__form">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <table class="table table-striped table-bordered table-hover"
                                                            id="model_proposional_ecl" width="100%">
                                                            <thead>
                                                                <tr>
                                                                    <th class="all" width=200px>Bucket</th>
                                                                    <th class="all" width=200px>PD Base</th>
                                                                    <th class="all" width=200px>MEV Effect</th>
                                                                    <!-- <th class="all" width=200px>Logit PD</th> -->
                                                                    <th class="all" width=200px>PD Forward Looking</th>
                                                                    <th class="all" width=200px>LGD</th>
                                                                    <th class="all" width=200px>EAD</th>
                                                                    <th class="all" width=200px>ECL Base</th>
                                                                    <th class="all" width=200px>ECL Forward Lookong</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="kt-datatable__body" id="proporsional_list">

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed">
                                        </div>
                                        <div class="kt-heading kt-heading--md">Metode Distribusi Solver</div>
                                        <div class="kt-form__section kt-form__section--first">
                                            <div class="kt-wizard-v1__form">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <table class="table table-striped table-bordered table-hover"
                                                            id="model_mevs_solver" width="100%">
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-heading kt-heading--md"></div>
                                        <div class="kt-form__section kt-form__section--first">
                                            <div class="kt-wizard-v1__form">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <table class="table table-striped table-bordered table-hover"
                                                            id="model_solver_ecl" width="100%">
                                                            <thead>
                                                                <tr>
                                                                    <th class="all" width=200px>Bucket</th>
                                                                    <th class="all" width=200px>PD Base</th>
                                                                    <!-- <th class="all" width=200px>MEV Effect</th> -->
                                                                    <th class="all" width=200px>Logit PD</th>
                                                                    <th class="all" width=200px>PD Forward Looking</th>
                                                                    <th class="all" width=200px>LGD</th>
                                                                    <th class="all" width=200px>EAD</th>
                                                                    <th class="all" width=200px>ECL Base</th>
                                                                    <th class="all" width=200px>ECL Forward Lookong</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="kt-datatable__body" id="solver_list">

                                                            </tbody>
                                                        </table>
                                                        <table class="table table-striped table-bordered table-hover"
                                                            id="model_target_scalling" width="100%">

                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="kt-heading kt-heading--md"></div>
                                        <div class="kt-form__section kt-form__section--first">
                                            <div class="kt-wizard-v1__form">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <table class="table table-striped table-bordered table-hover"
                                                            id="model_scaling_target" width="100%">
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <button onclick="ExportECL()" type="button"
                                                class="btn btn-sm btn-danger teratur-button float-right">
                                                <i class="flaticon-download-1"></i> Export Excel
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <!--end: Form Wizard Step 5-->

                                <!--begin: Form Wizard Step 6-->
                                <div id="content-5" class="kt-wizard-v1__content" data-ktwizard-type="step-content">
                                    <div class="kt-heading kt-heading--md">Summary PSAK 71</div>
                                    <div class="kt-form__section kt-form__section--first">
                                        <div class="kt-wizard-v1__review">
                                            <div class="row">
                                                <table class="table table-striped table-bordered table-hover"
                                                    id="modalJurnalEcl">
                                                    <thead>
                                                        <tr>
                                                            <th class="all" width=200px>Segmentasi</th>
                                                            <th class="all" width=200px>Input Regresi</th>
                                                            <th class="all" width=200px>Model Terpilih</th>
                                                            <th class="all" width=200px>Penyesuaian</th>
                                                            <th class="all" width=200px>Var 1</th>
                                                            <th class="all" width=200px>Var 2</th>
                                                            <th class="all" width=200px>Lag 1</th>
                                                            <th class="all" width=200px>Lag 2</th>
                                                            <th class="all" width=200px>Mape</th>
                                                            <th class="all" width=200px>Total Ecl</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="kt-datatable__body" id="table_model_jurnal"></tbody>
                                                </table>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Periode ECL</label>
                                                        <input type="text" id="jurnal_periode_ecl" class="form-control"
                                                            readOnly>
                                                        <span class="form-text text-muted">Periode
                                                            yang terpilih</span>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Saldo Penyisihan Eksiting</label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">Rp. </span>
                                                            </div>
                                                            <input type="number" class="form-control"
                                                                id="jurnal_saldo_ecl" placeholder="masukan saldo">
                                                        </div>

                                                        <span class="form-text text-muted">Masukan saldo penyisihan saat
                                                            ini</span>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Pilih Model ECL yang di inginkan</label>
                                                        <select id="jurnal_model_pilihan" class="form-control"></select>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Tekan Button untuk proses jurnal.</label>
                                                        <button onclick="prosesJurnal()" type="button"
                                                            class="form-control btn btn-sm btn-danger teratur-button">
                                                            <i class="flaticon2-fast-next"></i> Proses Jurnal
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed">
                                    </div>
                                    <div class="kt-heading kt-heading--md" id="title_jurnal_terpilih"
                                        style="display:none">Summary Model Terpilih dan ECL</div>
                                    <div class="kt-form__section kt-form__section--first">
                                        <div class="kt-wizard-v1__review">
                                            <div class="row">
                                                <table class="table table-striped table-bordered table-hover"
                                                    id="modalPSAK71">
                                                    <tbody class="kt-datatable__body" id="table_penerapan_awal">
                                                        <!-- <tr>
                                                            <td>Saldo penyisihan eksisting</td>
                                                            <td>150.000</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Saldo penyisihan PSAK 71</td>
                                                            <td width="500">171.924</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Selisih saldo penyisihan yang perlu
                                                                dibukukan</td>
                                                            <td width="500">21.94</td>
                                                        </tr> -->
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <button onclick="ExportJurnalEcl()" type="button"
                                                class="btn btn-sm btn-danger teratur-button float-right">
                                                <i class="flaticon-download-1"></i> Export Excel
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <!--end: Form Wizard Step 6-->

                                <!--begin: Form Actions -->
                                <div class="kt-form__actions">
                                    <button class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold"
                                        onclick='back()' type="button">
                                        Previous
                                    </button>

                                    <button id="ecl_simulate_next"
                                        class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold" onclick='next()'
                                        type="button">
                                        Next Step
                                    </button>

                                    <button id="ecl_simulate_submit" style="display:none;"
                                        class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold"
                                        onclick='return act_confirm()' type="submit">Simpan Hasil ECL
                                    </button>
                                </div>

                                <!--end: Form Actions -->
                            </form>

                            <!--end: Form Wizard Form-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- end:: Content -->
    </div>
</div>

<script type="text/javascript">
// function prosesJurnal() {
//     $('#title_jurnal_terpilih').show();
//     let periode = document.getElementById('jurnal_periode_ecl');
//     let saldo = document.getElementById('jurnal_saldo_ecl');
//     let pilihan = document.getElementById('jurnal_model_pilihan');

//     jurnal = $.ajax({
//         data: {
//             periode: periode,
//             saldo: saldo,
//             pilihan: pilihan,
//         },
//         type: "POST",
//         url: "<?php echo site_url('Model/get_jurnal_modal');?>",
//         async: false
//     }).responseText;
//     console.log("jurnal: " + jurnal);
//     $('#table_penerapan_awal').html(jurnal);
// }

function onChangeBucketSimulate(e) {
    // alert(e.target.value);
    ecls = $.ajax({
        data: {
            id_draft_bucket: e.target.value,
        },
        type: "POST",
        url: "<?php echo site_url('Model/get_fl_live');?>",
        async: false
    }).responseText;
    // console.log(ecls);
    let dataModel = JSON.parse(ecls);
    if (dataModel.status != 1) {
        alert(dataModel.message);
        return;
    }

    $("#fl").find("option").remove();
    for (let i = 0; i < dataModel.data.length; i++) {
        const element = dataModel.data[i];
        let id_model = element.id_model;
        let name = element.nomor + " ( Segment : " + element.segment.kode_segment + " )";
        $('#fl').append("<option value='" + id_model + "'>" + name + "</option>");
    }
    // $('#fl').selectpicker('refresh');
}

function changeFlagModel(radio) {
    if (radio.value == 1 || radio.value == "1") {
        $("#div_select_model").show();
        $("#div_mape_start").show();
        $("#div_mape_end").show();
        $("#div_periode_forecast").show();

        $("#div_skenario_normal").show();
        $("#div_skenario_pesimis").show();
        $("#div_skenario_optimis").show();

        $("#enormal").val("60");
        $("#eoptimis").val("20");
        $("#epesimis").val("20");

    } else {
        $("#div_select_model").hide();
        $("#div_mape_start").hide();
        $("#div_mape_end").hide();
        $("#div_periode_forecast").hide();

        $("#div_skenario_normal").hide();
        $("#div_skenario_pesimis").hide();
        $("#div_skenario_optimis").hide();

        $("#enormal").val("0");
        $("#eoptimis").val("0");
        $("#epesimis").val("0");
    }

}
</script>