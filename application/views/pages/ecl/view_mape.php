<style>
table {
    table-layout: fixed;
    display: block;
    overflow-x: auto;
    white-space: nowrap;
}

td {
    overflow: hidden;
    text-overflow: ellipsis;
}
</style>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        View Draft ECL</h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon-list-1"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            MAPE</a>
                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>
        <!-- <?php echo json_encode($data); ?> -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">

                    </div>

                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <!-- <a href="#" class="btn btn-brand btn-icon-sm teratur-button" data-toggle="modal"
                                    data-target="#add_profile">
                                    <i class="flaticon-plus"></i>
                                    Ubah Data
                                </a> -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">
                    <?php $idb = $data->data->id_draft_ecl; ?>
                    <div class="row">
                        <!-- <h2> Hasil MAPE</h2> -->
                        <div class="col-lg-6">
                            <div class="column" style="float: left;width: 40%;">
                                <p><b>Kode Model Simulate</b></p>
                                <p><b>Nama Model Simulate</b></p>
                                <p><b>Segmentasi</b></p>
                                <p><b>Input Regresi</b></p>
                                <p><b>Periode Bulan Forecast</b></p>
                                <p><b>Periode Awal Piutang</b></p>
                                <p><b>Periode Akhir Piutang</b></p>
                            </div>
                            <div class="column" style="float: left;width: 60%;" id="model_mape_terpilih">
                                <p>: <?php echo $data->data->draft_bucket->kode_draft_bucket; ?></p>
                                <p>: <?php echo $data->data->draft_bucket->nama_draft_bucket; ?></p>
                                <p>: <?php echo $data->data->segment->nama_segment; ?></p>
                                <p>: <?php echo $data->data->mape_draft->input_regresi; ?></p>
                                <p>: <?php echo $data->data->month_forecast; ?></p>
                                <p>: <?php echo date("d M Y",strtotime($data->data->draft_bucket->start_date)); ?></p>
                                <p>: <?php echo date("d M Y",strtotime($data->data->draft_bucket->end_date)); ?></p>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover" id="model_fl_mape">
                            <thead>
                                <tr>
                                    <th class="all" width=20px>No Model</th>
                                    <th class="all" width=200px>Var 1</th>
                                    <th class="all" width=200px>Var 2</th>
                                    <th class="all" width=200px>Lag 1</th>
                                    <th class="all" width=200px>Lag 2</th>
                                    <th class="all" width=200px>C Coef</th>
                                    <th class="all" width=200px>Koef Var 1</th>
                                    <th class="all" width=200px>Koef Var 2</th>
                                </tr>
                            </thead>
                            <tbody class="kt-datatable__body" id="model_mape">
                                <tr>
                                    <td><?php echo $data->data->mape_draft->model->nomor; ?></td>
                                    <td><?php echo $data->data->mape_draft->model->var1; ?></td>
                                    <td><?php echo $data->data->mape_draft->model->var2; ?></td>
                                    <td><?php echo $data->data->mape_draft->model->lag1; ?></td>
                                    <td><?php echo $data->data->mape_draft->model->lag2; ?></td>
                                    <td><?php echo $data->data->mape_draft->model->c_coef; ?></td>
                                    <td><?php echo $data->data->mape_draft->model->koef_var1; ?></td>
                                    <td><?php echo $data->data->mape_draft->model->koef_var2; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <h2> Hasil MAPE</h2>
                        <table class="table table-striped table-bordered table-hover" id="table_mape2">
                            <thead>
                                <tr id="mape_headers">
                                    <?php 
                                    foreach ( $data->data->mape_draft->header as $r ) {
                                        echo "<th class='all' width=200px>".$r."</th>";
                                    }
                                    ?>
                                </tr>
                            </thead>
                            <tbody class="kt-datatable__body" id="list_mape">
                                <?php 
                                $list_mape = "";
                                if ($data->data->mape_draft->id_regresi == '1' || $data->data->mape_draft->id_regresi == '2') {
                                    foreach ( $data->data->mape_draft->list_mape as $b ) {
                                        $list_mape .= 
                                        '<tr>'
                                        .'<td>'.$b->periode.'</td>'
                                        .'<td>'.$b->nilai_var1.'</td>'
                                        .'<td>'.$b->nilai_var2.'</td>'
                                        .'<td>'.$b->logit_expected.'</td>'
                                        .'<td>'.round(($b->expected*100), 2).' %</td>'
                                        .'<td>'.round(($b->actual*100), 2).' %</td>'
                                        .'<td>'.round(($b->mape*100), 2).' %</td>'
                                        .'</tr>';
                                    }
                            
                                    $average_tbl = 
                                    '<tr>'
                                        .'<td></td>'
                                        .'<td></td>'
                                        .'<td></td>'
                                        .'<td></td>'
                                        .'<td></td>'
                                        .'<td>Average MAPE</td>'
                                        .'<td>'.round(($data->data->mape_draft->average_mape * 100), 2).' %</td>'
                                        .'</tr>'; 
                                } else {
                                    foreach ( $data->data->mape_draft->list_mape as $b ) {
                                        $list_mape .= 
                                        '<tr>'
                                        .'<td>'.$b->periode.'</td>'
                                        .'<td>'.$b->nilai_var1.'</td>'
                                        .'<td>'.$b->nilai_var2.'</td>'
                                        .'<td>'.round(($b->expected*100), 2).' %</td>'
                                        .'<td>'.round(($b->actual*100), 2).' %</td>'
                                        .'<td>'.round(($b->mape*100), 2).' %</td>'
                                        .'</tr>';
                                    }
                                    $average_tbl = 
                                    '<tr>'
                                    .'<td></td>'
                                    .'<td></td>'
                                    .'<td></td>'
                                    .'<td></td>'
                                    .'<td>Average MAPE</td>'
                                    .'<td>'.round(($data->data->mape_draft->average_mape * 100), 2).' %</td>'
                                    .'</tr>'; 
                                }
                                echo $list_mape.$average_tbl; 
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Komentar</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                </button>
                            </div>
                            <div class="kt-portlet">
                                <div class="kt-portlet__body">
                                    <div class="kt-scroll kt-scroll--pull" style="overflow:scroll; height:350px;">
                                        <div class="kt-widget3">
                                            <?php if ($data->data->list_komentar == []) { ?>
                                            <div class="kt-widget3__item">
                                                <h4 class="center">Tidak ada Komentar</h1>
                                            </div>
                                            <?php } else { ?>
                                            <?php foreach ($data->data->list_komentar as $key => $kme) { 
                                                if ($kme->flag != 1) {
                                                    continue;
                                                }
                                                ?>
                                            <div class="kt-widget3__item">
                                                <div class="kt-widget3__header">
                                                    <div class="kt-widget3__user-img">
                                                        <?php if ( $kme->user->path_profile == "" ) { ?>
                                                        <img class="kt-widget3__img"
                                                            src="<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>"
                                                            alt="image">
                                                        <?php } else { ?>
                                                        <?php $src =  $this->config->item('api_host') .'/view_file'. '/' .$kme->user->path_profile ;?>
                                                        <img class="kt-widget3__img" src="<?php echo $src; ?>"
                                                            alt="image" width="40px" height="40px">
                                                        <?php } ?>
                                                    </div>
                                                    <div class="kt-widget3__info">
                                                        <a href="#" class="kt-widget3__username">
                                                            <?php echo $kme->user->nama; ?>
                                                        </a><br>
                                                        <span class="kt-widget3__time">
                                                            <?php
                                                            if ($kme->updated_date != null) {
                                                                echo get_timeago(strtotime($kme->updated_date)); 
                                                            } else {
                                                                echo get_timeago(strtotime($kme->created_date)); 
                                                            }
                                                            ?>
                                                        </span>
                                                    </div>
                                                    <span class="kt-widget3__status kt-font-info">
                                                        <?php if ($this->session->userdata("ids") == $kme->created_by) { ?>
                                                        <span id="btn_editkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                            class="kt-nav__link kt-font-info" href="#"
                                                            style="cursor:pointer"
                                                            onClick="return EditKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', '<?php echo $kme->komentar; ?>','<?php echo "1"; ?>')">Ubah
                                                        </span>
                                                        &nbsp
                                                        <span id="btn_batkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                            href="#" class="kt-nav__link kt-font-info" onClick="return batalKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', 
                                                            '<?php echo $kme->komentar; ?>', 1
                                                        )" style="display:none; cursor:pointer;">Batal
                                                        </span>
                                                        &nbsp
                                                        <a href="<?php echo site_url("Model/deletekomen_draft_bucket?idk=$kme->id_komentar_bucket&typ=2&flg=1&idb=$idb"); ?>"
                                                            class="kt-nav__link kt-font-info"
                                                            onclick='return del_confirm()'>Hapus
                                                        </a>
                                                        <?php } ?>
                                                    </span>
                                                </div>
                                                <div class="kt-widget3__body">
                                                    <p class="kt-widget3__text">
                                                        <?php echo $kme->komentar; ?>
                                                    </p>
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-portlet__foot">
                                    <form method="post" enctype="multipart/form-data"
                                        action="<?php echo site_url("model/addkomen_draft_bucket?idb=$idb"); ?>">
                                        <div class="form-row align-items-center">
                                            <div class="col-auto">
                                                <input value="2" name="type_coment" hidden></input>
                                                <input value="1" name="flag_coment" hidden></input>
                                                <input value="0" id="komen_id1" name="id_coment" hidden></input>
                                                <input value="<?php echo $idb; ?>" name="id_bucket" hidden></input>
                                                <textarea id="komen_content1" required
                                                    style="height: 50px; width: 400px;" placeholder="Type here..."
                                                    name="komentar"></textarea>
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit"
                                                    class="btn btn-primary teratur-button">Kirim</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
function get_timeago( $ptime )
{
    $estimate_time = time() - $ptime;
    if( $estimate_time < 1 )
    {
        return 'kurang dari 1 detik yang lalu';
    }
    $condition = array(
        12 * 30 * 24 * 60 * 60  =>  'tahun',
        30 * 24 * 60 * 60       =>  'bulan',
        24 * 60 * 60            =>  'hari',
        60 * 60                 =>  'jam',
        60                      =>  'menit',
        1                       =>  'detik'
    );
    foreach( $condition as $secs => $str )
    {
        $d = $estimate_time / $secs;
        if( $d >= 1 )
        {
            $r = round( $d );
            return 'sekitar ' . $r . ' ' . $str . ( $r > 1 ? '' : '' ) . ' yang lalu';
        }
    }
}
?>

<script>
// function reload_tab() {
//     alert('wwl');
//     $('#scroll1').refresh();
// }

function EditKomentar(id, content, flag) {
    // alert(id + " " + content + " " + flag);
    document.getElementById("komen_id" + flag).value = id;
    document.getElementById("komen_content" + flag).value = content;
    $("#btn_editkom" + id).hide();
    $("#btn_batkom" + id).show();
}

function batalKomentar(id, content, flag) {
    document.getElementById("komen_id" + flag).value = 0;
    document.getElementById("komen_content" + flag).value = "";
    $("#btn_editkom" + id).show();
    $("#btn_batkom" + id).hide();
}
</script>