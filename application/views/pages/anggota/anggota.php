<style>
table {
    table-layout: fixed;
}

td {
    overflow: hidden;
    text-overflow: ellipsis;
}
</style>

<?php 

// echo json_encode($data);


?>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Daftar User </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon-network"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            User </a>

                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container   kt-grid__item--fluid">
            <?php if ( $data->status == '1' || $data->status == 1 ) { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Berhasil!&nbsp;&nbsp;</strong><?php echo "Jumlah User : ".count($data->data); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } else { ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Perhatian!&nbsp;&nbsp;</strong> <?php echo "Tidak ada User"; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } ?>
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                <strong>Info!&nbsp;&nbsp;</strong><?php echo "Tekan button tambah data untuk melakukan penambahan user"; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">

                    </div>

                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <?php if ($this->session->userdata('role') == '1') { ?>
                                <a href="#" class="btn btn-brand btn-icon-sm teratur-button" data-toggle="modal"
                                    data-target="#add_profile">
                                    <i class="flaticon-plus"></i>
                                    Tambah Data
                                </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">


                    <table class="table table-striped table-bordered table-hover" id="tbl_list_default2" width="100%">
                        <thead>
                            <tr>
                                <th class="all" width=10px>#</th>
                                <th class="all" width=200px>Nama</th>
                                <th class="min-tablet" width=100px>Role</th>
                                <th class="min-tablet" width=100px>Username</th>
                                <th class="min-tablet" width=100px>Divisi</th>
                                <th class="min-tablet" width=100px>Jabatan</th>
                                <th class="min-tablet" width=100px>Status</th>
                                <th class="none">Jenis Kelamin</th>
                                <th class="none">Telepon</th>
                                <?php if ($this->session->userdata('role') == '1') { ?>
                                <th class="min-tablet" width=50px>Action</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody class="kt-datatable__body">
                            <?php $i = 1; foreach( $data->data as $d ): ?>
                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td>
                                    <span style="widht:300px;">
                                        <div class="kt-user-card-v2">
                                            <div class="kt-user-card-v2__pic">
                                                <?php if ( $d->path_profile == "" ) { ?>
                                                <img src="<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>"
                                                    alt="image">
                                                <?php } else { ?>
                                                <?php $src =  $this->config->item('api_host') .'/view_file'. '/' .   $d->path_profile ;?>
                                                <img src="<?php echo $src; ?>" alt="image" width="40px" height="40px">
                                                <?php } ?>
                                            </div>
                                            <div class="kt-user-card-v2__details">
                                                <h2 class="kt-user-card-v2__name">
                                                    <a href="#"><?php echo $d->nama; ?></a>
                                                </h2>
                                                <span class="kt-user-card-v2__desc"><?php echo  $d->email; ?></span>
                                            </div>
                                        </div>
                                    </span>
                                </td>
                                <td><?php echo $d->role->nama; ?></td>
                                <td><?php echo $d->username; ?></td>
                                <td><?php echo $d->divisi; ?></td>
                                <td><?php echo $d->jabatan; ?></td>
                                <td><?php echo $d->nm_status; ?></td>
                                <td><?php echo $d->nm_jk; ?></td>
                                <td><?php echo $d->no_telp; ?></td>
                                <?php if ($this->session->userdata('role') == '1') { ?>
                                <td>
                                    <div class="dropdown">
                                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                            data-toggle="dropdown">
                                            <i class="la la-cog"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-sm">
                                            <ul class="kt-nav">
                                                <li class="kt-nav__item">
                                                    <a href="<?php echo site_url("#"); ?>" data-toggle="modal"
                                                        data-target="#edit_profile" onClick="return EditProfiles(
                                                            '<?php echo  $d->id_user; ?>', 
                                                            '<?php echo  $d->nama; ?>', 
                                                            '<?php echo  $d->j_k; ?>', 
                                                            '<?php echo  $d->id_role; ?>', 
                                                            '<?php echo  $d->no_telp; ?>', 
                                                            '<?php echo  $d->email; ?>', 
                                                            '<?php echo  $d->status; ?>', 
                                                            '<?php echo  $d->path_profile; ?>', 
                                                            '<?php echo  $d->divisi; ?>', 
                                                            '<?php echo  $d->jabatan; ?>', 
                                                            )" class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon-edit"></i>
                                                        <span class="kt-nav__link-text">Ubah Data</span>
                                                    </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <a href="<?php echo site_url("#"); ?>" data-toggle="modal" onClick="return EditUbahPass(
                                                            '<?php echo  $d->id_user; ?>', 
                                                        )" data-target="#change_pass" class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon2-safe"></i>
                                                        <span class="kt-nav__link-text">Ubah Password</span>
                                                    </a>
                                                </li>

                                                <?php if ( $d->status == '1' ) { ?>

                                                <li class="kt-nav__item">
                                                    <a onclick='return act_confirm()'
                                                        href="<?php echo site_url("anggota/aktifasi/$d->id_user/0"); ?>"
                                                        class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon2-cancel-music"></i>
                                                        <span class="kt-nav__link-text">Non Aktifkan</span>
                                                    </a>
                                                </li>

                                                <?php } else { ?>
                                                <li class="kt-nav__item">
                                                    <a onclick='return act_confirm()'
                                                        href="<?php echo site_url("anggota/aktifasi/$d->id_user/1"); ?>"
                                                        class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon2-check-mark"></i>
                                                        <span class="kt-nav__link-text">Aktifkan</span>
                                                    </a>
                                                </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php } ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="add_profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('anggota/create'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Avatar</label>
                                    <div class="col-lg-9 col-xl-9">
                                        <div class="kt-avatar kt-avatar--outline" id="kt_user_avatar">
                                            <div class="kt-avatar__holder" id="avatar_image_add"
                                                style="background-image: url(<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>)">
                                            </div>
                                            <label class="kt-avatar__upload" data-toggle="kt-tooltip" title=""
                                                data-original-title="Change avatar">
                                                <i class="fa fa-pen"></i>
                                                <input type="file" name="add_image" id="add_image"
                                                    accept=".png, .jpg, .jpeg" onchange="readURLAdd(this);">
                                            </label>
                                            <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title=""
                                                data-original-title="Cancel avatar">
                                                <i class="fa fa-times"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nama</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_fullname" name="add_fullname"
                                            required>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Jenis Kelamin</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="gender" id="gender" class="form-control" required>
                                            <option value="1">Laki Laki</option>
                                            <option value="0">Perempuan</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Role</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="role" id="role" class="form-control" required>
                                            <option value="1">Administrator</option>
                                            <option value="2">Finance</option>
                                            <option value="3">Reviewer</option>
                                        </select>
                                    </div>
                                </div>

                                <!-- <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Alamat Detail</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_address" name="add_address"
                                            required>
                                    </div>
                                </div> -->

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Divisi</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_divisi" name="add_divisi"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Jabatan</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_jabatan" name="add_jabatan"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nomor Telepon</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="la la-phone"></i></span></div>
                                            <input type="text" class="form-control" id="add_phone" name="add_phone"
                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                placeholder="Phone" aria-describedby="basic-addon1" required>
                                        </div>
                                        <!-- <span class="form-text text-muted">We'll never share your phone number with
                                            anyone else.</span> -->
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Email</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="la la-at"></i></span></div>
                                            <input type="text" class="form-control" id="add_email" name="add_email"
                                                placeholder="Email" aria-describedby="basic-addon1" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Status</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <select name="add_status" id="add_status" class="form-control" required>
                                            <option value="1">Aktif</option>
                                            <option value="0">Tidak Aktif</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Username</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_username" name="add_username"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Password</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="password" id="add_password"
                                            name="add_password" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button" onclick="return act_confirm()">Tambah
                        User</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>

<div class="modal fade" id="edit_profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ubah Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('anggota/update'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Avatar</label>
                                    <div class="col-lg-9 col-xl-9">
                                        <div class="kt-avatar kt-avatar--outline" id="kt_user_avatar">
                                            <div class="kt-avatar__holder" id="avatar_image_edit"
                                                style="background-image: url(<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>)">
                                            </div>
                                            <label class="kt-avatar__upload" data-toggle="kt-tooltip" title=""
                                                data-original-title="Change avatar">
                                                <i class="fa fa-pen"></i>
                                                <input type="file" name="add_image" id="edit_image"
                                                    accept=".png, .jpg, .jpeg" onchange="readURL(this);">
                                            </label>
                                            <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title=""
                                                data-original-title="Cancel avatar">
                                                <i class="fa fa-times"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <input class="form-control" type="hidden" id="edit_ids" name="ids" required>
                                <input class="form-control" type="hidden" id="edit_path" name="path" required>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nama</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="edit_fullname" name="add_fullname"
                                            required>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Jenis Kelamin</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="gender" id="edit_gender" class="form-control" required>
                                            <option value="1">Laki Laki</option>
                                            <option value="0">Perempuan</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Role</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="role" id="edit_role" class="form-control" required>
                                            <option value="1">Administrator</option>
                                            <option value="2">Finance</option>
                                            <option value="3">Reviewer</option>
                                        </select>
                                    </div>
                                </div>

                                <!-- <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Alamat Detail</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="edit_address" name="add_address"
                                            required>
                                    </div>
                                </div> -->

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Divisi</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="edit_divisi" name="add_divisi"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Jabatan</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="edit_jabatan" name="add_jabatan"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nomor Telepon</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="la la-phone"></i></span></div>
                                            <input type="text" class="form-control" id="edit_phone" name="add_phone"
                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                placeholder="Phone" aria-describedby="basic-addon1" required>
                                        </div>
                                        <!-- <span class="form-text text-muted">We'll never share your phone number with
                                            anyone else.</span> -->
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Email</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="la la-at"></i></span></div>
                                            <input type="text" class="form-control" id="edit_email" name="add_email"
                                                placeholder="Email" aria-describedby="basic-addon1" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Status</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <select name="add_status" id="edit_status" class="form-control" required>
                                            <option value="1">Aktif</option>
                                            <option value="0">Tidak Aktif</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button" onclick="return act_confirm()">Ubah
                        User</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>

<div class="modal fade" id="change_pass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ubah Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('anggota/gantiPass'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <input class="form-control" type="hidden" id="idsss" name="ids" required>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Password baru </label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="password" id="new_passs" name="new_pass"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Konfirmasi password baru </label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="password" id="conf_passs" name="conf_pass"
                                            required>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button" onclick="return act_confirm()">Ubah
                        Password</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>


<script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            // $('#blah')
            //     .attr('src', e.target.result);
            document.getElementById('avatar_image_edit').style.backgroundImage = "url(" + e.target.result + ")";
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURLAdd(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            // $('#blah')
            //     .attr('src', e.target.result);
            document.getElementById('avatar_image_add').style.backgroundImage = "url(" + e.target.result + ")";
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function EditPenarikan(id, name, rek) {
    document.getElementById("add_name").value = name;
    document.getElementById("add_id").value = id;
    document.getElementById("add_rek").value = rek;
}

function EditUbahPass(ids) {
    document.getElementById("idsss").value = ids;
}
</script>