<style>
table {
    table-layout: fixed;
}

td {
    overflow: hidden;
    text-overflow: ellipsis;
}
</style>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Profile Anggota </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon-network"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Anggota </a>

                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                        
                    </div>

                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <?php if ( $this->session->userdata('role') == 'RL002' || $this->session->userdata('role') == 'RL005' ) { ?>
                                <a href="#" class="btn btn-brand btn-icon-sm teratur-button" data-toggle="modal"
                                    data-target="#edit_profile"
                                    onClick="return EditProfiles('<?php echo  $data->data[0]->id_member; ?>', '<?php echo  $data->data[0]->name; ?>', '<?php echo  $data->data[0]->no_ktp; ?>', '<?php echo  $data->data[0]->j_k; ?>', '<?php echo  $data->data[0]->provinsi->id; ?>', '<?php echo  $data->data[0]->kabupaten->id; ?>', '<?php echo  $data->data[0]->kecamatan->id; ?>', '<?php echo  $data->data[0]->desa->id; ?>', '<?php echo  $data->data[0]->address; ?>', '<?php echo  $data->data[0]->telephone1; ?>', '<?php echo  $data->data[0]->telephone2; ?>', '<?php echo  $data->data[0]->email; ?>', '<?php echo  $data->data[0]->tempat_lahir; ?>', '<?php echo  $data->data[0]->tgl_lahir; ?>', '<?php echo  $data->data[0]->kenegaraan; ?>', '<?php echo  $data->data[0]->no_npwp; ?>', '<?php echo  $data->data[0]->status; ?>',  '<?php echo  $data->data[0]->tgl_bergabung; ?>',  '<?php echo  $data->data[0]->id_agama; ?>', '<?php echo  $data->data[0]->data_user->data_role->id_role; ?>', '<?php echo  $data->data[0]->member_simpanan[0]->id_type_cus; ?>', '<?php echo  $data->data[0]->member_simpanan[1]->id_type_cus; ?>' )"
                                >
                                    <i class="la la-pencil"></i>
                                    Edit Profile
                                </a>
                                <a href="#" class="btn btn-brand btn-icon-sm teratur-button" 
                                    data-toggle="modal"
                                    data-target="#add_bank"
                                   
                                >
                                    <i class="la la-bank"></i>
                                    Tambah Bank
                                </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-widget kt-widget--user-profile-3">
                        <div class="kt-widget__top">
                            <div class="kt-widget__media kt-hidden-">
                                <?php if ( $data->data[0]->image_path == "" ) { ?>
                                    <img src="<?php  echo base_url().'/public/assets/media/users/300_21.jpg' ?>" alt="image">
                                <?php } else { ?>
                                    <?php $src =  $this->config->item('api_host') .'/view_file'. '/' .  $data->data[0]->image_path ;?>
                                    <img src= "<?php echo $src; ?>" width="100px" height="100px">
                                <?php } ?>
                            </div>
                            <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
                                <?php echo "Rio Yudha Pahlevi" ?>
                            </div>
                            <div class="kt-widget__content">
                                <div class="kt-widget__head">
                                    <a href="#" class="kt-widget__username">
                                        <?php echo $data->data[0]->name; ?>
                                        <i class="flaticon2-correct"></i>
                                    </a>
                                </div>
                                <div class="kt-widget__subhead">
                                    <a href="#"><i class="flaticon2-browser-1"></i><?php echo "koperasi.co.id"; ?></a>
                                    <a href="#"><i class="flaticon2-new-email"></i><?php echo $data->data[0]->email; ?></a>
                                    <a href="#"><i class="flaticon2-phone"></i><?php echo $data->data[0]->telephone1; ?></a>
                                    <a href="#"><i class="flaticon-user-ok"></i><?php if ( $data->data[0]->status == '1') {echo "Active";}else{echo "Non Active"; } ?></a>
                                    <a href="#"><i class="fas fa-dollar-sign"></i><?php echo number_format($saldo,2,",","."); ?></a>
                                </div>
                                <div class="kt-widget__info">
                                    <div class="kt-widget__desc">
                                        <?php echo $data->data[0]->tempat_lahir .", " . date("d F Y", strtotime($data->data[0]->tgl_lahir)) ?>
                                    </div>
                                </div>
                                <div class="kt-widget__info">
                                    <div class="kt-widget__desc">

                                        <?php if ( $data->data[0]->kecamatan == null || $data->data[0]->kabupaten == null || $data->data[0]->provinsi == null ) { echo ""; } else { ?>
                                            <?php echo $data->data[0]->address . " ,Kec. " .  $data->data[0]->kecamatan->nama. " , " . $data->data[0]->kabupaten->nama . " ,Prov. "  .  $data->data[0]->provinsi->nama ; ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-widget__bottom">
                            <div class="kt-widget__item">
                                <div class="kt-widget__icon">
                                    <i class="flaticon-buildings"></i>
                                </div>
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Koperasi</span>
                                    <span class="kt-widget__value"><?php echo $data->data[0]->data_koperasi->name_koperasi; ?></span>
                                </div>
                            </div>
                            <div class="kt-widget__item">
                                <div class="kt-widget__icon">
                                    <i class="flaticon-user"></i>
                                </div>
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Kode Member</span>
                                    <span class="kt-widget__value"><?php echo  $data->data[0]->kode_member; ?></span>
                                </div>
                            </div>
                            <div class="kt-widget__item">
                                <div class="kt-widget__icon">
                                    <i class="flaticon-pie-chart"></i>
                                </div>
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">No KTP</span>
                                    <span class="kt-widget__value"><?php 
                                        if (  $data->data[0]->no_ktp == "" ||  $data->data[0]->no_ktp == null) {
                                            echo "00000000" ;
                                        } else {
                                            echo  $data->data[0]->no_ktp; 
                                        }
                                        
                                    ?></span>
                                </div>
                            </div>
                            <div class="kt-widget__item">
                                <div class="kt-widget__icon">
                                    <i class="flaticon2-menu-3"></i>
                                </div>
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">No NPWP</span>
                                    <span class="kt-widget__value"><?php 
                                        if ( $data->data[0]->no_npwp == "" || $data->data[0]->no_npwp == null ) {
                                            echo "00000000" ;
                                        } else {
                                            echo  $data->data[0]->no_npwp; 
                                        }
                                        
                                    ?></span>
                                </div>
                            </div>
                            <div class="kt-widget__item">
                                <div class="kt-widget__icon">
                                    <i class="flaticon-earth-globe"></i>
                                </div>
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Kebangsaan</span>
                                    <span class="kt-widget__value"><?php echo "Inodenesia"; ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="kt-font-brand icon-teratur-blue la la-bank"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Data Bank
                        </h3>
                    </div>
                </div>

                <div class="kt-portlet__body">

                
                    <table class="table table-striped table-bordered table-hover" id="tbl_list_default3" width="100%">
                        <thead>
                            <tr>
                                <th class="all" width=10px>#</th>
                                <th class="min-tablet" width=200px>No Rekening</th>
                                <th class="min-tablet" width=150px>Nama Bank</th>
                                <th class="min-tablet" width=100px>Nama Pemilik</th>
                                <th class="min-tablet" width=50px>Action</th>
                            </tr>
                        </thead>
                        <tbody class="kt-datatable__body">
                           <?php $i = 1; foreach ( $bank->data as $b ) : ?>
                            <tr>
                                <td><?php echo $i++;  ?></td>
                                <td><?php echo $b->no_rekening; ?></td>
                                <td><?php echo $b->nama_bank; ?></td>
                                <td><?php echo  $b->nama_pemilik;; ?></td>
                                <td>
                                    <div class="dropdown">
                                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                            data-toggle="dropdown">
                                            <i class="la la-cog"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-sm">
                                            <ul class="kt-nav">
                                                <li class="kt-nav__item">
                                                    <a href="<?php echo site_url("#"); ?>"
                                                        data-toggle="modal"
                                                        data-target="#edit_bank"
                                                        onClick="return EditBank(
                                                            '<?php echo $b->id_rek_bank_mem; ?>',
                                                            '<?php echo $b->nama_bank; ?>',
                                                            '<?php echo $b->nama_pemilik; ?>',
                                                            '<?php echo $b->no_rekening; ?>',
                                                            '<?php echo $b->id_member; ?>',
                                                        )"
                                                        class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon-edit"></i>
                                                        <span class="kt-nav__link-text">Edit Data</span>
                                                    </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <a href="<?php echo site_url("bank/delete/$b->id_rek_bank_mem/$b->id_member"); ?>"
                                                        class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                        <span class="kt-nav__link-text">Delete Data</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>  
                                </td>
                            </tr>
                           <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="kt-font-brand icon-teratur-blue flaticon-coins"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Iuran Pokok
                        </h3>
                    </div>
                </div>

                <div class="kt-portlet__body">

                
                    <table class="table table-striped table-bordered table-hover" id="tbl_list_default2" width="100%">
                        <thead>
                            <tr>
                                <th class="all" width=10px>#</th>
                                <th class="min-tablet" width=100px>No Rekening</th>
                                <th class="min-tablet" width=80px>Periode Pembayaran</th>
                                <th class="min-tablet" width=80px>Jumlah</th>
                                <th class="min-tablet" width=80px>Nilai Pokok </th>
                                <th class="min-tablet" width=80px>Status </th>
                                <th class="min-tablet" width=80px>Tahun </th>
                                <th class="min-tablet" width=80px>Jenis </th>
                            </tr>
                        </thead>
                        <tbody class="kt-datatable__body">
                            <?php $n = 1; foreach ( $pokok as $p ) : ?>
                                <tr>
                                    <td><?php echo $n++;  ?></td>
                                    <td><?php echo $p->no_rekening; ?></td>
                                    <td><?php echo date(" F Y", strtotime($p->tanggal_pembayaran)) ?></td>
                                    <td><?php echo $p->nilai_iuran; ?></td>
                                    <td><?php echo $p->type_simpanan->nilai_simpanan; ?></td>
                                    <td><?php echo $p->nm_status; ?></td>
                                    <td><?php echo $p->tahun; ?></td>
                                    <td><?php echo $p->type_simpanan->nama_simpanan; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="kt-font-brand icon-teratur-blue flaticon-coins"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Iuran Wajib
                        </h3>
                    </div>
                </div>

                <div class="kt-portlet__body">                  
                    <table class="table table-striped table-bordered table-hover" id="tbl_list_default" width="100%">
                        <thead>
                            <tr>
                                <th class="all" width=10px>#</th>
                                <th class="min-tablet" width=100px>No Rekening</th>
                                <th class="min-tablet" width=80px>Periode Pembayaran</th>
                                <th class="min-tablet" width=80px>Jumlah</th>
                                <th class="min-tablet" width=80px>Nilai Wajib </th>
                                <th class="min-tablet" width=80px>Status </th>
                                <th class="min-tablet" width=80px>Tahun </th>
                                <th class="min-tablet" width=80px>Jenis </th>
                            </tr>
                        </thead>
                        <tbody class="kt-datatable__body">
                            <?php $n = 1; foreach ( $wajib as $w ) : ?>
                                <tr>
                                    <td><?php echo $n++;  ?></td>
                                    <td><?php echo $w->no_rekening; ?></td>
                                    <td><?php echo date(" F Y", strtotime($w->tanggal_pembayaran)) ?></td>
                                    <td><?php echo $w->nilai_iuran; ?></td>
                                    <td><?php echo $w->type_simpanan->nilai_simpanan; ?></td>
                                    <td><?php echo $w->nm_status; ?></td>
                                    <td><?php echo $w->tahun; ?></td>
                                    <td><?php echo $w->type_simpanan->nama_simpanan; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="kt-font-brand icon-teratur-blue flaticon-coins"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Iuran Sukarela
                        </h3>
                    </div>
                </div>

                <div class="kt-portlet__body">

                
                    <table class="table table-striped table-bordered table-hover" id="tbl_cust" width="100%">
                        <thead>
                            <tr>
                                <th class="all" width=10px>#</th>
                                <th class="min-tablet" width=100px>No Rekening</th>
                                <th class="min-tablet" width=80px>Periode Pembayaran</th>
                                <th class="min-tablet" width=80px>Jumlah</th>
                                <th class="min-tablet" width=80px>Nilai Sukarela </th>
                                <th class="min-tablet" width=80px>Status </th>
                                <th class="min-tablet" width=80px>Tahun </th>
                                <th class="min-tablet" width=80px>Jenis </th>
                            </tr>
                        </thead>
                        <tbody class="kt-datatable__body">
                            <?php $n = 1; foreach ( $sukarela as $s ) : ?>
                                <tr>
                                    <td><?php echo $n++;  ?></td>
                                    <td><?php echo $s->no_rekening; ?></td>
                                    <td><?php echo date(" F Y", strtotime($s->tanggal_pembayaran)) ?></td>
                                    <td><?php echo $s->nilai_iuran; ?></td>
                                    <td><?php echo $s->type_simpanan->nilai_simpanan; ?></td>
                                    <td><?php echo $s->nm_status; ?></td>
                                    <td><?php echo $s->tahun; ?></td>
                                    <td><?php echo $s->type_simpanan->nama_simpanan; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>


<div class="modal fade" id="edit_profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Anggota</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('anggota/update'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Avatar</label>
                                    <div class="col-lg-9 col-xl-9">
                                        <div class="kt-avatar kt-avatar--outline" id="kt_user_avatar">
                                            <div class="kt-avatar__holder" id="avatar_image_add"
                                                style="background-image: url(/metronic/themes/metronic/theme/default/demo9/dist/assets/media/users/100_13.jpg)">
                                            </div>
                                            <label class="kt-avatar__upload" data-toggle="kt-tooltip" title=""
                                                data-original-title="Change avatar">
                                                <i class="fa fa-pen"></i>
                                                <input type="file" name="edit_image" id="edit_image"
                                                    accept=".png, .jpg, .jpeg" onchange="readURLAdd(this);">
                                            </label>
                                            <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title=""
                                                data-original-title="Cancel avatar">
                                                <i class="fa fa-times"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <input class="form-control" type="hidden" id="edit_id_member" name="edit_id_member" required>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nama Lengkap</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="edit_fullname" name="edit_fullname"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">NIK</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="edit_nik" name="edit_nik"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Jenis Kelamin</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="gender" id="gender" class="form-control" required>
                                            <option value="0">Laki Laki</option>
                                            <option value="1">Perempuan</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Role</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="role" id="rolee" class="form-control" required>
                                            <option value="5">KEPALA KOPERASI CUSTOMER</option>
                                            <option value="3">PEGAWAI KOPERASI CUSTOMER</option>
                                            <option value="4">ANGGOTA KOPERASI CUSTOMER</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tempat Lahir</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="pobe" name="pob"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tanggal Lahir</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="dobe" name="dob"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Agama</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <select name="religion" id="edit_religion" class="form-control" required>
                                            <?php foreach ( $agama->data as $a ) : ?>
                                                <option value="<?php echo $a->id_agama ?>"><?php echo $a->nama ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Kewarganegaraan</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="natione" name="nation"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Provinsi</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <select class="kt-selectpicker form-control" data-live-search="true" data-size="8" id=provincee name=province required>
                                        <option>Pilih Provinsi</option>
                                        <?php
                                            foreach($province->data as $prov) :
                                                echo "<option value='$prov->id'> $prov->nama</option>";
                                            endforeach;
                                        ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Kota \ Kab</label>
                                    <div class="col-lg-9 col-xl-8">
                                    <select class="kt-selectpicker form-control" data-live-search="true" data-size="8" id=citye name=city placeholder="Pilih Kota" required> 
                                    </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Kecamatan</label>
                                    <div class="col-lg-9 col-xl-8">
                                    <select class="kt-selectpicker form-control" data-live-search="true" data-size="8" id=kece name=kec placeholder="Pilih Kecamatan" required> 
                                    </select>
                                    </div>

                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Desa</label>
                                    <div class="col-lg-9 col-xl-8">
                                    <select class="kt-selectpicker form-control" data-live-search="true" data-size="8" id=dess name=des placeholder="Pilih Desa" required> 
                                    </select>
                                    </div>

                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Alamat Detail</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="edit_address" name="edit_address"
                                            required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Contact Phone</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="la la-phone"></i></span></div>
                                            <input type="text" class="form-control" id="edit_phone" name="edit_phone" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                placeholder="Phone" aria-describedby="basic-addon1" required>
                                        </div>
                                        <span class="form-text text-muted">We'll never share your phone number with
                                            anyone else.</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Contact Phone </label>
                                    <div class="col-lg-9 col-xl-8">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="la la-phone"></i></span></div>
                                            <input type="text" class="form-control" id="edit_phone2" name="edit_phone2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                placeholder="Phone" aria-describedby="basic-addon1" required>
                                        </div>
                                        <span class="form-text text-muted">We'll never share your phone number with
                                            anyone else.</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Alamat Email</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="la la-at"></i></span></div>
                                            <input type="text" class="form-control" id="edit_email" name="edit_email"
                                                placeholder="Email" aria-describedby="basic-addon1" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">No NPWP</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="la la-cc-mastercard"></i></span></div>
                                            <input type="text" class="form-control" id="edit_npwp" name="edit_npwp"
                                                placeholder="No NPWP" aria-describedby="basic-addon1" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Simpanan Pokok</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <select class="kt-selectpicker form-control" data-live-search="true" data-size="8" id="edit_pokok" name=pokok required>
                                        <?php
                                            foreach($pokoks->data as $pk) :
                                                echo "<option value='$pk->id_cus_type'> $pk->nilai_simpanan </option>";
                                            endforeach;
                                        ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Simpanan Wajib</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <select class="kt-selectpicker form-control" data-live-search="true" data-size="8" id="edit_wajib" name=wajib required>
                                        <?php
                                            foreach($wajibs->data as $wj) :
                                                echo "<option value='$wj->id_cus_type'> $wj->nilai_simpanan </option>";
                                            endforeach;
                                        ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Status</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <select name="edit_status" id="edit_status" class="form-control" required>
                                            <option value="0">Non Active</option>
                                            <option value="1">Active</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tanggal Join</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="edit_join_at" name="join_at"
                                            required>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button">Prosess Data</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>

<div class="modal fade" id="add_bank" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Bank</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('bank/create'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                
                                <input class="form-control" type="hidden" id="add_id_member" name="id_member" value = "<?php echo  $data->data[0]->id_member; ?>" required>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nama Bank</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_bank" name="bank"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nama Pemilik</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_pemilik" name="pemilik"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">No Rekening</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_rek" name="rek"
                                            required>
                                    </div>
                                </div>

                    
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button">Prosess Data</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>

<div class="modal fade" id="edit_bank" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Bank</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('bank/update'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                
                                <input class="form-control" type="hidden" id="edit_id_bank" name="id_bank" required>
                                <input class="form-control" type="hidden" id="edit_id_members" name="id_member" required>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nama Bank</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="edit_banks" name="bank"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nama Pemilik</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="edit_pemilik" name="pemilik"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">No Rekening</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="edit_rek" name="rek"
                                            required>
                                    </div>
                                </div>

                    
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button">Prosess Data</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>

<script type="text/javascript">
    function EditBank(ids, bank, pemilik, rek, id_member) {

        document.getElementById("edit_id_bank").value = ids;
        document.getElementById("edit_banks").value = bank;
        document.getElementById("edit_pemilik").value = pemilik;
        document.getElementById("edit_rek").value = rek;
        document.getElementById("edit_id_members").value = id_member;
    }
</script>

<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                // $('#blah')
                //     .attr('src', e.target.result);
                document.getElementById('avatar_image').style.backgroundImage = "url(" + e.target.result + ")";
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURLAdd(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                // $('#blah')
                //     .attr('src', e.target.result);
                document.getElementById('avatar_image_add').style.backgroundImage = "url(" + e.target.result + ")";
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

