<style>
table {
    table-layout: fixed;
}

td {
    overflow: hidden;
    text-overflow: ellipsis;
}
</style>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Daftar Calon Anggota </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon-user-add"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Calon Anggota </a>

                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        
                    </div>

                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <a href="#" class="btn btn-brand btn-icon-sm teratur-button" data-toggle="modal"
                                    data-target="#add_profile">
                                    <i class="flaticon-plus"></i>
                                    Tambah
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">

                  
                    <table class="table table-striped table-bordered table-hover" id="tbl_list_default" width="100%">
                        <thead>
                            <tr>
                                <th class="all" width=10px>#</th>
                                <th class="all" width=300px>Nama Karyawan</th>
                                <th class="min-tablet" width=100px>NIP</th>
                                <th class="min-tablet" width=70p>Gender</th>
                                <th class="min-tablet" width=200px>Koperasi</th>
                                <th class="min-tablet" width=150px>DOB</th>
                                <th class="min-tablet" width=50px>Action</th>
                                <th class="none">Tanggal Join</th>
                                <th class="none">Status</th>

                                <th class="none">Email</th>
                                <th class="none">Telepon</th>
                                <th class="none">Provinsi</th>
                                <th class="none">Kota</th>
                                <th class="none">Kecamatan</th>
                                <th class="none">Alamat Lengkap</th>
                                <th class="none">Kewarganegaraan</th>
                                

                            </tr>
                        </thead>
                        <tbody class="kt-datatable__body">
                            <?php    
                                $i = 1;

                                foreach($data->data as $d) {
                                        ?>
                            <?php
                                    echo "<tr>";
                                        echo "<td>" . $i . "</td>";$i++; 
                                        echo "<td>";
                                        ?>
                                        <span style="widht:300px;">
                                            <div class="kt-user-card-v2">
                                                <div class="kt-user-card-v2__pic">
                                                   <img src="<?php  echo base_url().'/public/assets/media/users/300_21.jpg'; ?>">
                                                </div>
                                                <div class="kt-user-card-v2__details">
                                                    <h2 class="kt-user-card-v2__name">
                                                    <?php if ( $this->session->userdata('level') == '1' ) { ?>
                                                        <a href="<?php echo site_url("employee/profile/$d->Id"); ?>"><?php echo $d->name; ?></a>
                                                    <?php } else{ ?>
                                                        <?php echo $d->name; ?>
                                                    <?php } ?>
                                                    </h2>
                                                    <span
                                                        class="kt-user-card-v2__desc"><?php echo $d->email; ?></span>
                                                </div>
                                            </div>
                                        </span>
                                        <?php
                                        echo "</td>";
                                        echo "<td>$d->kode_member</td>";
                                        echo "<td>"; 
                                        if($d->j_k == "1") {echo "Laki Laki";}elseif($d->j_k == "2"){echo "Perempuan";}else{echo "Lainnya";}
                                        echo "</td>";
                                        echo "<td>";
                                        ?>
                                        <span style="widht:300px;">
                                            <div class="kt-user-card-v2">
                                                <div class="kt-user-card-v2__details">
                                                    <h2 class="kt-user-card-v2__name">
                                                        <?php echo $d->data_koperasi->name_koperasi; ?></h2>
                                                    <span
                                                        class="kt-user-card-v2__desc"><?php echo $d->data_koperasi->address; ?></span>
                                                </div>
                                            </div>
                                        </span>
                                        <?php
                                        echo "</td>";
                                        echo "<td>Jakarta, " . date("d F Y", strtotime("1996-11-10")) . "</td>";
                                        echo "<td>";
                                        ?>
                                        <div class="dropdown">
                                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                                data-toggle="dropdown">
                                                <i class="la la-cog"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-sm">
                                                <ul class="kt-nav">
                                                    <li class="kt-nav__item">
                                                        <a href="<?php echo site_url("#"); ?>"
                                                            class="kt-nav__link">
                                                            <i class="kt-nav__link-icon flaticon-visible"></i>
                                                            <span class="kt-nav__link-text">Detail Data</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <?php
                                        echo "</td>";
                                        echo "<td>" . date("d F Y", strtotime($d->created_date)) . "</td>";
                                        echo "<td>"; 
                                        if($d->status == 1) {echo "Active";}
                                        echo "</td>";

                                        echo "<td>$d->email</td>";
                                        echo "<td>$d->telephone1</td>";
                                        echo "<td>Jakarta</td>";
                                        echo "<td> Jakarta Timur </td>";
                                        echo "<td>TMII</td>";
                                        echo "<td>$d->address</td>";
                                        echo "<td>Indonesia</td>";

                                    echo "</tr>";
                                    
                                }
                                ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add_profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Anggota</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo base_url('employee/addProfile'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Avatar</label>
                                    <div class="col-lg-9 col-xl-9">
                                        <div class="kt-avatar kt-avatar--outline" id="kt_user_avatar">
                                            <div class="kt-avatar__holder" id="avatar_image_add"
                                                style="background-image: url(/metronic/themes/metronic/theme/default/demo9/dist/assets/media/users/100_13.jpg)">
                                            </div>
                                            <label class="kt-avatar__upload" data-toggle="kt-tooltip" title=""
                                                data-original-title="Change avatar">
                                                <i class="fa fa-pen"></i>
                                                <input type="file" name="add_image" id="add_image"
                                                    accept=".png, .jpg, .jpeg" onchange="readURLAdd(this);">
                                            </label>
                                            <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title=""
                                                data-original-title="Cancel avatar">
                                                <i class="fa fa-times"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nama Lengkap</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_fullname" name="add_fullname"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">NIK</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_nik" name="add_nik"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Jenis Kelamin</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="gender" id="gender" class="form-control" required>
                                            <option value="0">Laki Laki</option>
                                            <option value="1">Perempuan</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tempat Lahir</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="pob" name="pob"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tanggal Lahir</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="dateStart" name="dob"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Agama</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <select name="religion" id="religion" class="form-control" required>
                                            <option value="1">Islam</option>
                                            <option value="2">Katolik</option>
                                            <option value="3">Protestan</option>
                                            <option value="4">Hindu</option>
                                            <option value="5">Budha</option>
                                            <option value="6">Konguchu</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Kewarganegaraan</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="nation" name="nation"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Provinsi</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <select class="kt-selectpicker form-control" data-live-search="true" data-size="8" id=province name=province required>
                                        <option>Pilih Provinsi</option>
                                        <?php
                                            foreach($province->data as $prov) :
                                                echo "<option value='$prov->id'> $prov->nama</option>";
                                            endforeach;
                                        ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Kota \ Kab</label>
                                    <div class="col-lg-9 col-xl-8">
                                    <select class="kt-selectpicker form-control" data-live-search="true" data-size="8" id=city name=city placeholder="Pilih Kota" required> 
                                    </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Kecamatan</label>
                                    <div class="col-lg-9 col-xl-8">
                                    <select class="kt-selectpicker form-control" data-live-search="true" data-size="8" id=kec name=kec placeholder="Pilih Kecamatan" required> 
                                    </select>
                                    </div>

                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Alamat Detail</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_address" name="add_address"
                                            required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Contact Phone</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="la la-phone"></i></span></div>
                                            <input type="text" class="form-control" id="add_phone" name="add_phone" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                placeholder="Phone" aria-describedby="basic-addon1" required>
                                        </div>
                                        <span class="form-text text-muted">We'll never share your phone number with
                                            anyone else.</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Contact Phone </label>
                                    <div class="col-lg-9 col-xl-8">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="la la-phone"></i></span></div>
                                            <input type="text" class="form-control" id="add_phone2" name="add_phone2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                placeholder="Phone" aria-describedby="basic-addon1" required>
                                        </div>
                                        <span class="form-text text-muted">We'll never share your phone number with
                                            anyone else.</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Alamat Email</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="la la-at"></i></span></div>
                                            <input type="text" class="form-control" id="add_email" name="add_email"
                                                placeholder="Email" aria-describedby="basic-addon1" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">No Rekening</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="la la-credit-card"></i></span></div>
                                            <input type="text" class="form-control" id="add_rek" name="add_rek"
                                                placeholder="Rekening" aria-describedby="basic-addon1" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Status</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <select name="add_status" id="add_status" class="form-control" required>
                                            <option value="1">Active</option>
                                            <option value="0">Not Active</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Kategory Pajak </label>
                                    <div class="col-lg-9 col-xl-8">
                                        <select name="add_tax_status" id="add_tax_status" class="form-control" required>
                                            <option value="1">TK/0</option>
                                            <option value="2">TK/1</option>
                                            <option value="3">TK/2</option>
                                            <option value="4">TK/3</option>
                                            <option value="5">K/0</option>
                                            <option value="6">K/1</option>
                                            <option value="7">K/2</option>
                                            <option value="8">K/3</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tanggal Join</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="dateEnd" name="join_at"
                                            required>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button">Prosess Data</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>

<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                // $('#blah')
                //     .attr('src', e.target.result);
                document.getElementById('avatar_image').style.backgroundImage = "url(" + e.target.result + ")";
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURLAdd(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                // $('#blah')
                //     .attr('src', e.target.result);
                document.getElementById('avatar_image_add').style.backgroundImage = "url(" + e.target.result + ")";
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

