<style>
table {
    table-layout: fixed;
}

td {
    overflow: hidden;
    text-overflow: ellipsis;
}
</style>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Daftar Aktifitas </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon-time-1"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Aktifitas </a>

                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container   kt-grid__item--fluid">
            <?php if ( $status == '1' || $status == 1 ) { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Berhasil!&nbsp;&nbsp;</strong><?php echo "Jumlah Aktifitas : ".count($data); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } else { ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Perhatian!&nbsp;&nbsp;</strong> <?php echo "Tidak ada Aktifitas"; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } ?>
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                <strong>Info!&nbsp;&nbsp;</strong><?php echo "Tekan button pencarian untuk melakukan pencarian data aktifitas"; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">

                    </div>

                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <a href="#" class="btn btn-brand btn-icon-sm teratur-button" data-toggle="modal"
                                    data-target="#filter">
                                    <i class="flaticon-search"></i>
                                    Pencarian
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">


                    <table class="table table-striped table-bordered table-hover" id="tbl_list_default" width="100%">
                        <thead>
                            <tr>
                                <th class="all" width=10px>#</th>
                                <th class="min-tablet" width=100px>Nama</th>
                                <th class="min-tablet" width=50px>Tipe</th>
                                <th class="min-tablet" width=50px>Aktivitas</th>
                                <th class="min-tablet" width=150px>Info</th>
                                <th class="min-tablet" width=150px>Device</th>
                                <th class="min-tablet" width=80px>IP</th>
                            </tr>
                        </thead>
                        <tbody class="kt-datatable__body">
                            <?php  $i = 1; foreach ( $data as $d ) : ?>
                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td>
                                    <span style="widht:300px;">
                                        <div class="kt-user-card-v2">
                                            <div class="kt-user-card-v2__pic">
                                                <?php if ( $d->user->path_profile == "" ) { ?>
                                                <img src="<?php echo base_url(); ?>public/assets/custom/default_profile.png"
                                                    alt="image">
                                                <?php } else { ?>
                                                <?php $src =  $this->config->item('api_host') .'/view_file'. '/' .   $d->user->path_profile ;?>
                                                <img src="<?php echo $src; ?>" alt="image" width="40px" height="40px">
                                                <?php } ?>
                                            </div>
                                            <div class="kt-user-card-v2__details">
                                                <h2 class="kt-user-card-v2__name">
                                                    <a
                                                        href="<?php echo site_url("#"); ?>"><?php echo $d->user->nama; ?></a>
                                                </h2>
                                                <span
                                                    class="kt-user-card-v2__desc"><?php echo  $d->user->email; ?></span>
                                            </div>
                                        </div>
                                    </span>
                                </td>
                                <td> <?php echo $d->method_path ?> </td>
                                <td> <?php echo $d->caption; ?> </td>
                                <td><?php echo $d->keterangan; ?></td>
                                <td><?php echo $d->device ?></td>
                                <td><?php echo $d->ip?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="filter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"> Pencarian</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('riwayat'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">


                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tanggal Awal</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="dateStart" name="start" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tanggal Akhir</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="dateStart2" name="end" required>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button">Cari</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>