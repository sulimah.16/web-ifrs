<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/material.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>


<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">


    <!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Beranda </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <!-- <span class="kt-subheader__breadcrumbs-separator"></span> -->
                    <a href="" class="kt-subheader__breadcrumbs-link">
                    </a>
                    <!-- <span class="kt-subheader__breadcrumbs-separator"></span> -->
                    <a href="" class="kt-subheader__breadcrumbs-link">
                    </a>

                    <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
            </div>
            <div class="kt-subheader__toolbar">

            </div>
        </div>
    </div>



    <!-- end:: Subheader -->

    <!-- begin:: Content -->


    <!-- end:: Content -->
</div>



<div class="kt-container  kt-grid__item kt-grid__item--fluid">

    <div class="row">
        <div class="col-lg-6 col-xl-6 order-lg-12 order-xl-12">
            <div
                class="kt-portlet kt-portlet--fit kt-portlet--head-lg kt-portlet--head-overlay kt-portlet--skin-solid kt-portlet--height-fluid">
                <div class="kt-portlet__head kt-portlet__head--noborder kt-portlet__space-x">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Data Master
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">

                    </div>
                </div>
                <div class="kt-portlet__body kt-portlet__body--fit">
                    <div class="kt-widget17">
                        <div class="kt-widget17__visual kt-widget17__visual--chart kt-portlet-fit--top kt-portlet-fit--sides"
                            style="background-color: #25A499">
                            <div class="kt-widget17__chart" style="height:320px;">
                                <!-- <canvas id="chart_employee"></canvas> -->
                            </div>
                        </div>
                        <div class="kt-widget17__stats">
                            <div class="kt-widget17__items">
                                <div class="kt-widget17__item">
                                    <span class="kt-widget17__icon">
                                        <i class="fab fa-font-awesome-flag text-primary mr-5"></i>
                                    </span>

                                    <span class="kt-widget17__subtitle">
                                        Total Segmentasi
                                    </span>
                                    <span class="kt-widget17__desc">
                                        <?php echo $master->data->total_segment; ?> Segmen
                                    </span>
                                </div>
                                <div class="kt-widget17__item">
                                    <span class="kt-widget17__icon">
                                        <i class="fas fa-user-alt text-success mr-5"></i>
                                    </span>
                                    <span class="kt-widget17__subtitle">
                                        Total User
                                    </span>
                                    <span class="kt-widget17__desc">
                                        <?php echo $master->data->total_user; ?> User
                                    </span>
                                </div>
                            </div>
                            <div class="kt-widget17__items">
                                <div class="kt-widget17__item">
                                    <span class="kt-widget17__icon">
                                        <span class="kt-widget17__icon">
                                            <i class="fas fa-user-alt text-danger mr-5"></i>
                                        </span>
                                    </span>
                                    <span class="kt-widget17__subtitle">
                                        User Tidak Aktif
                                    </span>
                                    <span class="kt-widget17__desc">
                                        <?php echo $master->data->total_user_nonactive; ?> User
                                    </span>
                                </div>
                                <div class="kt-widget17__item">
                                    <span class="kt-widget17__icon">
                                        <i class="fas fa-file-alt text-warning mr-5"></i>
                                    </span>
                                    <span class="kt-widget17__subtitle">
                                        Total File Unggah
                                    </span>
                                    <span class="kt-widget17__desc">
                                        <?php echo  $master->data->total_file; ?> File
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-6 col-lg-6 order-lg-12 order-xl-1">
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-widget14">
                    <div class="kt-widget14__header kt-margin-b-30">
                        <h3 class="kt-widget14__title">
                            Data Draf
                        </h3>
                        <span class="kt-widget14__desc">
                        </span>
                    </div>
                    <div id="chartdivs" style="height:400px; width:100%"></div>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-xl-12 col-lg-12 order-lg-12 order-xl-1">
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-widget14">
                    <div class="kt-widget14__header kt-margin-b-30">
                        <h3 class="kt-widget14__title">
                            Aktifitas User Masuk
                        </h3>
                        <span class="kt-widget14__desc">
                        </span>
                    </div>
                    <div class="kt-widget14__chart" style="height:300px;">
                        <div id="chartdiv"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- <script src="//www.amcharts.com/lib/4/core.js"></script>
<script src="//www.amcharts.com/lib/4/charts.js"></script>
<script src="//www.amcharts.com/lib/4/themes/animated.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/material.js"></script> -->




    <!-- <div class="row">
    <div class="col-xl-12 col-lg-12 order-lg-12 order-xl-1">
        <div class="kt-portlet kt-portlet--height-fluid">
            <div class="kt-widget14">
                <div class="kt-widget14__header kt-margin-b-30">
                    <h3 class="kt-widget14__title">
                       Summary Attendance
                    </h3>
                    <span class="kt-widget14__desc">
                    </span>
                </div>
                <div class="kt-widget14__chart" style="height:300px;">
                    <div id="chartdiv"></div>
                </div>
            </div>
        </div>
    </div>
</div> -->

    <!-- <script>

        am4core.useTheme(am4themes_material);
        am4core.useTheme(am4themes_animated);

        // Create chart instance
        var chart = am4core.create("chartdiv", am4charts.XYChart);

        // Add data
        chart.data = <?php echo $att; ?>;
        

        // Create axes
        var categoryAxis = chart.xAxes.push(new am4charts.DateAxis());
        categoryAxis.renderer.grid.template.location = 0;
        //categoryAxis.renderer.minGridDistance = 30;

        // var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        var dateAxis = chart.yAxes.push(new am4charts.DateAxis());
        chart.dateFormatter.dateFormat = "HH:mm:ss";


        // Create series
        function createSeries(field, name) {
        var series = chart.series.push(new am4charts.LineSeries());
       
        series.dataFields.valueY = field;
        series.dataFields.dateX = "date";
        series.dataFields.dateY = field;
        series.name = name;
        series.tooltipText = "[b]{valueY}[/] : 00";
        series.strokeWidth = 2;
        
        var bullet = series.bullets.push(new am4charts.CircleBullet());
        bullet.events.on("hit", function(ev) {
        alert("Clicked on " + ev.target.dataItem.dateX + ": " + ev.target.dataItem.valueY);
        });
        }

        createSeries("value", "Pounch In");
        createSeries("value2", "Pounch Out");

        chart.legend = new am4charts.Legend();
        chart.cursor = new am4charts.XYCursor();
</script> -->




    <!--Begin::Row-->
    <div class="row">

        <div class="col-lg-6">


        </div>

        <div class="col-lg-6">

        </div>

    </div>
    <!--End::Row-->


</div>
<!-- end:: Content -->


<script>
am4core.useTheme(am4themes_animated);
am4core.useTheme(am4themes_material);

// Create chart instance
var chart = am4core.create("chartdiv", am4charts.XYChart);

// Add data
// chart.data = [{
//   "date": new Date(2019, 4, 19),
//   "value": 1.81,
// }, {
//   "date": new Date(2019, 5, 19),
//   "value": 1.95,
// }, {
//   "date": new Date(2019, 6, 19),
//   "value": 3.74,
// }, {
//   "date": new Date(2019, 7, 19),
//   "value": 4.20,
// }, {
//   "date": new Date(2019, 8, 19),
//   "value": 4.07,
// }, {
//   "date": new Date(2019, 9, 19),
//   "value": 7.09,
// }, {
//   "date": new Date(2019, 10, 19),
//   "value": 6.08,
// }, {
//   "date": new Date(2019, 11, 19),
//   "value": 4.53,

// },
// {
//   "date": new Date(2019, 12, 19),
//   "value": 3.85,
// }
// ];

chart.data = <?php echo $activity; ?>

// Create axes
var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
dateAxis.renderer.grid.template.location = 0;
dateAxis.renderer.minGridDistance = 30;

var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

// Create series
function createSeries(field, name) {
    var series = chart.series.push(new am4charts.LineSeries());
    series.dataFields.valueY = field;
    series.dataFields.dateX = "date";
    series.name = name;
    series.tooltipText = "{dateX}: [b]{valueY}[/]";
    series.strokeWidth = 2;

    series.smoothing = "monotoneX";

    var bullet = series.bullets.push(new am4charts.CircleBullet());
    bullet.circle.stroke = am4core.color("#fff");
    bullet.circle.strokeWidth = 2;

    return series;
}

valueAxis.renderer.labels.template.adapter.add("text", function(text) {
    return text;
});

createSeries("value", "User Masuk");
// createSeries("value2", "Series #2");
// createSeries("value3", "PD Forcast");

chart.legend = new am4charts.Legend();
chart.cursor = new am4charts.XYCursor();
</script>



<script>
am4core.ready(function() {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var chart = am4core.create("chartdivs", am4charts.PieChart);

    // Set data
    var selected;
    var types = [{
        type: "Per Hari",
        percent: "<?php echo $perse_hari ?>",
        color: chart.colors.getIndex(0),
        subs: [{
            type: "Oil",
            percent: "<?php echo $perse_hari ?>"
        }, ]
    }, {
        type: "Per Bucket",
        percent: "<?php echo $parse_bucket ?>",
        color: chart.colors.getIndex(1),
        subs: [{
            type: "Hydro",
            percent: "<?php echo $parse_bucket ?>"
        }, ]
    }];

    // Add data
    chart.data = generateChartData();

    // Add and configure Series
    var pieSeries = chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = "percent";
    pieSeries.dataFields.category = "type";
    pieSeries.slices.template.propertyFields.fill = "color";
    pieSeries.slices.template.propertyFields.isActive = "pulled";
    pieSeries.slices.template.strokeWidth = 0;

    function generateChartData() {
        var chartData = [];
        for (var i = 0; i < types.length; i++) {
            if (i == selected) {
                for (var x = 0; x < types[i].subs.length; x++) {
                    chartData.push({
                        type: types[i].subs[x].type,
                        percent: types[i].subs[x].percent,
                        color: types[i].color,
                        pulled: true
                    });
                }
            } else {
                chartData.push({
                    type: types[i].type,
                    percent: types[i].percent,
                    color: types[i].color,
                    id: i
                });
            }
        }
        return chartData;
    }

    pieSeries.slices.template.events.on("hit", function(event) {
        if (event.target.dataItem.dataContext.id != undefined) {
            selected = event.target.dataItem.dataContext.id;
        } else {
            selected = undefined;
        }
        chart.data = generateChartData();
    });

}); // end am4core.ready()
</script>