<style>
table {
    table-layout: fixed;
}

td {
    overflow: hidden;
    text-overflow: ellipsis;
}

#chartdiv {
  width: 100%;
  height: 500px;
}

/* #td_warning {
    background-color:#F7F563;
}

#td_primary {
    background-color:#d0f0c0;
} */
</style>

<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/material.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Trend PD </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-percentage"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                        Trend PD </a>

                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Success!&nbsp;</strong> <?php echo 'Lorem Imsum'; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        
                    </div>

                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">

                  
                    <table class="table table-striped table-bordered table-hover" id="tbl_cust" width="100%">
                        <thead>
                            <tr>
                                <th class="all" width=200px> Periode </th>
                                <th class="all" width=200px> PD Historis </th>
                                <th class="all" width=200px> PD Forecast </th>
                            </tr>
                        </thead>
                        <tbody class="kt-datatable__body">
                            <tr>
                                <td><?php echo date("d F Y", strtotime("2019-04-19")); ?></td>
                                <td id = 'td_warning'><?php echo ' 1,81% ' ?></td>
                                <td id = 'td_warning'><?php echo ' 2,00% ' ?></td>
                            </tr>
                            <tr>
                                <td><?php echo date("d F Y", strtotime("2019-05-19")); ?></td>
                                <td id = 'td_warning'><?php echo ' 1,95% ' ?></td>
                                <td id = 'td_warning'><?php echo ' 2,30% ' ?></td>
                            </tr>
                            <tr>
                                <td><?php echo date("d F Y", strtotime("2019-06-19")); ?></td>
                                <td id = 'td_warning'><?php echo ' 3,74% ' ?></td>
                                <td id = 'td_warning'><?php echo ' 4,00% ' ?></td>
                            </tr>
                            <tr>
                                <td><?php echo date("d F Y", strtotime("2019-07-19")); ?></td>
                                <td id = 'td_warning'><?php echo ' 4,20% ' ?></td>
                                <td id = 'td_warning'><?php echo ' 4,60% ' ?></td>
                            </tr>
                            <tr>
                                <td><?php echo date("d F Y", strtotime("2019-08-19")); ?></td>
                                <td id = 'td_warning'><?php echo ' 4,07% ' ?></td>
                                <td id = 'td_warning'><?php echo ' 4,20% ' ?></td>
                            </tr>
                            <tr>
                                <td><?php echo date("d F Y", strtotime("2019-09-19")); ?></td>
                                <td id = 'td_warning'><?php echo ' 7,09% ' ?></td>
                                <td id = 'td_warning'><?php echo ' 8,00% ' ?></td>
                            </tr>
                            <tr>
                                <td><?php echo date("d F Y", strtotime("2019-10-19")); ?></td>
                                <td id = 'td_warning'><?php echo ' 6,08% ' ?></td>
                                <td id = 'td_warning'><?php echo ' 5,00% ' ?></td>
                            </tr>
                            <tr>
                                <td><?php echo date("d F Y", strtotime("2019-11-19")); ?></td>
                                <td id = 'td_warning'><?php echo ' 4,53% ' ?></td>
                                <td id = 'td_warning'><?php echo '6,40% ' ?></td>
                            </tr>
                            <tr>
                                <td><?php echo date("d F Y", strtotime("2019-12-19")); ?></td>
                                <td id = 'td_warning'><?php echo ' 3,85% ' ?></td>
                                <td id = 'td_warning'><?php echo ' 5,30% ' ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        
                    </div>

                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">
                    <div class="kt-portlet kt-portlet--fit kt-portlet--head-noborder kt-portlet--height-fluid-half">
                        <div class="kt-widget14">
                            <div class="kt-widget14__header kt-margin-b-30">
                                <h3 class="kt-widget14__title">
                                    Trend Probability of Default (DP)
                                </h3>
                                <span class="kt-widget14__desc">
                                Trend Probability of Default (DP)
                                </span>
                            </div>
                            <div class="kt-widget14__chart" style="height:500px;">
                                <div id="chartdiv"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        



    </div>
</div>


<div class="modal fade" id="add_profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Anggota</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('anggota/create'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Avatar</label>
                                    <div class="col-lg-9 col-xl-9">
                                        <div class="kt-avatar kt-avatar--outline" id="kt_user_avatar">
                                            <div class="kt-avatar__holder" id="avatar_image_add"
                                                style="background-image: url(/metronic/themes/metronic/theme/default/demo9/dist/assets/media/users/100_13.jpg)">
                                            </div>
                                            <label class="kt-avatar__upload" data-toggle="kt-tooltip" title=""
                                                data-original-title="Change avatar">
                                                <i class="fa fa-pen"></i>
                                                <input type="file" name="add_image" id="add_image"
                                                    accept=".png, .jpg, .jpeg" onchange="readURLAdd(this);">
                                            </label>
                                            <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title=""
                                                data-original-title="Cancel avatar">
                                                <i class="fa fa-times"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nama Lengkap</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_fullname" name="add_fullname"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label"> No KTP</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_nik" name="add_nik"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Jenis Kelamin</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="gender" id="gender" class="form-control" required>
                                            <option value="0">Laki Laki</option>
                                            <option value="1">Perempuan</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Role</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="role" id="role" class="form-control" required>
                                            <option value="5">KEPALA KOPERASI CUSTOMER</option>
                                            <option value="3">PEGAWAI KOPERASI CUSTOMER</option>
                                            <option value="4">ANGGOTA KOPERASI CUSTOMER</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tempat Lahir</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="pob" name="pob"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tanggal Lahir</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="dateStart" name="dob"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Agama</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <select name="religion" id="religion" class="form-control" required>
                                           <?php foreach ( $agama->data as $a ) : ?>
                                                <option value="<?php echo $a->id_agama ?>"><?php echo $a->nama ?></option>
                                           <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Kewarganegaraan</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="nation" name="nation"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Provinsi</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <select class="kt-selectpicker form-control" data-live-search="true" data-size="8" id=province name=province required>
                                        <option>Pilih Provinsi</option>
                                        <?php
                                            foreach($province->data as $prov) :
                                                echo "<option value='$prov->id'> $prov->nama</option>";
                                            endforeach;
                                        ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Kota \ Kab</label>
                                    <div class="col-lg-9 col-xl-8">
                                    <select class="kt-selectpicker form-control" data-live-search="true" data-size="8" id=city name=city placeholder="Pilih Kota" required> 
                                    </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Kecamatan</label>
                                    <div class="col-lg-9 col-xl-8">
                                    <select class="kt-selectpicker form-control" data-live-search="true" data-size="8" id=kec name=kec placeholder="Pilih Kecamatan" required> 
                                    </select>
                                    </div>

                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Desa</label>
                                    <div class="col-lg-9 col-xl-8">
                                    <select class="kt-selectpicker form-control" data-live-search="true" data-size="8" id=des name=des placeholder="Pilih Kecamatan" required> 
                                    </select>
                                    </div>

                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Alamat Detail</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_address" name="add_address"
                                            required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Contact Phone</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="la la-phone"></i></span></div>
                                            <input type="text" class="form-control" id="add_phone" name="add_phone" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                placeholder="Phone" aria-describedby="basic-addon1" required>
                                        </div>
                                        <span class="form-text text-muted">We'll never share your phone number with
                                            anyone else.</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Contact Phone </label>
                                    <div class="col-lg-9 col-xl-8">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="la la-phone"></i></span></div>
                                            <input type="text" class="form-control" id="add_phone2" name="add_phone2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                placeholder="Phone" aria-describedby="basic-addon1" required>
                                        </div>
                                        <span class="form-text text-muted">We'll never share your phone number with
                                            anyone else.</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Alamat Email</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="la la-at"></i></span></div>
                                            <input type="text" class="form-control" id="add_email" name="add_email"
                                                placeholder="Email" aria-describedby="basic-addon1" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">No NPWP</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="la la-cc-mastercard"></i></span></div>
                                            <input type="text" class="form-control" id="add_npwp" name="add_npwp"
                                                placeholder="No NPWP" aria-describedby="basic-addon1" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Simpanan Pokok</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <select class="kt-selectpicker form-control" data-live-search="true" data-size="8" id="add_pokok" name=pokok required>
                                        <?php
                                            foreach($pokok->data as $pk) :
                                                echo "<option value='$pk->id_cus_type'> $pk->nilai_simpanan </option>";
                                            endforeach;
                                        ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Simpanan Wajib</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <select class="kt-selectpicker form-control" data-live-search="true" data-size="8" id="add_wajib" name=wajib required>
                                        <?php
                                            foreach($wajib->data as $wj) :
                                                echo "<option value='$wj->id_cus_type'> $wj->nilai_simpanan </option>";
                                            endforeach;
                                        ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Status</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <select name="add_status" id="add_status" class="form-control" required>
                                            <option value="1">Active</option>
                                            <option value="0">Not Active</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tanggal Join</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="dateEnd" name="join_at"
                                            required>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button">Prosess Data</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>

<div class="modal fade" id="add_penarikan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Penarikan Simpnan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo base_url('simpanan/penarikanSebagian'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                               
                                
                                <input class="form-control" type="hidden" id="add_id" name="ids"
                                            required>
                                        
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nama</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_name" name="name"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">No Rekening</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_rek" name="rek"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Jumlah</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_jum" name="jum" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                            required>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tujuan</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_tuj" name="tuj"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Catatan</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_cat" name="cat"
                                            required>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button">Prosess Data</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>

<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                // $('#blah')
                //     .attr('src', e.target.result);
                document.getElementById('avatar_image').style.backgroundImage = "url(" + e.target.result + ")";
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURLAdd(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                // $('#blah')
                //     .attr('src', e.target.result);
                document.getElementById('avatar_image_add').style.backgroundImage = "url(" + e.target.result + ")";
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function EditPenarikan (id, name, rek) {
        document.getElementById("add_name").value = name;
        document.getElementById("add_id").value = id;
        document.getElementById("add_rek").value = rek;
    }
</script>

<script>

am4core.useTheme(am4themes_animated);
am4core.useTheme(am4themes_material);

// Create chart instance
var chart = am4core.create("chartdiv", am4charts.XYChart);

// Add data
chart.data = [{
  "date": new Date(2019, 04, 19),
  "value": 1.81,
  "value3": 2.00,
}, {
  "date": new Date(2019, 05, 19),
  "value": 1.95,
  "value3": 2.30
}, {
  "date": new Date(2019, 06, 19),
  "value": 3.74,
  "value3": 4.00
}, {
  "date": new Date(2019, 07, 19),
  "value": 4.20,
  "value3": 4.60
}, {
  "date": new Date(2019, 08, 19),
  "value": 4.07,
  "value3": 4.20
}, {
  "date": new Date(2019, 09, 19),
  "value": 7.09,
  "value3": 8.00
}, {
  "date": new Date(2019, 10, 19),
  "value": 6.08,
  "value3": 5.00
}, {
  "date": new Date(2019, 11, 19),
  "value": 4.53,
  "value3": 6.40
},
{
  "date": new Date(2019, 12, 19),
  "value": 3.85,
  "value3": 5.30
}
];

// Create axes
var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
dateAxis.renderer.grid.template.location = 0;
dateAxis.renderer.minGridDistance = 30;

var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

// Create series
function createSeries(field, name) {
  var series = chart.series.push(new am4charts.LineSeries());
  series.dataFields.valueY = field;
  series.dataFields.dateX = "date";
  series.name = name;
  series.tooltipText = "{dateX}: [b]{valueY}[/]";
  series.strokeWidth = 2;
  
  series.smoothing = "monotoneX";
  
  var bullet = series.bullets.push(new am4charts.CircleBullet());
  bullet.circle.stroke = am4core.color("#fff");
  bullet.circle.strokeWidth = 2;
  
  return series;
}

valueAxis.renderer.labels.template.adapter.add("text", function(text) {
  return text + "%";
});

createSeries("value", "PD History");
// createSeries("value2", "Series #2");
createSeries("value3", "PD Forcast");

chart.legend = new am4charts.Legend();
chart.cursor = new am4charts.XYCursor();

// am4core.ready(function() {

// // Themes begin
// am4core.useTheme(am4themes_material);
// am4core.useTheme(am4themes_animated);
// // Themes end

// var chart = am4core.create("chartdiv", am4charts.XYChart);
// chart.paddingRight = 20;

// var data = [];
// var visits = 10;
// var previousValue;

// for (var i = 0; i < 100; i++) {
//     visits += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 10);

//     if(i > 0){
//         // add color to previous data item depending on whether current value is less or more than previous value
//         if(previousValue <= visits){
//             data[i - 1].color = chart.colors.getIndex(0);
//         }
//         else{
//             data[i - 1].color = chart.colors.getIndex(5);
//         }
//     }    
    
//     data.push({ date: new Date(2018, 0, i + 1), value: visits });
//     previousValue = visits;
// }

// chart.data = data;

// var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
// dateAxis.renderer.grid.template.location = 0;
// dateAxis.renderer.axisFills.template.disabled = true;
// dateAxis.renderer.ticks.template.disabled = true;

// var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
// valueAxis.tooltip.disabled = true;
// valueAxis.renderer.minWidth = 35;
// valueAxis.renderer.axisFills.template.disabled = true;
// valueAxis.renderer.ticks.template.disabled = true;

// var series = chart.series.push(new am4charts.LineSeries());
// series.dataFields.dateX = "date";
// series.dataFields.valueY = "value";
// series.strokeWidth = 2;
// series.tooltipText = "value: {valueY}, day change: {valueY.previousChange}";

// // set stroke property field
// series.propertyFields.stroke = "color";

// chart.cursor = new am4charts.XYCursor();

// var scrollbarX = new am4core.Scrollbar();
// chart.scrollbarX = scrollbarX;

// dateAxis.start = 0.7;
// dateAxis.keepSelection = true;


// }); // end am4core.ready()
</script>


