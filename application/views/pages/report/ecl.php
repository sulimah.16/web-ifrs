<style>
table {
    table-layout: fixed;
}

td {
    overflow: hidden;
    text-overflow: ellipsis;
}

/* #td_warning {
    background-color:#F7F563;
}

#td_succes {
    background-color:#add8e6;
}

#td_danger {
    background-color:#FF8784;
}

#td_primary {
    background-color:#d0f0c0;
} */

</style>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                    Summary ECL </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon-doc"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                        Summary ECL </a>

                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Success!&nbsp;</strong> <?php echo 'Lorem Imsum'; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        
                    </div>

                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">

    

                  
                    <table class="table table-striped table-bordered table-hover" id="tbl_cust" width="100%">
                        <thead>
                            <tr>
                                <th class="all" width=80px> Bucket </th>
                                <th class="all" width=200px> Outstanding 31 Des 2019 </th>
                                <th class="all" width=150px> ECL Base </th>
                                <th class="all" width=200px> ECL Forward Looking </th>
                                <th class="all" width=200px> % Forward Looking </th>
                            </tr>
                        </thead>
                        <tbody class="kt-datatable__body">
                            <tr>
                                <td><?php echo '1' ?></td>
                                <td id = 'td_danger'><?php echo '  4.860.844.158.235 ' ?></td>
                                <td id = 'td_primary'><?php echo '  3.143.651.296 ' ?></td>
                                <td id = 'td_primary'><?php echo '  3.275.076.403 ' ?></td>
                                <td id = 'td_warning'><?php echo ' 4,18% ' ?></td>
                            </tr>
                            <tr>
                                <td><?php echo '2' ?></td>
                                <td id = 'td_danger'><?php echo '   58.804.961.669  ' ?></td>
                                <td id = 'td_primary'><?php echo '   922.111.120  ' ?></td>
                                <td id = 'td_primary'><?php echo '   960.661.373 ' ?></td>
                                <td id = 'td_warning'><?php echo ' 4,18% ' ?></td>
                            </tr>
                            <tr>
                                <td><?php echo '3' ?></td>
                                <td id = 'td_danger'><?php echo '    55.580.991.538  ' ?></td>
                                <td id = 'td_primary'><?php echo '    2.125.081.723  ' ?></td>
                                <td id = 'td_primary'><?php echo '    2.213.923.986 ' ?></td>
                                <td id = 'td_warning'><?php echo ' 4,18% ' ?></td>
                            </tr>
                            <tr>
                                <td><?php echo '4' ?></td>
                                <td id = 'td_danger'><?php echo '     32.775.089.625 ' ?></td>
                                <td id = 'td_primary'><?php echo '     1.388.649.936  ' ?></td>
                                <td id = 'td_primary'><?php echo '     1.446.704.551 ' ?></td>
                                <td id = 'td_warning'><?php echo ' 4,18% ' ?></td>
                            </tr>
                            <tr>
                                <td><?php echo '5' ?></td>
                                <td id = 'td_danger'><?php echo '      31.712.965.743  ' ?></td>
                                <td id = 'td_primary'><?php echo '      1.795.164.829  ' ?></td>
                                <td id = 'td_primary'><?php echo '     1.870.214.416 ' ?></td>
                                <td id = 'td_warning'><?php echo ' 4,18% ' ?></td>
                            </tr>
                            <tr>
                                <td><?php echo '6' ?></td>
                                <td id = 'td_danger'><?php echo '       57.928.455.565   ' ?></td>
                                <td id = 'td_primary'><?php echo '      4.659.557.490  ' ?></td>
                                <td id = 'td_primary'><?php echo '      4.854.357.353 ' ?></td>
                                <td id = 'td_warning'><?php echo ' 4,18% ' ?></td>
                            </tr>
                            <tr>
                                <td><?php echo '7' ?></td>
                                <td id = 'td_danger'><?php echo '        26.746.039.119  ' ?></td>
                                <td id = 'td_primary'><?php echo '      3.072.197.637 ' ?></td>
                                <td id = 'td_primary'><?php echo '     3.200.635.515 ' ?></td>
                                <td id = 'td_warning'><?php echo ' 4,18% ' ?></td>
                            </tr>
                            <tr>
                                <td><?php echo '8' ?></td>
                                <td id = 'td_danger'><?php echo '   4.355.484.323   ' ?></td>
                                <td id = 'td_primary'><?php echo '      728.619.560  ' ?></td>
                                <td id = 'td_primary'><?php echo '     759.080.605  ' ?></td>
                                <td id = 'td_warning'><?php echo ' 4,18% ' ?></td>
                            </tr>
                            <tr>
                                <td><?php echo '9' ?></td>
                                <td id = 'td_danger'><?php echo '    6.683.549.041   ' ?></td>
                                <td id = 'td_primary'><?php echo '    2.655.162.880   ' ?></td>
                                <td id = 'td_primary'><?php echo '     2.766.165.988  ' ?></td>
                                <td id = 'td_warning'><?php echo ' 4,18% ' ?></td>
                            </tr>
                            <tr>
                                <td><?php echo '10' ?></td>
                                <td id = 'td_danger'><?php echo '     174.291.220.093   ' ?></td>
                                <td id = 'td_primary'><?php echo '     78.431.049.042   ' ?></td>
                                <td id = 'td_primary'><?php echo '      78.431.049.042  ' ?></td>
                                <td id = 'td_warning'><?php echo ' 0,00% ' ?></td>
                            </tr>
                            <tr>
                                <td><?php echo 'Jumlah' ?></td>
                                <td id = 'td_danger'><?php echo '     5.309.722.914.951   ' ?></td>
                                <td id = 'td_primary'><?php echo '     98.921.245.513   ' ?></td>
                                <td id = 'td_primary'><?php echo '       99.777.869.231  ' ?></td>
                                <td id = 'td_warning'><?php echo ' 0,87% ' ?></td>
                                
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="add_profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Anggota</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('anggota/create'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Avatar</label>
                                    <div class="col-lg-9 col-xl-9">
                                        <div class="kt-avatar kt-avatar--outline" id="kt_user_avatar">
                                            <div class="kt-avatar__holder" id="avatar_image_add"
                                                style="background-image: url(/metronic/themes/metronic/theme/default/demo9/dist/assets/media/users/100_13.jpg)">
                                            </div>
                                            <label class="kt-avatar__upload" data-toggle="kt-tooltip" title=""
                                                data-original-title="Change avatar">
                                                <i class="fa fa-pen"></i>
                                                <input type="file" name="add_image" id="add_image"
                                                    accept=".png, .jpg, .jpeg" onchange="readURLAdd(this);">
                                            </label>
                                            <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title=""
                                                data-original-title="Cancel avatar">
                                                <i class="fa fa-times"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nama Lengkap</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_fullname" name="add_fullname"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label"> No KTP</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_nik" name="add_nik"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Jenis Kelamin</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="gender" id="gender" class="form-control" required>
                                            <option value="0">Laki Laki</option>
                                            <option value="1">Perempuan</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Role</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="role" id="role" class="form-control" required>
                                            <option value="5">KEPALA KOPERASI CUSTOMER</option>
                                            <option value="3">PEGAWAI KOPERASI CUSTOMER</option>
                                            <option value="4">ANGGOTA KOPERASI CUSTOMER</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tempat Lahir</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="pob" name="pob"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tanggal Lahir</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="dateStart" name="dob"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Agama</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <select name="religion" id="religion" class="form-control" required>
                                           <?php foreach ( $agama->data as $a ) : ?>
                                                <option value="<?php echo $a->id_agama ?>"><?php echo $a->nama ?></option>
                                           <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Kewarganegaraan</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="nation" name="nation"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Provinsi</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <select class="kt-selectpicker form-control" data-live-search="true" data-size="8" id=province name=province required>
                                        <option>Pilih Provinsi</option>
                                        <?php
                                            foreach($province->data as $prov) :
                                                echo "<option value='$prov->id'> $prov->nama</option>";
                                            endforeach;
                                        ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Kota \ Kab</label>
                                    <div class="col-lg-9 col-xl-8">
                                    <select class="kt-selectpicker form-control" data-live-search="true" data-size="8" id=city name=city placeholder="Pilih Kota" required> 
                                    </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Kecamatan</label>
                                    <div class="col-lg-9 col-xl-8">
                                    <select class="kt-selectpicker form-control" data-live-search="true" data-size="8" id=kec name=kec placeholder="Pilih Kecamatan" required> 
                                    </select>
                                    </div>

                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Desa</label>
                                    <div class="col-lg-9 col-xl-8">
                                    <select class="kt-selectpicker form-control" data-live-search="true" data-size="8" id=des name=des placeholder="Pilih Kecamatan" required> 
                                    </select>
                                    </div>

                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Alamat Detail</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_address" name="add_address"
                                            required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Contact Phone</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="la la-phone"></i></span></div>
                                            <input type="text" class="form-control" id="add_phone" name="add_phone" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                placeholder="Phone" aria-describedby="basic-addon1" required>
                                        </div>
                                        <span class="form-text text-muted">We'll never share your phone number with
                                            anyone else.</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Contact Phone </label>
                                    <div class="col-lg-9 col-xl-8">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="la la-phone"></i></span></div>
                                            <input type="text" class="form-control" id="add_phone2" name="add_phone2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                placeholder="Phone" aria-describedby="basic-addon1" required>
                                        </div>
                                        <span class="form-text text-muted">We'll never share your phone number with
                                            anyone else.</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Alamat Email</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="la la-at"></i></span></div>
                                            <input type="text" class="form-control" id="add_email" name="add_email"
                                                placeholder="Email" aria-describedby="basic-addon1" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">No NPWP</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                        class="la la-cc-mastercard"></i></span></div>
                                            <input type="text" class="form-control" id="add_npwp" name="add_npwp"
                                                placeholder="No NPWP" aria-describedby="basic-addon1" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Simpanan Pokok</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <select class="kt-selectpicker form-control" data-live-search="true" data-size="8" id="add_pokok" name=pokok required>
                                        <?php
                                            foreach($pokok->data as $pk) :
                                                echo "<option value='$pk->id_cus_type'> $pk->nilai_simpanan </option>";
                                            endforeach;
                                        ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Simpanan Wajib</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <select class="kt-selectpicker form-control" data-live-search="true" data-size="8" id="add_wajib" name=wajib required>
                                        <?php
                                            foreach($wajib->data as $wj) :
                                                echo "<option value='$wj->id_cus_type'> $wj->nilai_simpanan </option>";
                                            endforeach;
                                        ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Status</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <select name="add_status" id="add_status" class="form-control" required>
                                            <option value="1">Active</option>
                                            <option value="0">Not Active</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tanggal Join</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="dateEnd" name="join_at"
                                            required>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button">Prosess Data</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>

<div class="modal fade" id="add_penarikan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Penarikan Simpnan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo base_url('simpanan/penarikanSebagian'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                               
                                
                                <input class="form-control" type="hidden" id="add_id" name="ids"
                                            required>
                                        
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nama</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_name" name="name"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">No Rekening</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_rek" name="rek"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Jumlah</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_jum" name="jum" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                            required>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tujuan</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_tuj" name="tuj"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Catatan</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="add_cat" name="cat"
                                            required>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button">Prosess Data</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>

<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                // $('#blah')
                //     .attr('src', e.target.result);
                document.getElementById('avatar_image').style.backgroundImage = "url(" + e.target.result + ")";
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURLAdd(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                // $('#blah')
                //     .attr('src', e.target.result);
                document.getElementById('avatar_image_add').style.backgroundImage = "url(" + e.target.result + ")";
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function EditPenarikan (id, name, rek) {
        document.getElementById("add_name").value = name;
        document.getElementById("add_id").value = id;
        document.getElementById("add_rek").value = rek;
    }
</script>

