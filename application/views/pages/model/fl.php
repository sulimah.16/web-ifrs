<style>
#tbdy {
    margin-top: 25px;
    width: 550px;
}
</style>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Rekap Model FL</h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon-interface-10"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Rekap Model FL </a>

                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container   kt-grid__item--fluid">
            <?php if ( $data->status == '1' || $data->status == 1 ) { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Berhasil!&nbsp;&nbsp;</strong><?php echo "Jumlah Data : ".count($data->data); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } else { ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Perhatian!&nbsp;&nbsp;</strong> <?php echo "Tidak ada data"; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } ?>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h2 class="kt-portlet__head-title">
                            <?php 
                            if (count($data->data) > 0) { 
                                echo "Rekap Model FL - Segmen ".$data->data[0]->segment->nama_segment;
                            } ?>
                        </h2>
                    </div>

                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <?php if ( $this->session->userdata('role') == '1' || $this->session->userdata('role') == 1 || $this->session->userdata('role') == '2' || $this->session->userdata('role') == 2 ) { ?>
                                <div id='tbdy'>
                                    <form method="post" enctype="multipart/form-data"
                                        action="<?php echo site_url('model/fl'); ?>">
                                        <div class="form-group row">
                                            <div class="col-lg-6">
                                                <select name="segment" id="segment" class="form-control">
                                                    <?php foreach( $segment->data as $s ):?>
                                                    <option value="<?php echo $s->id_segment; ?>">
                                                        <?php echo $s->nama_segment." (".$s->kode_segment.")"; ?>
                                                    </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-6">
                                                <button type="submit" class="btn btn-primary teratur-button"><i
                                                        class="flaticon-file-2 icon-xs"></i>Ubah Data</button>

                                                <a href="#" class="btn btn-brand btn-icon-sm teratur-button"
                                                    data-toggle="modal" data-target="#add_profile">
                                                    <i class="flaticon-plus"></i>Upload Data
                                                </a>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                                <?php } else { ?>
                                <div id='tbdy'>
                                    <form method="post" enctype="multipart/form-data"
                                        action="<?php echo site_url('model/fl'); ?>">
                                        <div class="form-group row">
                                            <div class="col-lg-8">
                                                <select name="segment" id="segment" class="form-control">
                                                    <?php foreach( $segment->data as $s ):?>
                                                    <option value="<?php echo $s->id_segment; ?>">
                                                        <?php echo $s->nama_segment." (".$s->kode_segment.")"; ?>
                                                    </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-4">
                                                <button type="submit" class="btn btn-primary teratur-button"><i
                                                        class="flaticon-file-2 icon-xs"></i>Ubah Data</button>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">
                    <table class="table table-responsive table-striped table-bordered table-hover" id="tbl_list_default"
                        width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Segment</th>
                                <th>Nomor</th>
                                <th>Var 1</th>
                                <th>Var 2</th>
                                <th>Lag 1</th>
                                <th>Lag 2</th>
                                <th>C_Coef</th>
                                <th>PValue</th>
                                <th>Koef Var 1</th>
                                <th>PValue</th>
                                <th>Koef Var 2</th>
                                <th>PValue</th>
                                <th>Adj R Square</th>
                                <th>F Prob</th>
                                <th>Tanda</th>
                                <th>Signifikan</th>
                            </tr>
                        </thead>
                        <tbody class="kt-datatable__body">
                            <?php $i = 1; foreach( $data->data as $d ): ?>
                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $d->segment->nama_segment." (".$d->segment->kode_segment.")"; ?></td>
                                <td><?php echo $d->nomor; ?></td>
                                <td><?php echo $d->var1; ?></td>
                                <td><?php echo $d->var2; ?></td>
                                <td><?php echo $d->lag1; ?></td>
                                <td><?php echo $d->lag2; ?></td>
                                <td><?php echo $d->c_coef; ?></td>
                                <td><?php echo $d->c_coef_pvalue; ?></td>
                                <td><?php echo $d->koef_var1; ?></td>
                                <td><?php echo $d->koef_var1_pvalue; ?></td>
                                <td><?php echo $d->koef_var2; ?></td>
                                <td><?php echo $d->koef_var2_pvalue; ?></td>
                                <td><?php echo $d->adjr_square; ?></td>
                                <td><?php echo $d->f_prob; ?></td>
                                <td><?php echo $d->tanda; ?></td>
                                <td><?php echo $d->signifikan; ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add_profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Upload Model FL</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('model/input_fl'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Segmen</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="segment" id="segment" class="form-control">
                                            <?php foreach( $segment->data as $s ):?>
                                            <option value="<?php echo $s->id_segment; ?>">
                                                <?php echo $s->nama_segment." (".$s->kode_segment.")"; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <span class="form-text text-muted">Pilih Segmentasi.</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">File Upload</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="customFile"
                                                    name="file_c">
                                                <label class="custom-file-label" for="customFile">Choose
                                                    file</label>
                                            </div>
                                        </div>
                                        <span class="form-text text-muted">contoh untuk upload file Model FL. <a
                                                id='url'
                                                href="<?php echo base_url(); ?>public/example/REKAV_MODELFL_INPUT.csv"
                                                download>klik disini</a></span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button" onclick="return act_confirm()">Upload
                        Data</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>