<style>
table {
    table-layout: fixed;
    display: block;
    overflow-x: auto;
    white-space: nowrap;
}

td {
    overflow: hidden;
    text-overflow: ellipsis;
}
</style>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Model Bucket Simulate</h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon-list-1"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Model Bucket Simulate No. <?php echo $data->data->kode_draft_bucket;?></a>
                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>

        <!-- <?php echo json_encode($data); ?> -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">

                    </div>

                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <!-- <a href="#" class="btn btn-brand btn-icon-sm teratur-button" data-toggle="modal"
                                    data-target="#add_profile">
                                    <i class="flaticon-plus"></i>
                                    Ubah Data
                                </a> -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">
                    <div class="row">
                        <div class="col-lg-6"><br />
                            <div class="column" style="float: left;width: 40%;">
                                <p><b>Kode Model Simulate</b></p>
                                <p><b>Nama Model Simulate</b></p>
                                <p><b>Segmentasi</b></p>
                                <p><b>Model Normalisasi Rollrate</b></p>
                                <p><b>Jumlah Moving</b></p>
                                <p><b>Periode Mulai</b></p>
                                <p><b>Periode Selesai</b></p>
                            </div>
                            <div class="column" style="float: left;width: 60%;">
                                <p>: <?php echo $data->data->kode_draft_bucket; ?></p>
                                <p>: <?php echo $data->data->nama_draft_bucket; ?></p>
                                <p>: <?php echo $data->data->segment->nama_segment; ?></p>
                                <p>: <?php echo $data->data->flag_normalisasi; ?></p>
                                <p>: <?php echo $data->data->moving_titik. " Baris"; ?></p>
                                <p>: <?php echo date("d M Y",strtotime($data->data->start_date)); ?></p>
                                <p>: <?php echo date("d M Y",strtotime($data->data->end_date)); ?></p>
                            </div>
                        </div>
                        <div class="col-lg-6"><br />
                            <div class="column" style="float: left;width: 30%;">
                                <p><b>Dibuat oleh</b></p>
                                <p><b>Tanggal dibuat</b></p>
                            </div>
                            <div class="column" style="float: left;width: 70%;">
                                <p>: <?php echo $data->data->created_user->nama; ?></p>
                                <p>: <?php echo date("d M Y H:i:s",strtotime($data->data->created_date)); ?></p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <ul class="nav nav-pills nav-fill" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tab_araging">AR Aging</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_rollrate_bn">Rollrate </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_rollrate_n">Rollrate normalisasi</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_average">Agregasi Rollrate</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_rollrate_na">Rollrate normalisasi
                                average</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_lossrate">Lossrate Bulanan</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_moving_avg">Moving Average Lossrate</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_sum_delta">Sum Delta Lossrate</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_odr">ODR</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <?php $idb = $data->data->id_draft_bucket; ?>
                        <div class="tab-pane active" id="tab_araging" role="tabpanel">
                            <table class="table table-striped table-bordered table-hover" id="draf_ar" width="100%">
                                <thead>
                                    <tr>
                                        <th class="all" width=20px>#</th>
                                        <th class="all" width=100px>Periode</th>
                                        <?php foreach ( $data->data->araging->header_bucket as $h ) : ?>
                                        <th class="min-tablet" width=100px><?php echo $h." hari"; ?></th>
                                        <?php endforeach ?>
                                        <th class="min-tablet" width=200px>Jumlah Outstanding</th>
                                    </tr>
                                </thead>
                                <tbody class="kt-datatable__body">
                                    <?php $t = 1; foreach ( $data->data->araging->list_bucket as $l ) : ?>
                                    <tr>
                                        <td><?php echo $t++; ?></td>
                                        <td><?php echo $l->periode; ?></td>
                                        <?php foreach ( $l->nilai as $n ) : ?>
                                        <td><?php echo "Rp. " . number_format( $n, 2, ',', '.');?></td>
                                        <?php endforeach; ?>
                                        <td><?php echo "Rp. " . number_format( $l->jumlah_outstanding, 2, ',', '.');?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Komentar</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    </button>
                                </div>
                                <div class="kt-portlet">
                                    <div class="kt-portlet__body">
                                        <div class="kt-scroll kt-scroll--pull" style="overflow:scroll; height:350px;">
                                            <div class="kt-widget3">
                                                <?php if ($data->data->list_komentar == []) { ?>
                                                <div class="kt-widget3__item">
                                                    <h4 class="center">Tidak ada Komentar</h1>
                                                </div>
                                                <?php } else { ?>
                                                <?php foreach ($data->data->list_komentar as $key => $kme) { 
                                                if ($kme->flag != 1) {
                                                    continue;
                                                }
                                                ?>
                                                <div class="kt-widget3__item">
                                                    <div class="kt-widget3__header">
                                                        <div class="kt-widget3__user-img">
                                                            <?php if ( $kme->user->path_profile == "" ) { ?>
                                                            <img class="kt-widget3__img"
                                                                src="<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>"
                                                                alt="image">
                                                            <?php } else { ?>
                                                            <?php $src =  $this->config->item('api_host') .'/view_file'. '/' .$kme->user->path_profile ;?>
                                                            <img class="kt-widget3__img" src="<?php echo $src; ?>"
                                                                alt="image" width="40px" height="40px">
                                                            <?php } ?>
                                                        </div>
                                                        <div class="kt-widget3__info">
                                                            <a href="#" class="kt-widget3__username">
                                                                <?php echo $kme->user->nama; ?>
                                                            </a><br>
                                                            <span class="kt-widget3__time">
                                                                <?php
                                                            if ($kme->updated_date != null) {
                                                                echo get_timeago(strtotime($kme->updated_date)); 
                                                            } else {
                                                                echo get_timeago(strtotime($kme->created_date)); 
                                                            }
                                                            ?>
                                                            </span>
                                                        </div>
                                                        <span class="kt-widget3__status kt-font-info">
                                                            <?php if ($this->session->userdata("ids") == $kme->created_by) { ?>
                                                            <span
                                                                id="btn_editkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                class="kt-nav__link kt-font-info" href="#"
                                                                style="cursor:pointer"
                                                                onClick="return EditKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', '<?php echo $kme->komentar; ?>','<?php echo "1"; ?>')">Ubah
                                                            </span>
                                                            &nbsp
                                                            <span
                                                                id="btn_batkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                href="#" class="kt-nav__link kt-font-info" onClick="return batalKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', 
                                                            '<?php echo $kme->komentar; ?>', 1
                                                        )" style="display:none; cursor:pointer;">Batal
                                                            </span>
                                                            &nbsp
                                                            <a href="<?php echo site_url("Model/deletekomen_draft_bucket?idk=$kme->id_komentar_bucket&typ=1&idb=$idb"); ?>"
                                                                class="kt-nav__link kt-font-info"
                                                                onclick='return del_confirm()'>Hapus
                                                            </a>
                                                            <?php } ?>
                                                        </span>
                                                    </div>
                                                    <div class="kt-widget3__body">
                                                        <p class="kt-widget3__text">
                                                            <?php echo $kme->komentar; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__foot">
                                        <form method="post" enctype="multipart/form-data"
                                            action="<?php echo site_url("model/addkomen_draft_bucket?idb=$idb"); ?>">
                                            <div class="form-row align-items-center">
                                                <div class="col-auto">
                                                    <input value="1" name="type_coment" hidden></input>
                                                    <input value="1" name="flag_coment" hidden></input>
                                                    <input value="0" id="komen_id1" name="id_coment" hidden></input>
                                                    <input value="<?php echo $idb; ?>" name="id_bucket" hidden></input>
                                                    <textarea id="komen_content1" required
                                                        style="height: 50px; width: 400px;" placeholder="Type here..."
                                                        name="komentar"></textarea>
                                                </div>
                                                <div class="col-auto">
                                                    <button type="submit"
                                                        class="btn btn-primary teratur-button">Kirim</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_rollrate_bn" role="tabpanel">
                            <table class="table table-striped table-bordered table-hover" id="draf_rollrate_bn"
                                width="100%">
                                <thead>
                                    <tr>
                                        <th class="all" width=20px>#</th>
                                        <th class="all" width=100px>Periode</th>
                                        <?php foreach ( $data->data->rollrate_belum_normal->header_bucket as $h ) : ?>
                                        <th class="min-tablet" width=100px><?php echo $h." hari"; ?></th>
                                        <?php endforeach ?>
                                    </tr>
                                </thead>
                                <tbody class="kt-datatable__body">
                                    <?php $t = 1; foreach ( $data->data->rollrate_belum_normal->list_bucket as $l ) : ?>
                                    <tr>
                                        <td><?php echo $t++; ?></td>
                                        <td><?php echo $l->periode; ?></td>
                                        <?php foreach ( $l->nilai as $n ) : ?>
                                        <td><?php if ( $n == 'Infinity') {
                                            $v = $n;
                                        } else {
                                            $v = $n *100;
                                            $v = round($v, 2);
                                        }
                                        echo $v." %";?></td>
                                        <?php endforeach; ?>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Komentar</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    </button>
                                </div>
                                <div class="kt-portlet">
                                    <div class="kt-portlet__body">
                                        <div class="kt-scroll kt-scroll--pull" style="overflow:scroll; height:350px;">
                                            <div class="kt-widget3">
                                                <?php if ($data->data->list_komentar == []) { ?>
                                                <div class="kt-widget3__item">
                                                    <h4 class="center">Tidak ada Komentar</h1>
                                                </div>
                                                <?php } else { ?>
                                                <?php foreach ($data->data->list_komentar as $key => $kme) { 
                                                if ($kme->flag != 2) {
                                                    continue;
                                                }
                                                ?>
                                                <div class="kt-widget3__item">
                                                    <div class="kt-widget3__header">
                                                        <div class="kt-widget3__user-img">
                                                            <?php if ( $kme->user->path_profile == "" ) { ?>
                                                            <img class="kt-widget3__img"
                                                                src="<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>"
                                                                alt="image">
                                                            <?php } else { ?>
                                                            <?php $src =  $this->config->item('api_host') .'/view_file'. '/' .$kme->user->path_profile ;?>
                                                            <img class="kt-widget3__img" src="<?php echo $src; ?>"
                                                                alt="image" width="40px" height="40px">
                                                            <?php } ?>
                                                        </div>
                                                        <div class="kt-widget3__info">
                                                            <a href="#" class="kt-widget3__username">
                                                                <?php echo $kme->user->nama; ?>
                                                            </a><br>
                                                            <span class="kt-widget3__time">
                                                                <?php
                                                            if ($kme->updated_date != null) {
                                                                echo get_timeago(strtotime($kme->updated_date)); 
                                                            } else {
                                                                echo get_timeago(strtotime($kme->created_date)); 
                                                            }
                                                            ?>
                                                            </span>
                                                        </div>
                                                        <span class="kt-widget3__status kt-font-info">
                                                            <?php if ($this->session->userdata("ids") == $kme->created_by) { ?>
                                                            <span
                                                                id="btn_editkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                class="kt-nav__link kt-font-info" href="#"
                                                                style="cursor:pointer"
                                                                onClick="return EditKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', '<?php echo $kme->komentar; ?>','<?php echo "2"; ?>')">Ubah
                                                            </span>
                                                            &nbsp
                                                            <span
                                                                id="btn_batkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                href="#" class="kt-nav__link kt-font-info" onClick="return batalKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', 
                                                            '<?php echo $kme->komentar; ?>', 2
                                                        )" style="display:none; cursor:pointer;">Batal
                                                            </span>
                                                            &nbsp
                                                            <a href="<?php echo site_url("Model/deletekomen_draft_bucket?idk=$kme->id_komentar_bucket&typ=1&idb=$idb"); ?>"
                                                                class="kt-nav__link kt-font-info"
                                                                onclick='return del_confirm()'>Hapus
                                                            </a>
                                                            <?php } ?>
                                                        </span>
                                                    </div>
                                                    <div class="kt-widget3__body">
                                                        <p class="kt-widget3__text">
                                                            <?php echo $kme->komentar; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__foot">
                                        <form method="post" enctype="multipart/form-data"
                                            action="<?php echo site_url("model/addkomen_draft_bucket?idb=$idb"); ?>">
                                            <div class="form-row align-items-center">
                                                <div class="col-auto">
                                                    <input value="1" name="type_coment" hidden></input>
                                                    <input value="2" name="flag_coment" hidden></input>
                                                    <input value="0" id="komen_id2" name="id_coment" hidden></input>
                                                    <input value="<?php echo $idb; ?>" name="id_bucket" hidden></input>
                                                    <textarea id="komen_content2" required
                                                        style="height: 50px; width: 400px;" placeholder="Type here..."
                                                        name="komentar"></textarea>
                                                </div>
                                                <div class="col-auto">
                                                    <button type="submit"
                                                        class="btn btn-primary teratur-button">Kirim</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_rollrate_n" role="tabpanel">
                            <table class="table table-striped table-bordered table-hover" id="draf_rollrate_n"
                                width="100%">
                                <thead>
                                    <tr>
                                        <th class="all" width=20px>#</th>
                                        <th class="all" width=100px>Periode</th>
                                        <?php foreach ( $data->data->rollrate_normal->header_bucket as $h ) : ?>
                                        <th class="min-tablet" width=100px><?php echo $h." hari"; ?></th>
                                        <?php endforeach ?>
                                    </tr>
                                </thead>
                                <tbody class="kt-datatable__body">
                                    <?php $t = 1; foreach ( $data->data->rollrate_normal->list_bucket as $l ) : ?>
                                    <tr>
                                        <td><?php echo $t++; ?></td>
                                        <td><?php echo $l->periode; ?></td>
                                        <?php foreach ( $l->nilai as $n ) : ?>
                                        <td><?php if ( $n == 'Infinity') {
                                            $v = $n;
                                        } else {
                                            $v = $n *100;
                                            $v = round($v, 2);
                                        }
                                        echo $v." %";?></td>
                                        <?php endforeach; ?>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Komentar</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    </button>
                                </div>
                                <div class="kt-portlet">
                                    <div class="kt-portlet__body">
                                        <div class="kt-scroll kt-scroll--pull" style="overflow:scroll; height:350px;">
                                            <div class="kt-widget3">
                                                <?php if ($data->data->list_komentar == []) { ?>
                                                <div class="kt-widget3__item">
                                                    <h4 class="center">Tidak ada Komentar</h1>
                                                </div>
                                                <?php } else { ?>
                                                <?php foreach ($data->data->list_komentar as $key => $kme) { 
                                                if ($kme->flag != 3) {
                                                    continue;
                                                }
                                                ?>
                                                <div class="kt-widget3__item">
                                                    <div class="kt-widget3__header">
                                                        <div class="kt-widget3__user-img">
                                                            <?php if ( $kme->user->path_profile == "" ) { ?>
                                                            <img class="kt-widget3__img"
                                                                src="<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>"
                                                                alt="image">
                                                            <?php } else { ?>
                                                            <?php $src =  $this->config->item('api_host') .'/view_file'. '/' .$kme->user->path_profile ;?>
                                                            <img class="kt-widget3__img" src="<?php echo $src; ?>"
                                                                alt="image" width="40px" height="40px">
                                                            <?php } ?>
                                                        </div>
                                                        <div class="kt-widget3__info">
                                                            <a href="#" class="kt-widget3__username">
                                                                <?php echo $kme->user->nama; ?>
                                                            </a><br>
                                                            <span class="kt-widget3__time">
                                                                <?php
                                                            if ($kme->updated_date != null) {
                                                                echo get_timeago(strtotime($kme->updated_date)); 
                                                            } else {
                                                                echo get_timeago(strtotime($kme->created_date)); 
                                                            }
                                                            ?>
                                                            </span>
                                                        </div>
                                                        <span class="kt-widget3__status kt-font-info">
                                                            <?php if ($this->session->userdata("ids") == $kme->created_by) { ?>
                                                            <span
                                                                id="btn_editkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                class="kt-nav__link kt-font-info" href="#"
                                                                style="cursor:pointer"
                                                                onClick="return EditKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', '<?php echo $kme->komentar; ?>','<?php echo "3"; ?>')">Ubah
                                                            </span>
                                                            &nbsp
                                                            <span
                                                                id="btn_batkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                href="#" class="kt-nav__link kt-font-info" onClick="return batalKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', 
                                                            '<?php echo $kme->komentar; ?>', 3
                                                        )" style="display:none; cursor:pointer;">Batal
                                                            </span>
                                                            &nbsp
                                                            <a href="<?php echo site_url("Model/deletekomen_draft_bucket?idk=$kme->id_komentar_bucket&typ=1&idb=$idb"); ?>"
                                                                class="kt-nav__link kt-font-info"
                                                                onclick='return del_confirm()'>Hapus
                                                            </a>
                                                            <?php } ?>
                                                        </span>
                                                    </div>
                                                    <div class="kt-widget3__body">
                                                        <p class="kt-widget3__text">
                                                            <?php echo $kme->komentar; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__foot">
                                        <form method="post" enctype="multipart/form-data"
                                            action="<?php echo site_url("model/addkomen_draft_bucket?idb=$idb"); ?>">
                                            <div class="form-row align-items-center">
                                                <div class="col-auto">
                                                    <input value="1" name="type_coment" hidden></input>
                                                    <input value="3" name="flag_coment" hidden></input>
                                                    <input value="0" id="komen_id13" name="id_coment" hidden></input>
                                                    <input value="<?php echo $idb; ?>" name="id_bucket" hidden></input>
                                                    <textarea id="komen_content3" required
                                                        style="height: 50px; width: 400px;" placeholder="Type here..."
                                                        name="komentar"></textarea>
                                                </div>
                                                <div class="col-auto">
                                                    <button type="submit"
                                                        class="btn btn-primary teratur-button">Kirim</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_average" role="tabpanel">
                            <table class="table table-striped table-bordered table-hover" id="draf_average"
                                width="100%">
                                <thead>
                                    <tr>
                                        <th class="all" width=20px>#</th>
                                        <th class="all" width=150px>Periode</th>
                                        <?php foreach ( $data->data->average_rollrate_lossrate->header_bucket as $h ) : ?>
                                        <th class="min-tablet" width=100px><?php echo $h." hari"; ?></th>
                                        <?php endforeach ?>
                                    </tr>
                                </thead>
                                <tbody class="kt-datatable__body">
                                    <?php $t = 1; foreach ( $data->data->average_rollrate_lossrate->list_bucket as $l ) : ?>
                                    <tr>
                                        <td><?php echo $t++; ?></td>
                                        <td><?php echo $l->periode; ?></td>
                                        <?php foreach ( $l->nilai as $n ) : ?>
                                        <td><?php if ( $n == 'Infinity') {
                                            $v = $n;
                                        } else {
                                            $v = $n *100;
                                            $v = round($v, 2);
                                        }
                                        echo $v." %";?></td>
                                        <?php endforeach; ?>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Komentar</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    </button>
                                </div>
                                <div class="kt-portlet">
                                    <div class="kt-portlet__body">
                                        <div class="kt-scroll kt-scroll--pull" style="overflow:scroll; height:350px;">
                                            <div class="kt-widget3">
                                                <?php if ($data->data->list_komentar == []) { ?>
                                                <div class="kt-widget3__item">
                                                    <h4 class="center">Tidak ada Komentar</h1>
                                                </div>
                                                <?php } else { ?>
                                                <?php foreach ($data->data->list_komentar as $key => $kme) { 
                                                if ($kme->flag != 4) {
                                                    continue;
                                                }
                                                ?>
                                                <div class="kt-widget3__item">
                                                    <div class="kt-widget3__header">
                                                        <div class="kt-widget3__user-img">
                                                            <?php if ( $kme->user->path_profile == "" ) { ?>
                                                            <img class="kt-widget3__img"
                                                                src="<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>"
                                                                alt="image">
                                                            <?php } else { ?>
                                                            <?php $src =  $this->config->item('api_host') .'/view_file'. '/' .$kme->user->path_profile ;?>
                                                            <img class="kt-widget3__img" src="<?php echo $src; ?>"
                                                                alt="image" width="40px" height="40px">
                                                            <?php } ?>
                                                        </div>
                                                        <div class="kt-widget3__info">
                                                            <a href="#" class="kt-widget3__username">
                                                                <?php echo $kme->user->nama; ?>
                                                            </a><br>
                                                            <span class="kt-widget3__time">
                                                                <?php
                                                            if ($kme->updated_date != null) {
                                                                echo get_timeago(strtotime($kme->updated_date)); 
                                                            } else {
                                                                echo get_timeago(strtotime($kme->created_date)); 
                                                            }
                                                            ?>
                                                            </span>
                                                        </div>
                                                        <span class="kt-widget3__status kt-font-info">
                                                            <?php if ($this->session->userdata("ids") == $kme->created_by) { ?>
                                                            <span
                                                                id="btn_editkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                class="kt-nav__link kt-font-info" href="#"
                                                                style="cursor:pointer"
                                                                onClick="return EditKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', '<?php echo $kme->komentar; ?>','<?php echo "4"; ?>')">Ubah
                                                            </span>
                                                            &nbsp
                                                            <span
                                                                id="btn_batkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                href="#" class="kt-nav__link kt-font-info" onClick="return batalKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', 
                                                            '<?php echo $kme->komentar; ?>', 4
                                                        )" style="display:none; cursor:pointer;">Batal
                                                            </span>
                                                            &nbsp
                                                            <a href="<?php echo site_url("Model/deletekomen_draft_bucket?idk=$kme->id_komentar_bucket&typ=1&idb=$idb"); ?>"
                                                                class="kt-nav__link kt-font-info"
                                                                onclick='return del_confirm()'>Hapus
                                                            </a>
                                                            <?php } ?>
                                                        </span>
                                                    </div>
                                                    <div class="kt-widget3__body">
                                                        <p class="kt-widget3__text">
                                                            <?php echo $kme->komentar; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__foot">
                                        <form method="post" enctype="multipart/form-data"
                                            action="<?php echo site_url("model/addkomen_draft_bucket?idb=$idb"); ?>">
                                            <div class="form-row align-items-center">
                                                <div class="col-auto">
                                                    <input value="1" name="type_coment" hidden></input>
                                                    <input value="4" name="flag_coment" hidden></input>
                                                    <input value="0" id="komen_id4" name="id_coment" hidden></input>
                                                    <input value="<?php echo $idb; ?>" name="id_bucket" hidden></input>
                                                    <textarea id="komen_content4" required
                                                        style="height: 50px; width: 400px;" placeholder="Type here..."
                                                        name="komentar"></textarea>
                                                </div>
                                                <div class="col-auto">
                                                    <button type="submit"
                                                        class="btn btn-primary teratur-button">Kirim</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_rollrate_na" role="tabpanel">
                            <table class="table table-striped table-bordered table-hover" id="draf_rollrate_na"
                                width="100%">
                                <thead>
                                    <tr>
                                        <th class="all" width=20px>#</th>
                                        <th class="all" width=100px>Periode</th>
                                        <?php foreach ( $data->data->rollrate_normalisasi_average->header_bucket as $h ) : ?>
                                        <th class="min-tablet" width=100px><?php echo $h." hari"; ?></th>
                                        <?php endforeach ?>
                                    </tr>
                                </thead>
                                <tbody class="kt-datatable__body">
                                    <?php $t = 1; foreach ( $data->data->rollrate_normalisasi_average->list_bucket as $l ) : ?>
                                    <tr>
                                        <td><?php echo $t++; ?></td>
                                        <td><?php echo $l->periode; ?></td>
                                        <?php foreach ( $l->nilai as $n ) : ?>
                                        <td><?php if ( $n == 'Infinity') {
                                            $v = $n;
                                        } else {
                                            $v = $n *100;
                                            $v = round($v, 2);
                                        }
                                        echo $v." %";?></td>
                                        <?php endforeach; ?>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Komentar</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    </button>
                                </div>
                                <div class="kt-portlet">
                                    <div class="kt-portlet__body">
                                        <div class="kt-scroll kt-scroll--pull" style="overflow:scroll; height:350px;">
                                            <div class="kt-widget3">
                                                <?php if ($data->data->list_komentar == []) { ?>
                                                <div class="kt-widget3__item">
                                                    <h4 class="center">Tidak ada Komentar</h1>
                                                </div>
                                                <?php } else { ?>
                                                <?php foreach ($data->data->list_komentar as $key => $kme) { 
                                                if ($kme->flag != 5) {
                                                    continue;
                                                }
                                                ?>
                                                <div class="kt-widget3__item">
                                                    <div class="kt-widget3__header">
                                                        <div class="kt-widget3__user-img">
                                                            <?php if ( $kme->user->path_profile == "" ) { ?>
                                                            <img class="kt-widget3__img"
                                                                src="<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>"
                                                                alt="image">
                                                            <?php } else { ?>
                                                            <?php $src =  $this->config->item('api_host') .'/view_file'. '/' .$kme->user->path_profile ;?>
                                                            <img class="kt-widget3__img" src="<?php echo $src; ?>"
                                                                alt="image" width="40px" height="40px">
                                                            <?php } ?>
                                                        </div>
                                                        <div class="kt-widget3__info">
                                                            <a href="#" class="kt-widget3__username">
                                                                <?php echo $kme->user->nama; ?>
                                                            </a><br>
                                                            <span class="kt-widget3__time">
                                                                <?php
                                                            if ($kme->updated_date != null) {
                                                                echo get_timeago(strtotime($kme->updated_date)); 
                                                            } else {
                                                                echo get_timeago(strtotime($kme->created_date)); 
                                                            }
                                                            ?>
                                                            </span>
                                                        </div>
                                                        <span class="kt-widget3__status kt-font-info">
                                                            <?php if ($this->session->userdata("ids") == $kme->created_by) { ?>
                                                            <span
                                                                id="btn_editkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                class="kt-nav__link kt-font-info" href="#"
                                                                style="cursor:pointer"
                                                                onClick="return EditKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', '<?php echo $kme->komentar; ?>','<?php echo "5"; ?>')">Ubah
                                                            </span>
                                                            &nbsp
                                                            <span
                                                                id="btn_batkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                href="#" class="kt-nav__link kt-font-info" onClick="return batalKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', 
                                                            '<?php echo $kme->komentar; ?>', 5
                                                        )" style="display:none; cursor:pointer;">Batal
                                                            </span>
                                                            &nbsp
                                                            <a href="<?php echo site_url("Model/deletekomen_draft_bucket?idk=$kme->id_komentar_bucket&typ=1&idb=$idb"); ?>"
                                                                class="kt-nav__link kt-font-info"
                                                                onclick='return del_confirm()'>Hapus
                                                            </a>
                                                            <?php } ?>
                                                        </span>
                                                    </div>
                                                    <div class="kt-widget3__body">
                                                        <p class="kt-widget3__text">
                                                            <?php echo $kme->komentar; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__foot">
                                        <form method="post" enctype="multipart/form-data"
                                            action="<?php echo site_url("model/addkomen_draft_bucket?idb=$idb"); ?>">
                                            <div class="form-row align-items-center">
                                                <div class="col-auto">
                                                    <input value="1" name="type_coment" hidden></input>
                                                    <input value="5" name="flag_coment" hidden></input>
                                                    <input value="0" id="komen_id5" name="id_coment" hidden></input>
                                                    <input value="<?php echo $idb; ?>" name="id_bucket" hidden></input>
                                                    <textarea id="komen_content5" required
                                                        style="height: 50px; width: 400px;" placeholder="Type here..."
                                                        name="komentar"></textarea>
                                                </div>
                                                <div class="col-auto">
                                                    <button type="submit"
                                                        class="btn btn-primary teratur-button">Kirim</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_lossrate" role="tabpanel">
                            <table class="table table-striped table-bordered table-hover" id="draf_lossrate"
                                width="100%">
                                <thead>
                                    <tr>
                                        <th class="all" width=20px>#</th>
                                        <th class="all" width=100px>Periode</th>
                                        <?php foreach ( $data->data->lossrate->header_bucket as $h ) : ?>
                                        <th class="min-tablet" width=100px><?php echo $h." hari"; ?></th>
                                        <?php endforeach ?>
                                        <th class="all" width=100px>Simple Average</th>
                                        <th class="all" width=100px>Sum Loss Rate</th>
                                    </tr>
                                </thead>
                                <tbody class="kt-datatable__body">
                                    <?php $t = 1; foreach ( $data->data->lossrate->list_bucket as $l ) : ?>
                                    <tr>
                                        <td><?php echo $t++; ?></td>
                                        <td><?php echo $l->periode; ?></td>
                                        <?php foreach ( $l->nilai as $n ) : ?>
                                        <td><?php if ( $n == 'Infinity') {
                                            $v = $n;
                                        } else {
                                            $v = $n *100;
                                            $v = round($v, 2);
                                        }
                                        echo $v." %";?></td>
                                        <?php endforeach; ?>
                                        <td><?php  echo round($l->average, 4) * 100 . "%" ; ?></td>
                                        <td><?php  echo round($l->sum, 4) * 100 . "%" ; ?></td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Komentar</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    </button>
                                </div>
                                <div class="kt-portlet">
                                    <div class="kt-portlet__body">
                                        <div class="kt-scroll kt-scroll--pull" style="overflow:scroll; height:350px;">
                                            <div class="kt-widget3">
                                                <?php if ($data->data->list_komentar == []) { ?>
                                                <div class="kt-widget3__item">
                                                    <h4 class="center">Tidak ada Komentar</h1>
                                                </div>
                                                <?php } else { ?>
                                                <?php foreach ($data->data->list_komentar as $key => $kme) { 
                                                if ($kme->flag != 6) {
                                                    continue;
                                                }
                                                ?>
                                                <div class="kt-widget3__item">
                                                    <div class="kt-widget3__header">
                                                        <div class="kt-widget3__user-img">
                                                            <?php if ( $kme->user->path_profile == "" ) { ?>
                                                            <img class="kt-widget3__img"
                                                                src="<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>"
                                                                alt="image">
                                                            <?php } else { ?>
                                                            <?php $src =  $this->config->item('api_host') .'/view_file'. '/' .$kme->user->path_profile ;?>
                                                            <img class="kt-widget3__img" src="<?php echo $src; ?>"
                                                                alt="image" width="40px" height="40px">
                                                            <?php } ?>
                                                        </div>
                                                        <div class="kt-widget3__info">
                                                            <a href="#" class="kt-widget3__username">
                                                                <?php echo $kme->user->nama; ?>
                                                            </a><br>
                                                            <span class="kt-widget3__time">
                                                                <?php
                                                            if ($kme->updated_date != null) {
                                                                echo get_timeago(strtotime($kme->updated_date)); 
                                                            } else {
                                                                echo get_timeago(strtotime($kme->created_date)); 
                                                            }
                                                            ?>
                                                            </span>
                                                        </div>
                                                        <span class="kt-widget3__status kt-font-info">
                                                            <?php if ($this->session->userdata("ids") == $kme->created_by) { ?>
                                                            <span
                                                                id="btn_editkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                class="kt-nav__link kt-font-info" href="#"
                                                                style="cursor:pointer"
                                                                onClick="return EditKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', '<?php echo $kme->komentar; ?>','<?php echo "6"; ?>')">Ubah
                                                            </span>
                                                            &nbsp
                                                            <span
                                                                id="btn_batkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                href="#" class="kt-nav__link kt-font-info" onClick="return batalKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', 
                                                            '<?php echo $kme->komentar; ?>', 6
                                                        )" style="display:none; cursor:pointer;">Batal
                                                            </span>
                                                            &nbsp
                                                            <a href="<?php echo site_url("Model/deletekomen_draft_bucket?idk=$kme->id_komentar_bucket&typ=1&idb=$idb"); ?>"
                                                                class="kt-nav__link kt-font-info"
                                                                onclick='return del_confirm()'>Hapus
                                                            </a>
                                                            <?php } ?>
                                                        </span>
                                                    </div>
                                                    <div class="kt-widget3__body">
                                                        <p class="kt-widget3__text">
                                                            <?php echo $kme->komentar; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__foot">
                                        <form method="post" enctype="multipart/form-data"
                                            action="<?php echo site_url("model/addkomen_draft_bucket?idb=$idb"); ?>">
                                            <div class="form-row align-items-center">
                                                <div class="col-auto">
                                                    <input value="1" name="type_coment" hidden></input>
                                                    <input value="6" name="flag_coment" hidden></input>
                                                    <input value="0" id="komen_id6" name="id_coment" hidden></input>
                                                    <input value="<?php echo $idb; ?>" name="id_bucket" hidden></input>
                                                    <textarea id="komen_content6" required
                                                        style="height: 50px; width: 400px;" placeholder="Type here..."
                                                        name="komentar"></textarea>
                                                </div>
                                                <div class="col-auto">
                                                    <button type="submit"
                                                        class="btn btn-primary teratur-button">Kirim</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_moving_avg" role="tabpanel">
                            <table class="table table-striped table-bordered table-hover" id="draf_moving_avg"
                                width="100%">
                                <thead>
                                    <tr>
                                        <th class="all" width=20px>#</th>
                                        <th class="all" width=100px>Periode</th>
                                        <?php foreach ( $data->data->moving_average->header_bucket as $h ) : ?>
                                        <th class="min-tablet" width=100px><?php echo $h." hari"; ?></th>
                                        <?php endforeach ?>
                                        <th class="all" width=100px>Simple Average</th>
                                        <th class="all" width=100px>Sum Loss Rate</th>
                                    </tr>
                                </thead>
                                <tbody class="kt-datatable__body">
                                    <?php $t = 1; foreach ( $data->data->moving_average->list_bucket as $l ) : ?>
                                    <tr>
                                        <td><?php echo $t++; ?></td>
                                        <td><?php echo $l->periode; ?></td>
                                        <?php foreach ( $l->nilai as $n ) : ?>
                                        <td><?php if ( $n == 'Infinity') {
                                            $v = $n;
                                        } else {
                                            $v = $n *100;
                                            $v = round($v, 2);
                                        }
                                        echo $v." %";?></td>
                                        <?php endforeach; ?>
                                        <td><?php  echo round($l->average, 4) * 100 . "%" ; ?></td>
                                        <td><?php  echo round($l->sum, 4) * 100 . "%" ; ?></td>

                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Komentar</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    </button>
                                </div>
                                <div class="kt-portlet">
                                    <div class="kt-portlet__body">
                                        <div class="kt-scroll kt-scroll--pull" style="overflow:scroll; height:350px;">
                                            <div class="kt-widget3">
                                                <?php if ($data->data->list_komentar == []) { ?>
                                                <div class="kt-widget3__item">
                                                    <h4 class="center">Tidak ada Komentar</h1>
                                                </div>
                                                <?php } else { ?>
                                                <?php foreach ($data->data->list_komentar as $key => $kme) { 
                                                if ($kme->flag != 7) {
                                                    continue;
                                                }
                                                ?>
                                                <div class="kt-widget3__item">
                                                    <div class="kt-widget3__header">
                                                        <div class="kt-widget3__user-img">
                                                            <?php if ( $kme->user->path_profile == "" ) { ?>
                                                            <img class="kt-widget3__img"
                                                                src="<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>"
                                                                alt="image">
                                                            <?php } else { ?>
                                                            <?php $src =  $this->config->item('api_host') .'/view_file'. '/' .$kme->user->path_profile ;?>
                                                            <img class="kt-widget3__img" src="<?php echo $src; ?>"
                                                                alt="image" width="40px" height="40px">
                                                            <?php } ?>
                                                        </div>
                                                        <div class="kt-widget3__info">
                                                            <a href="#" class="kt-widget3__username">
                                                                <?php echo $kme->user->nama; ?>
                                                            </a><br>
                                                            <span class="kt-widget3__time">
                                                                <?php
                                                            if ($kme->updated_date != null) {
                                                                echo get_timeago(strtotime($kme->updated_date)); 
                                                            } else {
                                                                echo get_timeago(strtotime($kme->created_date)); 
                                                            }
                                                            ?>
                                                            </span>
                                                        </div>
                                                        <span class="kt-widget3__status kt-font-info">
                                                            <?php if ($this->session->userdata("ids") == $kme->created_by) { ?>
                                                            <span
                                                                id="btn_editkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                class="kt-nav__link kt-font-info" href="#"
                                                                style="cursor:pointer"
                                                                onClick="return EditKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', '<?php echo $kme->komentar; ?>','<?php echo "7"; ?>')">Ubah
                                                            </span>
                                                            &nbsp
                                                            <span
                                                                id="btn_batkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                href="#" class="kt-nav__link kt-font-info" onClick="return batalKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', 
                                                            '<?php echo $kme->komentar; ?>', 7
                                                        )" style="display:none; cursor:pointer;">Batal
                                                            </span>
                                                            &nbsp
                                                            <a href="<?php echo site_url("Model/deletekomen_draft_bucket?idk=$kme->id_komentar_bucket&typ=1&idb=$idb"); ?>"
                                                                class="kt-nav__link kt-font-info"
                                                                onclick='return del_confirm()'>Hapus
                                                            </a>
                                                            <?php } ?>
                                                        </span>
                                                    </div>
                                                    <div class="kt-widget3__body">
                                                        <p class="kt-widget3__text">
                                                            <?php echo $kme->komentar; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__foot">
                                        <form method="post" enctype="multipart/form-data"
                                            action="<?php echo site_url("model/addkomen_draft_bucket?idb=$idb"); ?>">
                                            <div class="form-row align-items-center">
                                                <div class="col-auto">
                                                    <input value="1" name="type_coment" hidden></input>
                                                    <input value="7" name="flag_coment" hidden></input>
                                                    <input value="0" id="komen_id7" name="id_coment" hidden></input>
                                                    <input value="<?php echo $idb; ?>" name="id_bucket" hidden></input>
                                                    <textarea id="komen_content7" required
                                                        style="height: 50px; width: 400px;" placeholder="Type here..."
                                                        name="komentar"></textarea>
                                                </div>
                                                <div class="col-auto">
                                                    <button type="submit"
                                                        class="btn btn-primary teratur-button">Kirim</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_sum_delta" role="tabpanel">
                            <table class="table table-striped table-bordered table-hover" id="draf_sum_delta"
                                width="100%">
                                <thead>
                                    <tr>
                                        <th class="all" width=20px>#</th>
                                        <th class="all" width=100px>Periode</th>
                                        <?php foreach ($data->data->sum_delta_lossrate->header_bucket as $h ) : ?>
                                        <th class="min-tablet" width=100px><?php echo $h." hari"; ?></th>
                                        <?php endforeach ?>
                                        <th class="all" width=100px>Simple Average</th>
                                        <th class="all" width=100px>Sum Loss Rate</th>
                                    </tr>
                                </thead>
                                <tbody class="kt-datatable__body">
                                    <?php $t = 1; foreach ( $data->data->sum_delta_lossrate->list_bucket as $l ) : ?>
                                    <tr>
                                        <td><?php echo $t++; ?></td>
                                        <td><?php echo $l->periode; ?></td>
                                        <?php foreach ( $l->nilai as $n ) : ?>
                                        <td><?php if ( $n == 'Infinity') {
                                            $v = $n;
                                        } else {
                                            $v = $n *100;
                                            $v = round($v, 2);
                                        }
                                        echo $v." %";?></td>
                                        <?php endforeach; ?>
                                        <td><?php  echo round($l->average, 4) * 100 . "%" ; ?></td>
                                        <td><?php  echo round($l->sum, 4) * 100 . "%" ; ?></td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Komentar</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    </button>
                                </div>
                                <div class="kt-portlet">
                                    <div class="kt-portlet__body">
                                        <div class="kt-scroll kt-scroll--pull" style="overflow:scroll; height:350px;">
                                            <div class="kt-widget3">
                                                <?php if ($data->data->list_komentar == []) { ?>
                                                <div class="kt-widget3__item">
                                                    <h4 class="center">Tidak ada Komentar</h1>
                                                </div>
                                                <?php } else { ?>
                                                <?php foreach ($data->data->list_komentar as $key => $kme) { 
                                                if ($kme->flag != 8) {
                                                    continue;
                                                }
                                                ?>
                                                <div class="kt-widget3__item">
                                                    <div class="kt-widget3__header">
                                                        <div class="kt-widget3__user-img">
                                                            <?php if ( $kme->user->path_profile == "" ) { ?>
                                                            <img class="kt-widget3__img"
                                                                src="<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>"
                                                                alt="image">
                                                            <?php } else { ?>
                                                            <?php $src =  $this->config->item('api_host') .'/view_file'. '/' .$kme->user->path_profile ;?>
                                                            <img class="kt-widget3__img" src="<?php echo $src; ?>"
                                                                alt="image" width="40px" height="40px">
                                                            <?php } ?>
                                                        </div>
                                                        <div class="kt-widget3__info">
                                                            <a href="#" class="kt-widget3__username">
                                                                <?php echo $kme->user->nama; ?>
                                                            </a><br>
                                                            <span class="kt-widget3__time">
                                                                <?php
                                                            if ($kme->updated_date != null) {
                                                                echo get_timeago(strtotime($kme->updated_date)); 
                                                            } else {
                                                                echo get_timeago(strtotime($kme->created_date)); 
                                                            }
                                                            ?>
                                                            </span>
                                                        </div>
                                                        <span class="kt-widget3__status kt-font-info">
                                                            <?php if ($this->session->userdata("ids") == $kme->created_by) { ?>
                                                            <span
                                                                id="btn_editkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                class="kt-nav__link kt-font-info" href="#"
                                                                style="cursor:pointer"
                                                                onClick="return EditKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', '<?php echo $kme->komentar; ?>','<?php echo "8"; ?>')">Ubah
                                                            </span>
                                                            &nbsp
                                                            <span
                                                                id="btn_batkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                href="#" class="kt-nav__link kt-font-info" onClick="return batalKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', 
                                                            '<?php echo $kme->komentar; ?>', 8
                                                        )" style="display:none; cursor:pointer;">Batal
                                                            </span>
                                                            &nbsp
                                                            <a href="<?php echo site_url("Model/deletekomen_draft_bucket?idk=$kme->id_komentar_bucket&typ=1&idb=$idb"); ?>"
                                                                class="kt-nav__link kt-font-info"
                                                                onclick='return del_confirm()'>Hapus
                                                            </a>
                                                            <?php } ?>
                                                        </span>
                                                    </div>
                                                    <div class="kt-widget3__body">
                                                        <p class="kt-widget3__text">
                                                            <?php echo $kme->komentar; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__foot">
                                        <form method="post" enctype="multipart/form-data"
                                            action="<?php echo site_url("model/addkomen_draft_bucket?idb=$idb"); ?>">
                                            <div class="form-row align-items-center">
                                                <div class="col-auto">
                                                    <input value="1" name="type_coment" hidden></input>
                                                    <input value="8" name="flag_coment" hidden></input>
                                                    <input value="0" id="komen_id8" name="id_coment" hidden></input>
                                                    <input value="<?php echo $idb; ?>" name="id_bucket" hidden></input>
                                                    <textarea id="komen_content8" required
                                                        style="height: 50px; width: 400px;" placeholder="Type here..."
                                                        name="komentar"></textarea>
                                                </div>
                                                <div class="col-auto">
                                                    <button type="submit"
                                                        class="btn btn-primary teratur-button">Kirim</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_odr" role="tabpanel">
                            <table class="table table-striped table-bordered table-hover" id="draf_odr" width="100%">
                                <thead>
                                    <tr>
                                        <th class="all" width=20px>#</th>
                                        <th class="all" width=100px>Periode</th>
                                        <?php foreach ( $data->data->odr->header_bucket as $h ) : ?>
                                        <th class="min-tablet" width=100px><?php echo $h." hari"; ?></th>
                                        <?php endforeach ?>
                                        <th class="all" width=100px>Jumlah ODR</th>
                                        <th class="all" width=100px>Persentase ODR</th>
                                    </tr>
                                </thead>
                                <tbody class="kt-datatable__body">
                                    <?php $t = 1; foreach ( $data->data->odr->list_bucket as $l ) : ?>
                                    <tr>
                                        <td><?php echo $t++; ?></td>
                                        <td><?php echo $l->periode; ?></td>
                                        <?php foreach ( $l->nilai as $n ) : ?>
                                        <td><?php echo "Rp. " . number_format( $n, 2, ',', '.');?></td>
                                        <?php endforeach; ?>
                                        <td><?php echo "Rp. " . number_format( $l->sum, 2, ' ,', '.');?></td>
                                        <td><?php  echo round($l->persentase_odr, 4) * 100 . "%" ; ?></td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Komentar</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    </button>
                                </div>
                                <div class="kt-portlet">
                                    <div class="kt-portlet__body">
                                        <div class="kt-scroll kt-scroll--pull" style="overflow:scroll; height:350px;">
                                            <div class="kt-widget3">
                                                <?php if ($data->data->list_komentar == []) { ?>
                                                <div class="kt-widget3__item">
                                                    <h4 class="center">Tidak ada Komentar</h1>
                                                </div>
                                                <?php } else { ?>
                                                <?php foreach ($data->data->list_komentar as $key => $kme) { 
                                                if ($kme->flag != 9) {
                                                    continue;
                                                }
                                                ?>
                                                <div class="kt-widget3__item">
                                                    <div class="kt-widget3__header">
                                                        <div class="kt-widget3__user-img">
                                                            <?php if ( $kme->user->path_profile == "" ) { ?>
                                                            <img class="kt-widget3__img"
                                                                src="<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>"
                                                                alt="image">
                                                            <?php } else { ?>
                                                            <?php $src =  $this->config->item('api_host') .'/view_file'. '/' .$kme->user->path_profile ;?>
                                                            <img class="kt-widget3__img" src="<?php echo $src; ?>"
                                                                alt="image" width="40px" height="40px">
                                                            <?php } ?>
                                                        </div>
                                                        <div class="kt-widget3__info">
                                                            <a href="#" class="kt-widget3__username">
                                                                <?php echo $kme->user->nama; ?>
                                                            </a><br>
                                                            <span class="kt-widget3__time">
                                                                <?php
                                                            if ($kme->updated_date != null) {
                                                                echo get_timeago(strtotime($kme->updated_date)); 
                                                            } else {
                                                                echo get_timeago(strtotime($kme->created_date)); 
                                                            }
                                                            ?>
                                                            </span>
                                                        </div>
                                                        <span class="kt-widget3__status kt-font-info">
                                                            <?php if ($this->session->userdata("ids") == $kme->created_by) { ?>
                                                            <span
                                                                id="btn_editkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                class="kt-nav__link kt-font-info" href="#"
                                                                style="cursor:pointer"
                                                                onClick="return EditKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', '<?php echo $kme->komentar; ?>','<?php echo "9"; ?>')">Ubah
                                                            </span>
                                                            &nbsp
                                                            <span
                                                                id="btn_batkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                href="#" class="kt-nav__link kt-font-info" onClick="return batalKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', 
                                                            '<?php echo $kme->komentar; ?>', 9
                                                        )" style="display:none; cursor:pointer;">Batal
                                                            </span>
                                                            &nbsp
                                                            <a href="<?php echo site_url("Model/deletekomen_draft_bucket?idk=$kme->id_komentar_bucket&typ=1&idb=$idb"); ?>"
                                                                class="kt-nav__link kt-font-info"
                                                                onclick='return del_confirm()'>Hapus
                                                            </a>
                                                            <?php } ?>
                                                        </span>
                                                    </div>
                                                    <div class="kt-widget3__body">
                                                        <p class="kt-widget3__text">
                                                            <?php echo $kme->komentar; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__foot">
                                        <form method="post" enctype="multipart/form-data"
                                            action="<?php echo site_url("model/addkomen_draft_bucket?idb=$idb"); ?>">
                                            <div class="form-row align-items-center">
                                                <div class="col-auto">
                                                    <input value="1" name="type_coment" hidden></input>
                                                    <input value="9" name="flag_coment" hidden></input>
                                                    <input value="0" id="komen_id9" name="id_coment" hidden></input>
                                                    <input value="<?php echo $idb; ?>" name="id_bucket" hidden></input>
                                                    <textarea id="komen_content9" required
                                                        style="height: 50px; width: 400px;" placeholder="Type here..."
                                                        name="komentar"></textarea>
                                                </div>
                                                <div class="col-auto">
                                                    <button type="submit"
                                                        class="btn btn-primary teratur-button">Kirim</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-12">
                                <button onclick="ExportDrafBucket()" type="button"
                                    class="btn btn-sm btn-danger teratur-button float-right">
                                    <i class="flaticon-download-1"></i> Export Excel
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
function get_timeago( $ptime )
{
    $estimate_time = time() - $ptime;
    if( $estimate_time < 1 )
    {
        return 'kurang dari 1 detik yang lalu';
    }
    $condition = array(
        12 * 30 * 24 * 60 * 60  =>  'tahun',
        30 * 24 * 60 * 60       =>  'bulan',
        24 * 60 * 60            =>  'hari',
        60 * 60                 =>  'jam',
        60                      =>  'menit',
        1                       =>  'detik'
    );
    foreach( $condition as $secs => $str )
    {
        $d = $estimate_time / $secs;
        if( $d >= 1 )
        {
            $r = round( $d );
            return 'sekitar ' . $r . ' ' . $str . ( $r > 1 ? '' : '' ) . ' yang lalu';
        }
    }
}
?>

<script>
// function reload_tab() {
//     alert('wwl');
//     $('#scroll1').refresh();
// }

function EditKomentar(id, content, flag) {
    // alert(id + " " + content + " " + flag);
    document.getElementById("komen_id" + flag).value = id;
    document.getElementById("komen_content" + flag).value = content;
    $("#btn_editkom" + id).hide();
    $("#btn_batkom" + id).show();
}

function batalKomentar(id, content, flag) {
    document.getElementById("komen_id" + flag).value = 0;
    document.getElementById("komen_content" + flag).value = "";
    $("#btn_editkom" + id).show();
    $("#btn_batkom" + id).hide();
}
</script>