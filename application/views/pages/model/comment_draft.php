<!-- <style>
table {
    table-layout: fixed;
}

td {
    overflow: hidden;
    text-overflow: ellipsis;
}
</style> -->

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Model Bucket Simulate</h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon-list-1"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Model Bucket Simulate</a>
                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container   kt-grid__item--fluid">
            <?php if ( $data->status == '1' || $data->status == 1 ) { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Berhasil!&nbsp;&nbsp;</strong><?php echo "Jumlah Model Bucket Simulate: ".count($data->data); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } else { ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Perhatian!&nbsp;&nbsp;</strong> <?php echo "Tidak ada draft bucket"; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } ?>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">

                    </div>
                    <?php if ( $this->session->userdata('role') == '1' || $this->session->userdata('role') == 1 || $this->session->userdata('role') == '2' || $this->session->userdata('role') == 2 ) { ?>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <a href="#" class="btn btn-brand btn-icon-sm teratur-button" data-toggle="modal"
                                    data-target="#input">
                                    <i class="flaticon-plus"></i>
                                    Buat Bucket Simulate
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>

                <div class="kt-portlet__body">
                    <div class="kt-scroll kt-scroll--pull">
                        <div class="kt-widget3">
                            <div class="kt-widget3__item">
                                <div class="kt-widget3__header">
                                    <div class="kt-widget3__user-img">
                                        <img class="kt-widget3__img"
                                            src="<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>"
                                            alt="">
                                    </div>
                                    <div class="kt-widget3__info">
                                        <a href="#" class="kt-widget3__username">
                                            Melania Trump
                                        </a><br>
                                        <span class="kt-widget3__time">
                                            2 day ago
                                        </span>
                                    </div>
                                    <span class="kt-widget3__status kt-font-info">
                                        Pending
                                    </span>
                                </div>
                                <div class="kt-widget3__body">
                                    <p class="kt-widget3__text">
                                        Lorem ipsum dolor sit amet,consectetuer edipiscing elit,sed diam nonummy
                                        nibh euismod tinciduntut laoreet doloremagna aliquam erat volutpat.
                                    </p>
                                </div>
                            </div>
                            <div class="kt-widget3__item">
                                <div class="kt-widget3__header">
                                    <div class="kt-widget3__user-img">
                                        <img class="kt-widget3__img"
                                            src="<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>"
                                            alt="">
                                    </div>
                                    <div class="kt-widget3__info">
                                        <a href="#" class="kt-widget3__username">
                                            Melania Trump
                                        </a><br>
                                        <span class="kt-widget3__time">
                                            2 day ago
                                        </span>
                                    </div>
                                    <span class="kt-widget3__status kt-font-info">
                                        Pending
                                    </span>
                                </div>
                                <div class="kt-widget3__body">
                                    <p class="kt-widget3__text">
                                        Lorem ipsum dolor sit amet,consectetuer edipiscing elit,sed diam nonummy
                                        nibh euismod tinciduntut laoreet doloremagna aliquam erat volutpat.
                                    </p>
                                </div>
                            </div>
                            <div class="kt-widget3__item">
                                <div class="kt-widget3__header">
                                    <div class="kt-widget3__user-img">
                                        <img class="kt-widget3__img"
                                            src="<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>"
                                            alt="">
                                    </div>
                                    <div class="kt-widget3__info">
                                        <a href="#" class="kt-widget3__username">
                                            Melania Trump
                                        </a><br>
                                        <span class="kt-widget3__time">
                                            2 day ago
                                        </span>
                                    </div>
                                    <span class="kt-widget3__status kt-font-info">
                                        Pending
                                    </span>
                                </div>
                                <div class="kt-widget3__body">
                                    <p class="kt-widget3__text">
                                        Lorem ipsum dolor sit amet,consectetuer edipiscing elit,sed diam nonummy
                                        nibh euismod tinciduntut laoreet doloremagna aliquam erat volutpat.
                                    </p>
                                </div>
                            </div>
                            <div class="kt-widget3__item">
                                <div class="kt-widget3__header">
                                    <div class="kt-widget3__user-img">
                                        <img class="kt-widget3__img"
                                            src="<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>"
                                            alt="">
                                    </div>
                                    <div class="kt-widget3__info">
                                        <a href="#" class="kt-widget3__username">
                                            Melania Trump
                                        </a><br>
                                        <span class="kt-widget3__time">
                                            2 day ago
                                        </span>
                                    </div>
                                    <span class="kt-widget3__status kt-font-info">
                                        Pending
                                    </span>
                                </div>
                                <div class="kt-widget3__body">
                                    <p class="kt-widget3__text">
                                        Lorem ipsum dolor sit amet,consectetuer edipiscing elit,sed diam nonummy
                                        nibh euismod tinciduntut laoreet doloremagna aliquam erat volutpat.
                                    </p>
                                </div>
                            </div>
                            <div class="kt-widget3__item">
                                <div class="kt-widget3__header">
                                    <div class="kt-widget3__user-img">
                                        <img class="kt-widget3__img"
                                            src="<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>"
                                            alt="">
                                    </div>
                                    <div class="kt-widget3__info">
                                        <a href="#" class="kt-widget3__username">
                                            Lebron King James
                                        </a><br>
                                        <span class="kt-widget3__time">
                                            1 day ago
                                        </span>
                                    </div>
                                    <span class="kt-widget3__status kt-font-brand">
                                        Open
                                    </span>
                                </div>
                                <div class="kt-widget3__body">
                                    <p class="kt-widget3__text">
                                        Lorem ipsum dolor sit amet,consectetuer edipiscing elit,sed diam nonummy
                                        nibh euismod tinciduntut laoreet doloremagna aliquam erat volutpat.Ut wisi
                                        enim ad minim veniam,quis nostrud exerci tation ullamcorper.
                                    </p>
                                </div>
                            </div>
                            <div class="kt-widget3__item">
                                <div class="kt-widget3__header">
                                    <div class="kt-widget3__user-img">
                                        <img class="kt-widget3__img"
                                            src="<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>"
                                            alt="">
                                    </div>
                                    <div class="kt-widget3__info">
                                        <a href="#" class="kt-widget3__username">
                                            Deb Gibson
                                        </a><br>
                                        <span class="kt-widget3__time">
                                            3 weeks ago
                                        </span>
                                    </div>
                                    <span class="kt-widget3__status kt-font-success">
                                        Closed
                                    </span>
                                </div>
                                <div class="kt-widget3__body">
                                    <p class="kt-widget3__text">
                                        Lorem ipsum dolor sit amet,consectetuer edipiscing elit,sed diam nonummy
                                        nibh euismod tinciduntut laoreet doloremagna aliquam erat volutpat.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__foot">
                    <div class="">
                        <div class="">
                            <textarea style="height: 50px" placeholder="Type here..."></textarea>
                        </div>
                        <div class="kt-chat__toolbar">
                            <div class="kt_chat__tools">
                                <a href="#"><i class="flaticon2-link"></i></a>
                                <a href="#"><i class="flaticon2-photograph"></i></a>
                                <a href="#"><i class="flaticon2-photo-camera"></i></a>
                            </div>
                            <div class="">
                                <button type="button"
                                    class="btn btn-brand btn-md btn-upper btn-bold kt-chat__reply">reply</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
function EditDraft(id, nama) {
    document.getElementById("edit_ids").value = id;
    document.getElementById("edit_nama").value = nama;
}
</script>