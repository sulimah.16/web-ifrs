<!-- <style>
table {
    table-layout: fixed;
}

td {
    overflow: hidden;
    text-overflow: ellipsis;
}
</style> -->

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Rekap Mev </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon-list-2"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Rekap Mev </a>

                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container   kt-grid__item--fluid">
            <?php if ( $data->status == '1' || $data->status == 1 ) { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Berhasil!&nbsp;&nbsp;</strong>
                <?php 
                if (count($data->data) > 0) {
                    echo "Jumlah Data : ".(count($data->data)-1);
                } else {
                    echo "Jumlah Data : ".(count($data->data));
                } ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } else { ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Perhatian!&nbsp;&nbsp;</strong> <?php echo "Tidak ada data"; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } ?>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">

                    </div>

                    <?php if ( $this->session->userdata('role') == '1' || $this->session->userdata('role') == 1 || $this->session->userdata('role') == '2' || $this->session->userdata('role') == 2 ) { ?>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <a href="#" class="btn btn-brand btn-icon-sm teratur-button" data-toggle="modal"
                                    data-target="#add_profile">
                                    <i class="flaticon-plus"></i>
                                    <?php 
                                    if ($data->status == "2" || count($data->data) <= 0) {
                                        echo "Upload Data";
                                    } else {
                                        echo "Upload Data Kembali";
                                    }
                                    ?>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>

                <div class="kt-portlet__body">

                    <table class="table table-responsive table-striped table-bordered table-hover" id="tbl_list_default"
                        width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Periode</th>
                                <!-- <th>GDP</th>
                                <th>LGDP</th>
                                <th>DGDP_1</th>
                                <th>DGDP_12</th>
                                <th>LGDP_1</th>
                                <th>LGDP_12</th>
                                <th>GDPGR</th>
                                <th>KURS</th>
                                <th>LKURS</th>
                                <th>DKURS</th>
                                <th>DLKURS</th>
                                <th>I7RR</th>
                                <th>DI7RR</th>
                                <th>INF</th>
                                <th>DINF</th>
                                <th>GOVSP</th>
                                <th>LGOVSP</th>
                                <th>DGOVSP</th>
                                <th>DLGOVSP</th>
                                <th>GOVSPGR</th>
                                <th>HOUSEP</th>
                                <th>LHOUSEP</th>
                                <th>DHOUSEP</th>
                                <th>DLHOUSEP</th>
                                <th>DLGDP</th>
                                <th>DGDPGR</th>
                                <th>OIL</th>
                                <th>LOIL</th>
                                <th>DOIL</th>
                                <th>DLOIL</th> -->

                                <!-- UPDATE SESUIKAN DENGAN DATA ICON -->
                                <th>GDP</th>
                                <th>LGDP</th>
                                <th>DLGDP</th>
                                <th>GDPGR</th>
                                <th>DGDPGR</th>
                                <th>INF</th>
                                <th>DINF</th>
                                <th>KURS</th>
                                <th>LKURS</th>
                                <th>DKURS</th>
                                <th>DLKURS</th>
                                <th>OIL</th>
                                <th>LOIL</th>
                                <th>DOIL</th>
                                <th>DLOIL</th>
                            </tr>
                        </thead>
                        <tbody class="kt-datatable__body">
                            <?php $i = 1; foreach( $data->data as $d ): ?>
                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php 
                                    if ($d->bulan == 0 && $d->tahun == 0) {
                                        echo "STDEV"; 
                                    } else {
                                        echo date("M Y",strtotime($d->tanggal_mev)); 
                                    }
                                ?></td>
                                <!-- <td><?php echo $d->gdp; ?></td>
                                <td><?php echo $d->lgdp; ?></td>
                                <td><?php echo $d->dgdp_1; ?></td>
                                <td><?php echo $d->dgdp_12; ?></td>
                                <td><?php echo $d->lgdp_1; ?></td>
                                <td><?php echo $d->lgdp_12; ?></td>
                                <td><?php echo $d->gdpgr; ?></td>
                                <td><?php echo $d->kurs; ?></td>
                                <td><?php echo $d->lkurs; ?></td>
                                <td><?php echo $d->dkurs; ?></td>
                                <td><?php echo $d->dlkurs; ?></td>
                                <td><?php echo $d->i7rr; ?></td>
                                <td><?php echo $d->di7rr; ?></td>
                                <td><?php echo $d->inf; ?></td>
                                <td><?php echo $d->dinf; ?></td>
                                <td><?php echo $d->govsp; ?></td>
                                <td><?php echo $d->lgovsp; ?></td>
                                <td><?php echo $d->dgovsp; ?></td>
                                <td><?php echo $d->dlgovsp; ?></td>
                                <td><?php echo $d->govspgr; ?></td>
                                <td><?php echo $d->housep; ?></td>
                                <td><?php echo $d->lhousep; ?></td>
                                <td><?php echo $d->dhousep; ?></td>
                                <td><?php echo $d->dlhousep; ?></td>
                                <td><?php echo $d->dlgdp; ?></td>
                                <td><?php echo $d->dgdpgr; ?></td>
                                <td><?php echo $d->oil; ?></td>
                                <td><?php echo $d->loil; ?></td>
                                <td><?php echo $d->doil; ?></td>
                                <td><?php echo $d->dloil; ?></td> -->

                                <!-- UPDATE SESUIKAN DENGAN DATA ICON -->
                                <td><?php echo $d->gdp; ?></td>
                                <td><?php echo $d->lgdp; ?></td>
                                <td><?php echo $d->dlgdp; ?></td>
                                <td><?php echo $d->gdpgr; ?></td>
                                <td><?php echo $d->dgdpgr; ?></td>
                                <td><?php echo $d->inf; ?></td>
                                <td><?php echo $d->dinf; ?></td>
                                <td><?php echo $d->kurs; ?></td>
                                <td><?php echo $d->lkurs; ?></td>
                                <td><?php echo $d->dkurs; ?></td>
                                <td><?php echo $d->dlkurs; ?></td>
                                <td><?php echo $d->oil; ?></td>
                                <td><?php echo $d->loil; ?></td>
                                <td><?php echo $d->doil; ?></td>
                                <td><?php echo $d->dloil; ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="add_profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Upload MEV</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('model/input_mev'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">File Upload</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="customFile"
                                                    name="file_c">
                                                <label class="custom-file-label" for="customFile">Choose
                                                    file</label>
                                            </div>
                                        </div>
                                        <span class="form-text text-muted">contoh untuk upload file MEV. <a id='url'
                                                href="<?php echo base_url(); ?>public/example/REKAV_MEV_INPUT.csv"
                                                download>klik disini</a></span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button" onclick="return act_confirm()">Upload
                        Data</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>