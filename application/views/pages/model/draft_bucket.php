<!-- <style>
table {
    table-layout: fixed;
}

td {
    overflow: hidden;
    text-overflow: ellipsis;
}
</style> -->

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Model Bucket Simulate</h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon-list-1"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Model Bucket Simulate</a>
                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container   kt-grid__item--fluid">
            <?php if ( $data->status == '1' || $data->status == 1 ) { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Berhasil!&nbsp;&nbsp;</strong><?php echo "Jumlah Model Bucket Simulate: ".count($data->data); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } else { ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Perhatian!&nbsp;&nbsp;</strong> <?php echo "Tidak ada draft bucket"; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } ?>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">

                    </div>
                    <?php if ( $this->session->userdata('role') == '1' || $this->session->userdata('role') == 1 || $this->session->userdata('role') == '2' || $this->session->userdata('role') == 2 ) { ?>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <a href="#" class="btn btn-brand btn-icon-sm teratur-button" data-toggle="modal"
                                    data-target="#input">
                                    <i class="flaticon-plus"></i>
                                    Buat Bucket Simulate
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <!-- <?php echo json_encode($data->data); ?> -->
                <div class="kt-portlet__body">
                    <table class="table table-striped table-bordered table-hover" id="tbl_list_default" width="100%">
                        <thead>
                            <tr>
                                <th class="all" width=10px>#</th>
                                <th class="min-tablet">Kode</th>
                                <th class="min-tablet">Nama Model</th>
                                <th class="min-tablet">Segmentasi</th>
                                <th class="min-tablet">Dibuat oleh</th>
                                <th class="min-tablet">Tanggal Mulai</th>
                                <th class="min-tablet">Tanggal Akhir</th>
                                <th class="min-tablet">Tanggal Buat</th>
                                <th class="off">Normalisasi Rollrate</th>
                                <th class="off">Moving Average</th>
                                <th class="min-tablet">Action</th>
                            </tr>
                        </thead>
                        <tbody class="kt-datatable__body">
                            <?php $i = 1; foreach( $data->data as $d ): ?>
                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $d->kode_draft_bucket; ?></td>
                                <td><?php echo $d->nama_draft_bucket; ?></td>
                                <td><?php echo $d->segment->nama_segment; ?></td>
                                <td><?php echo $d->dibuat_oleh->nama; ?></td>
                                <td><?php echo date("j F Y", strtotime("$d->start_date")); ?></td>
                                <td><?php echo date("j F Y", strtotime("$d->end_date"));  ?></td>
                                <td><?php echo date("j F Y", strtotime("$d->dibuat_tgl"));  ?></td>
                                <td><?php echo $d->flag_normalisasi; ?></td>
                                <td><?php echo $d->moving_average. " Baris"; ?></td>
                                <td>
                                    <?php  $irb = base64_encode($d->id_draft_bucket); ?>
                                    <div class="dropdown">
                                        <a href="javascript;" class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                            data-toggle="dropdown">
                                            <i class="la la-cog"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-sm">
                                            <ul class="kt-nav">
                                                <li class="kt-nav__item">
                                                    <a href="<?php echo site_url("model/view_draft_bucket?idb=".$irb); ?>"
                                                        class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon2-search"></i>
                                                        <span class="kt-nav__link-text">Lihat Bucket Simulate</span>
                                                    </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <a href="<?php echo site_url("#"); ?>" data-toggle="modal"
                                                        data-target="#view_comment<?php echo $d->id_draft_bucket; ?>"
                                                        class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon-comment"></i>
                                                        <span class="kt-nav__link-text">Lihat Komentar</span>
                                                        <?php if ($d->list_komentar != []) { ?>
                                                        <!-- <span
                                                            class="kt-badge kt-badge--success"><?php echo count($d->list_komentar); ?></span> -->
                                                        <?php } ?>
                                                    </a>
                                                </li>
                                                <?php if ( $this->session->userdata('role') == '1' || $this->session->userdata('role') == 1 || $this->session->userdata('role') == '2' || $this->session->userdata('role') == 2 ) { ?>
                                                <li class="kt-nav__item">
                                                    <a href="<?php echo site_url("#"); ?>" data-toggle="modal"
                                                        data-target="#edit_profile" onClick="return EditDraft(
                                                            '<?php echo  $d->id_draft_bucket; ?>', 
                                                            '<?php echo  $d->nama_draft_bucket; ?>', 
                                                        )" class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon-edit"></i>
                                                        <span class="kt-nav__link-text">Ganti Nama</span>
                                                    </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <a onclick='return del_confirm()'
                                                        href="<?php echo site_url("model/del_draft_bucket/$d->id_draft_bucket"); ?>"
                                                        class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                        <span class="kt-nav__link-text">Hapus</span>
                                                    </a>
                                                </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="input" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Buat Model Bucket Simulate</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('model/inputBucket'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Mode Normalisasi RollRate</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <select name="default" id="default" class="form-control" required>
                                            <option value="<?php echo '1'; ?>"><?php echo "Default"; ?></option>
                                            <option value="<?php echo '2'; ?>"><?php echo "Average"; ?></option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Draf</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="draft" id="pick_draft" class="form-control" required>
                                            <?php foreach( $draft->data as $dr ):?>
                                            <option value="<?php echo $dr->id_draft . '|' . $dr->flag_draft; ?>">
                                                <?php echo $dr->nama_draft; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nama Bucket</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="nama" name="nama" required>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Moving berapa Titik</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <select name="titik" id="titik" class="form-control" required>
                                            <?php for ( $xl = 1; $xl <= 5; $xl ++  ) { ?>
                                            <option value="<?php echo $xl; ?>"><?php echo $xl; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Segmentasi</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="segment" id="segment" class="form-control" required>
                                            <?php foreach( $segment->data as $s ):?>
                                            <option value="<?php echo $s->id_segment; ?>">
                                                <?php echo $s->nama_segment." (".$s->kode_segment.")"; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div id='f1' class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Jumlah hari bucket pertama
                                        (hari)</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <!-- <select name="first" id="first" class="form-control">
                                            <?php for ( $x = 2; $x <= 20; $x += 2 ) { ?>
                                            <option value="<?php echo $x; ?>"><?php echo $x; ?></option>
                                            <?php } ?>
                                        </select> -->
                                        <input class="form-control" type="number" id="first" name="first" min="0"
                                            max="200">
                                    </div>
                                </div>
                                <div id='f2' class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Jarak hari antar bucket
                                        (hari)</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <!-- <select name="interval" id="interval" class="form-control">
                                            <?php for ( $x = 1; $x <= 100; $x ++ ) { ?>
                                            <option value="<?php echo $x; ?>"><?php echo $x; ?></option>
                                            <?php } ?>
                                        </select> -->
                                        <input class="form-control" type="number" id="interval" name="interval" min="1"
                                            max="100">
                                    </div>
                                </div>
                                <div id='f3' class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Jumlah Bucket</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <!-- <select name="bucket" id="bucket" class="form-control">
                                            <?php for ( $x = 5; $x <= 20; $x += 5 ) { ?>
                                            <option value="<?php echo $x; ?>"><?php echo $x; ?></option>
                                            <?php } ?>
                                        </select> -->
                                        <input class="form-control" type="number" id="bucket" name="bucket" min="1"
                                            max="50">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label"></label>
                                    <div class="col-lg-9 col-xl-4">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="inlineRadioOptions"
                                                id="inlineRadio1" value="1" checked onchange="getValue(this)">
                                            <label class="form-check-label" for="inlineRadio1">Semua</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="inlineRadioOptions"
                                                id="inlineRadio2" value="2" onchange="getValue(this)">
                                            <label class="form-check-label" for="inlineRadio2">Periode</label>
                                        </div>
                                    </div>

                                </div>

                                <div id="prdTgl">
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Tanggal Awal</label>
                                        <div class="col-lg-9 col-xl-4">
                                            <input class="form-control" type="text" id="dateStart" name="start">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Tanggal Akhir</label>
                                        <div class="col-lg-9 col-xl-4">
                                            <input class="form-control" type="text" id="dateStart2" name="end">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button" onclick="return act_confirm()">Prosess
                        Data</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>

<div class="modal fade" id="edit_profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ganti Nama Draf</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('model/update_draft_bucket'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">

                                <input class="form-control" type="hidden" id="edit_ids" name="ids" required>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nama Draf</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="edit_nama" name="nama" required>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button" onclick="return act_confirm()">Prosess
                        Data</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>

<?php $i = 1; foreach( $data->data as $d ): ?>
<div class="modal fade" id="view_comment<?php echo $d->id_draft_bucket; ?>" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Komentar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__body">
                    <div class="kt-scroll kt-scroll--pull" style="overflow:scroll; height:350px;">
                        <div class="kt-widget3">
                            <?php if ($d->list_komentar == []) { ?>
                            <div class="kt-widget3__item">
                                <h4 class="center">Tidak ada Komentar</h1>
                            </div>
                            <?php } else { ?>
                            <?php foreach ($d->list_komentar as $key => $kme) { 
                            if ($kme->flag != 0) {
                                continue;
                            }
                            ?>
                            <div class="kt-widget3__item">
                                <div class="kt-widget3__header">
                                    <div class="kt-widget3__user-img">
                                        <?php if ( $kme->user->path_profile == "" ) { ?>
                                        <img class="kt-widget3__img"
                                            src="<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>"
                                            alt="image">
                                        <?php } else { ?>
                                        <?php $src =  $this->config->item('api_host') .'/view_file'. '/' .$kme->user->path_profile ;?>
                                        <img class="kt-widget3__img" src="<?php echo $src; ?>" alt="image" width="40px"
                                            height="40px">
                                        <?php } ?>
                                    </div>
                                    <div class="kt-widget3__info">
                                        <a href="#" class="kt-widget3__username">
                                            <?php echo $kme->user->nama; ?>
                                        </a><br>
                                        <span class="kt-widget3__time">
                                            <?php
                                            if ($kme->updated_date != null) {
                                                echo get_timeago(strtotime($kme->updated_date)); 
                                            } else {
                                                echo get_timeago(strtotime($kme->created_date)); 
                                            }
                                            ?>
                                        </span>
                                    </div>
                                    <span class="kt-widget3__status kt-font-info">
                                        <?php if ($this->session->userdata("ids") == $kme->created_by) { ?>
                                        <a id="btn_editkom<?php echo  $kme->id_komentar_bucket; ?>" href="#"
                                            class="kt-nav__link kt-font-info" onClick="return EditKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', 
                                                            '<?php echo $kme->komentar; ?>',
                                                            '<?php echo $d->id_draft_bucket; ?>' 
                                                        )">Ubah
                                        </a>
                                        &nbsp
                                        <a id="btn_batkom<?php echo  $kme->id_komentar_bucket; ?>" href="#"
                                            class="kt-nav__link kt-font-info" onClick="return batalKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', 
                                                            '<?php echo $kme->komentar; ?>',
                                                            '<?php echo $d->id_draft_bucket; ?>'
                                                        )" style="display:none">Batal
                                        </a>
                                        &nbsp
                                        <a href="<?php echo site_url("Model/deletekomen_draft_bucket?idk=$kme->id_komentar_bucket&typ=1"); ?>"
                                            class="kt-nav__link kt-font-info" onclick='return del_confirm()'>Hapus
                                        </a>
                                        <?php } ?>
                                    </span>
                                </div>
                                <div class="kt-widget3__body">
                                    <p class="kt-widget3__text">
                                        <?php echo $kme->komentar; ?>
                                    </p>
                                </div>
                            </div>
                            <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__foot">
                    <form method="post" enctype="multipart/form-data"
                        action="<?php echo site_url('model/addkomen_draft_bucket'); ?>">
                        <div class="form-row align-items-center">
                            <div class="col-auto">
                                <input value="1" name="type_coment" hidden></input>
                                <input value="0" name="flag_coment" hidden></input>
                                <input value="0" id="komen_id<?php echo $d->id_draft_bucket; ?>" name="id_coment"
                                    hidden></input>
                                <input value="<?php echo $d->id_draft_bucket; ?>" name="id_bucket" hidden></input>
                                <textarea id="komen_content<?php echo $d->id_draft_bucket; ?>" required
                                    style="height: 50px; width: 400px;" placeholder="Type here..."
                                    name="komentar"></textarea>
                            </div>
                            <div class="col-auto">
                                <button type="submit" class="btn btn-primary teratur-button">Kirim</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>

<?php
function get_timeago( $ptime )
{
    $estimate_time = time() - $ptime;
    if( $estimate_time < 1 )
    {
        return 'kurang dari 1 detik yang lalu';
    }
    $condition = array(
        12 * 30 * 24 * 60 * 60  =>  'tahun',
        30 * 24 * 60 * 60       =>  'bulan',
        24 * 60 * 60            =>  'hari',
        60 * 60                 =>  'jam',
        60                      =>  'menit',
        1                       =>  'detik'
    );
    foreach( $condition as $secs => $str )
    {
        $d = $estimate_time / $secs;
        if( $d >= 1 )
        {
            $r = round( $d );
            return 'sekitar ' . $r . ' ' . $str . ( $r > 1 ? '' : '' ) . ' yang lalu';
        }
    }
}
?>

<script type="text/javascript">
function EditDraft(id, nama) {
    document.getElementById("edit_ids").value = id;
    document.getElementById("edit_nama").value = nama;
}

function EditKomentar(id, content, id_draft_bucket) {
    document.getElementById("komen_id" + id_draft_bucket).value = id;
    document.getElementById("komen_content" + id_draft_bucket).value = content;
    $("#btn_editkom" + id).hide();
    $("#btn_batkom" + id).show();
}

function batalKomentar(id, content, id_draft_bucket) {
    document.getElementById("komen_id" + id_draft_bucket).value = 0;
    document.getElementById("komen_content" + id_draft_bucket).value = "";
    $("#btn_editkom" + id).show();
    $("#btn_batkom" + id).hide();
}
</script>