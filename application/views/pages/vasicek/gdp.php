<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Daftar GDP </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon-graphic-2"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Daftar GDP </a>

                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container   kt-grid__item--fluid">
            <?php if ( $data->status == '1' || $data->status == 1 ) { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Berhasil!&nbsp;&nbsp;</strong><?php echo $data->message; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } else { ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Perhatian!&nbsp;&nbsp;</strong> <?php echo $data->message; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } ?>
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                <strong>Info!&nbsp;&nbsp;</strong><?php echo "Tekan button tambah data untuk melakukan penambahan data"; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">

                    </div>

                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <?php if ( $this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' ) { ?>
                                <a href="#" class="btn btn-brand btn-icon-sm teratur-button" data-toggle="modal"
                                    data-target="#add_gdp">
                                    <i class="flaticon-plus"></i>
                                    Tambah Data
                                </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">
                    <table class="table table-striped table-bordered table-hover" id="tbl_list_default2" width="100%">
                        <thead>
                            <tr>
                                <th class="all" width=10px>#</th>
                                <th class="all" width=100px>Periode</th>
                                <!-- <th class="all" width=200px>Bulan</th>
                                <th class="all" width=200px>Tahun</th> -->
                                <th class="all" width=200px>Nilai GDP</th>
                                <th class="all" width=200px>Keterangan</th>
                                <?php if ( $this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' ) { ?>
                                <th class="all" width=200px>Action</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody class="kt-datatable__body">
                            <?php $i = 1; foreach( $data->data as $d ): ?>
                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo date("M Y",strtotime($d->periode)) ?></td>
                                <!-- <td><?php echo $d->bulan ?></td>
                                <td><?php echo $d->tahun ?></td> -->
                                <td><?php echo $d->value_gdp ?></td>
                                <td><?php if (  $d->flg_projection == '0') { echo "Aktual"; } else { echo "Proyeksi"; } ?>
                                </td>
                                <?php if ( $this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' ) { ?>
                                <td>
                                    <div class="dropdown">
                                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                            data-toggle="dropdown">
                                            <i class="la la-cog"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-sm">
                                            <ul class="kt-nav">
                                                <li class="kt-nav__item">
                                                    <a href="<?php echo site_url("#"); ?>" data-toggle="modal"
                                                        data-target="#edit_gdp" onClick="return EditGDP(
                                                            '<?php echo  $d->id_vas_gdp; ?>',  
                                                            '<?php echo  $d->bulan; ?>',  
                                                            '<?php echo  $d->tahun; ?>',  
                                                            '<?php echo  $d->value_gdp; ?>',  
                                                            '<?php echo  $d->flg_projection; ?>',  
                                                            )" class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon-edit"></i>
                                                        <span class="kt-nav__link-text">Ubah Data</span>
                                                    </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <a href="<?php echo site_url('Vasicek/deleteGDP/'.$d->id_vas_gdp); ?>"
                                                        onclick='return del_confirm()' class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon-delete"></i>
                                                        <span class="kt-nav__link-text">Hapus Data</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                                <?php } ?>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add_gdp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('vasicek/createGDP '); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tipe Input Data</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <select name="tipe" id="tipeInput" class="form-control" required>
                                            <option value="<?php echo '0'; ?>"><?php echo 'Input Data Form'; ?></option>
                                            <option value="<?php echo '1'; ?>"><?php echo 'Upload File'; ?>
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div id="formGDP">
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Bulan</label>
                                        <div class="col-lg-9 col-xl-8">
                                            <input class="form-control" type="text" id="tambahBulan" name="bulan">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Tahun</label>
                                        <div class="col-lg-9 col-xl-8">
                                            <input class="form-control" type="text" id="tambahTahun" name="tahun">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Nilai GDP</label>
                                        <div class="col-lg-9 col-xl-8">
                                            <input class="form-control" type="text" id="tambahGDP" name="gdp"
                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Keterangan</label>
                                        <div class="col-lg-9 col-xl-4">

                                            <select name="flag" id="createFlag" class="form-control">
                                                <option value="0">Aktual</option>
                                                <option value="1">Proyeksi</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div id="uploadGDP">
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">File Upload</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="customFile"
                                                        name="file">
                                                    <label class="custom-file-label" for="customFile">Choose
                                                        file</label>
                                                </div>
                                            </div>
                                            <span class="form-text text-muted">contoh untuk upload file GDP. <a id='url'
                                                    href="<?php echo base_url(); ?>public/example/UPLOAD_GDP.csv"
                                                    download>klik disini </a></span>
                                            <small>Flag 0 artinya Aktual , Flag 1 artinya Proyeksi</small>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button" onclick="return act_confirm()">Proses
                        Data</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>

<div class="modal fade" id="edit_gdp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ubah Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('vasicek/editGDP '); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">

                                <input class="form-control" type="hidden" id="ubahId" name="id" required>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Bulan</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="ubahBulan" name="bulan" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tahun</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="ubahTahun" name="tahun" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Angka GDP</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="ubahGDP" name="gdp"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Keterangan</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="flag" id="ubahFlag" class="form-control" required>
                                            <option value="0">Aktual</option>
                                            <option value="1">Proyeksi</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button" onclick="return act_confirm()">Proses
                        Data</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>



<script type="text/javascript">
function EditGDP(id, bulan, tahun, gdp, flag) {
    document.getElementById("ubahId").value = id;
    document.getElementById("ubahBulan").value = bulan;
    document.getElementById("ubahTahun").value = tahun;
    document.getElementById("ubahGDP").value = gdp;
    document.getElementById("ubahFlag").value = flag;
}
</script>