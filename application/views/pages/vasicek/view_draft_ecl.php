<style>
table {
    table-layout: fixed;
    display: block;
    overflow-x: auto;
    white-space: nowrap;
}

td {
    overflow: hidden;
    text-overflow: ellipsis;
}
</style>

<!-- <?php echo json_encode($data); ?> -->

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        View Draft ECL</h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon-list-1"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Vasicek Draf Aset No. <?php echo $data->data->result_draft->aset_draft->kode_draft;?></a>
                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">

                    </div>

                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <!-- <a href="#" class="btn btn-brand btn-icon-sm teratur-button" data-toggle="modal"
                                    data-target="#add_profile">
                                    <i class="flaticon-plus"></i>
                                    Ubah Data
                                </a> -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">
                    <div class="row">
                        <div class="col-lg-6"><br />
                            <div class="column" style="float: left;width: 40%;">
                                <p><b>Kode Draft Aset</b></p>
                                <p><b>Nama Draft Aset</b></p>
                                <p><b>Interval</b></p>
                                <p><b>Periode Mulai</b></p>
                                <p><b>Periode Selesai</b></p>
                                <p><b>Periode ECL</b></p>
                            </div>
                            <div class="column" style="float: left;width: 60%;">
                                <p>: <?php echo $data->data->result_draft->aset_draft->kode_draft; ?></p>
                                <p>: <?php echo $data->data->result_draft->aset_draft->nama_draft; ?></p>
                                <p>: <?php echo $data->data->result_draft->interval_bucket; ?></p>
                                <p>: <?php echo $data->data->result_draft->periode_mulai; ?></p>
                                <p>: <?php echo $data->data->result_draft->periode_selesai; ?></p>
                                <p>: <?php echo $data->data->result_draft->periode_ecl; ?></p>
                            </div>
                        </div>
                        <div class="col-lg-6"><br />
                            <div class="column" style="float: left;width: 30%;">
                                <p><b>Persen Normal</b></p>
                                <p><b>Persen DownTurn</b></p>
                                <p><b>Persen UpTurn</b></p>
                                <p><b>Dibuat oleh</b></p>
                                <p><b>Tanggal dibuat</b></p>
                            </div>
                            <div class="column" style="float: left;width: 70%;">
                                <p>: <?php echo ($data->data->result_draft->persen_normal * 100)." %"; ?></p>
                                <p>: <?php echo ($data->data->result_draft->persen_downtum * 100)." %"; ?></p>
                                <p>: <?php echo ($data->data->result_draft->persen_uptum * 100)." %"; ?></p>
                                <p>: <?php echo $data->data->result_draft->name_created->username; ?></p>
                                <p>: <?php echo date("d M Y H:i:s",strtotime($data->data->result_draft->created_date)); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <ul class="nav nav-pills nav-fill" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tab_araging">Penyesuaian
                                Makroekonomi</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_rollrate_bn">PD Vasicek</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_rollrate_n">Perhitungan ECL Vasicek</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_average">Summary ECL Vasicek</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <?php $idb = $data->data->result_draft->id_vasicek_result_draft; ?>
                        <div class="tab-pane active" id="tab_araging" role="tabpanel">
                            <div class="kt-heading kt-heading--md">Hasil Penyesuaian Makroekonomi</div>
                            <div class="kt-form__section kt-form__section--first">
                                <div class="kt-wizard-v1__form">
                                    <div class="row">
                                        <div class="col-12">
                                            <table class="table table-striped table-bordered table-hover"
                                                id="tablePersen" width="100%">
                                                <?php 
                                                $thead = "";$tboody="";
                                                if ( $data->status == 1 ) {
                                                    $thead = "<thead>"
                                                    ."<tr>"
                                                    ."<td class='all' width=800px > Skenario Normal </td>"
                                                    ."<td class='all' width=800px>" . $data->data->result_draft->persen_normal * 100 . " %</td>"
                                                    ."</tr>"
                                                    ." </thead>";
                                            
                                                    $tbody =  "<tbody>"
                                                    ."<tr>"
                                                    ."<td> Skenario Optimis </td>"
                                                    ."<td>" . $data->data->result_draft->persen_uptum * 100  . " %</td>"
                                                    ."</tr>"
                                                    ."<tr>"
                                                    ."<td> Skenario Pesimis </td>"
                                                    ."<td>" . $data->data->result_draft->persen_downtum * 100 . " %</td>"
                                                    ."</tr>"
                                                    ." </tbody>";
                                                } else {
                                                    $thead = "<thead>"
                                                    ."<tr>"
                                                    ."<th class='all' width=800px> Normal </th>"
                                                    ."<th class='all' width=800px></th>"
                                                    ."</tr>"
                                                    ." </thead>";
                                            
                                                    $tbody =  "<tbody>"
                                                    ."<tr>"
                                                    ."<td> Up Turn </td>"
                                                    ."<td></td>"
                                                    ."</tr>"
                                                    ."<tr>"
                                                    ."<td> Down Turn </td>"
                                                    ."<td></td>"
                                                    ."</tr>"
                                                    ." </tbody>";
                                                }
                                                echo $thead . $tbody;
                                                ?>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-wizard-v1__form">
                                    <div class="row">
                                        <div class="col-12">
                                            <table class="table table-striped table-bordered table-hover"
                                                id="tableMicro" width="100%">
                                                <?php 
                                            if ( $data->status == 1 ) {
                                                $thead = "";
                                                $TH = "";
                                                $tbody = "";
                                                $TD1 = "<td>Year</td>";
                                                $TD2 = "";
                                                $projection = $data->data->list_flg_projection;
                                                $i = 2;
                                                foreach ( $projection as $p ) {
                                    
                                                    if ( $p == 1 ) {
                                                        $th = "<th class='all' width=200px>"
                                                        . "Proyeksi"
                                                        ."</th>";
                                                    } else {
                                                        $th = "<th class='all' width=200px>"."</th>";
                                                    }
                                    
                                                    $TH .= $th;
                                                   
                                                }
                                    
                                                $head = $data->data->header_macroeconomic;
                                    
                                                if ( $head == null ) {
                                                    $thead = "<thead><tr><th class='all' width=800px>History</th><th class='all' width=800px ></th>/tr></thead>";
                                                    $tbody = "<tbody><tr><td>Kosong</td><td>Kosong</td>/tr></tbody>";
                                                } else {
                                                    foreach ( $head as $h ) {
                                                        $TD1 .= "<td>".$h."</td>";
                                        
                                                    }
                                        
                                                    $body = $data->data->list_macroeconomic;
                                                    if ($body !=null) {
                                                        foreach ( $body as $b ) {
                                                            $td1 = "<td>".$b->title."</td>";
                                                            $td2 = "";
                                                            foreach ( $b->nilai as $n ) {
                                                                $td2 .= "<td>".$n."</td>";
                                                            }
                                            
                                                            $TD2 .= "<tr>"
                                                            .$td1
                                                            .$td2
                                                            ."</tr>";
                                                        }
                                                    }
                                        
                                                    $thead = "<thead><tr><th></th>".$TH."</tr></thead>";
                                                    $tbody ="<tbody><tr>"
                                                    .$TD1
                                                    ."</tr>"
                                                    .$TD2
                                                    ."</tbody>";
                                                }
                                    
                                            } else {
                                                $thead = "<thead><tr><th class='all' width=800px>History</th><th class='all' width=800px ></th>/tr></thead>";
                                                $tbody = "<tbody><tr><td>Kosong</td><td>Kosong</td>/tr></tbody>";
                                            }
                                    
                                            echo $thead . $tbody;
                                            ?>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-wizard-v1__form">
                                    <div class="row">
                                        <div class="col-12">
                                            <table class="table table-striped table-bordered table-hover"
                                                id="tableHistory" width="100%">
                                                <?php 
                                            if ( $data->status == 1 ) {
                                                $thead = "<thead>"
                                                ."<tr>"
                                                ."<td class='all' width=800px > History Mean </td>"
                                                ."<td class='all' width=800px>" . round($data->data->historical_mean, 10) . "</td>"
                                                ."</tr>"
                                                ." </thead>";
                                        
                                                $tbody =  "<tbody>"
                                                ."<tr>"
                                                ."<td> History STDEV </td>"
                                                ."<td>" . round($data->data->historical_stdev, 10)  . "</td>"
                                                ."</tr>"
                                                ."<tr>"
                                                ."<td> Confidence Level </td>"
                                                ."<td>" . round($data->data->confidence_level, 10)  . " %</td>"
                                                ."</tr>"
                                                ."<tr>"
                                                ."<td> T Distribution </td>"
                                                ."<td>" . round($data->data->t_distribution, 10)  . "</td>"
                                                ."</tr>"
                                                ." </tbody>";
                                            } else {
                                                $thead = "<thead>"
                                                ."<tr>"
                                                ."<th class='all' width=800px > History Mean </th>"
                                                ."<th class='all' width=800px></th>"
                                                ."</tr>"
                                                ." </thead>";
                                        
                                                $tbody =  "<tbody>"
                                                ."<tr>"
                                                ."<td> History STDEV </td>"
                                                ."<td></td>"
                                                ."</tr>"
                                                ."<tr>"
                                                ."<td> Confidence Level </td>"
                                                ."<td></td>"
                                                ."</tr>"
                                                ."<tr>"
                                                ."<td> T Distribution </td>"
                                                ."<td></td>"
                                                ."</tr>"
                                                ." </tbody>";
                                            }
                                            echo $thead . $tbody;
                                            ?>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button onclick="ExportMicro()" type="button"
                                        class="btn btn-sm btn-danger teratur-button float-right">
                                        <i class="flaticon-download-1"></i> Export Excel
                                    </button>
                                </div>
                            </div>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Komentar</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    </button>
                                </div>
                                <div class="kt-portlet">
                                    <div class="kt-portlet__body">
                                        <div class="kt-scroll kt-scroll--pull" style="overflow:scroll; height:350px;">
                                            <div class="kt-widget3">
                                                <?php if ($data->data->list_komentar == []) { ?>
                                                <div class="kt-widget3__item">
                                                    <h4 class="center">Tidak ada Komentar</h1>
                                                </div>
                                                <?php } else { ?>
                                                <?php foreach ($data->data->list_komentar as $key => $kme) { 
                                                if ($kme->flag != 1) {
                                                    continue;
                                                }
                                                ?>
                                                <div class="kt-widget3__item">
                                                    <div class="kt-widget3__header">
                                                        <div class="kt-widget3__user-img">
                                                            <?php if ( $kme->user->path_profile == "" ) { ?>
                                                            <img class="kt-widget3__img"
                                                                src="<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>"
                                                                alt="image">
                                                            <?php } else { ?>
                                                            <?php $src =  $this->config->item('api_host') .'/view_file'. '/' .$kme->user->path_profile ;?>
                                                            <img class="kt-widget3__img" src="<?php echo $src; ?>"
                                                                alt="image" width="40px" height="40px">
                                                            <?php } ?>
                                                        </div>
                                                        <div class="kt-widget3__info">
                                                            <a href="#" class="kt-widget3__username">
                                                                <?php echo $kme->user->nama; ?>
                                                            </a><br>
                                                            <span class="kt-widget3__time">
                                                                <?php
                                                            if ($kme->updated_date != null) {
                                                                echo get_timeago(strtotime($kme->updated_date)); 
                                                            } else {
                                                                echo get_timeago(strtotime($kme->created_date)); 
                                                            }
                                                            ?>
                                                            </span>
                                                        </div>
                                                        <span class="kt-widget3__status kt-font-info">
                                                            <?php if ($this->session->userdata("ids") == $kme->created_by) { ?>
                                                            <span
                                                                id="btn_editkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                class="kt-nav__link kt-font-info" href="#"
                                                                style="cursor:pointer"
                                                                onClick="return EditKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', '<?php echo $kme->komentar; ?>','<?php echo "1"; ?>')">Ubah
                                                            </span>
                                                            &nbsp
                                                            <span
                                                                id="btn_batkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                href="#" class="kt-nav__link kt-font-info" onClick="return batalKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', 
                                                            '<?php echo $kme->komentar; ?>', 1
                                                        )" style="display:none; cursor:pointer;">Batal
                                                            </span>
                                                            &nbsp
                                                            <a href="<?php echo site_url("Model/deletekomen_draft_bucket?idk=$kme->id_komentar_bucket&typ=1&idb=$idb"); ?>"
                                                                class="kt-nav__link kt-font-info"
                                                                onclick='return del_confirm()'>Hapus
                                                            </a>
                                                            <?php } ?>
                                                        </span>
                                                    </div>
                                                    <div class="kt-widget3__body">
                                                        <p class="kt-widget3__text">
                                                            <?php echo $kme->komentar; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__foot">
                                        <form method="post" enctype="multipart/form-data"
                                            action="<?php echo site_url("model/addkomen_draft_bucket?idb=$idb"); ?>">
                                            <div class="form-row align-items-center">
                                                <div class="col-auto">
                                                    <input value="3" name="type_coment" hidden></input>
                                                    <input value="1" name="flag_coment" hidden></input>
                                                    <input value="0" id="komen_id1" name="id_coment" hidden></input>
                                                    <input value="<?php echo $idb; ?>" name="id_bucket" hidden></input>
                                                    <textarea id="komen_content1" required
                                                        style="height: 50px; width: 400px;" placeholder="Type here..."
                                                        name="komentar"></textarea>
                                                </div>
                                                <div class="col-auto">
                                                    <button type="submit"
                                                        class="btn btn-primary teratur-button">Kirim</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_rollrate_bn" role="tabpanel">
                            <div class="kt-heading kt-heading--md">Perhitungan PD Vasicek</div>
                            <div class="kt-form__section kt-form__section--first">
                                <div class="kt-wizard-v1__form">
                                    <div class="row">
                                        <div class="col-12">
                                            <table class="table table-striped table-bordered table-hover" id="tablePD"
                                                width="100%">
                                                <thead>
                                                    <tr>
                                                        <th class='all' width=200px>Pefindo Rating</th>
                                                        <th class='all' width=200px>TTC PD (Pefindo)</th>
                                                        <th class='all' width=200px>Asset Correlation (Basel
                                                            formula) Corporate </th>
                                                        <th class='all' width=200px>PIT PD (Vasicek) </th>
                                                        <!-- <th class='all' width=200px>PIT PD (Vasicek) (Adjusted) -->
                                                        </th>
                                                        <th class='all' width=200px>PIT PD (Vasicek) </th>
                                                        <!-- <th class='all' width=200px>PIT PD (Vasicek)(Adjusted) -->
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody id="listPD">
                                                    <?php 
                                                    $TD1 = "<tr>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>(Normal Scenario)</td>
                                                    <td>(Weighted Scenario)</td>
                                                    </tr>";
                                                    if ( $data->status == 1 ) {
                                                        
                                                        $TD2 = "";
                                            
                                                        $pd = $data->data->list_pd_vasicek;
                                            
                                                        if ( $pd == null ) {
                                            
                                                            $TD2 = "<tr><
                                                            td>-</td>
                                                            td>-</td>
                                                            td>-</td>
                                                            td>-</td>
                                                            td>-</td>
                                                            </tr>";
                                                        } else {
                                                            foreach( $pd as $p ) {
                                                                $TD2 .= "<tr>"
                                                                ."<td>".$p->rating->kode_rating."</td>"
                                                                ."<td>". round(($p->value_rating * 100), 2)." %</td>"
                                                                ."<td>". round(($p->value_base_formula * 100), 2)." %</td>"
                                                                ."<td>". round(($p->vasicek_normal * 100), 2) ." %</td>"
                                                                // ."<td>". round(($p->vasicek_adjusted_normal * 100), 2) ." %</td>"
                                                                ."<td>". round(($p->vasicek_weighted * 100), 2) ." %</td>"
                                                                // ."<td>". round(($p->vasicek_adjusted_weighted * 100), 2) ." %</td>"
                                                                ."</tr>";
                                                            }
                                                        }
                                                        
                                                    } else {
                                                    
                                                        $TD2 = "<tr><
                                                        td>-</td>
                                                        td>-</td>
                                                        td>-</td>
                                                        td>-</td>
                                                        td>-</td>
                                                        </tr>";
                                                    }
                                                    echo $TD1 . $TD2;
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <button onclick="ExportPD()" type="button"
                                            class="btn btn-sm btn-danger teratur-button float-right">
                                            <i class="flaticon-download-1"></i> Export Excel
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Komentar</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    </button>
                                </div>
                                <div class="kt-portlet">
                                    <div class="kt-portlet__body">
                                        <div class="kt-scroll kt-scroll--pull" style="overflow:scroll; height:350px;">
                                            <div class="kt-widget3">
                                                <?php if ($data->data->list_komentar == []) { ?>
                                                <div class="kt-widget3__item">
                                                    <h4 class="center">Tidak ada Komentar</h1>
                                                </div>
                                                <?php } else { ?>
                                                <?php foreach ($data->data->list_komentar as $key => $kme) { 
                                                if ($kme->flag != 2) {
                                                    continue;
                                                }
                                                ?>
                                                <div class="kt-widget3__item">
                                                    <div class="kt-widget3__header">
                                                        <div class="kt-widget3__user-img">
                                                            <?php if ( $kme->user->path_profile == "" ) { ?>
                                                            <img class="kt-widget3__img"
                                                                src="<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>"
                                                                alt="image">
                                                            <?php } else { ?>
                                                            <?php $src =  $this->config->item('api_host') .'/view_file'. '/' .$kme->user->path_profile ;?>
                                                            <img class="kt-widget3__img" src="<?php echo $src; ?>"
                                                                alt="image" width="40px" height="40px">
                                                            <?php } ?>
                                                        </div>
                                                        <div class="kt-widget3__info">
                                                            <a href="#" class="kt-widget3__username">
                                                                <?php echo $kme->user->nama; ?>
                                                            </a><br>
                                                            <span class="kt-widget3__time">
                                                                <?php
                                                            if ($kme->updated_date != null) {
                                                                echo get_timeago(strtotime($kme->updated_date)); 
                                                            } else {
                                                                echo get_timeago(strtotime($kme->created_date)); 
                                                            }
                                                            ?>
                                                            </span>
                                                        </div>
                                                        <span class="kt-widget3__status kt-font-info">
                                                            <?php if ($this->session->userdata("ids") == $kme->created_by) { ?>
                                                            <span
                                                                id="btn_editkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                class="kt-nav__link kt-font-info" href="#"
                                                                style="cursor:pointer"
                                                                onClick="return EditKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', '<?php echo $kme->komentar; ?>','<?php echo "2"; ?>')">Ubah
                                                            </span>
                                                            &nbsp
                                                            <span
                                                                id="btn_batkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                href="#" class="kt-nav__link kt-font-info" onClick="return batalKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', 
                                                            '<?php echo $kme->komentar; ?>', 2
                                                        )" style="display:none; cursor:pointer;">Batal
                                                            </span>
                                                            &nbsp
                                                            <a href="<?php echo site_url("Model/deletekomen_draft_bucket?idk=$kme->id_komentar_bucket&typ=1&idb=$idb"); ?>"
                                                                class="kt-nav__link kt-font-info"
                                                                onclick='return del_confirm()'>Hapus
                                                            </a>
                                                            <?php } ?>
                                                        </span>
                                                    </div>
                                                    <div class="kt-widget3__body">
                                                        <p class="kt-widget3__text">
                                                            <?php echo $kme->komentar; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__foot">
                                        <form method="post" enctype="multipart/form-data"
                                            action="<?php echo site_url("model/addkomen_draft_bucket?idb=$idb"); ?>">
                                            <div class="form-row align-items-center">
                                                <div class="col-auto">
                                                    <input value="3" name="type_coment" hidden></input>
                                                    <input value="2" name="flag_coment" hidden></input>
                                                    <input value="0" id="komen_id2" name="id_coment" hidden></input>
                                                    <input value="<?php echo $idb; ?>" name="id_bucket" hidden></input>
                                                    <textarea id="komen_content2" required
                                                        style="height: 50px; width: 400px;" placeholder="Type here..."
                                                        name="komentar"></textarea>
                                                </div>
                                                <div class="col-auto">
                                                    <button type="submit"
                                                        class="btn btn-primary teratur-button">Kirim</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_rollrate_n" role="tabpanel">
                            <div class="kt-heading kt-heading--md">Perhitungan ECL Vasicek</div>
                            <div class="kt-form__section kt-form__section--first">
                                <div class="kt-wizard-v1__form">
                                    <div class="row">
                                        <div class="col-12">
                                            <table class="table table-striped table-bordered table-hover"
                                                id="tableKertas" width="100%">

                                                <thead>
                                                    <tr>
                                                        <th class='all' width=200px>Jenis Aset Keuangan</th>
                                                        <th class='all' width=200px>Nomor Rekening</th>
                                                        <th class='all' width=200px>Mata Uang</th>
                                                        <th class='all' width=200px>Nama Bank</th>
                                                        <th class='all' width=200px>Akronim Bank</th>
                                                        <th class='all' width=200px>Rating</th>
                                                        <th class='all' width=200px>Suku Bunga</th>
                                                        <th class='all' width=200px>Jatuh tempo</th>
                                                        <th class='all' width=200px id="eclperiode"></th>
                                                        <th class='all' width=200px>PD (normal Scenario)</th>
                                                        <th class='all' width=200px>PD (weighted Scenario)</th>
                                                        <th class='all' width=200px>LGD</th>
                                                        <th class='all' width=200px>Nilai ECL (normal scen)</th>
                                                        <th class='all' width=200px>Nilai ECL (weighted scen)
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody id="listKertas">
                                                    <?php 
                                                if ( $data->status == 1 ) {
           

                                                    $TD2 = "";
                                        
                                                    $ecl = $data->data->list_aset;
                                        
                                                    if ( $ecl == null ) {
                                                        $TD2 = "<tr><
                                                        td>-</td>
                                                        td>-</td>
                                                        td>-</td>
                                                        td>-</td>
                                                        td>-</td>
                                                        td>-</td>
                                                        td>-</td>
                                                        td>-</td>
                                                        td>-</td>
                                                        td>-</td>
                                                        td>-</td>
                                                        td>-</td>
                                                        td>-</td>
                                                        td>-</td>
                                                        </tr>";
                                                    } else {
                                                        foreach( $ecl as $e ) {
                                        
                                                            if ( $e->id_aset == 0 ) {
                                                                $TD2 .= "<tr>"
                                                                ."<td>-</td>"
                                                                ."<td>-</td>"
                                                                ."<td>-</td>"
                                                                ."<td>-</td>"
                                                                ."<td>-</td>"
                                                                ."<td>-</td>"
                                                                ."<td>-</td>"
                                                                ."<td>-</td>"
                                                                ."<td>-</td>"
                                                                ."<td>-</td>"
                                                                ."<td>-</td>"
                                                                ."<td>Jumlah</td>"
                                                                ."<td>". "Rp. " . number_format( $e->ecl_normal_scan, 2, ',', '.')."</td>"
                                                                ."<td>"."Rp. " . number_format( $e->ecl_weighted_scan, 2, ',', '.')."</td>"
                                                                ."</tr>";
                                                            } else {
                                                                $TD2 .= "<tr>"
                                                                ."<td>".$e->aset->nama_aset."</td>"
                                                                ."<td>".$e->no_rekening."</td>"
                                                                ."<td>".$e->mata_uang."</td>"
                                                                ."<td>".$e->bank->nama_bank."</td>"
                                                                ."<td>".$e->bank->akronim."</td>"
                                                                ."<td>".$e->bank->rating->kode_rating."</td>"
                                                                ."<td>". round(($e->suku_bunga * 100), 2) ." %</td>"
                                                                ."<td>". date("d/M/Y", strtotime($e->tgl_jatuh_tempo))."</td>"
                                                                ."<td>"."Rp. " . number_format( $e->saldo, 2, ',', '.')."</td>"
                                                                ."<td>". round(($e->pd_normal_scan * 100), 2)." %</td>"
                                                                ."<td>". round(($e->pd_weighted_scan * 100), 2)." %</td>"
                                                                ."<td>". round(($e->lgd * 100), 2)." %</td>"
                                                                ."<td>"."Rp. " . number_format( $e->ecl_normal_scan, 2, ',', '.')."</td>"
                                                                ."<td>"."Rp. " . number_format( $e->ecl_weighted_scan, 2, ',', '.')."</td>"
                                                                ."</tr>";
                                                            }
                                                        }
                                                    }
                                        
                                                  
                                                } else {
                                                   
                                                    $TD2 = "<tr><
                                                    td>-</td>
                                                    td>-</td>
                                                    td>-</td>
                                                    td>-</td>
                                                    td>-</td>
                                                    td>-</td>
                                                    td>-</td>
                                                    td>-</td>
                                                    td>-</td>
                                                    td>-</td>
                                                    td>-</td>
                                                    td>-</td>
                                                    td>-</td>
                                                    td>-</td>
                                                    </tr>";
                                                }
                                        
                                                echo $TD2;
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <button onclick="ExportKertas()" type="button"
                                            class="btn btn-sm btn-danger teratur-button float-right">
                                            <i class="flaticon-download-1"></i> Export Excel
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Komentar</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    </button>
                                </div>
                                <div class="kt-portlet">
                                    <div class="kt-portlet__body">
                                        <div class="kt-scroll kt-scroll--pull" style="overflow:scroll; height:350px;">
                                            <div class="kt-widget3">
                                                <?php if ($data->data->list_komentar == []) { ?>
                                                <div class="kt-widget3__item">
                                                    <h4 class="center">Tidak ada Komentar</h1>
                                                </div>
                                                <?php } else { ?>
                                                <?php foreach ($data->data->list_komentar as $key => $kme) { 
                                                if ($kme->flag != 3) {
                                                    continue;
                                                }
                                                ?>
                                                <div class="kt-widget3__item">
                                                    <div class="kt-widget3__header">
                                                        <div class="kt-widget3__user-img">
                                                            <?php if ( $kme->user->path_profile == "" ) { ?>
                                                            <img class="kt-widget3__img"
                                                                src="<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>"
                                                                alt="image">
                                                            <?php } else { ?>
                                                            <?php $src =  $this->config->item('api_host') .'/view_file'. '/' .$kme->user->path_profile ;?>
                                                            <img class="kt-widget3__img" src="<?php echo $src; ?>"
                                                                alt="image" width="40px" height="40px">
                                                            <?php } ?>
                                                        </div>
                                                        <div class="kt-widget3__info">
                                                            <a href="#" class="kt-widget3__username">
                                                                <?php echo $kme->user->nama; ?>
                                                            </a><br>
                                                            <span class="kt-widget3__time">
                                                                <?php
                                                            if ($kme->updated_date != null) {
                                                                echo get_timeago(strtotime($kme->updated_date)); 
                                                            } else {
                                                                echo get_timeago(strtotime($kme->created_date)); 
                                                            }
                                                            ?>
                                                            </span>
                                                        </div>
                                                        <span class="kt-widget3__status kt-font-info">
                                                            <?php if ($this->session->userdata("ids") == $kme->created_by) { ?>
                                                            <span
                                                                id="btn_editkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                class="kt-nav__link kt-font-info" href="#"
                                                                style="cursor:pointer"
                                                                onClick="return EditKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', '<?php echo $kme->komentar; ?>','<?php echo "3"; ?>')">Ubah
                                                            </span>
                                                            &nbsp
                                                            <span
                                                                id="btn_batkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                href="#" class="kt-nav__link kt-font-info" onClick="return batalKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', 
                                                            '<?php echo $kme->komentar; ?>', 3
                                                        )" style="display:none; cursor:pointer;">Batal
                                                            </span>
                                                            &nbsp
                                                            <a href="<?php echo site_url("Model/deletekomen_draft_bucket?idk=$kme->id_komentar_bucket&typ=1&idb=$idb"); ?>"
                                                                class="kt-nav__link kt-font-info"
                                                                onclick='return del_confirm()'>Hapus
                                                            </a>
                                                            <?php } ?>
                                                        </span>
                                                    </div>
                                                    <div class="kt-widget3__body">
                                                        <p class="kt-widget3__text">
                                                            <?php echo $kme->komentar; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__foot">
                                        <form method="post" enctype="multipart/form-data"
                                            action="<?php echo site_url("model/addkomen_draft_bucket?idb=$idb"); ?>">
                                            <div class="form-row align-items-center">
                                                <div class="col-auto">
                                                    <input value="3" name="type_coment" hidden></input>
                                                    <input value="3" name="flag_coment" hidden></input>
                                                    <input value="0" id="komen_id13" name="id_coment" hidden></input>
                                                    <input value="<?php echo $idb; ?>" name="id_bucket" hidden></input>
                                                    <textarea id="komen_content3" required
                                                        style="height: 50px; width: 400px;" placeholder="Type here..."
                                                        name="komentar"></textarea>
                                                </div>
                                                <div class="col-auto">
                                                    <button type="submit"
                                                        class="btn btn-primary teratur-button">Kirim</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_average" role="tabpanel">
                            <div class="kt-heading kt-heading--md">Summary ECL Vasicek</div>
                            <div class="kt-form__section kt-form__section--first">
                                <div class="kt-wizard-v1__form">
                                    <div class="row">
                                        <div class="col-12">
                                            <table class="table table-striped table-bordered table-hover" id="tableECL"
                                                width="100%">

                                                <thead>
                                                    <tr>
                                                        <th class='all' width=400px>Jenis Aset Keuangan</th>
                                                        <?php
                                                            $ecl1 = ""; $ecl2 = ""; $ecl3 = "";
                                                            if ( $data->status == 1 ) {
                                                                $prd = $data->data->periode_ecl;
                                                                $ecl1 = "Saldo per " . $prd . " dalam IDR";
                                                                $ecl2 = "Hasil ECL PSAK 71 per " . $prd . " (Normal Scenario)";
                                                                $ecl3 = "Hasil ECL PSAK 71 per " . $prd . " (Weighted Scenario)";
                                                            }
                                                        ?>
                                                        <th class='all' width=400px id="ecl1"><?php echo $ecl1; ?></th>
                                                        <th class='all' width=400px id="ecl2"><?php echo $ecl2; ?></th>
                                                        <th class='all' width=400px id="ecl3"><?php echo $ecl3; ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="listECL">
                                                    <?php
                                                    if ( $data->status == 1 ) {

                                                        $TD2 = "";
                                            
                                                        $ecl = $data->data->list_summary;
                                            
                                                        if ( $ecl == null ) {
                                                            $TD2 = "<tr><
                                                            td>-</td>
                                                            td>-</td>
                                                            td>-</td>
                                                            td>-</td>
                                                            </tr>";
                                                        } else {
                                                            foreach( $ecl as $e ) {
                                            
                                                                if ( $e->id_aset == 0 ) {
                                                                    $TD2 .= "<tr>"
                                                                    ."<td>Jumlah</td>"
                                                                    ."<td>"."Rp. " . number_format( $e->saldo, 2, ' ,', '.')."</td>"
                                                                    ."<td>"."Rp. " . number_format( $e->ecl_normal_scan, 2, ',', '.')."</td>"
                                                                    ."<td>"."Rp. " . number_format( $e->ecl_weighted_scan, 2, ',', '.')."</td>"
                                                                    ."</tr>";
                                                                } else {
                                                                    $TD2 .= "<tr>"
                                                                    ."<td>".$e->aset->nama_aset."</td>"
                                                                    ."<td>"."Rp. " . number_format( $e->saldo, 2, ' ,', '.')."</td>"
                                                                    ."<td>"."Rp. " . number_format( $e->ecl_normal_scan, 2, ',', '.')."</td>"
                                                                    ."<td>"."Rp. " . number_format( $e->ecl_weighted_scan, 2, ',', '.')."</td>"
                                                                    ."</tr>";
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        
                                                        $TD2 = "<tr><
                                                        td>-</td>
                                                        td>-</td>
                                                        td>-</td>
                                                        td>-</td>
                                                        </tr>";
                                                    }
                                                    echo $TD2;
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <button onclick="ExportSumECL()" type="button"
                                            class="btn btn-sm btn-danger teratur-button float-right">
                                            <i class="flaticon-download-1"></i> Export Excel
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Komentar</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    </button>
                                </div>
                                <div class="kt-portlet">
                                    <div class="kt-portlet__body">
                                        <div class="kt-scroll kt-scroll--pull" style="overflow:scroll; height:350px;">
                                            <div class="kt-widget3">
                                                <?php if ($data->data->list_komentar == []) { ?>
                                                <div class="kt-widget3__item">
                                                    <h4 class="center">Tidak ada Komentar</h1>
                                                </div>
                                                <?php } else { ?>
                                                <?php foreach ($data->data->list_komentar as $key => $kme) { 
                                                if ($kme->flag != 4) {
                                                    continue;
                                                }
                                                ?>
                                                <div class="kt-widget3__item">
                                                    <div class="kt-widget3__header">
                                                        <div class="kt-widget3__user-img">
                                                            <?php if ( $kme->user->path_profile == "" ) { ?>
                                                            <img class="kt-widget3__img"
                                                                src="<?php  echo base_url().'/public/assets/custom/default_profile.png'; ?>"
                                                                alt="image">
                                                            <?php } else { ?>
                                                            <?php $src =  $this->config->item('api_host') .'/view_file'. '/' .$kme->user->path_profile ;?>
                                                            <img class="kt-widget3__img" src="<?php echo $src; ?>"
                                                                alt="image" width="40px" height="40px">
                                                            <?php } ?>
                                                        </div>
                                                        <div class="kt-widget3__info">
                                                            <a href="#" class="kt-widget3__username">
                                                                <?php echo $kme->user->nama; ?>
                                                            </a><br>
                                                            <span class="kt-widget3__time">
                                                                <?php
                                                            if ($kme->updated_date != null) {
                                                                echo get_timeago(strtotime($kme->updated_date)); 
                                                            } else {
                                                                echo get_timeago(strtotime($kme->created_date)); 
                                                            }
                                                            ?>
                                                            </span>
                                                        </div>
                                                        <span class="kt-widget3__status kt-font-info">
                                                            <?php if ($this->session->userdata("ids") == $kme->created_by) { ?>
                                                            <span
                                                                id="btn_editkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                class="kt-nav__link kt-font-info" href="#"
                                                                style="cursor:pointer"
                                                                onClick="return EditKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', '<?php echo $kme->komentar; ?>','<?php echo "4"; ?>')">Ubah
                                                            </span>
                                                            &nbsp
                                                            <span
                                                                id="btn_batkom<?php echo  $kme->id_komentar_bucket; ?>"
                                                                href="#" class="kt-nav__link kt-font-info" onClick="return batalKomentar(
                                                            '<?php echo $kme->id_komentar_bucket; ?>', 
                                                            '<?php echo $kme->komentar; ?>', 4
                                                        )" style="display:none; cursor:pointer;">Batal
                                                            </span>
                                                            &nbsp
                                                            <a href="<?php echo site_url("Model/deletekomen_draft_bucket?idk=$kme->id_komentar_bucket&typ=1&idb=$idb"); ?>"
                                                                class="kt-nav__link kt-font-info"
                                                                onclick='return del_confirm()'>Hapus
                                                            </a>
                                                            <?php } ?>
                                                        </span>
                                                    </div>
                                                    <div class="kt-widget3__body">
                                                        <p class="kt-widget3__text">
                                                            <?php echo $kme->komentar; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__foot">
                                        <form method="post" enctype="multipart/form-data"
                                            action="<?php echo site_url("model/addkomen_draft_bucket?idb=$idb"); ?>">
                                            <div class="form-row align-items-center">
                                                <div class="col-auto">
                                                    <input value="3" name="type_coment" hidden></input>
                                                    <input value="4" name="flag_coment" hidden></input>
                                                    <input value="0" id="komen_id4" name="id_coment" hidden></input>
                                                    <input value="<?php echo $idb; ?>" name="id_bucket" hidden></input>
                                                    <textarea id="komen_content4" required
                                                        style="height: 50px; width: 400px;" placeholder="Type here..."
                                                        name="komentar"></textarea>
                                                </div>
                                                <div class="col-auto">
                                                    <button type="submit"
                                                        class="btn btn-primary teratur-button">Kirim</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-12">
                                <button onclick="ExportDrafBucket()" type="button"
                                    class="btn btn-sm btn-danger teratur-button float-right">
                                    <i class="flaticon-download-1"></i> Export Excel
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
function get_timeago( $ptime )
{
    $estimate_time = time() - $ptime;
    if( $estimate_time < 1 )
    {
        return 'kurang dari 1 detik yang lalu';
    }
    $condition = array(
        12 * 30 * 24 * 60 * 60  =>  'tahun',
        30 * 24 * 60 * 60       =>  'bulan',
        24 * 60 * 60            =>  'hari',
        60 * 60                 =>  'jam',
        60                      =>  'menit',
        1                       =>  'detik'
    );
    foreach( $condition as $secs => $str )
    {
        $d = $estimate_time / $secs;
        if( $d >= 1 )
        {
            $r = round( $d );
            return 'sekitar ' . $r . ' ' . $str . ( $r > 1 ? '' : '' ) . ' yang lalu';
        }
    }
}
?>

<script>
// function reload_tab() {
//     alert('wwl');
//     $('#scroll1').refresh();
// }

function EditKomentar(id, content, flag) {
    // alert(id + " " + content + " " + flag);
    document.getElementById("komen_id" + flag).value = id;
    document.getElementById("komen_content" + flag).value = content;
    $("#btn_editkom" + id).hide();
    $("#btn_batkom" + id).show();
}

function batalKomentar(id, content, flag) {
    document.getElementById("komen_id" + flag).value = 0;
    document.getElementById("komen_content" + flag).value = "";
    $("#btn_editkom" + id).show();
    $("#btn_batkom" + id).hide();
}
</script>