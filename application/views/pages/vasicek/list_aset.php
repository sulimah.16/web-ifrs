<style>
table {
    table-layout: fixed;
}

td {
    overflow: hidden;
    text-overflow: ellipsis;
}

#tbdy {
    margin-top: 25px;
    width: 550px;
}

#url {
    color: blue;
    text-decoration: underline;
}
</style>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Daftar Aset Keuangan</h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-box-1"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Aset </a>

                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container   kt-grid__item--fluid">
            <?php if ( $data->status == '1' || $data->status == 1 ) { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Berhasil!&nbsp;&nbsp;</strong><?php echo $data->message; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } else { ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Perhatian!&nbsp;&nbsp;</strong> <?php echo $data->message; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } ?>
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                <strong>Info!&nbsp;&nbsp;</strong><?php echo "Tekan button tambah data untuk melakukan penambahan data"; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h2 class="kt-portlet__head-title">
                            <?php 
                            if ($id_draft != 0 && count($data->data) > 0) { 
                                echo $data->data[0]->draft->nama_draft;
                            } ?>
                        </h2>
                    </div>

                    <?php if ( $this->session->userdata('role') == '1' || $this->session->userdata('role') == 1 || $this->session->userdata('role') == '2' || $this->session->userdata('role') == 2 ) { ?>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <div id='tbdy'>
                                    <form method="post" enctype="multipart/form-data"
                                        action="<?php echo site_url('vasicek/list_aset'); ?>">
                                        <div class="form-group row">
                                            <div class="col-lg-6">
                                                <select name="draft" id="draft" class="form-control" required>
                                                    <option value="0"><?php echo "Pilih Draft" ?></option>
                                                    <?php foreach( $draft->data as $dr ):?>
                                                    <option value="<?php echo $dr->id_draft_vasicek; ?>">
                                                        <?php echo $dr->nama_draft; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-6">
                                                <button type="submit" class="btn btn-primary teratur-button"><i
                                                        class="flaticon-file-2 icon-xs"></i>Lihat Data</button>
                                                <span></span>
                                                <button type="button" class="btn btn-primary teratur-button"
                                                    data-toggle="modal" data-target="#add_aset"><i
                                                        class="flaticon-plus icon-xs"></i>Input Data</button>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } else { ?>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <div id='tbdy'>
                                    <form method="post" enctype="multipart/form-data"
                                        action="<?php echo site_url('vasicek/list_aset'); ?>">
                                        <div class="form-group row">
                                            <div class="col-lg-8">
                                                <select name="draft" id="draft" class="form-control" required>
                                                    <option value="0"><?php echo "Pilih Draft" ?></option>
                                                    <?php foreach( $draft->data as $dr ):?>
                                                    <option value="<?php echo $dr->id_draft_vasicek; ?>">
                                                        <?php echo $dr->nama_draft; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-4">
                                                <button type="submit" class="btn btn-primary teratur-button"><i
                                                        class="flaticon-file-2 icon-xs"></i>Lihat Data</button>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>

                <div class="kt-portlet__body">


                    <table class="table table-striped table-bordered table-hover" id="tbl_list_default2" width="100%">
                        <thead>
                            <tr>
                                <th class="all" width=10px>#</th>
                                <th class="all">Jenis Aset Keuangan</th>
                                <th class="all">Nama Bank</th>
                                <th class="none">Bank Akronim</th>
                                <th class="all">No Rekening</th>
                                <th class="all">Mata Uang</th>
                                <th class="all">Rating</th>
                                <th class="all">Suku Bunga</th>
                                <th class="all">Jatuh Tempo</th>
                                <th class="all">Saldo</th>
                                <th class="none">Update Oleh</th>
                                <th class="all">Update Terakhir</th>
                                <?php if ( $this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' ) { ?>
                                <th class="all">Action</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody class="kt-datatable__body">
                            <?php $i = 1; foreach( $data->data as $d ): ?>
                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $d->aset->nama_aset; ?></td>
                                <td><?php echo $d->bank->nama_bank; ?></td>
                                <td><?php echo $d->bank->akronim; ?></td>
                                <td><?php echo $d->no_rekening; ?></td>
                                <td><?php echo $d->mata_uang; ?></td>
                                <td><?php echo $d->bank->rating->kode_rating; ?></td>
                                <td><?php echo round($d->suku_bunga * 100 , 4). " %"; ?></td>
                                <td><?php echo date("d M Y", strtotime($d->tgl_jatuh_tempo)); ?></td>
                                <td><?php echo "Rp. " . number_format( $d->saldo, 2, ',', '.'); ?></td>
                                <?php if ($d->updated_by != 0) { ?>
                                <td><?php echo $d->updated_user->username; ?></td>
                                <td><?php echo date("d M Y H:i:s", strtotime($d->updated_date)); ?></td>
                                <?php } else { ?>
                                <td><?php echo $d->created_user->username; ?></td>
                                <td><?php echo date("d M Y H:i:s", strtotime($d->created_date)); ?></td>
                                <?php } ?>
                                <?php if ( $this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' ) { ?>
                                <td>
                                    <div class="dropdown">
                                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                            data-toggle="dropdown">
                                            <i class="la la-cog"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-sm">
                                            <ul class="kt-nav">
                                                <li class="kt-nav__item">
                                                    <a href="<?php echo site_url("#"); ?>" data-toggle="modal"
                                                        data-target="#edit_aset" onClick="return EditAset(
                                                            '<?php echo  $d->id_list_aset; ?>',  
                                                            '<?php echo  $d->id_jenis; ?>',  
                                                            '<?php echo  $d->id_bank; ?>', 
                                                            '<?php echo  $d->no_rekening; ?>',  
                                                            '<?php echo  $d->mata_uang; ?>', 
                                                            '<?php echo  ($d->suku_bunga * 100); ?>',  
                                                            '<?php echo  date('d/m/Y', strtotime($d->tgl_jatuh_tempo)); ?>', 
                                                            '<?php echo  $d->saldo; ?>', 
                                                            )" class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon-edit"></i>
                                                        <span class="kt-nav__link-text">Ubah Data</span>
                                                    </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <a onclick='return del_confirm()'
                                                        href="<?php echo site_url('Vasicek/listAset_del/'.$d->id_list_aset); ?>"
                                                        class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon2-trash"></i>
                                                        <span class="kt-nav__link-text">Hapus Data</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                                <?php } ?>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add_aset" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Input Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>

            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('Vasicek/addListaset'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tipe Input Data</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <select name="tipe" id="tipe_input" class="form-control" required
                                            onChange="ubahForm();">
                                            <option value="<?php echo '0'; ?>"><?php echo 'Input Data Manual'; ?>
                                            </option>
                                            <option value="<?php echo '1'; ?>"><?php echo 'Upload Daftar Aset'; ?>
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row" id="content_manual1x">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Draf</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <select name="drafts" id="draft" class="form-control">
                                            <option value="<?php echo ""; ?>"><?php echo "Buat Draf Baru"; ?>
                                            </option>
                                            <?php foreach( $draft->data as $dr ):?>
                                            <option value="<?php echo $dr->id_draft_vasicek; ?>">
                                                <?php echo "Relasi dengan: ".$dr->nama_draft; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <span class="form-text text-muted">Buat Draf baru atau tambahkan ke draf
                                            yang sudah ada.</span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Jenis Aset</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="add_jenis" id="add_jenis" class="form-control">
                                            <?php foreach( $jenis->data as $s ):?>
                                            <option value="<?php echo $s->id_vas_aset; ?>">
                                                <?php echo $s->nama_aset; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Filter Bank</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <select class="form-control" onchange="select_bank(event)">
                                            <?php foreach( $grup_bank as $dr ):?>
                                            <option value="<?php echo $dr->nama_bank; ?>">
                                                <?php echo $dr->nama_bank; ?>
                                            </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Bank</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <select name="add_bank" id="add_bank" class="form-control">
                                            <?php foreach( $bank->data as $dr ):
                                            if ($dr->nama_bank != $grup_bank[0]->nama_bank) {
                                                continue;
                                            }
                                            ?>
                                            <option value="<?php echo $dr->id_vas_bank; ?>">
                                                <?php echo $dr->nama_bank." (".$dr->rating->kode_rating.") tahun: ".$dr->tahun; ?>
                                            </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div id="content_manual1">
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">No Rekening</label>
                                        <div class="col-lg-9 col-xl-4">
                                            <input class="form-control" type="text" id="add_rekening"
                                                placeholder="masukan no_rekening" name="add_rekening">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Mata Uang</label>
                                        <div class="col-lg-9 col-xl-4">
                                            <input class="form-control" type="text" id="add_mata_uang"
                                                placeholder="set mata uang" name="add_mata_uang">
                                            <span class="form-text text-muted">Mata Uang Fungsional.</span>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Suku Bunga</label>
                                        <div class="col-lg-9 col-xl-4">
                                            <div class="input-group">
                                                <input class="form-control" type="text" id="add_suku_bunga"
                                                    name="add_suku_bunga" placeholder="masukan suku bunga"
                                                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">%</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Saldo</label>
                                        <div class="col-lg-9 col-xl-4">
                                            <input class="form-control" type="text" id="add_saldo" name="add_saldo"
                                                placeholder="masukan saldo aset"
                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">

                                            <span class="form-text text-muted">Di isi dengan saldo dalam mata uang
                                                fungsional.</span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Tanggal Jatuh Tempo</label>
                                        <div class="col-lg-9 col-xl-4">
                                            <input class="form-control" type="text" id="dateStart" name="add_tgl_jatuh">
                                        </div>
                                    </div>
                                </div>
                                <div id="file_upload_x" class="form-group row" style="display:none">
                                    <label class=" col-xl-3 col-lg-3 col-form-label">Masukan File</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="customFile"
                                                    name="file_b">
                                                <label class="custom-file-label" for="customFile">Choose
                                                    file</label>
                                            </div>
                                        </div>
                                        <span class="form-text text-muted">contoh untuk upload file daftar aset.
                                            <a href="<?php echo base_url(); ?>public/example/UPLOAD_LIST_ASET.csv"
                                                download>klik disini </a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button" onclick="return act_confirm()">Prosess
                        Data</button>
                </div>
            </form>

        </div>
    </div>
</div>

<div class="modal fade" id="edit_aset" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ubah Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('vasicek/editListAset'); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Jenis Aset</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="edit_idaset" name="edit_idaset"
                                            hidden>
                                        <select name="edit_jenis" id="edit_jenis" class="form-control">
                                            <?php foreach( $jenis->data as $s ):?>
                                            <option value="<?php echo $s->id_vas_aset; ?>">
                                                <?php echo $s->nama_aset; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Filter Bank</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <select class="form-control" onchange="select_bank(event)">
                                            <?php foreach( $grup_bank as $dr ):?>
                                            <option value="<?php echo $dr->nama_bank; ?>">
                                                <?php echo $dr->nama_bank; ?>
                                            </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Bank</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <select name="edit_bank" id="edit_bank" class="form-control">
                                            <?php foreach( $bank->data as $dr ):
                                            if ($dr->nama_bank != $grup_bank[0]->nama_bank) {
                                                continue;
                                            }   
                                            ?>
                                            <option value="<?php echo $dr->id_vas_bank; ?>">
                                                <?php echo $dr->nama_bank." (".$dr->rating->kode_rating.")"; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">No Rekening</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="edit_rekening" name="edit_rekening">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Mata Uang</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="edit_mata_uang"
                                            name="edit_mata_uang">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Suku Bunga</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <div class="input-group">
                                            <input class="form-control" type="text" id="edit_suku_bunga"
                                                name="edit_suku_bunga"
                                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                            <div class="input-group-append">
                                                <span class="input-group-text">%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Saldo</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="edit_saldo" name="edit_saldo"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tanggal Jatuh Tempo</label>
                                    <div class="col-lg-9 col-xl-4">
                                        <input class="form-control" type="text" id="dateStart2" name="edit_tgl_jatuh">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button" onclick="return act_confirm()">Proses
                        Data</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>

<script type="text/javascript">
function EditAset(id, id_jenis, id_bank, no_rekening, mata_uang, suku_bunga, tgl_jatuh_tempo, saldo) {
    document.getElementById("edit_idaset").value = id;
    document.getElementById("edit_jenis").value = id_jenis;
    document.getElementById("edit_bank").value = id_bank;
    document.getElementById("edit_rekening").value = no_rekening;
    document.getElementById("edit_mata_uang").value = mata_uang;
    document.getElementById("edit_suku_bunga").value = suku_bunga;
    document.getElementById("dateStart2").value = tgl_jatuh_tempo;
    document.getElementById("edit_saldo").value = saldo;
    $('#edit_jenis').selectpicker('refresh');
    $('#edit_bank').selectpicker('refresh');
}

function ubahForm() {
    let val = document.getElementById("tipe_input").value;
    if (val == 0) {
        $('#content_manual1').show();
        $('#file_upload_x').hide();
    } else {
        $('#content_manual1').hide();
        $('#file_upload_x').show();
    }
}

function select_bank(e) {
    $("#add_bank").find("option").remove();
    $("#edit_bank").find("option").remove();

    //GET COAS DARI ID PROJECT
    dataC = $.ajax({
        data: {
            nama_bank: e.target.value
        },
        type: "POST",
        url: "<?php echo site_url('Vasicek/getFilterBank');?>",
        success: function(msg) {
            console.log(msg);
            dataP = JSON.parse(msg);
            for (let i = 0; i < dataP.length; i++) {
                const element = dataP[i];
                let id_vas_bank = element.id_vas_bank;
                let nama_bank = element.nama_bank + " ( " + element.rating.kode_rating + " ) tahun:" +
                    element.tahun;
                // console.log(id_vas_bank);
                // console.log(nama_bank);
                $('#add_bank').append("<option value='" + id_vas_bank + "'>" + nama_bank + "</option>");
                $('#edit_bank').append("<option value='" + id_vas_bank + "'>" + nama_bank + "</option>");
            }
            // $('#add_bank').selectpicker('refresh');
            // $('#edit_bank').selectpicker('refresh');
        }
    });
}
</script>