<style>
table {
    table-layout: fixed;
    display: block;
    overflow-x: auto;
    white-space: nowrap;
}

td {
    overflow: hidden;
    text-overflow: ellipsis;
}
</style>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Simulasi Vasicek ECL</h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-refresh"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Vasicek ECL</a>

                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet">
                <div class="kt-portlet__body kt-portlet__body--fit">
                    <div class="kt-grid kt-wizard-v1 kt-wizard-v1--white">
                        <div class="kt-grid__item">

                            <!--begin: Form Wizard Nav -->
                            <div class="kt-wizard-v1__nav">

                                <div class="kt-wizard-v1__nav-items">
                                    <div id="nav-0" class="kt-wizard-v1__nav-item" data-ktwizard-type="step"
                                        data-ktwizard-state="current">
                                        <div class="kt-wizard-v1__nav-body">
                                            <div class="kt-wizard-v1__nav-icon">
                                                <i class="flaticon-pie-chart"></i>
                                            </div>
                                            <div class="kt-wizard-v1__nav-label">
                                                1. Pengaturan Metode Vasicek
                                            </div>
                                        </div>
                                    </div>
                                    <div id="nav-1" class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
                                        <div class="kt-wizard-v1__nav-body">
                                            <div class="kt-wizard-v1__nav-icon">
                                                <i class="flaticon-list"></i>
                                            </div>
                                            <div class="kt-wizard-v1__nav-label">
                                                2. Pengaturan ECL
                                            </div>
                                        </div>
                                    </div>
                                    <div id="nav-2" class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
                                        <div class="kt-wizard-v1__nav-body">
                                            <div class="kt-wizard-v1__nav-icon">
                                                <i class='flaticon2-paper'></i>
                                            </div>
                                            <div class="kt-wizard-v1__nav-label">
                                                4. Penyesuaian Makroekonomi
                                            </div>
                                        </div>
                                    </div>
                                    <div id="nav-3" class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
                                        <div class="kt-wizard-v1__nav-body">
                                            <div class="kt-wizard-v1__nav-icon">
                                                <i class='flaticon2-checking'></i>
                                            </div>
                                            <div class="kt-wizard-v1__nav-label">
                                                4. PD Vasicek
                                            </div>
                                        </div>
                                    </div>
                                    <div id="nav-4" class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
                                        <div class="kt-wizard-v1__nav-body">
                                            <div class="kt-wizard-v1__nav-icon">
                                                <i class="flaticon-clipboard"></i>
                                            </div>
                                            <div class="kt-wizard-v1__nav-label">
                                                5. Perhitungan ECL Vasicek
                                            </div>
                                        </div>
                                    </div>
                                    <div id="nav-5" class="kt-wizard-v1__nav-item" data-ktwizard-type="step">
                                        <div class="kt-wizard-v1__nav-body">
                                            <div class="kt-wizard-v1__nav-icon">
                                                <i class="flaticon2-check-mark"></i>
                                            </div>
                                            <div class="kt-wizard-v1__nav-label">
                                                6. Summary ECL Vasicek
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--end: Form Wizard Nav -->
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin: Form Wizard Form-->
                            <form class="kt-form" id="kt_form" method="post" enctype="multipart/form-data"
                                action="<?php echo site_url('Vasicek/ecl_createDraft'); ?>">

                                <!--begin: Form Wizard Step 1-->
                                <div id="content-0" class="kt-wizard-v1__content" data-ktwizard-type="step-content"
                                    data-ktwizard-state="current">
                                    <div class="kt-heading kt-heading--md">Pengaturan Metode Vasicek</div>
                                    <div class="kt-form__section kt-form__section--first">
                                        <div class="kt-wizard-v1__form">
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Draft Aset :</label>
                                                        <select name="draft" id="draft" class="form-control" required>
                                                            <?php foreach( $draft->data as $dr ):?>
                                                            <option value="<?php echo $dr->id_draft_vasicek; ?>">
                                                                <?php echo $dr->kode_draft." - ".$dr->nama_draft; ?>
                                                            </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <span class="form-text text-muted">Pilih daftar aset yang
                                                            digunakan untuk simulate.</span>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Interval :</label>
                                                        <select name="interval" id="interval" class="form-control">
                                                            <option value="3">Triwulan</option>
                                                            <option value="6">Semester</option>
                                                            <option value="12">Tahun</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Skenario Normal</label>
                                                        <div class="input-group">
                                                            <input type="number" class="form-control" id="persenNormal"
                                                                name="normal" value="60" min="1" max="100"
                                                                aria-describedby="basic-addon2">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">%</span>
                                                            </div>
                                                        </div>
                                                        <span class="form-text text-muted">Masukan skenario normal
                                                            yang di inginkan</span>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Skenario Up Turn</label>
                                                        <!-- <input type="number" id="persenUpturn" class="form-control"
                                                            name="upturn" value="20" min="1" max="100"> -->
                                                        <div class="input-group">
                                                            <input type="number" class="form-control" id="persenUpturn"
                                                                name="upturn" value="20" min="1" max="100"
                                                                aria-describedby="basic-addon2">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">%</span>
                                                            </div>
                                                        </div>
                                                        <span class="form-text text-muted">Masukan skenario up turn
                                                            yang di inginkan</span>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Skenario Down Turn</label>
                                                        <!-- <input type="number" id="persenDownturn" class="form-control"
                                                            value="20" name="downturn" min="1" max="100"> -->
                                                        <div class="input-group">
                                                            <input type="number" class="form-control"
                                                                id="persenDownturn" name="downturn" value="20" min="1"
                                                                max="100" aria-describedby="basic-addon2">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">%</span>
                                                            </div>
                                                        </div>
                                                        <span class="form-text text-muted">Masukan skenario down turn
                                                            yang di inginkan</span>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Periode Awal</label>
                                                        <input type="text" id="dateStart" class="form-control"
                                                            name="start" placeholder="Periode Awal">
                                                        <span class="form-text text-muted">Masukan Periode awal GDP
                                                            yang di inginkan</span>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Periode Akhir</label>
                                                        <input type="text" id="dateStart2" class="form-control"
                                                            name="end" placeholder="Periode Akhir">
                                                        <span class="form-text text-muted">Masukan Periode akhir GDP
                                                            yang di inginkan</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--end: Form Wizard Step 1-->

                                <!--begin: Form Wizard Step 2-->
                                <div id="content-1" class="kt-wizard-v1__content" data-ktwizard-type="step-content"
                                    data-ktwizard-state="">
                                    <div class="kt-heading kt-heading--md">Pengaturan ECL</div>
                                    <div class="kt-form__section kt-form__section--first">
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="form-group">
                                                    <label>LGD :</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="lgd" name="lgd"
                                                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                            placeholder="Persen LGD" value='20'>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">%</span>
                                                        </div>
                                                    </div>
                                                    <span class="form-text text-muted">Masukan Persen LGD
                                                        yang di inginkan</span>
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="form-group">
                                                    <label>Periode ECL :</label>
                                                    <input type="text" class="form-control" id="eperiode"
                                                        name="periode_ecl" placeholder="Periode ECL">
                                                    <span class="form-text text-muted">Masukan Periode ECL
                                                        saat ini</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--end: Form Wizard Step 2-->

                                <!--begin: Form Wizard Step 3-->
                                <div id="content-2" class="kt-wizard-v1__content" data-ktwizard-type="step-content">
                                    <div class="kt-heading kt-heading--md">Hasil Penyesuaian Makroekonomi</div>
                                    <div class="kt-form__section kt-form__section--first">
                                        <div class="kt-wizard-v1__form">
                                            <div class="row">
                                                <div class="col-12">
                                                    <table class="table table-striped table-bordered table-hover"
                                                        id="tablePersen" width="100%">

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-wizard-v1__form">
                                            <div class="row">
                                                <div class="col-12">
                                                    <table class="table table-striped table-bordered table-hover"
                                                        id="tableMicro" width="100%">

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-wizard-v1__form">
                                            <div class="row">
                                                <div class="col-12">
                                                    <table class="table table-striped table-bordered table-hover"
                                                        id="tableHistory" width="100%">

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <button onclick="ExportMicro()" type="button"
                                                class="btn btn-sm btn-danger teratur-button float-right">
                                                <i class="flaticon-download-1"></i> Export Excel
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <!--end: Form Wizard Step 3-->

                                <!--begin: Form Wizard Step 4-->
                                <div id="content-3" class="kt-wizard-v1__content" data-ktwizard-type="step-content">
                                    <div class="kt-heading kt-heading--md">Perhitungan PD Vasicek</div>
                                    <div class="kt-form__section kt-form__section--first">
                                        <div class="kt-wizard-v1__form">
                                            <div class="row">
                                                <div class="col-12">
                                                    <table class="table table-striped table-bordered table-hover"
                                                        id="tablePD" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th class='all' width=200px>Pefindo Rating</th>
                                                                <th class='all' width=200px>TTC PD (Pefindo)</th>
                                                                <th class='all' width=200px>Asset Correlation (Basel
                                                                    formula) Corporate </th>
                                                                <th class='all' width=200px>PIT PD (Vasicek) </th>
                                                                <!-- <th class='all' width=200px>PIT PD (Vasicek) (Adjusted)
                                                                </th> -->
                                                                <th class='all' width=200px>PIT PD (Vasicek) </th>
                                                                <!-- <th class='all' width=200px>PIT PD (Vasicek)(Adjusted)
                                                                </th> -->
                                                            </tr>
                                                        </thead>
                                                        <tbody id="listPD">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <button onclick="ExportPD()" type="button"
                                                    class="btn btn-sm btn-danger teratur-button float-right">
                                                    <i class="flaticon-download-1"></i> Export Excel
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="content-4" class="kt-wizard-v1__content" data-ktwizard-type="step-content">
                                    <div class="kt-heading kt-heading--md">Perhitungan ECL Vasicek</div>
                                    <div class="kt-form__section kt-form__section--first">
                                        <div class="kt-wizard-v1__form">
                                            <div class="row">
                                                <div class="col-12">
                                                    <table class="table table-striped table-bordered table-hover"
                                                        id="tableKertas" width="100%">

                                                        <thead>
                                                            <tr>
                                                                <th class='all' width=200px>Jenis Aset Keuangan</th>
                                                                <th class='all' width=200px>Nomor Rekening</th>
                                                                <th class='all' width=200px>Mata Uang</th>
                                                                <th class='all' width=200px>Nama Bank</th>
                                                                <th class='all' width=200px>Akronim Bank</th>
                                                                <th class='all' width=200px>Rating</th>
                                                                <th class='all' width=200px>Suku Bunga</th>
                                                                <th class='all' width=200px>Jatuh tempo</th>
                                                                <th class='all' width=200px id="eclperiode"></th>
                                                                <th class='all' width=200px>PD (normal scenario)</th>
                                                                <th class='all' width=200px>PD (weighted scenario)</th>
                                                                <th class='all' width=200px>LGD</th>
                                                                <th class='all' width=200px>Nilai ECL (normal scenario)
                                                                </th>
                                                                <th class='all' width=200px>Nilai ECL (weighted
                                                                    scenario)
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="listKertas">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <button onclick="ExportKertas()" type="button"
                                                    class="btn btn-sm btn-danger teratur-button float-right">
                                                    <i class="flaticon-download-1"></i> Export Excel
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <!--end: Form Wizard Step 5-->

                                <!--begin: Form Wizard Step 6-->
                                <div id="content-5" class="kt-wizard-v1__content" data-ktwizard-type="step-content">
                                    <div class="kt-heading kt-heading--md">Summary ECL Vasicek</div>
                                    <div class="kt-form__section kt-form__section--first">
                                        <div class="kt-wizard-v1__form">
                                            <div class="row">
                                                <div class="col-12">
                                                    <table class="table table-striped table-bordered table-hover"
                                                        id="tableECL" width="100%">

                                                        <thead>
                                                            <tr>
                                                                <th class='all' width=400px>Jenis Aset Keuangan</th>
                                                                <th class='all' width=400px id="ecl1"></th>
                                                                <th class='all' width=400px id="ecl2"></th>
                                                                <th class='all' width=400px id="ecl3"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="listECL">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <button onclick="ExportSumECL()" type="button"
                                                    class="btn btn-sm btn-danger teratur-button float-right">
                                                    <i class="flaticon-download-1"></i> Export Excel
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--end: Form Wizard Step 6-->

                                <!--begin: Form Actions -->
                                <div class="kt-form__actions">
                                    <button class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold"
                                        onclick='backVesicek()' type="button">
                                        Previous
                                    </button>

                                    <button id="vasicek_simulate_next"
                                        class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold"
                                        onclick='nextVesicek2()' type="button">
                                        Next Step
                                    </button>

                                    <button id="ecl_simulate_submit" style="display:none;"
                                        class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold"
                                        onclick='return act_confirm()' type="submit">Simpan Hasil ECL
                                    </button>

                                    <!-- <button id="ecl_simulate_submit_no" style="display:none;"
                                        class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold"
                                        onclick='return act_confirm()' type="submit">Buang Hasil ECL
                                    </button> -->
                                </div>

                                <!--end: Form Actions -->
                            </form>

                            <!--end: Form Wizard Form-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- end:: Content -->
    </div>
</div>