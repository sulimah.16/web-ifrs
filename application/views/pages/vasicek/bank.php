<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Daftar Bank </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-list"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            Bank </a>

                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container   kt-grid__item--fluid">
            <?php if ( $data->status == '1' || $data->status == 1 ) { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Berhasil!&nbsp;&nbsp;</strong><?php echo $data->message; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } else { ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Perhatian!&nbsp;&nbsp;</strong> <?php echo $data->message; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php } ?>
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                <strong>Info!&nbsp;&nbsp;</strong><?php echo "Tekan button tambah data untuk melakukan penambahan data"; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">

                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">

                    </div>

                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                <?php if ( $this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' ) { ?>
                                <a href="#" class="btn btn-brand btn-icon-sm teratur-button" data-toggle="modal"
                                    data-target="#add_bank">
                                    <i class="flaticon-plus"></i>
                                    Tambah Data
                                </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">
                    <!-- <?php echo json_encode($data); ?> -->
                    <table class="table table-striped table-bordered table-hover" id="tbl_list_default2" width="100%">
                        <thead>
                            <tr>
                                <th class="all" width=10px>#</th>
                                <th class="all" width=200px>Nama Bank</th>
                                <th class="all" width=200px>Akronim Bank</th>
                                <th class="all" width=200px>Tahun Referensi</th>
                                <th class="all" width=200px>Referensi</th>
                                <th class="all" width=200px>Kode Rating</th>
                                <th class="all" width=200px>Nilai PD</th>
                                <?php if ( $this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' ) { ?>
                                <th class="all" width=200px>Action</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody class="kt-datatable__body">
                            <?php $i = 1; foreach( $data->data as $d ): ?>
                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $d->nama_bank ?></td>
                                <td><?php echo $d->akronim ?></td>
                                <td><?php echo $d->tahun ?></td>
                                <td><?php echo $d->referensi ?></td>
                                <td><?php echo $d->rating->kode_rating ?></td>
                                <td><?php echo ($d->nilai * 100)." %"; ?></td>
                                <?php if ( $this->session->userdata('role') == '1' || $this->session->userdata('role') == '2' ) { ?>
                                <td>
                                    <div class="dropdown">
                                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                            data-toggle="dropdown">
                                            <i class="la la-cog"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-sm">
                                            <ul class="kt-nav">
                                                <li class="kt-nav__item">
                                                    <a href="<?php echo site_url("#"); ?>" data-toggle="modal"
                                                        data-target="#edit_bank" onClick="return EditBank(
                                                            '<?php echo  $d->id_vas_bank; ?>',  
                                                            '<?php echo  $d->nama_bank; ?>',  
                                                            '<?php echo  $d->akronim; ?>',  
                                                            '<?php echo  $d->id_rating; ?>',  
                                                            '<?php echo  $d->tahun; ?>', 
                                                            '<?php echo  $d->referensi; ?>',  
                                                            )" class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon-edit"></i>
                                                        <span class="kt-nav__link-text">Ubah Data</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                                <?php } ?>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
.autocomplete {
    /*the container must be positioned relative:*/
    position: relative;
    display: inline-block;
}

.autocomplete-items {
    /* position: absolute; */
    border: 1px solid #d4d4d4;
    border-bottom: none;
    border-top: none;
    z-index: 99;
    /*position the autocomplete items to be the same width as the container:*/
    top: 100%;
    left: 0;
    right: 0;
}

.autocomplete-items div {
    padding: 10px;
    cursor: pointer;
    background-color: #fff;
    border-bottom: 1px solid #d4d4d4;
}

.autocomplete-items div:hover {
    /*when hovering an item:*/
    background-color: #e9e9e9;
}

.autocomplete-active {
    /*when navigating through the items using the arrow keys:*/
    background-color: DodgerBlue !important;
    color: #ffffff;
}
</style>

<div class="modal fade" id="add_bank" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('vasicek/createBank '); ?>" autocomplete="off">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nama Bank</label>
                                    <div class="col-lg-9 col-xl-8 autocomplete">
                                        <input class="form-control" type="text" id="tambahNama" name="nama" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Akronim Bank</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="tambahAkronim" name="akronim"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tahun Referensi</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="years" name="tahun" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Referensi</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="referensi" name="referensi"
                                            value="-" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">PD Pefindo</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="rating" id="tambahRating" class="form-control" required>
                                            <?php foreach ( $rating->data as $r ) : ?>
                                            <option value="<?php echo $r->id_rating; ?>"><?php echo $r->kode_rating; ?>
                                            </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button" onclick="return act_confirm()">Proses
                        Data</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>


<div class="modal fade" id="edit_bank" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ubah Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!-- Star Form -->
            <form class="kt-form kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo site_url('vasicek/editBank '); ?>">
                <div class="modal-body">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">

                                <input class="form-control" type="hidden" id="ubahId" name="id" required>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Nama Bank</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="ubahNama" name="nama" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Akronim Bank</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="ubahAkronim" name="akronim"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tahun Referensi</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="edit_years" name="tahun" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Referensi</label>
                                    <div class="col-lg-9 col-xl-8">
                                        <input class="form-control" type="text" id="edit_referensi" name="referensi"
                                            value="-" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">PD Pefindo</label>
                                    <div class="col-lg-9 col-xl-4">

                                        <select name="rating" id="ubahRating" class="form-control" required>
                                            <?php foreach ( $rating->data as $r ) : ?>
                                            <option value="<?php echo $r->id_rating; ?>"><?php echo $r->kode_rating; ?>
                                            </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary teratur-button" onclick="return act_confirm()">Proses
                        Data</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>

<script type="text/javascript">
var autcon = <?php echo json_encode($grup_name); ?>;
var autcon_akronim = <?php echo json_encode($grup_akronim); ?>;
// var autcon = ["Afghanistan", "Albania", "Algeria", "Andorra"];
autocomplete(document.getElementById("tambahNama"), autcon);
autocomplete(document.getElementById("ubahNama"), autcon);
autocomplete(document.getElementById("tambahAkronim"), autcon_akronim);
autocomplete(document.getElementById("ubahAkronim"), autcon_akronim);

function EditBank(id, nama, akronim, rating, tahun, referensi) {
    document.getElementById("ubahId").value = id;
    document.getElementById("ubahNama").value = nama;
    document.getElementById("ubahAkronim").value = akronim;
    document.getElementById("ubahRating").value = rating;
    document.getElementById("edit_years").value = tahun;
    document.getElementById("edit_referensi").value = referensi;
}

function autocomplete(inp, arr) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function(e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) {
            return false;
        }
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i].substr(val.length);
                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function(e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
            }
        }
    });

    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
    }

    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }

    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function(e) {
        closeAllLists(e.target);
    });
}
</script>