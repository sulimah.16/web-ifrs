<!DOCTYPE html>
<html lang="en">
	<head>
		<base href="<?php echo base_url(); ?>">
		<meta charset="utf-8" />
		<title>Teratur Dasboard</title>
		<meta name="description" content="Teratur Dashboard">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
		<link href="<?php echo base_url(); ?>public/assets/css/pages/login/login-1.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>public/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>public/assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="<?php echo base_url(); ?>public/assets/teratur/teratur_favicon_2.ico" />
	</head>
	<body class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--left kt-aside--fixed kt-page--loading">

		<div class="kt-grid kt-grid--ver kt-grid--root kt-page">
			<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v1" id="kt_login">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">

					<!-- <div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside" style="background-image: url(public/assets/media/bg/bg-4.jpg);"> -->
					<div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside" style="background-image: url(public/assets/teratur/bg-dashboard@3x.png);">
						<div class="kt-grid__item">
							<a href="#" class="kt-login__logo">
								<img height="40px" src="<?php echo base_url(); ?>public/assets/teratur/teratur_3.png">
							</a>
                        </div>
                        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
                        </div>
                        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
						</div>
						<br><br><br>
						<br><br><br>
						<br><br><br>
						<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
							<div class="kt-grid__item kt-grid__item--middle">
								<h3 class="kt-login__title teratur-htext">People Process</h3>
                                <h4 class="kt-login__subtitle teratur-htext-blue">Sebuah platform dimana proses pekerjaan harian hingga bulanan akan menjadi Teratur.</h4>
                                <p><a id="learn_more" class="btn btn-default btn-elevate kt-login__btn-primary teratur-color">Learn More</a></p>
							</div>
						</div>
					</div>
					<div class="teratur-bg-color kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">

						<div class="kt-login__body">
							<div class="kt-login__form">
								<div class="kt-login__title">
                                    <h4 class="teratur-font-putih">Selangkah lagi untuk pemulihan akun anda</h4>
									
									
									<div class="alert teratur-alert-body" role="alert">
										<div class="alert-text teraturp">
											<p>Anda telah menerima email notifikasi pemulihan password anda. Kode verifikasi adalah angka acak yang terlah terlampir dalam email anda. Silahkan lengkapi form berikut, selanjutnya anda akan menggunakan password baru untuk mengakses teratur HR.</p>
										</div>
									</div>
								</div>
								<form class="kt-form" method="post" action="<?php echo base_url("recover/execrecover"); ?>" oninput='vercode.setCustomValidity(vercode.value != vercodedata.value ? "Kode verifikasi salah." : ""), passwordre.setCustomValidity(passwordnew.value != passwordre.value ? "Periksa ulang password anda!" : "")' >
									<div class="form-group">
										<input type=hidden name=vercodedata id=vercodedata value="<?php echo $data->Verification_code; ?>"" />
										<input type=hidden name=uuid id=uuid value="<?php echo $data->Uniq_id; ?>"" />
										<input type=hidden name=cmpID id=cmpID value="<?php echo $data->Company_id; ?>"" />
										<input type=hidden name=empID id=empID value="<?php echo $data->Employee_id; ?>"" />
										<input class="form-control teratur-form-email" type="text" placeholder="Kode Verifikasi" name="vercode" autocomplete="off" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>
									</div>
									<div class="form-group">
										<input class="form-control teratur-form-password" type="password" placeholder="Password" name="passwordnew" autocomplete="off" required>
									</div>
									<div class="form-group">
										<input class="form-control teratur-form-password" type="password" placeholder="Ulangi Password" name="passwordre" autocomplete="off" required>
									</div>

									<div class="kt-login__actions">
										<a href="#" data-toggle="modal" data-target="#forgot-modal" class="kt-link kt-login__link-forgot teratur-font-putih">
											
										</a>
										<button type="submit" class="btn btn-primary btn-elevate kt-login__btn-primary teratur-button-login">Proses</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
        
        <div class="modal fade " id="warningmodal" tabindex="1" role="basic" aria-hidden="true">
            <div class="modal-dialog kt-ribbon kt-ribbon--dark kt-ribbon--ver kt-ribbon--shadow">
                <div class="modal-content">
					<div class="kt-ribbon__target teratur-ribbon" style="top: -2px; right: 12px;">
						<i class="fa flaticon2-bell-alarm-symbol"></i>
					</div>
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Teratur Message</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						</button>
					</div>
                    <div class="modal-body"> <?php echo $this->session->flashdata("message"); ?> </div>
                        <div class="modal-footer">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
		</div>
		
		<!--begin::Modal-->
<div class="modal fade" id="forgot-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-md kt-ribbon kt-ribbon--dark kt-ribbon--shadow kt-ribbon--left kt-ribbon--round" role="document">
        <div class="modal-content">
			<div class="kt-ribbon__target" style="top: 12px; right: -2px;">
				Teratur.id <!-- <i class="fa fa-star"></i> -->
			</div>
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pemulihan Password Anda</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <!--begin::Form-->
            <form class="kt-form kt-form--fit kt-form--label-center" method="post" enctype="multipart/form-data"
                action="<?php echo base_url('auth/recoverpassword'); ?>">
                <div class="modal-body">
					<div class="alert teratur-alert" role="alert">
						<div class="alert-text">
							Untuk mendapatkan akses pemulihan akun anda silahkan masukan email Anda. Anda akan menerima email konfirmasi pada email anda yang telah terdaftar.
						
						
						</div>
					</div>
					<br>
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                

                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Email Anda</label>
                                    <div class="col-lg-9 col-xl-9">
                                        <input type="text" class="form-control" id="email" name="email"
                                            placeholder="Contoh : bambang@teratur.id" required />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-brand btn-icon-sm teratur-button">Kirim Data</button>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
</div>
<!--end::Modal-->

		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#591df1",
						"light": "#ffffff",
						"dark": "#282a3c",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
        </script>
    
		<script src="<?php echo base_url(); ?>public/assets/plugins/global/plugins.bundle.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>public/assets/js/scripts.bundle.js" type="text/javascript"></script>
        
        <?php if($this->session->flashdata('message') != NULL) { ?>
        <script type="text/javascript">
            $(window).on('load',function(){
                $('#warningmodal').modal('show');
            });
        </script>
        <?php } ?>
    </body>
</html>