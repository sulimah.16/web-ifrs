<?php

Class Server Extends CI_Model {
    private $SERVER_HOST;
    private $SERVER_PORT;
    private $TOKEN;
    
    function __construct() {
        parent::__construct();

        $this->SERVER_HOST = $this->config->item('api_host');
        $this->SERVER_PORT = $this->config->item('api_port');

        if($this->session->userdata("authtoken") != null) {
            $this->TOKEN = 'Bearer '. $this->session->userdata("authtoken");
        } else {
            $this->TOKEN = null;
        }
    }

    function GET($url) { 
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $this->SERVER_HOST . "/" . $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_POSTFIELDS => "{\n    \"email\" : \"hanifalbaaits@gmail.com\",\n    \"updated_by\" : \"1\"\n}",
        CURLOPT_HTTPHEADER => array(
            "Authorization: $this->TOKEN ",
            "Content-Type: application/json",
            "Postman-Token: dcc31840-b417-47f3-a488-b3edb19169c5",
            "cache-control: no-cache"
        ),
        ));

        $json_response = curl_exec($curl); 
        curl_close($curl); 
        
        if($json_response == '') { 
            $json_response = json_encode(array("status" => "0", "message" => "Server not response"));
            $this->session->set_flashdata("err-message", "Tidak ada respon dari server, Silahkan Coba Kembali");
            redirect("auth");
        } 

        $respon = json_decode($json_response); 
        
        if($respon == null || $respon->status != '1'  || $respon->status == '420' || $respon->status == '405' || $respon->status == '421' || $respon->status == '422' || $respon->status == '423' || $respon->status == '400')  {
            $this->session->set_flashdata("err-message", "Session Telah berakhir, Login Kembali");
            redirect("auth");
        }
        
        return $respon;
    }

    function GET_PLUS_BODY($url, $param) {
        $arr = json_encode($param); 

        $curl = curl_init($this->SERVER_HOST . "/" . $url); 

        curl_setopt($curl, CURLOPT_HEADER, false); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("teraturtoken: $this->TOKEN"));
        // curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $arr); 
        
        $json_response = curl_exec($curl); 

        curl_close($curl); 
        
        if($json_response == '') { 
            $json_response = json_encode(array("status" => "0", "message" => "Server not response"));
            $this->session->set_flashdata("err-message", "Tidak ada respon dari server, Silahkan Coba Kembali");
            redirect("auth");
        } 

        $respon = json_decode($json_response); 
        
        if($respon == null || $respon->status == '420' || $respon->status == '421' || $respon->status == '422' || $respon->status == '423') {
            $this->session->set_flashdata("err-message", "Session Telah berakhir, Login Kembali");
            redirect("auth");
        }
        
        return $respon;
    }

    function DELETE($url) {  

        $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => $this->SERVER_HOST . "/" . $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "DELETE",
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => array(
                "Authorization:  $this->TOKEN ",
                "Postman-Token: f50123cc-2b24-4eed-a141-1438ed0be086",
                "cache-control: no-cache"
            ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if($response == '') { 
                $json_response = json_encode(array("status" => "0", "message" => "Server not response"));
                $this->session->set_flashdata("err-message", "Tidak ada respon dari server, Silahkan Coba Kembali");
                redirect("auth");
            } 
    
            $respon = json_decode($response); 
    
            if($respon == null || $respon->status == '420' || $respon->status == '421' || $respon->status == '422' || $respon->status == '423') {
                $this->session->set_flashdata("err-message", "Session Telah berakhir, Login Kembali");
                redirect("auth");
            }
    
            
            return $respon;
    }

    function PUT($url, $param) { 

        $param = json_encode($param); 
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->SERVER_HOST . "/" . $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "PUT",
          CURLOPT_POSTFIELDS => $param,
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_HTTPHEADER => array(
            "Authorization: $this->TOKEN",
            "Content-Type: application/json",
            "Postman-Token: b176880a-1cc3-4a98-915a-9406d23d78ce",
            "cache-control: no-cache"
          ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl); 
        
        if($response == '') { 
            $json_response = json_encode(array("status" => "0", "message" => "Server not response"));
            $this->session->set_flashdata("err-message", "Tidak ada respon dari server, Silahkan Coba Kembali");
            redirect("auth");
        } 

        $respon = json_decode($response); 

        if($respon == null || $respon->status == '420' || $respon->status == '421' || $respon->status == '422' || $respon->status == '423') {
            $this->session->set_flashdata("err-message", "Session Telah berakhir, Login Kembali");
            redirect("auth");
        }

        
        return $respon;

    }

    function UPLOAD ($url, $param) {

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $this->SERVER_HOST . "/" . $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $param,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_HTTPHEADER => array(
            "Authorization:  $this->TOKEN" ,
            "Postman-Token: 0175e612-7019-4d97-8392-6f00a149b2c4",
            "cache-control: no-cache",
            
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        
        if($response == '') { 
            $json_response = json_encode(array("status" => "0", "message" => "Server not response"));
            $this->session->set_flashdata("err-message", "Tidak ada respon dari server, Silahkan Coba Kembali");
            redirect("auth");
        } 

        $respon = json_decode($response); 

        if($respon == null || $respon->status == '420' || $respon->status == '421' || $respon->status == '422' || $respon->status == '423') {
            $this->session->set_flashdata("err-message", "Session Telah berakhir, Login Kembali");
            redirect("auth");
        }

        
        return $respon;
    } 

    function POST($url, $param) {
        $param = json_encode($param); 
        // echo $param."<br />";
        // echo $this->SERVER_HOST . "/" . $url."<br />";
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $this->SERVER_HOST . "/" . $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $param,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_HTTPHEADER => array(
            "Authorization: $this->TOKEN ",
            "Content-Type: application/json",
            "Postman-Token: a9c22a50-192d-4d79-8915-d341bbe6d991",
            "cache-control: no-cache"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl); 

        // var_dump($response); exit;
        
        if($response == '') { 
            $json_response = json_encode(array("status" => "0", "message" => "Server not response"));
            $this->session->set_flashdata("err-message", "Tidak ada respon dari server, Silahkan Coba Kembali");
            redirect("auth");
        } 

        $respon = json_decode($response); 

        if($respon == null || $respon->status == '420' || $respon->status == '421' || $respon->status == '422' || $respon->status == '423' ) {
            $this->session->set_flashdata("err-message", "Session Telah berakhir, Login Kembali");
            redirect("auth");
        }

        
        return $respon;
    }

    function POST_JSON($url, $param) {
        $arr = json_encode($param); 
        $curl = curl_init($this->SERVER_HOST . "/" . $url);

        curl_setopt($curl, CURLOPT_HEADER, false); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json", "teraturtoken: $this->TOKEN")); 
        curl_setopt($curl, CURLOPT_POST, true); 
        curl_setopt($curl, CURLOPT_POSTFIELDS, $arr); 
        
        $json_response = curl_exec($curl); 
        curl_close($curl); 
        
        if($json_response == '') { 
            $json_response = json_encode(array("status" => "0", "message" => "Server not response"));
            $this->session->set_flashdata("err-message", "Tidak ada respon dari server, Silahkan Coba Kembali");
            redirect("auth");
        } 

        $respon = json_decode($json_response); 

        if($respon == null || $respon->status == '420' || $respon->status == '421' || $respon->status == '422' || $respon->status == '423') {
            $this->session->set_flashdata("err-message", "Session Telah berakhir, Login Kembali");
            redirect("auth");
        }
        
        return $respon;
    }

    function permision_validate($pagePermision="5") {
        if($this->session->userdata("logstatus") == '1') {
            if($this->session->userdata("level") > $pagePermision) {
                return 2;
            }else {
                return 1;
            }
        } else {
            return 0;
        }
    }
}